<?php

namespace App;

class JsTrans
{
    protected $keys = [
        'upay.general.delete_modal_title',
        'upay.general.delete_modal_text',
        'upay.general.delete_modal_ok',
        'upay.general.delete_modal_cancel',
        'upay.general.deleted_title',
        'upay.general.deleted_text',
        'upay.general.saved_modal.title',
        'upay.general.saved_modal.to_list',
        'upay.general.saved_modal.to_edit',
        'upay.general.popup.ok',
        'upay.general.errors.APP_ERROR',
        'upay.general.errors.UNAUTHORIZED',
        'upay.general.errors.TOKEN_MISMATCH',
        'upay.general.errors.NOT_FOUND',
        'upay.general.errors.ERROR',
        'upay.general.errors.FORBIDDEN',
        'upay.general.show_status.active',
        'upay.general.show_status.inactive',
        'upay.general.confirm_modal_ok',
        'upay.general.confirm_modal_cancel'
    ];

    public function add($keys)
    {
        $this->keys = array_merge($this->keys, is_array($keys) ? $keys : [$keys]);
    }

    public function output()
    {
        $transes = [];
        foreach ($this->keys as $key) {
            $transes[$key] = trans($key);
        }
        echo '<script> var $jsTrans = ' . json_encode($transes) . '</script>';
    }
}
