<?php

namespace App\UPay\Transaction;

use \App\Model;
use App\UPay\Cashbox\Cashbox;
use App\UPay\Operator\Operator;
use App\UPay\Service\Service;

class TransactionCancellation extends Model
{
    const TYPE_CASHIER = 'cashier';
    const TYPE_CUSTOMER = 'customer';

    const STATUS_NEW = 'new';
    const STATUS_ABORTED = 'aborted';
    const STATUS_PROCESSING = 'processing';
    const STATUS_PRE_APPROVED = 'pre_approved';
    const STATUS_PRE_DECLINED = 'pre_declined';
    const STATUS_FINALLY_APPROVED = 'finally_approved';
    const STATUS_FINALLY_DECLINED = 'finally_declined';
    const STATUS_COMPLETED = 'completed';
    const STATUS_REJECTED = 'rejected';

    protected $table = 'transaction_cancellations';

    protected $fillable = [
        'transaction_id',
        'operator_id',
        'cashbox_id',
        'type',
        'details',
        'notes',
        'status',
    ];

    protected $casts = [
        'details' => 'array'
    ];

    public static function statuses()
    {
        return [
            TransactionCancellation::STATUS_NEW,
            TransactionCancellation::STATUS_ABORTED,
            TransactionCancellation::STATUS_PROCESSING,
            TransactionCancellation::STATUS_PRE_APPROVED,
            TransactionCancellation::STATUS_PRE_DECLINED,
            TransactionCancellation::STATUS_FINALLY_APPROVED,
            TransactionCancellation::STATUS_FINALLY_DECLINED,
            TransactionCancellation::STATUS_COMPLETED,
            TransactionCancellation::STATUS_REJECTED
        ];
    }
    public function history()
    {
        return $this->hasMany(TransactionCancellationHistory::class, 'cancellation_id', 'id')->orderBy('id', 'asc');
    }

    public function transaction()
    {
        return $this->belongsTo(Transaction::class, 'transaction_id', 'id');
    }

    public function operator()
    {
        return $this->belongsTo(Operator::class, 'operator_id', 'id');
    }

    // -- these should be called if its joined to transactions --
    public function service()
    {
        return $this->belongsTo(Service::class, 'service_id', 'id')->joinMl();
    }

    public function cashbox()
    {
        return $this->belongsTo(Cashbox::class, 'cashbox_id', 'id')->joinMl();
    }
    // -- these should be called if its joined to transactions --
}
