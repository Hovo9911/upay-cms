<?php

return [
    'lang' => [
        'path' => env('LANG_PATH', ''),
        'group' => 'cms'
    ],
    'admin' => [
        'password_active_time' => '3 months' //passed to strtotime
    ],
    'armsoft' => [
        'customer_cancellation' => [
            'debit_account' => '52170090600',
            'credit_account' => '52170090600',
        ]
    ],
    'adapter_payment_attempts' => [
        'max_count' => 3,
    ],
];
