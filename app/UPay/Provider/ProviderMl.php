<?php

namespace App\UPay\Provider;

use App\MultiPKModel;

class ProviderMl extends MultiPKModel
{
    protected $table = 'providers_ml';
    public $timestamps = false;
    protected $primaryKey = ['provider_id', 'lng_id'];
    public $incrementing = false;

    protected $fillable = [
        'provider_id',
        'lng_id',
        'name',
        'show_status',
    ];
}
