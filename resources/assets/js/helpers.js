function trans(key) {
    return ($jsTrans[key] != undefined) ? $jsTrans[key] : key;
}
function init_checkboxes(target){
    if (target == undefined) {
        target = $('.i-checks');
    }
    target.iCheck({
        checkboxClass: 'icheckbox_square-green',
        radioClass: 'iradio_square-green',
    });
}
function globalProcessResponse(response)
{
    var msg = 'Something went wrong';
    var title = 'Error';
    if (response.message != undefined) {
        msg = response.message;
    } else if (response.data != undefined && response.data.message != undefined) {
        msg = response.data.message;
    }
    if (response.status != undefined) {
        title = trans('upay.general.errors.'+response.status);
    }
    swal({
        title: title,
        text: msg,
        type: "error",
        showCancelButton: false,
        confirmButtonText: trans('upay.general.popup.ok'),
        closeOnConfirm: true
    }, function () {
        if (response.redirect_url != undefined) {
            window.location.href = response.redirect_url;
            return false;
        }
        if (response.status != undefined && response.status == 'TOKEN_MISMATCH') {
            window.location.reload();
        }
    });
}
