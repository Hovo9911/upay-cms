<?php

namespace App\UPay\Career;

use App\MultiPKModel;

class CareerMl extends MultiPKModel
{
    protected $table = 'career_ml';
    public $timestamps = false;
    protected $primaryKey = ['career_id', 'lng_id'];
    public $incrementing = false;

    protected $fillable = [
        'career_id',
        'lng_id',
        'description',
        'title',
        'show_status',
    ];
}
