<?php

namespace App\Exceptions;

class InvalidDataException extends \Exception
{
    private $errors = [];

    public function getErrors(){
        return $this->errors;
    }

    public function setErrors($errors){
        $this->errors = $errors;
    }

    public static function withErrors($errors){
        $ex = new self();
        $ex->setErrors($errors);
        return $ex;
    }
}
