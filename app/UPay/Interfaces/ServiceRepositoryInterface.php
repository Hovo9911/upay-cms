<?php
namespace App\UPay\Interfaces;

use \App\UPay\Service\Service;
use Illuminate\Database\Eloquent\Collection;

interface ServiceRepositoryInterface
{
    public function create($data): Service;

    public function mock(): Service;

    public function findOrFail($id, $withMl = false): Service;

    public function update($id, $data): Service;

    public function saveMl(Service $service, array $mls): void;

    public function saveCommissions(Service $service, array $data): void;

    public function approveCommissions($id): void;

    public function saveLogo($newLogo, $oldLogo = null): void;

    public function all(): Collection;
}
