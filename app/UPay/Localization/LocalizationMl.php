<?php
namespace App\UPay\Localization;

use App\MultiPKModel;

class LocalizationMl extends MultiPKModel
{
    protected $table = 'localizations_ml';
    public $timestamps = false;
    protected $primaryKey = ['localization_id', 'lng_id'];
    public $incrementing = false;

    protected $fillable = [
        'localization_id',
        'lng_id',
        'value',
    ];
}
