<?php
use App\Model;
$activeMenu = 'website-career';
$canDelete = \Auth::user()->hasPermission('career.delete');
$canEdit = \Auth::user()->hasPermission('career.edit');
$title = 'upay.career.' . ($canEdit ? $mode : 'view') . '_title';
$disabled = $canEdit ? '' : 'disabled';


$gUploader = new \App\GUploader('website_career_image');
?>
@extends('layout')
@section('page_title') @lang($title) @stop
@section('content')
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox ">
                    <div class="ibox-title">
                        <h1>@lang($title)</h1>
                    </div>
                    <div class="ibox-content">
                        <form id="save-form" action="{{url('/website/career/save/'.($career->id ?? ''))}}"
                              method="POST">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="tabs-container">
                                        <ul class="nav nav-tabs" role="tablist">
                                            <li><a class="nav-link active" data-toggle="tab"
                                                   href="#tab-general">@lang('upay.tab.general')</a></li>
                                            @foreach($languages as $lng)
                                                <li><a class="nav-link" data-toggle="tab"
                                                       href="#tab-lng-{{$lng->id}}">{{$lng->name}}</a></li>
                                            @endforeach
                                        </ul>
                                        <div class="tab-content">
                                            <div role="tabpanel" id="tab-general" class="tab-pane active">
                                                <div class="panel-body">
                                                    <div class="form-group required row"><label
                                                            class="col-lg-2 col-form-label">@lang('upay.website.career.image')
                                                            <span class="align-self-center hint-tooltip"><i
                                                                    class="fa fa-lg fa-question-circle"
                                                                    data-toggle="tooltip" data-placement="right"
                                                                    title=""
                                                                    data-original-title="@lang('upay.website.career.image_hint')"></i></span></label>
                                                        <div class="col-lg-3">
                                                            <div class="g-upload-container">
                                                                @if(empty($disabled))
                                                                    <div class="g-upload-box">
                                                                        <input id="image" type="file"
                                                                               class="d-none g-upload" name="image"
                                                                               <?php echo $gUploader->isMultiple() ? 'multiple' : ''; ?> data-conf="website_career_image"
                                                                               data-action="<?php echo url('/g-uploader/upload'); ?>"
                                                                               accept="<?php echo $gUploader->getAcceptTypes(); ?>">
                                                                        <label for="image"
                                                                               class="btn btn-success">@lang('upay.general.uploader.label')</label>
                                                                    </div>
                                                                @endif

                                                                <div class="g-uploaded-list">
                                                                    @if(!empty($career->image))
                                                                        <div
                                                                            class="g-uploaded border fb fs14 position-relative text-break text-wrap">
                                                                            <img
                                                                                src="{{$gUploader->getFileUrl($career->image)}}"/>
                                                                            {{$gUploader->getFilename($career->image)}}
                                                                            <i class="g-upload-x fa fa-2x fa-remove text-danger"></i>
                                                                            <input type="hidden" name="image"
                                                                                   value="{{$career->image}}"/>
                                                                        </div>
                                                                    @endif
                                                                </div>

                                                                <div
                                                                    class="form-text text-danger form-error-text form-error-image"></div>
                                                                <div
                                                                    class="form-text text-danger form-error-text form-error-files"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row"><label class="col-lg-2 col-form-label">@lang('upay.website.career.category_type')</label>
                                                        <div class="col-lg-10">
                                                            <select name="career_category_id" {{$disabled}} class="form-control m-b select-picker">
                                                                <option value="">@lang('upay.general.dropdown.select')</option>
                                                                @foreach($careerCategories as $careerCategory)
                                                                    <option value="{{$careerCategory->id}}" @if($career->career_category_id == $careerCategory->id) selected="selected" @endif>{{$careerCategory->title}}</option>
                                                                @endforeach
                                                            </select>
                                                            <span class="form-text text-danger form-error-text form-error-career_category_id"></span>
                                                        </div>
                                                    </div>
                                                    <div class="form-group required row"><label
                                                            class="col-lg-2 col-form-label">@lang('upay.website.career.show_on_homepage')</label>
                                                        <div class="col-lg-10">
                                                            <input type="checkbox" name="show_on_homepage" value="1"
                                                                   @if($career->show_on_homepage) checked
                                                                   @endif {{$disabled}} data-toggle="switchbutton"
                                                                   data-onlabel="@lang('upay.website.career.show_on_homepage.yes')"
                                                                   data-offlabel="@lang('upay.website.career.show_on_homepage.no')"
                                                                   data-width="86" data-height="33">
                                                            <span
                                                                class="form-text text-danger form-error-text form-error-show_on_homepage"></span>
                                                        </div>
                                                    </div>
                                                    <div class="form-group required row"><label
                                                            class="col-lg-2 col-form-label">@lang('upay.general.sort_order')</label>
                                                        <div class="col-lg-2">
                                                            <input type="text" name="sort_order"
                                                                   value="{{$career->sort_order}}"
                                                                   {{$disabled}} class="form-control">
                                                            <span
                                                                class="form-text text-danger form-error-text form-error-sort_order"></span>
                                                        </div>
                                                    </div>

                                                    <div class="form-group row"><label for="general-status"
                                                                                       class="col-lg-2 col-form-label">@lang('upay.general.show_status')</label>
                                                        <div class="col-lg-10">
                                                            <input id="general-status" type="checkbox"
                                                                   name="show_status" value="{{Model::STATUS_ACTIVE}}"
                                                                   @if($career->show_status == Model::STATUS_ACTIVE) checked
                                                                   @endif {{$disabled}} data-toggle="switchbutton"
                                                                   data-onlabel="@lang('upay.general.show_status.active')"
                                                                   data-offlabel="@lang('upay.general.show_status.inactive')"
                                                                   data-width="86" data-height="33">
                                                            <span
                                                                class="form-text text-danger form-error-text form-error-show_status"></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            @foreach($languages as $lng)
                                                <div role="tabpanel" id="tab-lng-{{$lng->id}}" class="tab-pane">
                                                    <div class="panel-body">

                                                        <div class="form-group required row"><label
                                                                class="col-lg-2 col-form-label">@lang('upay.website.career.title')</label>
                                                            <div class="col-lg-10">
                                                                <textarea rows="3" name="ml[{{$lng->id}}][title]"
                                                                          {{$disabled}} class="form-control">{{isset($career->ml[$lng->id]) ? $career->ml[$lng->id]->title : ''}}</textarea>
                                                                <span
                                                                    class="form-text text-danger form-error-text form-error-ml_{{$lng->id}}_title"></span>
                                                            </div>
                                                        </div>
                                                        <div class="form-group required row"><label
                                                                class="col-lg-2 col-form-label">@lang('upay.website.career.description')</label>
                                                            <div class="col-lg-10">
                                                                <textarea rows="3" name="ml[{{$lng->id}}][description]"
                                                                          {{$disabled}} class="form-control content">{{isset($career->ml[$lng->id]) ? $career->ml[$lng->id]->description : ''}}</textarea>
                                                                <span
                                                                    class="form-text text-danger form-error-text form-error-ml_{{$lng->id}}_description"></span>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row"><label
                                                                class="col-lg-2 col-form-label">@lang('upay.general.show_status')</label>
                                                            <div class="col-lg-10">
                                                                <input type="checkbox"
                                                                       name="ml[{{$lng->id}}][show_status]"
                                                                       value="{{Model::STATUS_ACTIVE}}"
                                                                       @if(!isset($career->ml[$lng->id]) || $career->ml[$lng->id]->show_status == Model::STATUS_ACTIVE) checked
                                                                       @endif {{$disabled}} data-toggle="switchbutton"
                                                                       data-onlabel="@lang('upay.general.show_status.active')"
                                                                       data-offlabel="@lang('upay.general.show_status.inactive')"
                                                                       data-width="86" data-height="33">
                                                                <span
                                                                    class="form-text text-danger form-error-text form-error-ml_{{$lng->id}}_show_status"></span>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>

                            {{csrf_field()}}

                            <hr/>
                            <div class="form-group row">
                                <div class="col-lg-4 col-lg-offset-2">
                                    <a href="{{url('/website/career/list')}}"
                                       class="btn btn-white">@lang('upay.general.buttons.cancel')</a>
                                    @if($mode == 'edit' && $canDelete)
                                        <button onclick="gDelete({{$career->id}});return false;" type="button"
                                                class="btn btn-danger">@lang('upay.general.buttons.delete')</button>@endif
                                    @if($canEdit)
                                        <button class="btn btn-primary submit-btn"
                                                type="submit">@lang('upay.general.buttons.save')</button>@endif
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @if($mode == 'edit' && $canDelete)
        @include('components.delete', ['url' => url('/website/career/delete')])
    @endif
    <script src="{{script_url('upay/js/website/career.js')}}"></script>
@stop
