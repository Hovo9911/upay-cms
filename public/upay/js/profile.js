var $changePasswordForm = $.extend({}, $gForm);
$changePasswordForm.formId = "change-password-form";
$changePasswordForm.onSuccess = function(response) {
    swal({
        title: response.data.message,
        type: "success",
        showCancelButton: false,
        showConfirmButton: true,
        closeOnConfirm: true
    }, function() {
        window.location.href = response.data.redirect_url;
    });
};

$(document).ready(function() {
    $changePasswordForm.init();
});
