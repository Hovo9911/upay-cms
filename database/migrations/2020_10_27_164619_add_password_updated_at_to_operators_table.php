<?php

use App\DB\Migration;
use App\DB\Blueprint;

class AddPasswordUpdatedAtToOperatorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->getSchemaBuilder()->table('operators', function (Blueprint $table) {
            $table->timestamp('password_updated_at')->default(date('Y-m-d H:i:s'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $this->getSchemaBuilder()->table('operators', function (Blueprint $table) {
            $table->dropColumn('password_updated_at');
        });
    }
}
