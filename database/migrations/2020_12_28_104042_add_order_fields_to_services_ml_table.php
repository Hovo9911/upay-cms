<?php

use App\DB\Migration;
use App\DB\Blueprint;

class AddOrderFieldsToServicesMlTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->getSchemaBuilder()->table('services_ml', function (Blueprint $table) {
            $table->string('order_purpose_password', 1024)->default('');
            $table->string('order_received', 1024)->default('');
            $table->string('order_reason', 1024)->default('');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $this->getSchemaBuilder()->table('services_ml', function (Blueprint $table) {
            $table->dropColumn('order_purpose_password');
            $table->dropColumn('order_received');
            $table->dropColumn('order_reason');
        });
    }
}
