<?php

namespace App\Http\Controllers;

use App\Exceptions\InvalidDataException;
use App\Http\Requests\Cashbox\EditRequest;
use App\Http\Requests\Cashbox\SearchRequest;
use App\Http\Requests\DeleteRequest;
use App\UPay\Cashbox\CashboxSearch;
use App\UPay\Interfaces\BranchRepositoryInterface;
use App\UPay\Interfaces\CashboxRepositoryInterface;
use App\UPay\Interfaces\LanguageRepositoryInterface;
use Illuminate\Http\Request;

class CashboxController extends Controller
{
    private $cashboxRepository;
    private $languageRepository;
    private $branchRepository;

    public function __construct(CashboxRepositoryInterface $cashboxRepository, LanguageRepositoryInterface $languageRepository, BranchRepositoryInterface $branchRepository)
    {
        $this->branchRepository = $branchRepository;
        $this->languageRepository = $languageRepository;
        $this->cashboxRepository = $cashboxRepository;
    }

    public function index()
    {
        $branches = $this->branchRepository->all();
        return view('cashbox.list')->with([
            'branches' => $branches
        ]);
    }

    public function edit(Request $request, $id = null)
    {
        if ($id != null) {
            $cashbox = $this->cashboxRepository->findOrFail($id);
            $cashbox->load('ml');
            $cashbox->load('timeline.assignees.operator');
        } else {
            $cashbox = $this->cashboxRepository->mock();
        }
        $cashbox->ml = $cashbox->ml->keyBy('lng_id');
        $languages = $this->languageRepository->all();
        $branches = $this->branchRepository->all();
        return view('cashbox.edit')->with([
            'mode' => $id != null ? 'edit' : 'add',
            'cashbox' => $cashbox,
            'languages' => $languages,
            'branches' => $branches
        ]);
    }

    public function save(EditRequest $request, $id = null)
    {
        $data = $request->validated();
        if (!empty($data['branch_id']) && !$this->branchRepository->found($data['branch_id'])) {
            throw InvalidDataException::withErrors(['branch_id' => trans('upay.cashbox.branch_id.not_found')]);
        }
        if ($id != null) {
            $cashbox = $this->cashboxRepository->update($id, $data);
        } else {
            $cashbox = $this->cashboxRepository->create($data);
        }
        return $this->sendOK([
            'message' => trans('upay.cashbox.saved.message'),
            'edit_url' => url('/cashbox/edit/'.$cashbox->id),
            'list_url' => url('/cashbox/list')
        ]);
    }

    public function delete(DeleteRequest $request)
    {
        $this->cashboxRepository->delete($request->validated()['ids']);
        return $this->sendOK([
            'redirect_url' => url('/cashbox/list')
        ]);
    }

    public function getList(SearchRequest $request)
    {
        $searcher = new CashboxSearch($request->validated());
        return response()->json([
            'data' => $searcher->search(),
            'recordsTotal' => $searcher->totalCount(),
            'recordsFiltered' => $searcher->filteredCount(),
        ]);
    }
}
