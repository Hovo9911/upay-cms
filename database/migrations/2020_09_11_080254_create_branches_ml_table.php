<?php
use App\DB\Migration;
use App\DB\Blueprint;

class CreateBranchesMlTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->getSchemaBuilder()->create('branches_ml', function (Blueprint $table) {
            $table->integer('branch_id')->unsigned();
            $table->integer('lng_id')->unsigned();
            $table->string('name');
            $table->string('address');
            $table->showStatus();
            $table->primary(['branch_id', 'lng_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('branches_ml');
    }
}
