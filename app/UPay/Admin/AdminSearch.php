<?php

namespace App\UPay\Admin;

use App\Search;

class AdminSearch extends Search
{
    protected $orderables = [
        'id',
        'email',
        'name'
    ];

    protected function query()
    {
        $filters = $this->filters;
        $query = Admin::isNotRoot()
            ->when(!empty($filters['id']), function($query) use ($filters) {
                $query->where('id', $filters['id']);
            })
            ->when(!empty($filters['name']), function($query) use ($filters) {
                $query->where('name', 'ILIKE', '%'.$filters['name'].'%');
            })
            ->when(!empty($filters['email']), function($query) use ($filters) {
                $query->where('email', 'ILIKE', '%'.$filters['email'].'%');
            })
            ->when(!empty($filters['status']), function($query) use ($filters) {
                if ($filters['status'] == Admin::PASSWORD_RESET_STATUS_PENDING) {
                    $query->where('password_reset_status', Admin::PASSWORD_RESET_STATUS_PENDING)->where('show_status', '!=', Admin::STATUS_DELETED);
                } else {
                    $query->where('password_reset_status', 'is distinct from', Admin::PASSWORD_RESET_STATUS_PENDING)
                        ->where('show_status', $filters['status']);
                }
            }, function($query) use ($filters) {
                $query->where('show_status', '!=', Admin::STATUS_DELETED);
            })
            ->when(!empty($filters['role']), function($query) use ($filters) {
                $query->whereHas('roles', function ($query) use($filters) {
                    $query->where('role_id', $filters['role']);
                });
            })
            ->with([
                'roles' => function($query) { $query->notDeleted(); },
                'roles.role' => function($query) { $query->notDeleted(); }
                ]);
        return $query;
    }

    public function totalCount()
    {
        return Admin::notDeleted()->isNotRoot()->count();
    }
}
