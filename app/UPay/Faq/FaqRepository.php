<?php

namespace App\UPay\Faq;

use App\UPay\Faq\FaqMl;
use App\UPay\Interfaces\FaqRepositoryInterface;
use DB;
use Illuminate\Database\Eloquent\Collection;

class FaqRepository implements FaqRepositoryInterface
{
    public function create($data): Faq
    {
        $faq = new Faq($data);
        DB::beginTransaction();
        $faq->save();
        $this->saveMl($faq, $data['ml']);
        DB::commit();
        return $faq;
    }

    public function mock(): Faq
    {
        return new Faq([
            'category_id' => null,
            'sort_order' => '',
            'show_status' => Faq::STATUS_ACTIVE,
        ]);
    }

    public function findOrFail($id, $withMl = false): Faq
    {
        $faq = Faq::where('id', $id)->where('faqs.show_status', '!=', Faq::STATUS_DELETED);

        if ($withMl) {
            $faq->joinMl();
        }
        $faq = $faq->first();

        if (empty($faq)) {
            abort(404);
        }
        return $faq;
    }

    public function update($id, $data): Faq
    {
        $faq = $this->findOrFail($id);

        DB::beginTransaction();
        $faq->update($data);
        $this->saveMl($faq, $data['ml']);
        DB::commit();
        return $faq;
    }

    public function delete($ids): int
    {
        DB::beginTransaction();
        FaqMl::whereIn('faq_id', $ids)->update(['show_status' => FaqMl::STATUS_DELETED]);
        $faq = Faq::whereIn('id', $ids)->update(['show_status' => Faq::STATUS_DELETED]);
        DB::commit();
        return $faq;
    }

    public function saveMl(Faq $faq, array $mls): void
    {
        foreach ($mls as $lngId => $ml) {
            $faq->ml()->updateOrCreate(
                ['faq_id' => $faq->id, 'lng_id' => $lngId],
                [
                    'question' => $ml['question'],
                    'answer' => $ml['answer'],
                    'show_status' => $ml['show_status'],
                ]
            );
        }
    }

    public function all(): Collection
    {
        return Faq::joinMl()
            ->where('faqs.show_status', Faq::STATUS_ACTIVE)
            ->where('ml.show_status', Faq::STATUS_ACTIVE)
            ->get();
    }
}

