<?php

namespace App\UPay\Transaction;

use App\Search;

class TransactionCancellationSearch extends Search
{
    protected $orderables = [
        'id',
        'cancellation_date',
        'transaction_date',
        'total_amount',
        'receipt_id',
    ];

    protected function query()
    {
        $filters = $this->filters;
        $query = TransactionCancellation::join('transactions', function ($join) {
            $join->on('transaction_cancellations.transaction_id', '=', 'transactions.id');
        })
        ->select(
            'transaction_cancellations.id',
            'transaction_cancellations.created_at as cancellation_date',
            'transactions.created_at as transaction_date',
            'transactions.receipt_id',
            'transactions.cashbox_id',
            'transactions.service_id',
            'transactions.total_amount',
            'transactions.code',
            'transaction_cancellations.operator_id', //operator name
            'transaction_cancellations.status',
            'transaction_cancellations.type'
        )
        ->when(!empty($filters['status']), function($query) use ($filters) {
            $query->where('transaction_cancellations.status', $filters['status']);
        })
        ->when(!empty($filters['amount_from']), function($query) use ($filters) {
            $query->where('transactions.total_amount', '>=', $filters['amount_from']);
        })
        ->when(!empty($filters['amount_to']), function($query) use ($filters) {
            $query->where('transactions.total_amount', '<=', $filters['amount_to']);
        })
        ->when(!empty($filters['cancellation_date_from']), function($query) use ($filters) {
            $query->where('transaction_cancellations.created_at', '>=', $filters['cancellation_date_from'].' 00:00:00');
        })
        ->when(!empty($filters['cancellation_date_to']), function($query) use ($filters) {
            $query->where('transaction_cancellations.created_at', '<=', $filters['cancellation_date_to'].' 23:59:59');
        })
            ->when(!empty($filters['transaction_date_from']), function($query) use ($filters) {
            $query->where('transactions.created_at', '>=', $filters['transaction_date_from'].' 00:00:00');
        })
        ->when(!empty($filters['transaction_date_to']), function($query) use ($filters) {
            $query->where('transactions.created_at', '<=', $filters['transaction_date_to'].' 23:59:59');
        })
        ->when(!empty($filters['receipt_id']), function($query) use ($filters) {
            $query->where('transactions.receipt_id', $filters['receipt_id']);
        })
        ->when(!empty($filters['code']), function($query) use ($filters) {
            $query->where('transactions.code', $filters['code']);
        })
        ->when(!empty($filters['service_id']), function($query) use ($filters) {
            $query->where('transactions.service_id', $filters['service_id']);
        })
        ->when(!empty($filters['type']), function($query) use ($filters) {
            $query->where('transaction_cancellations.type', $filters['type']);
        })
        ->with(['service', 'cashbox', 'operator']);
        return $query;
    }

    public function totalCount()
    {
        return TransactionCancellation::count();
    }
}
