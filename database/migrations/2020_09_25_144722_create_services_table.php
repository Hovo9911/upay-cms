<?php

use App\DB\Migration;
use App\DB\Blueprint;

class CreateServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->getSchemaBuilder()->create('services', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('provider_id')->unsigned()->nullable();
            $table->string('branch_account_number');
            $table->string('branch_commission_account_number');
            $table->string('mobile_account_number');
            $table->string('mobile_commission_account_number');
            $table->integer('account_min')->nullable();
            $table->integer('account_max')->nullable();
            $table->boolean('use_fixed_amount');
            $table->integer('sort_order');
            $table->showStatus();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('services');
    }
}
