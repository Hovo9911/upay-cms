<?php

namespace App\UPay\Operator;

use App\UPay\Interfaces\OperatorRepositoryInterface;
use Illuminate\Database\Eloquent\Collection;

class OperatorRepository implements OperatorRepositoryInterface
{
    public function create($data): Operator
    {
        $data['password'] = '';
        $data['password_updated_at'] = date('Y-m-d H:i:s');
        $operator = new Operator($data);
        $operator->save();
        return $operator;
    }

    public function mock(): Operator
    {
        return new Operator([
            'first_name' => '',
            'last_name' => '',
            'email' => '',
            'phone' => '',
            'show_status' => Operator::STATUS_ACTIVE,
            'password_reset_status' => null
        ]);
    }

    public function findOrFail($id): Operator
    {
        $operator = Operator::where('id', $id)->notDeleted()->first();
        if (empty($operator)) {
            abort(404);
        }
        return $operator;
    }

    public function update(Operator $operator, $data): Operator
    {
        $operator->fill($data);
        $operator->save();
        return $operator;
    }

    public function emailFound($email, $id): bool
    {
        if (empty($email)) {
            return false;
        }
        $query = Operator::where('email', 'ILIKE', $email)->notDeleted();
        if (!empty($id)) {
            $query->where('id', '!=', $id);
        }
        return !empty($query->first());
    }

    public function all(bool $onlyActives = true): Collection
    {
        if ($onlyActives) {
            return Operator::active()->get();
        }
        return Operator::all();
    }

    public function checkIds($ids): bool
    {
        return Operator::active()->whereIn('id', $ids)->count() == count($ids);
    }
}
