<?php

namespace App\Http\Requests\Admin;

use App\Http\Requests\DatatableSearchRequest;
use App\Model;
use App\UPay\Admin\Admin;

class SearchRequest extends DatatableSearchRequest
{
    public function rules()
    {
        return [
            'filters.id' => 'integer',
            'filters.email' => 'string|max:255',
            'filters.name' => 'string|max:255',
            'filters.role' => 'integer',
            'filters.status' => 'in:'.Model::STATUS_ACTIVE.','.Model::STATUS_INACTIVE.','.Admin::PASSWORD_RESET_STATUS_PENDING,
            'order' => 'array',
            'start' => 'integer',
        ];
    }
}
