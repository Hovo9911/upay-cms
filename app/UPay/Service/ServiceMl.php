<?php

namespace App\UPay\Service;

use App\MultiPKModel;

class ServiceMl extends MultiPKModel
{
    protected $table = 'services_ml';
    public $timestamps = false;
    protected $primaryKey = ['service_id', 'lng_id'];
    public $incrementing = false;

    protected $fillable = [
        'service_id',
        'lng_id',
        'name',
        'payment_purpose',
        'invoice_note',
        'title',
        'description',
        'order_purpose_password',
        'order_received',
        'order_reason',
        'show_status',
    ];
}
