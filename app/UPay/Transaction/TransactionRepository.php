<?php

namespace App\UPay\Transaction;

use App\Exceptions\GeneralErrorException;
use App\UPay\Interfaces\TransactionRepositoryInterface;

class TransactionRepository implements TransactionRepositoryInterface
{
    public function findCancellationOrFail($id) : TransactionCancellation
    {
        $cancellation = TransactionCancellation::where('id', $id)->first();
        if (empty($cancellation)) {
            abort(404);
        }
        return $cancellation;
    }

    public function changeCancellationStatus(TransactionCancellation $cancellation, $data) : void
    {
        if ($this->cancellationStatusChangeAllowed($cancellation, $cancellation->status, $data['status'])) {
            \DB::beginTransaction();

            $cancellation->update(['status' => $data['status']]);
            $tch = new TransactionCancellationHistory([
                'cancellation_id' => $cancellation->id,
                'doer_id' => \Auth::user()->id,
                'date' => date('Y-m-d H:i:s'),
                'status' => $data['status'],
                'notes' => $data['notes'],
            ]);
            $tch->save();

            if ($cancellation->status == TransactionCancellation::STATUS_COMPLETED) {
                $cancellation->transaction->update(['is_cancelled' => true]);
                $this->copyToArmsoft($cancellation->transaction);
            }

            \DB::commit();
            return;
        }
        throw new GeneralErrorException(trans('upay.cancellation.change_status.not_allowed'));
    }

    public function cancellationStatusChangeAllowed($cancellation, $from, $to): bool
    {
        switch ($from) {
            case TransactionCancellation::STATUS_NEW:
                return ($to == TransactionCancellation::STATUS_ABORTED || $to == TransactionCancellation::STATUS_PROCESSING);
            case TransactionCancellation::STATUS_PROCESSING:
                return ($to == TransactionCancellation::STATUS_PRE_APPROVED || $to == TransactionCancellation::STATUS_PRE_DECLINED);
            case TransactionCancellation::STATUS_PRE_APPROVED:
                return ($to == TransactionCancellation::STATUS_FINALLY_APPROVED);
            case TransactionCancellation::STATUS_PRE_DECLINED:
                return ($to == TransactionCancellation::STATUS_FINALLY_DECLINED);
            case TransactionCancellation::STATUS_FINALLY_APPROVED:
                return ($cancellation->type == TransactionCancellation::TYPE_CUSTOMER && $to == TransactionCancellation::STATUS_COMPLETED);
            case TransactionCancellation::STATUS_FINALLY_DECLINED:
                return ($cancellation->type == TransactionCancellation::TYPE_CUSTOMER && $to == TransactionCancellation::STATUS_REJECTED);
        }
        return false;
    }

    public function copyToArmsoft(Transaction $transaction)
    {
        //հաճախորդի չեղարկում, հաստատում
        \DB::beginTransaction();
        $credit = config('upay.armsoft.customer_cancellation.credit_account');
        $debit = $transaction->service->branch_account_number;
        $data = [
            'transaction_type' => ArmsoftTransaction::TYPE_CANCELLATION,
            'transaction_id' => $transaction->id,
            'transaction_date' => $transaction->created_at,
            'amount' => $transaction->amount,
            'account_credit' => $credit,
            'account_debit' => $debit,
            'outer_account_credit' => '',
            'outer_account_debit' => '',
            'aim' => trans('upay.cancellation.purpose', ['purpose' => $transaction->purpose, 'receipt_id' => $transaction->receipt_id]),
            'branch_code' => $transaction->branch->armsoft_code,
            'receipt_id' => $transaction->receipt_id,
            'status' => ArmsoftTransaction::STATUS_PENDING,
            'currency' => 'AMD'
        ];
        $armTr = new ArmsoftTransaction($data);
        $armTr->save();

        if ($transaction->commission != 0) {
            $data['amount'] = $transaction->commission;
            $data['account_debit'] = $transaction->service->branch_commission_account_number;
            $data['aim'] .= trans('upay.armsoft.purpose.commisson');
            $armTrFee = new ArmsoftTransaction($data);
            $armTrFee->save();
        }
        \DB::commit();
    }

    public function repeat($id)
    {
        Transaction::where('id', $id)
            ->where('repeat_status', 'is distinct from', Transaction::REPEAT_STATUS_PENDING)
            ->failed()
            ->update([
                'repeat_status' => Transaction::REPEAT_STATUS_PENDING
            ]);
    }

    public function complete($id)
    {
        Transaction::where('id', $id)
            ->where('repeat_status', 'is distinct from', Transaction::REPEAT_STATUS_PENDING)
            ->failed()
            ->update([
                'repeat_status' => Transaction::REPEAT_STATUS_COMPLETED
            ]);
    }
}
