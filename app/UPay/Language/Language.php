<?php

namespace App\UPay\Language;

use \App\Model;

class Language extends Model
{
    protected $table = 'languages';

    protected $fillable = [
        'code',
        'name',
        'show_status',
    ];
}
