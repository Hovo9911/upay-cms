<?php
    use App\UPay\Transaction\TransactionCancellation;

    $activeMenu = 'cancellation';
    $canEdit = \Auth::user()->hasPermission('cancellation.edit');
    $jsTrans->add([
        'upay.cancellation_change_status.confirm_modal_cancel',
        'upay.cancellation_change_status.confirm_modal_ok',
        'upay.cancellation_change_status.confirm_modal_title',
        'upay.cancellation_change_status.confirm_modal_text',
    ]);
?>
@extends('layout')
@section('page_title') @lang('upay.cancellation.view_title') @stop
@section('content')
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox">
                    <div class="ibox-title">
                        <h1>@lang('upay.cancellation.view.'.$cancellation->type)</h1>
                        <span class="badge {{getCancellationStatusColor($cancellation->status)}}">@lang('upay.cancellation.'.($cancellation->status == TransactionCancellation::STATUS_COMPLETED || $cancellation->status == TransactionCancellation::STATUS_REJECTED ? $cancellation->type.'.' : '').'status.'.$cancellation->status)</span>
                    </div>
                    <div class="ibox-content">
                        <div class="row">
                            <div class="col-lg-6">
                                <table class="table table-bordered">
                                    <tbody>
                                        <tr>
                                            <td>@lang('upay.transaction.date')</td>
                                            <td>{{format_date($cancellation->transaction->created_at)}}</td>
                                        </tr>
                                        <tr>
                                            <td>@lang('upay.transaction.cashbox')</td>
                                            <td>{{$cancellation->transaction->cashbox->name}}</td>
                                        </tr>
                                        <tr>
                                            <td>@lang('upay.transaction.amount')</td>
                                            <td>{{$cancellation->transaction->total_amount}}</td>
                                        </tr>
                                        <tr>
                                            <td>@lang('upay.transaction.operator')</td>
                                            <td>{{$cancellation->operator->first_name}} {{$cancellation->operator->last_name}}</td>
                                        </tr>
                                        <tr>
                                            <td>@lang('upay.transaction.payment_type')</td>
                                            <td>{{$cancellation->transaction->cash ? trans('upay.transaction.payment_type.cash') : trans('upay.transaction.payment_type.non_cash')}}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-lg-6">
                                <table class="table table-bordered">
                                    <tbody>
                                    <tr>
                                        <td>@lang('upay.transaction.receipt_id')</td>
                                        <td>{{$cancellation->transaction->receipt_id}}</td>
                                    </tr>
                                    <tr>
                                        <td>@lang('upay.transaction.purpose')</td>
                                        <td>{{$cancellation->transaction->purpose}}</td>
                                    </tr>
                                    @if($cancellation->transaction->service_type == \App\UPay\Transaction\Transaction::SERVICE_TYPE_PAYMENT)
                                    <tr>
                                        <td>@lang('upay.transaction.code')</td>
                                        <td>{{$cancellation->transaction->code}}</td>
                                    </tr>
                                    @endif
                                    <tr>
                                        <td>@lang('upay.transaction.contact_number')</td>
                                        <td>{{$cancellation->transaction->contact_number}}</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        @if($cancellation->type == TransactionCancellation::TYPE_CUSTOMER)
                            <hr>

                            <div class="row mb-2"><div class="col-lg-12">@lang('upay.cancellation.type')</div></div>

                            <div class="row">
                                <div class="col-lg-6">
                                    <table class="table table-bordered">
                                        <tbody>
                                        <tr>
                                            <td>@lang('upay.cancellation.details.name')</td>
                                            <td>{{$cancellation->details['applicant']['name']}}</td>
                                        </tr>
                                        <tr>
                                            <td>@lang('upay.cancellation.details.phone')</td>
                                            <td>{{$cancellation->details['applicant']['phone']}}</td>
                                        </tr>
                                        <tr>
                                            <td>@lang('upay.cancellation.details.passport_number')</td>
                                            <td>{{$cancellation->details['applicant']['passport_number']}}</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="col-lg-6">
                                    <table class="table table-bordered">
                                        <tbody>
                                        <tr>
                                            <td>@lang('upay.cancellation.details.passport_issued_at')</td>
                                            <td>{{$cancellation->details['applicant']['passport_issued_at']}}</td>
                                        </tr>
                                        <tr>
                                            <td>@lang('upay.cancellation.details.passport_expires_at')</td>
                                            <td>{{$cancellation->details['applicant']['passport_expires_at']}}</td>
                                        </tr>
                                        <tr>
                                            <td>@lang('upay.cancellation.details.passport_issuer')</td>
                                            <td>{{$cancellation->details['applicant']['passport_issuer']}}</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        @endif

                        <hr>

                        <div class="row">
                            <div class="col-lg-12">@lang('upay.cancellation.type') - <b>@lang('upay.cancellation.type.'.$cancellation->type)</b></div>
                        </div>
                        <br>
                        @if($cancellation->type == TransactionCancellation::TYPE_CASHIER)
                            <div class="row">
                                <div class="col-lg-12">@lang('upay.cancellation.details.reason') - <b>@lang('upay.cancellation.details.reason.'.$cancellation->details['reason'])</b></div>
                            </div>
                        @else
                            <div class="row">
                                <div class="col-lg-12">@lang('upay.cancellation.details.service_details') - <b>{{$cancellation->details['service']['name']}}</b>
                                    @foreach($cancellation->details['service_display_data'] as $item)
                                        {{$item['label']}} <b>{{$item['value']}} </b>
                                    @endforeach</div>
                            </div>
                        @endif

                        <hr>

                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>@lang('upay.cancellation.history.date')</th>
                                <th>@lang('upay.cancellation.history.doer')</th>
                                <th>@lang('upay.cancellation.history.status')</th>
                                <th>@lang('upay.cancellation.history.notes')</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($cancellation->history as $row)
                                <tr>
                                    <td>{{format_date($row->date)}}</td>
                                    <td>{{$row->admin_did ? $row->admin->name : $row->operator->first_name.' '.$row->operator->last_name}}</td>
                                    <td><span class="badge {{getCancellationStatusColor($row->status)}}">@lang('upay.cancellation.'.($row->status == TransactionCancellation::STATUS_COMPLETED || $row->status == TransactionCancellation::STATUS_REJECTED ? $cancellation->type.'.' : '').'status.'.$row->status)</span></td>
                                    <td>{{$row->notes}}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                        @if($cancellation->type == TransactionCancellation::TYPE_CUSTOMER)
                            <div class="row"><div class="col-lg-12">@lang('upay.cancellation.history.attached_files')</div></div>
                            <div class="row mb-4">
                                <div class="col-lg-12">
                                @foreach($cancellation->details['files'] as $file)
                                    <a href="{{$file['url']}}" target="_blank">{{$file['label']}}{{$loop->last ? '' : '; '}}</a>
                                @endforeach
                                </div>
                            </div>
                        @endif

                        <form id="change-status-form" action="{{url('/cancellation/change-status/'.$cancellation->id)}}" method="POST">
                            @if(in_array($cancellation->status, [TransactionCancellation::STATUS_NEW, TransactionCancellation::STATUS_PROCESSING, TransactionCancellation::STATUS_PRE_DECLINED, TransactionCancellation::STATUS_PRE_APPROVED]) ||
                            ($cancellation->type == TransactionCancellation::TYPE_CUSTOMER && in_array($cancellation->status, [TransactionCancellation::STATUS_FINALLY_DECLINED, TransactionCancellation::STATUS_FINALLY_APPROVED])))
                            <div class="form-group row">
                                <div class="col-lg-6">
                                    <label for="notes">@lang('upay.cancellation.change_status.notes')</label>
                                    <textarea id="notes" rows="3" name="notes" class="form-control"></textarea>
                                    <span class="form-text text-danger form-error-text form-error-notes"></span>
                                </div>
                            </div>
                            @endif
                            <div class="form-group row">
                                <span class="form-text text-danger form-error-text form-error-status"></span>
                                <div class="col-lg-12 col-lg-offset-2">
                                    <a href="{{url('/cancellation/list')}}" class="btn btn-white">@lang('upay.general.buttons.cancel')</a>
                                    @if($canEdit)
                                        @if($cancellation->status == TransactionCancellation::STATUS_NEW)
                                            <button onclick="$saveForm.changeStatus('{{TransactionCancellation::STATUS_ABORTED}}', this)" data-confirm="true" class="btn btn-danger submit-btn" type="button">@lang('upay.cancellation.buttons.abort')</button>
                                            <button onclick="$saveForm.changeStatus('{{TransactionCancellation::STATUS_PROCESSING}}', this)" data-confirm="false" class="btn btn-primary submit-btn" type="button">@lang('upay.cancellation.buttons.process')</button>
                                        @elseif($cancellation->status == TransactionCancellation::STATUS_PROCESSING)
                                            <button onclick="$saveForm.changeStatus('{{TransactionCancellation::STATUS_PRE_DECLINED}}', this)" data-confirm="false" class="btn btn-danger submit-btn" type="button">@lang('upay.cancellation.buttons.pre_decline')</button>
                                            <button onclick="$saveForm.changeStatus('{{TransactionCancellation::STATUS_PRE_APPROVED}}', this)" data-confirm="false" class="btn btn-primary submit-btn" type="button">@lang('upay.cancellation.buttons.pre_approve')</button>
                                        @elseif($cancellation->status == TransactionCancellation::STATUS_PRE_APPROVED)
                                            <button onclick="$saveForm.changeStatus('{{TransactionCancellation::STATUS_FINALLY_APPROVED}}', this)" data-confirm="false" class="btn btn-primary submit-btn" type="button">@lang('upay.cancellation.buttons.finally_approve')</button>
                                        @elseif($cancellation->status == TransactionCancellation::STATUS_PRE_DECLINED)
                                            <button onclick="$saveForm.changeStatus('{{TransactionCancellation::STATUS_FINALLY_DECLINED}}', this)" data-confirm="false" class="btn btn-danger submit-btn" type="button">@lang('upay.cancellation.buttons.finally_decline')</button>
                                        @elseif($cancellation->status == TransactionCancellation::STATUS_FINALLY_APPROVED && $cancellation->type == TransactionCancellation::TYPE_CUSTOMER)
                                            <button onclick="$saveForm.changeStatus('{{TransactionCancellation::STATUS_COMPLETED}}', this)" data-confirm="false" class="btn btn-primary submit-btn" type="button">@lang('upay.cancellation.buttons.complete')</button>
                                        @elseif($cancellation->status == TransactionCancellation::STATUS_FINALLY_DECLINED && $cancellation->type == TransactionCancellation::TYPE_CUSTOMER)
                                            <button onclick="$saveForm.changeStatus('{{TransactionCancellation::STATUS_REJECTED}}', this)" data-confirm="false" class="btn btn-primary submit-btn" type="button">@lang('upay.cancellation.buttons.reject')</button>
                                        @endif
                                    @endif
                                </div>
                            </div>
                            <input type="hidden" name="status" value="">
                            {{csrf_field()}}
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="{{script_url('upay/js/cancellation.js')}}"></script>
@stop
