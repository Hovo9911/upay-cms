<?php

use App\DB\Migration;
use App\DB\Blueprint;
use App\UPay\Transaction\TransactionCancellation;

class CreateTransactionCancellationHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->getSchemaBuilder()->create('transaction_cancellation_history', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('cancellation_id');
            $table->unsignedInteger('doer_id')->nullable()->default(null);
            $table->enum('status', TransactionCancellation::statuses());
            $table->timestamp('date');
            $table->string('notes', 1024);
            $table->index('cancellation_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaction_cancellation_history');
    }
}
