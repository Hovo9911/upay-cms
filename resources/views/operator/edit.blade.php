<?php
    use App\Model;
    use App\UPay\Operator\Operator;
    $activeMenu = 'operator';
    $canEdit = \Auth::user()->hasPermission('operator.edit');
    $canResetPassword = \Auth::user()->hasPermission('operator.reset_password');
    $title = 'upay.operator.'.($canEdit ? $mode : 'view').'_title';
    $disabled = $canEdit ? '' : 'disabled';
    $jsTrans->add([
        'upay.operator.status.password_reset_pending',
        'upay.operator.reset_password.confirm_modal_title',
        'upay.operator.reset_password.confirm_modal_text'
    ]);
?>
@extends('layout')
@section('page_title') @lang($title) @stop
@section('content')
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox ">
                    <div class="ibox-title">
                        <h1>@lang($title)</h1>
                    </div>
                    <div class="ibox-content">
                        <form id="save-form" action="{{url('/operator/save/'.($operator->id ?? ''))}}" method="POST">

                            @if($mode == 'edit' && $operator->show_status == Model::STATUS_ACTIVE && $operator->password_reset_status == Operator::PASSWORD_RESET_STATUS_PENDING)
                                <div class="form-group row">
                                    <div class="text-warning p-xs b-r-sm"><i class="fa fa-clock-o"></i> @lang('upay.operator.status.password_reset_pending.info')</div>
                                </div>
                            @endif

                            <div class="form-group required row"><label class="col-lg-2 col-form-label">@lang('upay.operator.email')</label>
                                <div class="col-lg-10">
                                    <input type="text" name="email" value="{{$operator->email}}" @if(!empty($disabled) || $mode == 'edit') disabled="disabled" @endif class="form-control">
                                    <span class="form-text text-danger form-error-text form-error-email"></span>
                                </div>
                            </div>
                            <div class="form-group required row"><label class="col-lg-2 col-form-label">@lang('upay.operator.first_name')</label>
                                <div class="col-lg-10">
                                    <input type="text" name="first_name" value="{{$operator->first_name}}" {{$disabled}} class="form-control">
                                    <span class="form-text text-danger form-error-text form-error-first_name"></span>
                                </div>
                            </div>
                            <div class="form-group required row"><label class="col-lg-2 col-form-label">@lang('upay.operator.last_name')</label>
                                <div class="col-lg-10">
                                    <input type="text" name="last_name" value="{{$operator->last_name}}" {{$disabled}} class="form-control">
                                    <span class="form-text text-danger form-error-text form-error-last_name"></span>
                                </div>
                            </div>
                            <div class="form-group required row"><label class="col-lg-2 col-form-label">@lang('upay.operator.phone')</label>
                                <div class="col-lg-10">
                                    <div class="input-group m-b">
                                        <div class="input-group-prepend">
                                            <span class="input-group-addon">+374</span>
                                        </div>
                                        <input type="text" name="phone" id="phone" value="{{$operator->phone}}" {{$disabled}} class="form-control">
                                    </div>
                                    <div class="form-text text-danger form-error-text form-error-phone"></div>
                                </div>
                            </div>
                            <div class="form-group row"><label class="col-lg-2 col-form-label">@lang('upay.general.show_status')</label>
                                <div class="col-lg-10">
                                    <input type="checkbox" name="show_status" value="{{Model::STATUS_ACTIVE}}" @if($operator->show_status == Model::STATUS_ACTIVE) checked @endif {{$disabled}} data-toggle="switchbutton" data-onlabel="@lang('upay.general.show_status.active')" data-offlabel="@lang('upay.general.show_status.inactive')">
                                    <span class="form-text text-danger form-error-text form-error-show_status"></span>
                                </div>
                            </div>
                            @if($mode == 'edit' && $operator->show_status == Model::STATUS_ACTIVE && $canResetPassword)
                                <div class="form-group row"><label class="col-lg-2 col-form-label">@lang('upay.operator.reset_password')</label>
                                    <div class="col-lg-10">
                                        <button class="btn btn-warning reset-password-btn" type="button"><i class="fa fa-warning"></i> <span class="bold">@lang('upay.operator.reset_password.btn')</span></button>
                                    </div>
                                </div>
                            @endif

                            {{csrf_field()}}

                            <hr/>
                            <div class="form-group row">
                                <div class="col-lg-4 col-lg-offset-2">
                                    <a href="{{url('/operator/list')}}" class="btn btn-white">@lang('upay.general.buttons.cancel')</a>
                                    @if($canEdit)<button class="btn btn-primary submit-btn" type="submit">@lang('upay.general.buttons.save')</button>@endif
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @if($mode == 'edit' && $operator->show_status == Model::STATUS_ACTIVE && $canResetPassword)
        <form id="reset-password-form" action="{{url('/operator/reset-password/'.$operator->id)}}" method="POST">
            {{csrf_field()}}
        </form>
    @endif

    <script src="{{script_url('upay/js/operator.js')}}"></script>
@stop
