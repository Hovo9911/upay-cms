<?php

namespace App\Http\Requests\Transaction;

use App\Http\Requests\DatatableSearchRequest;
use App\Model;
use App\UPay\Transaction\TransactionCancellation;

class CancellationSearchRequest extends DatatableSearchRequest
{
    public function rules()
    {
        return [
            'filters.status' => 'in:'.implode(',', TransactionCancellation::statuses()),
            'filters.amount_from' => 'integer',
            'filters.amount_to' => 'integer',
            'filters.service_id' => 'integer',
            'filters.receipt_id' => 'string|regex:/^[0-9]{10,13}$/',
            'filters.cancellation_date_from' => 'date',
            'filters.cancellation_date_to' => 'date',
            'filters.transaction_date_from' => 'date',
            'filters.transaction_date_to' => 'date',
            'filters.code' => 'string',
            'filters.type' => 'string|in:'.TransactionCancellation::TYPE_CASHIER.','.TransactionCancellation::TYPE_CUSTOMER,
            'order' => 'array',
            'start' => 'integer',
        ];
    }
}
