<?php

namespace App\Http\Controllers;

use App\Exceptions\InvalidDataException;
use App\GUploader;
use Illuminate\Http\Request;
use File;
use Input;

class GUploaderController extends Controller
{
	public function upload(Request $request)
    {
        $files = $request->file('files');
        if (empty($files)) {
            throw InvalidDataException::withErrors(['files' => trans('upay.general.uploader.file_required')]);
        }

        $guploader = new GUploader($request->input('conf'));
        $conf = $guploader->getConfig();

        $errors = [];
        $data = [];
        foreach($files as $file) {
            if (!$guploader->checkFilename($file)) {
                $errors []= trans('upay.general.uploader.invalid_filename');
                continue;
            }
            $type = $guploader->checkType($file);
            if ($type === false) {
                $errors []= trans('upay.general.uploader.invalid_type');
                continue;
            }
            if (!$guploader->checkSize($file)) {
                $errors []= trans('upay.general.uploader.invalid_size');
                continue;
            }

            $fileData = $guploader->storeTemp($file);
            $data []= [
                'thumbnail' => ($type == 'photo') ? url($conf['disk'] . '/' . $fileData['file_path']) : '',
                'file_name' => $fileData['tmp_name'],
                'label' => $fileData['file_name']
            ];

            if (empty($errors) && !$conf['multiple']) {
                continue;
            }
        }

        if (!empty($errors)) {
            throw InvalidDataException::withErrors(['files' => implode(', ', $errors)]);
        }

        return $this->sendOK($data);
    }
}
