<?php

use App\DB\Migration;
use App\DB\Blueprint;

class CreateLocalizationsMlTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->getSchemaBuilder()->create('localizations_ml', function (Blueprint $table) {
            $table->integer('localization_id')->unsigned();
            $table->integer('lng_id')->unsigned();
            $table->text('value');
            $table->primary(['localization_id', 'lng_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('localizations_ml');
    }
}
