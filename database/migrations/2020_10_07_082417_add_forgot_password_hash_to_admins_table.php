<?php

use App\DB\Migration;
use App\DB\Blueprint;

class AddForgotPasswordHashToAdminsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->getSchemaBuilder()->table('admins', function (Blueprint $table) {
            $table->string('forgot_password_hash')->nullable()->default(null);
            $table->timestamp('forgot_password_hash_exp_date')->nullable()->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $this->getSchemaBuilder()->table('admins', function (Blueprint $table) {
            $table->dropColumn('forgot_password_hash');
            $table->dropColumn('forgot_password_hash_exp_date');
        });
    }
}
