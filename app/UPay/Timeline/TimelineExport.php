<?php

namespace App\UPay\Timeline;

use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class TimelineExport implements FromArray, WithHeadings, WithStyles
{
    private $year = null;
    private $month = null;

    public function __construct($year, $month)
    {
        $this->year = $year;
        $this->month = $month;
    }
    public function styles(Worksheet $sheet)
    {
        $last = $sheet->getHighestRowAndColumn();
        $sheet->getStyle('C2:'.$last['column'].$last['row'])->getAlignment()->setWrapText(true);
    }

    public function array(): array
    {
        $timeline = Timeline::whereYear('date', '=', $this->year)
            ->whereMonth('date', $this->month)
            ->with(['cashbox', 'assignees.operator'])
            ->orderBy('cashbox_id', 'desc') //to be able to sort by only sort_order then (chain sorts not available for collections)
            ->get()
        ->sortBy(function($item){
            return $item->cashbox->sort_order;
        })->groupBy(function($item){
            return $item->cashbox->branch_id;
        })->map(function($item){
            return  $item->groupBy('cashbox_id');
        });

        $result = [];
        foreach ($timeline as $k => $itemsByBranch) {
            foreach ($itemsByBranch as $cashboxId => $items) {
                $items = $items->keyBy(function($item){ return date('j', strtotime($item->date)); });
                $row = [
                    $items->first()->cashbox->branch->name,
                    $items->first()->cashbox->name
                ];
                for ($i=1; $i<=date('t', strtotime($this->year.'-'.lead0($this->month).'-01')); $i++) {
                    $row[]= !empty($items[$i]) ? $items[$i]->assignees->map(function($assignee){
                        return username($assignee->operator->email);
                    })->implode("\r\n") : '';
                }
                $result[] = $row;
            }
        }
        return $result;
    }

    public function headings(): array
    {
        $headings = [
            trans('upay.timeline.branch'),
            trans('upay.timeline.cashbox'),
        ];
        for ($i=1; $i<=date('t', strtotime($this->year.'-'.lead0($this->month).'-01')); $i++) {
            $headings []= $i.'/'.$this->month.'/'.substr($this->year, -2);
        }
        return $headings;
    }
}
