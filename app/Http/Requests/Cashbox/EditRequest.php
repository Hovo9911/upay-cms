<?php

namespace App\Http\Requests\Cashbox;

use App\Http\Requests\Request;
use App\Model;

class EditRequest extends Request
{
    protected $module = 'cashbox';

    public function rules()
    {
        $rules = [
            'branch_id' => 'required|integer|nullable',
            'account_number' => 'required|max:255|regex:/^[0-9]+$/',
            'certificate_id' => 'string|max:255',
            'allow_close' => 'boolean',
            'allow_pos' => 'boolean',
            'pos_account_number' => 'required_if:allow_pos,1|max:255|regex:/^[0-9]+$/',
            'sort_order' => 'required|integer',
            'show_status' => 'in:'.Model::STATUS_ACTIVE.','.Model::STATUS_INACTIVE,
            'ml' => 'required|array',
        ];
        $rules = $this->checkStatusForRequired($rules, request()->input('show_status'));

        $rules = $this->addMlRules($rules, [
            'name' => 'required|max:255',
            'show_status' => 'in:'.Model::STATUS_ACTIVE.','.Model::STATUS_INACTIVE
        ]);
        return $rules;
    }

    protected function defaultValues()
    {
        return [
            'branch_id' => null,
            'account_number' => '',
            'certificate_id' => '',
            'allow_close' => 0,
            'allow_pos' => 0,
            'pos_account_number' => '',
            'sort_order' => 0,
            'show_status' => Model::STATUS_INACTIVE,
            'ml' => [
                'name' => '',
                'show_status' => Model::STATUS_INACTIVE,
            ]
        ];
    }

    public function messages()
    {
        $messages = parent::messages();
        return array_merge([
            'pos_account_number.required_if' => trans('upay.'.$this->module.'.pos_account_number.required')
        ], $messages);
    }
}
