<?php

namespace App\Http\Controllers;

use App\Http\Requests\Report\ServiceSearchRequest;
use App\UPay\Interfaces\ServiceRepositoryInterface;
use App\UPay\Transaction\TransactionByServiceExport;
use App\UPay\Transaction\TransactionByServiceSearch;

class ReportServicesController extends Controller
{
    public function index(ServiceRepositoryInterface $serviceRepository)
    {
        return view('report.services.list')->with([
            'services' => $serviceRepository->all()
        ]);
    }

    public function getList(ServiceSearchRequest $request)
    {
        $searcher = new TransactionByServiceSearch($request->validated());
        $result = $searcher->search();
        return response()->json([
            'data' => $result
        ]);
    }

    public function export(ServiceSearchRequest $request)
    {
        ini_set('memory_limit', '-1');
        $params = $request->validated();
        $format = !empty($params['format']) && $params['format'] == 'csv' ? 'csv' : 'xlsx';
        $type = $format == 'csv' ? \Maatwebsite\Excel\Excel::CSV : \Maatwebsite\Excel\Excel::XLSX;
        return (new TransactionByServiceExport($params))->download(trans('upay.report_services.export_file_name').'.'.$format, $type);
    }
}
