<?php

namespace App\Http\Controllers;

use App\Exceptions\InvalidDataException;
use App\Http\Requests\Timeline\EditRequest;
use App\UPay\Interfaces\CashboxRepositoryInterface;
use App\UPay\Interfaces\OperatorRepositoryInterface;
use App\UPay\Interfaces\TimelineRepositoryInterface;
use App\UPay\Operator\Operator;
use App\UPay\Timeline\TimelineExport;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class TimelineController extends Controller
{
    private $cashboxRepository;
    private $operatorRepository;
    private $timelineRepository;

    public function __construct(CashboxRepositoryInterface $cashboxRepository, OperatorRepositoryInterface $operatorRepository, TimelineRepositoryInterface $timelineRepository)
    {
        $this->cashboxRepository = $cashboxRepository;
        $this->operatorRepository = $operatorRepository;
        $this->timelineRepository = $timelineRepository;
    }

    public function index(Request $request)
    {
        $month = $request->input('m');
        $year = $request->input('y');
        $month = empty($month) ? date('n') : $month;
        $year = empty($year) ? date('Y') : $year;
        if (!is_string($year) || !is_string($month) || !checkdate($month, 1, $year)) {
            abort(404);
        }
        $timelines = $this->timelineRepository->all();
        $years = $timelines->map(function($item){ return $item->year; })->unique();
        if ($years->first() != date('Y')) {
            $years->prepend(date('Y'));
        }
        if (date('m') == 12) {
            $years->prepend(date('Y')+1);
        }

        $timeline = $this->timelineRepository->getTimeline($year, $month);
        $usedCashboxes = $timeline->map(function($item){ return $item->cashbox_id; })->unique()->all();

        $yearMonth = $year.'-'.lead0($month);
        $withActives = ($yearMonth >= date('Y-m'));
        $cashboxes = $this->cashboxRepository->withBranches($usedCashboxes, $withActives)->groupBy('branch_id');
        $operators = $this->operatorRepository->all($timeline->isEmpty());

        return view('timeline.index')->with([
            'cashboxes' => $cashboxes,
            'operators' => $operators,
            'timeline' => $timeline->keyBy(function($item){ return $item->cashbox_id.'_'.date('j', strtotime($item->date)); }),
            'timelineYears' => $years->all(),
            'year' => $year,
            'month' => $month,
            'yearMonth' => $yearMonth
        ]);
    }

    public function save(EditRequest $request)
    {
        $data = $request->validated();
        $cashboxes = $this->cashboxRepository->getByBranch($data['branch_id']);
        $cashboxIds = $cashboxes->pluck('id');
        $yearMonth = $data['year'].'-'.lead0($data['month']);
        $errors = [];

        if (!checkdate($data['month'], 1, $data['year'])) {
            $errors['month'] = trans('upay.timeline.date.invalid');
        } else {
            //allow edit current month and next month
            if ($yearMonth < date('Y-m') || $yearMonth > date('Y-m', strtotime('next month'))) {
                $errors['month'] = trans('upay.timeline.date.not_allowed');
            } else {
                if (empty($data['assignees'])) {
                    $data['assignees'] = [];
                }
                $today = date('Y-m-d');
                $nextMonth = date('Y-m-t', strtotime('next month'));
                foreach ($data['assignees'] as $key => $assignee) {
                    if (!checkdate($data['month'], $assignee['day'], $data['year'])) {
                        $errors['assignees_'.$key.'_day'] = trans('upay.timeline.date.invalid');
                    }
                    //allow edit only >=today and  <=next month's last day
                    $assigneeDay = $data['year'] . '-' . lead0($data['month']) . '-' . lead0($assignee['day']);
                    if ($assigneeDay < $today || $assigneeDay > $nextMonth) {
                        $errors['assignees_'.$key.'_day'] = trans('upay.timeline.date.not_allowed');
                    } else {
                        //max 10 operators are allowed on the same cashbox/day
                        if (!empty($assignee['operators']) && count($assignee['operators']) > 10) {
                            $errors['assignees_' . $key . '_operators'] = trans('upay.timeline.operators.max_error');
                        } elseif (!empty($assignee['operators'])) {
                            //same operator can be assigned only one cashbox in the same day
                            if (!$this->timelineRepository->checkOperatorsForDay($assignee['cashbox_id'], $assigneeDay, $assignee['operators'])) {
                                $errors['assignees_' . $key . '_operators'] = trans('upay.timeline.operators.same_day_operator_error');
                            } else {
                                if (!$this->operatorRepository->checkIds(array_unique($assignee['operators']))) {
                                    $errors['assignees_' . $key . '_operators'] = trans('upay.timeline.operators.not_found');
                                }
                            }
                        }
                        if (!$this->cashboxRepository->found($assignee['cashbox_id']) && !empty($assignee['operators'])) {
                            $errors['assignees_' . $assignee['cashbox_id']] = trans('upay.timeline.cashbox_id.not_found');
                        }
                    }
                }
            }
        }
        if (!empty($errors)) {
             throw InvalidDataException::withErrors($errors);
        }
        $data['cashbox_ids'] = $cashboxIds;
        $this->timelineRepository->save($data);
        return $this->sendOK([
            'message' => trans('upay.timeline.saved.message'),
        ]);
    }

    public function export(Request $request)
    {
        ini_set('memory_limit', '-1');
        $month = $request->input('m');
        $year = $request->input('y');
        if (!is_string($year) || !is_string($month) || !checkdate($month, 1, $year)) {
            abort(404);
        }
        return Excel::download(new TimelineExport($year, $month), 'timeline_'.$month.'_'.$year.'.xlsx');
    }
}
