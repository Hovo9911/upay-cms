<?php

namespace App\Http\Controllers;

use App\Exceptions\InvalidDataException;
use App\Http\Requests\DeleteRequest;
use App\Http\Requests\Localization\EditRequest;
use App\Http\Requests\Localization\SearchRequest;
use App\UPay\Interfaces\LanguageRepositoryInterface;
use App\UPay\Interfaces\LocalizationRepositoryInterface;
use App\UPay\Localization\LocalizationSearch;
use DB;

class LocalizationController extends Controller
{
    private $localizationRepository;
    private $languageRepository;

    public function __construct(LocalizationRepositoryInterface $localizationRepository, LanguageRepositoryInterface $languageRepository)
    {
        $this->localizationRepository = $localizationRepository;
        $this->languageRepository = $languageRepository;
    }

    public function index()
    {
        $languages = $this->languageRepository->all();
        return view('localization.list')->with([
            'groups' => $this->localizationRepository->getGroups(),
            'languages' => $languages,
        ]);
    }

    public function save(EditRequest $request, $id = null)
    {
        $data = $request->validated();
        if ($this->localizationRepository->keyFound($data['key'], $data['group'], $id)) {
            throw InvalidDataException::withErrors(['key' => trans('upay.localization.key.exists')]);
        }
        if ($id != null) {
            $this->localizationRepository->update($id, $data);
        } else {
            $this->localizationRepository->create($data);
        }
        \Artisan::call('sync-localization', ['group' => $data['group']]);
        return $this->sendOK([
            'message' => trans('upay.localization.saved.message'),
        ]);
    }

    public function delete(DeleteRequest $request)
    {
        $this->localizationRepository->delete($request->validated()['ids']);
        \Artisan::call('sync-localization');
        return $this->sendOK();
    }

    public function getList(SearchRequest $request)
    {
        $searcher = new LocalizationSearch($request->validated());
        return response()->json([
            'data' => $searcher->search(),
            'recordsTotal' => $searcher->totalCount(),
            'recordsFiltered' => $searcher->filteredCount(),
        ]);
    }
}
