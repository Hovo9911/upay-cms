<?php

namespace App\UPay\Transaction;

use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithCustomValueBinder;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use PhpOffice\PhpSpreadsheet\Cell\Cell;
use PhpOffice\PhpSpreadsheet\Cell\DataType;

class TransactionExport extends TransactionSearch implements FromQuery, WithMapping, WithHeadings, WithCustomValueBinder
{
    use Exportable;

    public function query()
    {
        $query = parent::query();
        $this->setOrdering($query);
        return $query;
    }

    public function headings(): array
    {
        return [
            trans('upay.report_transactions.external_id'),
            trans('upay.report_transactions.date'),
            trans('upay.report_transactions.time'),
            trans('upay.report_transactions.cashbox'),
            trans('upay.report_transactions.service'),
            trans('upay.report_transactions.amount'),
            trans('upay.report_transactions.contact_number'),
            trans('upay.report_transactions.cash'),
            trans('upay.report_transactions.pos_auth_code'),
            trans('upay.report_transactions.code'),
            trans('upay.report_transactions.receipt_id'),
            trans('upay.report_transactions.fee'),
            trans('upay.report_transactions.is_cancelled'),
            trans('upay.report_transactions.operator'),
        ];
    }

    public function map($transaction): array
    {
        return [
            $transaction->external_id,
            format_date($transaction->created_at, false),
            date('H:i:s', strtotime($transaction->created_at)),
            $transaction->cashbox->name,
            $transaction->service->name,
            $transaction->amount,
            $transaction->contact_number,
            $transaction->cash ? trans('upay.transaction.payment_type.cash') : trans('upay.transaction.payment_type.non_cash'),
            $transaction->pos_auth_code,
            $transaction->code,
            $transaction->receipt_id,
            $transaction->commission,
            $transaction->is_cancelled ? trans('upay.report_transactions.is_cancelled.yes') : trans('upay.report_transactions.is_cancelled.no'),
            $transaction->operator->first_name . ' ' . $transaction->operator->last_name
        ];
    }

    public function bindValue(Cell $cell, $value)
    {
        $cell->setValueExplicit($value, DataType::TYPE_STRING);
        return true;
    }
}
