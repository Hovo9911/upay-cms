var $search = $.extend({}, $gsearch);
$search.defaultOrder = [];
$search.modifyColumns = {
    cashbox: {
        render: function (data, type, row) {
            return type === 'display' ? data.name : data;
        }
    },
    service: {
        render: function (data, type, row) {
            return type === 'display' ? data.name : data;
        }
    },
    repeat: {
        width: '61px',
        className: 'text-center',
        render: function (data, type, row) {
            return type === 'display' ? '<button '+(!$permissions.repeat || (row.repeat_status == 'pending') ? 'disabled="disabled"' : '')+' class="btn btn-xs btn-success" onclick="$search.repeat(' + row.id + ', this)"><i class="fa fa-refresh '+(row.repeat_status == 'pending' ? 'rotating' : '')+'"></i></button>' : data;
        }
    },
    complete: {
        width: '61px',
        className: 'text-center',
        render: function (data, type, row) {
            return type === 'display' ? '<button '+(!$permissions.complete || (row.repeat_status == 'pending') ? 'disabled="disabled"' : '')+' class="btn btn-xs btn-primary" onclick="$search.complete(' + row.id + ', this)"><i class="fa fa-check"></i></button>' : data;
        }
    },
    amount: {
        className: 'text-right',
    },
    created_at: {
        render: function (data, type, row) {
            return type === 'display' ? format_date(data) : data;
        }
    },
};

$search.repeat = function(id, context){
    $("#"+$repeatForm.formId+" input[name=id]").val(id);
    $(context).attr('disabled', 'disabled');
    $(".fa", $(context)).addClass('rotating');
    $repeatForm.form().submit();
};

$search.complete = function(id, context){
    $("#"+$completeForm.formId+" input[name=id]").val(id);
    $(context).attr('disabled', 'disabled');
    $(".fa", $(context)).removeClass('fa-check').addClass('fa-circle-o-notch').addClass('rotating');
    $completeForm.form().submit();
};

var $repeatForm = $.extend({}, $gForm);
$repeatForm.formId = "repeat-form";
$repeatForm.onSuccess = function(response) {
    window.location.reload();
};

var $completeForm = $.extend({}, $gForm);
$completeForm.formId = "complete-form";
$completeForm.onSuccess = function(response) {
    window.location.reload();
};

$(document).ready(function() {
    if ($($search.searchFormSelector).length) {
        $search.init();
        $repeatForm.init();
        $completeForm.init();
    }
});
