<?php

namespace App\UPay\Cashbox;

use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithCustomValueBinder;
use PhpOffice\PhpSpreadsheet\Cell\Cell;
use PhpOffice\PhpSpreadsheet\Cell\DataType;

class ClosingItemExport implements FromArray, WithCustomValueBinder
{
    use Exportable;

    protected $itemDetails = null;

    public function __construct($itemDetails)
    {
        $this->itemDetails = $itemDetails;
    }

    public function array(): array
    {
        $cashboxState = $this->itemDetails['cashboxState'];
        $data = [
            [ trans('upay.report_closing.initial_balance'), $cashboxState->initial_balance, '','' ],
            [ trans('upay.report_closing.cash_in'), $cashboxState->cash_in, '','' ],
            [ trans('upay.report_closing.cash_out'), $cashboxState->cash_out, '','' ],
            [ trans('upay.report_closing.non_cash'), $cashboxState->non_cash, '','' ],
            [ trans('upay.report_closing.final_balance'), $cashboxState->final_balance, '','' ],
            [ trans('upay.report_closing.closing_date'), format_date($cashboxState->closing_date), '','' ],
            [ trans('upay.report_closing.cashbox'), $cashboxState->cashbox->name, '','' ],
            [ trans('upay.report_closing.operator'), $cashboxState->operator != null ? $cashboxState->operator->first_name.' '.$cashboxState->operator->last_name : trans('upay.report_closing.closed_automatically'), '','' ],
            ['', '', '', ''],
            ['', '', '', ''],
            [
                trans('upay.report_closing.details.service'),
                trans('upay.report_closing.details.cash'),
                trans('upay.report_closing.details.non_cash'),
                trans('upay.report_closing.details.count'),
            ]
        ];
        foreach ($this->itemDetails['transactions'] as $item) {
            $data []= [
                $item->service->name,
                $item->cash,
                $item->non_cash,
                $item->count
            ];
        }
        return $data;
    }

    public function bindValue(Cell $cell, $value)
    {
        $cell->setValueExplicit($value, DataType::TYPE_STRING);
        return true;
    }
}
