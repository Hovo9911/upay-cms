<?php

return [
    'localization' => [
        'alias' => 'localization',
        'icon' => 'globe',
        'url' => '/localization/list',
        'permission' => 'localization.view'
    ],
    'role' => [
        'alias' => 'role',
        'icon' => 'users',
        'url' => '/role/list',
        'permission' => 'role.view'
    ],
    'admin' => [
        'alias' => 'admin',
        'icon' => 'user',
        'url' => '/admin/list',
        'permission' => 'admin.view'
    ],
    'operator' => [
        'alias' => 'operator',
        'icon' => 'user-circle-o',
        'url' => '/operator/list',
        'permission' => 'operator.view'
    ],
    'branch' => [
        'alias' => 'branch',
        'icon' => 'map-marker',
        'url' => '/branch/list',
        'permission' => 'branch.view'
    ],
    'city' => [
        'alias' => 'city',
        'icon' => 'building-o',
        'url' => '/city/list',
        'permission' => 'city.view'
    ],
    'cashbox' => [
        'alias' => 'cashbox',
        'icon' => 'desktop',
        'url' => '/cashbox/list',
        'permission' => 'cashbox.view'
    ],
    'provider' => [
        'alias' => 'provider',
        'icon' => 'th-list',
        'url' => '/provider/list',
        'permission' => 'provider.view'
    ],
    'service' => [
        'alias' => 'service',
        'icon' => 'lightbulb-o',
        'url' => '/service/list',
        'permission' => 'service.view'
    ],
    'timeline' => [
        'alias' => 'timeline',
        'icon' => 'calendar',
        'url' => '/timeline',
        'permission' => 'timeline.view'
    ],
    'cancellation' => [
        'alias' => 'cancellation',
        'icon' => 'times',
        'url' => '/cancellation/list',
        'permission' => 'cancellation.view'
    ],
    'report_closing' => [
        'alias' => 'report_closing',
        'icon' => 'list',
        'url' => '/report-closing/list',
        'permission' => 'report_closing.view'
    ],
    'report_transactions' => [
        'alias' => 'report_transactions',
        'icon' => 'list',
        'url' => '/report-transactions/list',
        'permission' => 'report_transactions.view'
    ],
    'report_services' => [
        'alias' => 'report_services',
        'icon' => 'list',
        'url' => '/report-services/list',
        'permission' => 'report_services.view'
    ],
    'failed_transactions' => [
        'alias' => 'failed_transactions',
        'icon' => 'times-circle',
        'url' => '/failed-transactions/list',
        'permission' => 'failed_transactions.view'
    ],
    'website' => [
        'alias' => 'website',
        'icon' => 'globe',
        'permission' => 'website.view',
        'subs' => [
            [
                'alias' => 'sliders',
                'icon' => 'sliders',
                'url' => '/website/slider/list',
                'permission' => 'slider.view',
            ],
            [
                'alias' => 'news',
                'icon' => 'newspaper-o',
                'url' => '/website/news/list',
                'permission' => 'news.view'
            ],
            [
                'alias' => 'career',
                'icon' => 'briefcase',
                'url' => '/website/career/list',
                'permission' => 'career.view'
            ],
            [
                'alias' => 'career_category',
                'icon' => 'briefcase',
                'url' => '/website/career-category/list',
                'permission' => 'career_category.view'
            ],
            [
                'alias' => 'faq',
                'icon' => 'question-circle',
                'url' => '/website/faq/list',
                'permission' => 'faq.view'
            ],
            [
                'alias' => 'faq_category',
                'icon' => 'question-circle',
                'url' => '/website/faq-category/list',
                'permission' => 'faq_category.view'
            ],
        ]
    ]
];
