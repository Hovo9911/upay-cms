<?php

use App\DB\Migration;
use App\DB\Blueprint;

class AddOrderToServiceCommissionsApproved extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->getSchemaBuilder()->table('service_commission_values_approved', function (Blueprint $table) {
            $table->increments('order');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $this->getSchemaBuilder()->table('service_commission_values_approved', function (Blueprint $table) {
            $table->dropColumn('order');
        });
    }
}
