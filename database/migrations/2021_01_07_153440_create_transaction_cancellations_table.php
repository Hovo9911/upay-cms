<?php

use App\DB\Migration;
use App\DB\Blueprint;
use App\UPay\Transaction\TransactionCancellation;

class CreateTransactionCancellationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->getSchemaBuilder()->create('transaction_cancellations', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('transaction_id');
            $table->unsignedInteger('operator_id');
            $table->enum('type', [TransactionCancellation::TYPE_CASHIER, TransactionCancellation::TYPE_CUSTOMER]);
            $table->jsonb('details');
            $table->string('notes', 1024);
            $table->enum('status', TransactionCancellation::statuses());
            $table->timestamps();
            $table->index('transaction_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaction_cancellations');
    }
}
