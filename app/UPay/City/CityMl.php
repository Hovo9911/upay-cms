<?php

namespace App\UPay\City;

use App\MultiPKModel;

class CityMl extends MultiPKModel
{
    protected $table = 'cities_ml';
    public $timestamps = false;
    protected $primaryKey = ['city_id', 'lng_id'];
    public $incrementing = false;

    protected $fillable = [
        'city_id',
        'lng_id',
        'name',
        'show_status',
    ];
}
