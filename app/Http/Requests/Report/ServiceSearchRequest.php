<?php

namespace App\Http\Requests\Report;

use App\Http\Requests\DatatableSearchRequest;

class ServiceSearchRequest extends DatatableSearchRequest
{
    public function rules()
    {
        return [
            'filters.service' => 'integer',
            'filters.date_from' => 'date',
            'filters.date_to' => 'date',
            'order' => 'array',
            'start' => 'integer',
            'format' => 'string'
        ];
    }
}
