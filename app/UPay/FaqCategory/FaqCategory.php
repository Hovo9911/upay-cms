<?php

namespace App\UPay\FaqCategory;

use App\Model;

class FaqCategory extends Model
{
    protected $table = 'faq_categories';

    protected $fillable = [
        'sort_order',
        'show_status',
    ];

    public function ml()
    {
        return $this->hasMany(FaqCategoryMl::class, 'faq_category_id', 'id');
    }

    public function scopeJoinMl($query)
    {
        $query->join('faq_categories_ml as ml', function ($join) {
            $join->on('faq_categories.id', '=', 'ml.faq_category_id')
                ->where('ml.lng_id', '=', c_lng('id'))
                ->where('ml.show_status', '!=', FaqCategoryMl::STATUS_DELETED);
        });
    }

    public static function getIds()
    {
        return self::where('show_status', Model::STATUS_ACTIVE)->pluck('id')->all();
    }
}
