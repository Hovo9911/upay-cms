<?php

use App\DB\Migration;
use App\DB\Blueprint;
use App\UPay\Provider\Provider;

class AddTypeToProvidersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->getSchemaBuilder()->table('providers', function (Blueprint $table) {
            $table->enum('type', [Provider::TYPE_PAYMENT, Provider::TYPE_CASHBOX])->default(Provider::TYPE_PAYMENT)->after('logo');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $this->getSchemaBuilder()->table('providers', function (Blueprint $table) {
            $table->dropColumn('type');
        });
    }
}
