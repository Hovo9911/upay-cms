<?php

namespace App\Http\Middleware;

use Closure;

class CheckForPermission
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $permission, $guard = null)
    {
        if (!empty($permission) && !\Auth::user()->hasPermission($permission)) {
            abort(403);
        }
        return $next($request);
    }
}
