<?php

namespace App\UPay\Cashbox;

use App\Search;

class ClosingSearch extends Search
{
    protected $orderables = [
        'id',
        'closing_date',
        'final_balance',
    ];

    protected function query()
    {
        $filters = $this->filters;
        $query = CashboxState::with(['cashbox', 'operator'])
            ->where('is_closed', true)
            ->when(!empty($filters['cashbox']), function($query) use ($filters) {
                $query->where('cashbox_id', $filters['cashbox']);
            })
            ->when(!empty($filters['operator']), function($query) use ($filters) {
                $query->where('operator_id', $filters['operator']);
            })
            ->when(!empty($filters['auto_closed']) && $filters['auto_closed'], function($query) use ($filters) {
                $query->whereNull('operator_id');
            })
            ->when(!empty($filters['date_from']), function($query) use ($filters) {
                $query->where('closing_date', '>=', $filters['date_from'].' 00:00:00');
            })
            ->when(!empty($filters['date_to']), function($query) use ($filters) {
                $query->where('closing_date', '<=', $filters['date_to'].' 23:59:59');
            });
        return $query;
    }

    public function totalCount()
    {
        return CashboxState::where('is_closed', true)->count();
    }

    public function totalDetails()
    {
        return $this->query()->select(
            \DB::raw('SUM(initial_balance) as initial_balance'),
            \DB::raw('SUM(cash_in) as cash_in'),
            \DB::raw('SUM(cash_out) as cash_out'),
            \DB::raw('SUM(final_balance) as final_balance'),
            \DB::raw('COUNT(*) as count')
        )->first();
    }
}
