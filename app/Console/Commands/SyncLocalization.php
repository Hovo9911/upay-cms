<?php

namespace App\Console\Commands;

use App\UPay\Interfaces\LocalizationRepositoryInterface;
use Illuminate\Console\Command;

class SyncLocalization extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync-localization {group=null}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sync localization DB <=> Files';

    private $localizationRepository = null;

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(LocalizationRepositoryInterface $localizationRepository)
    {
        $this->localizationRepository = $localizationRepository;
        $this->localizationRepository->sync($this->argument('group'));
    }
}
