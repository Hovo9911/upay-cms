<?php

namespace App\Http\Controllers;

use App\Http\Requests\Faq\EditRequest;
use App\Http\Requests\Faq\SearchRequest;
use App\Http\Requests\DeleteRequest;
use App\UPay\Faq\FaqSearch;
use App\UPay\FaqCategory\FaqCategoryRepository;
use App\UPay\Interfaces\FaqRepositoryInterface;
use App\UPay\Interfaces\LanguageRepositoryInterface;
use DB;
use Illuminate\Http\Request;

class FaqController extends Controller
{
    private $languageRepository;
    private $faqRepository;
    private $faqCategoryRepository;

    public function __construct(FaqRepositoryInterface $faqRepository,FaqCategoryRepository $faqCategoryRepository,LanguageRepositoryInterface $languageRepository)
    {
        $this->faqRepository = $faqRepository;
        $this->faqCategoryRepository = $faqCategoryRepository;
        $this->languageRepository = $languageRepository;
    }

    public function index()
    {
        $faqCategories = $this->faqCategoryRepository->all();

        return view('website.faq.list', [
            'faqCategories' => $faqCategories
        ]);
    }

    public function edit(Request $request, $id = null)
    {
        if ($id != null) {
            $faq = $this->faqRepository->findOrFail($id);
            $faq->load([
                'ml',
            ]);

        } else {
            $faq = $this->faqRepository->mock();
        }

        $faq->ml = $faq->ml->keyBy('lng_id');

        $faqCategories = $this->faqCategoryRepository->all();
        $languages = $this->languageRepository->all();

        return view('website.faq.edit')->with([
            'mode' => $id != null ? 'edit' : 'add',
            'faq' => $faq,
            'faqCategories' => $faqCategories,
            'languages' => $languages,
        ]);
    }

    public function save(EditRequest $request, $id = null)
    {
        $data = $request->validated();

        if ($id != null) {
            $this->faqRepository->findOrFail($id);
            $faq = $this->faqRepository->update($id, $data);
        } else {
            $faq = $this->faqRepository->create($data);
        }

        return $this->sendOK([
            'message' => trans('upay.website.faq.saved.message'),
            'edit_url' => url('/website/faq/edit/'.$faq->id),
            'list_url' => url('/website/faq/list')
        ]);
    }

    public function delete(DeleteRequest $request)
    {
        $this->faqRepository->delete($request->validated()['ids']);
        return $this->sendOK([
            'redirect_url' => url('/website/faq/list')
        ]);
    }

    public function getList(SearchRequest $request)
    {
        $searcher = new FaqSearch($request->validated());

        return response()->json([
            'data' => $searcher->search(),
            'recordsTotal' => $searcher->totalCount(),
            'recordsFiltered' => $searcher->filteredCount(),
        ]);
    }
}
