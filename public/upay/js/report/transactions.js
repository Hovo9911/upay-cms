var $search = $.extend({}, $gsearch);
$search.defaultOrder = [[1, 'desc']];
$search.hasExport = true;
$search.modifyColumns = {
    cashbox: {
        render: function (data, type, row) {
            return type === 'display' ? data.name : data;
        }
    },
    service: {
        render: function (data, type, row) {
            return type === 'display' ? data.name : data;
        }
    },
    operator: {
        render: function (data, type, row) {
            return type === 'display' ? (data == null ? trans('upay.report_closing.closed_automatically') : data.first_name+' '+data.last_name) : data;
        }
    },
    cash: {
        render: function (data, type, row) {
            return type === 'display' ? (data ? trans('upay.transaction.payment_type.cash') : trans('upay.transaction.payment_type.non_cash')) : data;
        }
    },
    is_cancelled: {
        width: '61px',
        className: 'text-center',
        render: function (data, type, row) {
            return type === 'display' ? (data ? '<i class="fa fa-times text-navy text-danger"></i>' : '') : data;
        }
    },
    amount: {
        className: 'text-right',
    },
    commission: {
        className: 'text-right',
    },
    created_at: {
        render: function (data, type, row) {
            return type === 'display' ? format_date(data, false) : data;
        }
    },
    time: {
        render: function (data, type, row) {
            return type === 'display' ? row.created_at.split(' ')[1] : row.created_at;
        }
    },
};

$(document).ready(function() {
    if ($($search.searchFormSelector).length) {
        $search.init();
    }
});
