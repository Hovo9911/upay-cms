<?php

use App\DB\Migration;
use App\DB\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCareerCategoriesMlTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->getSchemaBuilder()->create('career_categories_ml', function (Blueprint $table) {
            $table->integer('career_category_id')->unsigned();
            $table->integer('lng_id')->unsigned();
            $table->string('title');
            $table->showStatus();
            $table->primary(['career_category_id', 'lng_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('career_categories_ml');
    }
}
