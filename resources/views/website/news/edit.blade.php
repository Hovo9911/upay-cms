<?php
use App\Model;
$activeMenu = 'website-news';
$canDelete = \Auth::user()->hasPermission('news.delete');
$canEdit = \Auth::user()->hasPermission('news.edit');
$title = 'upay.news.' . ($canEdit ? $mode : 'view') . '_title';
$disabled = $canEdit ? '' : 'disabled';

$gUploader = new \App\GUploader('website_news_image');

?>
@extends('layout')
@section('page_title') @lang($title) @stop
@section('content')
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox ">
                    <div class="ibox-title">
                        <h1>@lang($title)</h1>
                    </div>
                    <div class="ibox-content">
                        <form id="save-form" action="{{url('/website/news/save/'.($news->id ?? ''))}}"
                              method="POST">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="tabs-container">
                                        <ul class="nav nav-tabs" role="tablist">
                                            <li><a class="nav-link active" data-toggle="tab"
                                                   href="#tab-general">@lang('upay.tab.general')</a></li>
                                            @foreach($languages as $lng)
                                                <li><a class="nav-link" data-toggle="tab"
                                                       href="#tab-lng-{{$lng->id}}">{{$lng->name}}</a></li>
                                            @endforeach
                                        </ul>
                                        <div class="tab-content">
                                            <div role="tabpanel" id="tab-general" class="tab-pane active">
                                                <div class="panel-body">
                                                    <div class="form-group required row"><label
                                                            class="col-lg-2 col-form-label">@lang('upay.website.news.image')
                                                            <span class="align-self-center hint-tooltip"><i
                                                                    class="fa fa-lg fa-question-circle"
                                                                    data-toggle="tooltip" data-placement="right"
                                                                    title=""
                                                                    data-original-title="@lang('upay.website.news.image_hint')"></i></span></label>
                                                        <div class="col-lg-3">
                                                            <div class="g-upload-container">
                                                                @if(empty($disabled))
                                                                    <div class="g-upload-box">
                                                                        <input id="image" type="file"
                                                                               class="d-none g-upload" name="image"
                                                                               <?php echo $gUploader->isMultiple() ? 'multiple' : ''; ?> data-conf="website_news_image"
                                                                               data-action="<?php echo url('/g-uploader/upload'); ?>"
                                                                               accept="<?php echo $gUploader->getAcceptTypes(); ?>">
                                                                        <label for="image"
                                                                               class="btn btn-success">@lang('upay.general.uploader.label')</label>
                                                                    </div>
                                                                @endif

                                                                <div class="g-uploaded-list">
                                                                    @if(!empty($news->image))
                                                                        <div
                                                                            class="g-uploaded border fb fs14 position-relative text-break text-wrap">
                                                                            <img
                                                                                src="{{$gUploader->getFileUrl($news->image)}}"/>
                                                                            {{$gUploader->getFilename($news->image)}}
                                                                            <i class="g-upload-x fa fa-2x fa-remove text-danger"></i>
                                                                            <input type="hidden" name="image"
                                                                                   value="{{$news->image}}"/>
                                                                        </div>
                                                                    @endif
                                                                </div>

                                                                <div
                                                                    class="form-text text-danger form-error-text form-error-image"></div>
                                                                <div
                                                                    class="form-text text-danger form-error-text form-error-files"></div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="form-group required row"><label
                                                            class="col-lg-2 col-form-label">@lang('upay.website.news.show_on_homepage')</label>
                                                        <div class="col-lg-10">
                                                            <input type="checkbox" name="show_on_homepage" value="1"
                                                                   @if($news->show_on_homepage) checked
                                                                   @endif {{$disabled}} data-toggle="switchbutton"
                                                                   data-onlabel="@lang('upay.website.news.show_on_homepage.yes')"
                                                                   data-offlabel="@lang('upay.website.news.show_on_homepage.no')"
                                                                   data-width="86" data-height="33">
                                                            <span
                                                                class="form-text text-danger form-error-text form-error-show_on_homepage"></span>
                                                        </div>
                                                    </div>

                                                    <div class="form-group required row">
                                                        <label
                                                            class="col-lg-2 col-form-label">@lang('upay.website.news.publish_date')</label>
                                                        <div class="col-lg-2">
                                                            <input name="publish_date" type="text"
                                                                   class="form-control datetimepicker"
                                                                   value="{{$news->publish_date}}">
                                                            <span
                                                                class="form-text text-danger form-error-text form-error-publish_date"></span>
                                                        </div>
                                                    </div>

                                                    <div class="form-group required row">
                                                        <label
                                                            class="col-lg-2 col-form-label">@lang('upay.website.news.date')</label>
                                                        <div class="col-lg-2">
                                                            <input
                                                                name="date" type="text"
                                                                class="form-control datetimepicker" value="{{$news->date}}">
                                                            <span
                                                                class="form-text text-danger form-error-text form-error-date"></span>
                                                        </div>
                                                    </div>

                                                    <div class="form-group required row"><label
                                                            class="col-lg-2 col-form-label">@lang('upay.general.sort_order')</label>
                                                        <div class="col-lg-2">
                                                            <input type="text" name="sort_order"
                                                                   value="{{$news->sort_order}}"
                                                                   {{$disabled}} class="form-control">
                                                            <span
                                                                class="form-text text-danger form-error-text form-error-sort_order"></span>
                                                        </div>
                                                    </div>

                                                    <div class="form-group row"><label for="general-status"
                                                                                       class="col-lg-2 col-form-label">@lang('upay.general.show_status')</label>
                                                        <div class="col-lg-10">
                                                            <input id="general-status" type="checkbox"
                                                                   name="show_status" value="{{Model::STATUS_ACTIVE}}"
                                                                   @if($news->show_status == Model::STATUS_ACTIVE) checked
                                                                   @endif {{$disabled}} data-toggle="switchbutton"
                                                                   data-onlabel="@lang('upay.general.show_status.active')"
                                                                   data-offlabel="@lang('upay.general.show_status.inactive')"
                                                                   data-width="86" data-height="33">
                                                            <span
                                                                class="form-text text-danger form-error-text form-error-show_status"></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            @foreach($languages as $lng)
                                                <div role="tabpanel" id="tab-lng-{{$lng->id}}" class="tab-pane">
                                                    <div class="panel-body">

                                                        <div class="form-group required row"><label
                                                                class="col-lg-2 col-form-label">@lang('upay.website.news.title')</label>
                                                            <div class="col-lg-10">
                                                                <input type="text" name="ml[{{$lng->id}}][title]"
                                                                       {{$disabled}} class="form-control"
                                                                       value="{{isset($news->ml[$lng->id]) ? $news->ml[$lng->id]->title : ''}}">
                                                                <span
                                                                    class="form-text text-danger form-error-text form-error-ml_{{$lng->id}}_title"></span>
                                                            </div>
                                                        </div>

                                                        <div class="form-group required row"><label
                                                                class="col-lg-2 col-form-label">@lang('upay.website.news.alias')</label>
                                                            <div class="col-lg-10">
                                                                <input type="text" name="ml[{{$lng->id}}][alias]"
                                                                       {{$disabled}} class="form-control"
                                                                       value="{{isset($news->ml[$lng->id]) ? $news->ml[$lng->id]->alias : ''}}">
                                                                <span
                                                                    class="form-text text-danger form-error-text form-error-ml_{{$lng->id}}_alias"></span>
                                                            </div>
                                                        </div>

                                                        <div class="form-group row"><label
                                                                class="col-lg-2 col-form-label">@lang('upay.website.news.short_description')</label>
                                                            <div class="col-lg-10">
                                                                <textarea rows="3"
                                                                          name="ml[{{$lng->id}}][short_description]"
                                                                          {{$disabled}} class="form-control">{{isset($news->ml[$lng->id]) ? $news->ml[$lng->id]->short_description : ''}}</textarea>
                                                                <span
                                                                    class="form-text text-danger form-error-text form-error-ml_{{$lng->id}}_short_description"></span>
                                                            </div>
                                                        </div>

                                                        <div class="form-group required row"><label
                                                                class="col-lg-2 col-form-label">@lang('upay.website.news.body')</label>
                                                            <div class="col-lg-10">
                                                                <textarea rows="3" name="ml[{{$lng->id}}][body]"
                                                                          {{$disabled}} class="form-control content">{{isset($news->ml[$lng->id]) ? $news->ml[$lng->id]->body : ''}}</textarea>
                                                                <span
                                                                    class="form-text text-danger form-error-text form-error-ml_{{$lng->id}}_body"></span>
                                                            </div>
                                                        </div>

                                                        <div class="form-group row"><label
                                                                class="col-lg-2 col-form-label">@lang('upay.general.show_status')</label>
                                                            <div class="col-lg-10">
                                                                <input type="checkbox"
                                                                       name="ml[{{$lng->id}}][show_status]"
                                                                       value="{{Model::STATUS_ACTIVE}}"
                                                                       @if(!isset($news->ml[$lng->id]) || $news->ml[$lng->id]->show_status == Model::STATUS_ACTIVE) checked
                                                                       @endif {{$disabled}} data-toggle="switchbutton"
                                                                       data-onlabel="@lang('upay.general.show_status.active')"
                                                                       data-offlabel="@lang('upay.general.show_status.inactive')"
                                                                       data-width="86" data-height="33">
                                                                <span
                                                                    class="form-text text-danger form-error-text form-error-ml_{{$lng->id}}_show_status"></span>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>

                            {{csrf_field()}}

                            <hr/>
                            <div class="form-group row">
                                <div class="col-lg-4 col-lg-offset-2">
                                    <a href="{{url('/website/news/list')}}"
                                       class="btn btn-white">@lang('upay.general.buttons.cancel')</a>
                                    @if($mode == 'edit' && $canDelete)
                                        <button onclick="gDelete({{$news->id}});return false;" type="button"
                                                class="btn btn-danger">@lang('upay.general.buttons.delete')</button>@endif
                                    @if($canEdit)
                                        <button class="btn btn-primary submit-btn"
                                                type="submit">@lang('upay.general.buttons.save')</button>@endif
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @if($mode == 'edit' && $canDelete)
        @include('components.delete', ['url' => url('/website/news/delete')])
    @endif
    <script src="{{script_url('upay/js/website/news.js')}}"></script>
@stop
