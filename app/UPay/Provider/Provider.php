<?php

namespace App\UPay\Provider;

use App\GUploader;
use \App\Model;

class Provider extends Model
{
    const TYPE_PAYMENT = 'payment';
    const TYPE_CASHBOX = 'cashbox';

    protected $table = 'providers';

    protected $fillable = [
        'parent_id',
        'logo',
        'type',
        'sort_order',
        'show_status',
    ];

    protected $appends = ['provider_logo'];

    public function ml(){
        return $this->hasMany(ProviderMl::class, 'provider_id', 'id');
    }

    public function children(){
        return $this->hasMany(Provider::class, 'parent_id', 'id')->where('providers.show_status', '!=', Provider::STATUS_DELETED)->sorted()->joinMl();
    }

    public function scopeJoinMl($query){
        $query->join('providers_ml as ml', function ($join){
            $join->on('providers.id', '=', 'ml.provider_id')
                ->where('ml.lng_id', '=', c_lng('id'))
                ->where('ml.show_status', '!=', ProviderMl::STATUS_DELETED);
        });
    }

    public function scopeJoinParent($query){
        $query->leftJoin('providers as parents', function ($join){
            $join->on('providers.parent_id', '=', 'parents.id')
                ->where('parents.show_status', '!=', Provider::STATUS_DELETED);
        })->leftJoin('providers_ml as parent_ml', function ($join){
            $join->on('parents.id', '=', 'parent_ml.provider_id')
                ->where('parent_ml.lng_id', '=', c_lng('id'));
        });
    }

    public function getProviderLogoAttribute()
    {
        $gUploader = new GUploader('provider_logo');
        if (!empty($this->logo)) {
            return $gUploader->getFileUrl($this->logo);
        }
        return '';
    }
}
