<?php

namespace App\Http\Requests\Localization;

use App\Http\Requests\DatatableSearchRequest;
use App\UPay\Localization\Localization;

class SearchRequest extends DatatableSearchRequest
{
    public function rules()
    {
        return [
            'filters.id' => 'integer',
            'filters.group' => 'required|in:'.Localization::GROUP_CMS.','.Localization::GROUP_OPERATOR_WWW.','.Localization::GROUP_OPERATOR_API,
            'filters.key' => 'string|max:255',
            'filters.value' => 'string|max:30000',
            'order' => 'array',
            'start' => 'integer',
        ];
    }
}
