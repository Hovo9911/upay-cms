<?php

namespace App\UPay\CareerCategory;

use App\MultiPKModel;

class CareerCategoryMl extends MultiPKModel
{
    protected $table = 'career_categories_ml';
    public $timestamps = false;
    protected $primaryKey = ['career_category_id', 'lng_id'];
    public $incrementing = false;

    protected $fillable = [
        'career_category_id',
        'lng_id',
        'title',
        'show_status',
    ];
}
