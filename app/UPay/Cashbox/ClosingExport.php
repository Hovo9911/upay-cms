<?php

namespace App\UPay\Cashbox;

use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithCustomValueBinder;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use PhpOffice\PhpSpreadsheet\Cell\Cell;
use PhpOffice\PhpSpreadsheet\Cell\DataType;

class ClosingExport extends ClosingSearch implements FromQuery, WithMapping, WithHeadings, WithCustomValueBinder
{
    use Exportable;

    public function query()
    {
        $query = parent::query();
        $this->setOrdering($query);
        return $query;
    }

    public function headings(): array
    {
        return [
            trans('upay.report_closing.closing_date'),
            trans('upay.report_closing.cashbox'),
            trans('upay.report_closing.operator'),
            trans('upay.report_closing.initial_balance'),
            trans('upay.report_closing.cash_in'),
            trans('upay.report_closing.cash_out'),
            trans('upay.report_closing.final_balance'),
        ];
    }

    public function map($item): array
    {
        return [
            format_date($item->closing_date),
            $item->cashbox->name,
            $item->operator == null ? trans('upay.report_closing.closed_automatically') : $item->operator->first_name.' '. $item->operator->last_name,
            $item->initial_balance,
            $item->cash_in,
            $item->cash_out,
            $item->final_balance,
        ];
    }

    public function bindValue(Cell $cell, $value)
    {
        $cell->setValueExplicit($value, DataType::TYPE_STRING);
        return true;
    }
}
