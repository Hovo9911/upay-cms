<?php

namespace App\UPay\Provider;

use App\Search;

class ProviderSearch extends Search
{
    protected $orderables = [
        'id',
        'name',
        'parent',
        'show_status',
    ];

    protected function query()
    {
        $filters = $this->filters;
        $query = Provider::joinMl()->select(
            'providers.id',
            'providers.logo',
            'ml.name',
            'parent_ml.name as parent',
            'providers.show_status'
        )
        ->joinParent()
        ->when(!empty($filters['id']), function($query) use ($filters) {
            $query->where('providers.id', $filters['id']);
        })
        ->when(!empty($filters['name']), function($query) use ($filters) {
            $query->where('ml.name', 'ILIKE', '%'.$filters['name'].'%');
        })
        ->when(!empty($filters['parent']), function($query) use ($filters) {
            $query->where('providers.parent_id', $filters['parent']);
        })
        ->when(!empty($filters['show_status']), function($query) use ($filters) {
            $query->where('providers.show_status', $filters['show_status']);
        }, function($query) use ($filters) {
            $query->where('providers.show_status', '!=', Provider::STATUS_DELETED);
        });
        return $query;
    }

    public function totalCount()
    {
        return Provider::notDeleted()->count();
    }
}
