<?php
namespace App\UPay\Interfaces;

use \App\UPay\CareerCategory\CareerCategory;
use Illuminate\Database\Eloquent\Collection;

interface CareerCategoryRepositoryInterface
{
    public function create($data): CareerCategory;

    public function mock(): CareerCategory;

    public function findOrFail($id, $withMl = false): CareerCategory;

    public function update($id, $data): CareerCategory;

    public function delete($ids): int;

    public function saveMl(CareerCategory $careerCategory, array $mls): void;

    public function all(): Collection;
}
