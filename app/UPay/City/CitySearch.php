<?php

namespace App\UPay\City;

use App\Search;

class CitySearch extends Search
{
    protected $orderables = [
        'id',
        'region',
        'name',
        'show_status',
    ];

    protected function query()
    {
        $filters = $this->filters;
        $query = City::joinMl()->joinRegion()->select(
            'cities.id',
            'region_ml.name as region',
            'ml.name',
            'cities.show_status'
        )
        ->when(!empty($filters['id']), function($query) use ($filters) {
            $query->where('cities.id', $filters['id']);
        })
        ->when(!empty($filters['region']), function($query) use ($filters) {
            $query->where('cities.region_id', $filters['region']);
        })
        ->when(!empty($filters['name']), function($query) use ($filters) {
            $query->where('ml.name', 'ILIKE', '%'.$filters['name'].'%');
        })
        ->when(!empty($filters['show_status']), function($query) use ($filters) {
            $query->where('cities.show_status', $filters['show_status']);
        }, function($query) use ($filters) {
            $query->where('cities.show_status', '!=', City::STATUS_DELETED);
        });
        return $query;
    }

    public function totalCount()
    {
        return City::notDeleted()->count();
    }
}
