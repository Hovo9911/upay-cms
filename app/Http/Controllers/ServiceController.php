<?php

namespace App\Http\Controllers;

use App\Exceptions\InvalidDataException;
use App\Http\Requests\Service\EditRequest;
use App\Http\Requests\Service\SearchRequest;
use App\UPay\Interfaces\ProviderRepositoryInterface;
use App\UPay\Service\Commission;
use App\UPay\Service\CommissionValue;
use App\UPay\Service\ServiceSearch;
use App\UPay\Interfaces\ServiceRepositoryInterface;
use App\UPay\Interfaces\LanguageRepositoryInterface;
use Illuminate\Http\Request;

class ServiceController extends Controller
{
    private $serviceRepository;
    private $languageRepository;
    private $providerRepository;

    public function __construct(ServiceRepositoryInterface $serviceRepository, ProviderRepositoryInterface $providerRepository, LanguageRepositoryInterface $languageRepository)
    {
        $this->languageRepository = $languageRepository;
        $this->serviceRepository = $serviceRepository;
        $this->providerRepository = $providerRepository;
    }

    public function index()
    {
        $providers = $this->providerRepository->all();
        return view('service.list')->with([
            'providers' => $providers
        ]);
    }

    public function edit(Request $request, $id = null)
    {
        if ($id != null) {
            $service = $this->serviceRepository->findOrFail($id);
            $service->load([
                'provider',
                'ml',
                'commissions' => function($query){ $query->notDeleted()->with('commissionValues'); },
                'commissionsHistory'
            ]);
            $service->commissionsHistory = $service->commissionsHistory->groupBy('type', true)->transform(function($item){return $item->keyBy('source');});
        } else {
            $service = $this->serviceRepository->mock();
        }
        $service->ml = $service->ml->keyBy('lng_id');
        $languages = $this->languageRepository->all();
        $providers = $this->providerRepository->all();

        return view('service.edit')->with([
            'mode' => $id != null ? 'edit' : 'add',
            'service' => $service,
            'languages' => $languages,
            'providers' => $providers
        ]);
    }

    public function save(EditRequest $request, $id = null)
    {
        $data = $request->validated();

        if ($data['is_new'] && !$data['show_on_homepage']) {
            throw InvalidDataException::withErrors([
                'is_new' => trans('upay.service.is_new.should_be_on_home')
            ]);
        }

        $commissionErrors = [];
        foreach (Commission::getTypes() as $type) {
            foreach (Commission::getSources() as $src) {
                if (!empty($data['commissions_'.$type.'_'.$src])) {
                    $prevKey = null;
                    foreach($data['commissions_'.$type.'_'.$src] as $key => $items) {
                        if ($items['type'] == CommissionValue::TYPE_FIXED && !is_null($items['amount']) && $items['amount'] <= 0) {
                            $commissionErrors['commissions_'.$type.'_'.$src.'_'.$key.'_amount'] = trans('upay.service.commissions.amount.invalid');
                        } elseif ($items['type'] == CommissionValue::TYPE_PERCENT && !is_null($items['percent']) && $items['percent'] <= 0) {
                            $commissionErrors['commissions_'.$type.'_'.$src.'_'.$key.'_percent'] = trans('upay.service.commissions.percent.invalid');
                        }
                        if (!is_null($items['min']) && !is_null($items['max']) && $items['min'] > $items['max']) {
                            $commissionErrors['commissions_'.$type.'_'.$src.'_'.$key.'_min'] = trans('upay.service.commissions.max.lt_min');
                        }
                        if (!is_null($prevKey)) {
                            if ($data['commissions_'.$type.'_'.$src][$key]['min'] <= $data['commissions_'.$type.'_'.$src][$prevKey]['max']) {
                                $commissionErrors['commissions_'.$type.'_'.$src.'_'.$key.'_min'] = trans('upay.service.commissions.min.lt_prev_max');
                            }
                        }
                        $prevKey = $key;
                    }
                }
            }
        }
        if (!empty($commissionErrors)) {
            throw InvalidDataException::withErrors($commissionErrors);
        }
        if (!empty($data['provider_id']) && !$this->providerRepository->found($data['provider_id'])) {
            throw InvalidDataException::withErrors(['provider_id' => trans('upay.service.provider_id.not_found')]);
        }
        if (empty($data['is_encashment']) || !$data['is_encashment']) {
            $data['encashment_type'] = null;
        }
        if ($id != null) {
            $service = $this->serviceRepository->update($id, $data);
        } else {
            $service = $this->serviceRepository->create($data);
        }

        return $this->sendOK([
            'message' => trans('upay.service.saved.message'),
            'edit_url' => url('/service/edit/'.$service->id),
            'list_url' => url('/service/list')
        ]);
    }

    public function getList(SearchRequest $request)
    {
        $searcher = new ServiceSearch($request->validated());
        $result = $searcher->search();
        return response()->json([
            'data' => $result,
            'recordsTotal' => $searcher->totalCount(),
            'recordsFiltered' => $searcher->filteredCount(),
        ]);
    }

    public function approveCommissions(Request $request, $id)
    {
        $this->serviceRepository->approveCommissions($id);
        return $this->sendOK([
            'message' => trans('upay.service.commissions.approve.success_message'),
        ]);
    }
}
