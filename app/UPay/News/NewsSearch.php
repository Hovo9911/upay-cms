<?php

namespace App\UPay\News;

use App\Search;

class NewsSearch extends Search
{
    protected $orderables = [
        'id',
        'title',
        'sort_order',
        'show_status',
    ];

    protected function query()
    {
        $filters = $this->filters;

        return  News::joinMl()->select(
            'news.id',
            'ml.title',
            'news.show_status'
        )
            ->when(!empty($filters['id']), function($query) use ($filters) {
                $query->where('news.id', $filters['id']);
            })
            ->when(!empty($filters['title']), function($query) use ($filters) {
                $query->where('ml.title', 'ILIKE', '%'.$filters['title'].'%');
            })
            ->when(!empty($filters['show_status']), function($query) use ($filters) {

                $query->where('news.show_status', $filters['show_status']);
            }, function($query) use ($filters) {
                $query->where('news.show_status', '!=', News::STATUS_DELETED);
            });


    }

    public function totalCount()
    {
        return News::notDeleted()->count();
    }
}
