<?php
use App\DB\Migration;
use App\DB\Blueprint;
use App\UPay\Service\Service;

class AddIsEncashmentToServices extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->getSchemaBuilder()->table('services', function (Blueprint $table) {
            $table->boolean('is_encashment')->default(false)->after('use_fixed_amount');
            $table->enum('encashment_type', [Service::ENCASHMENT_IN, Service::ENCASHMENT_OUT])->nullable()->default(null)->after('is_encashment');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $this->getSchemaBuilder()->table('services', function (Blueprint $table) {
            $table->dropColumn('is_encashment');
            $table->dropColumn('encashment_type');
        });
    }
}
