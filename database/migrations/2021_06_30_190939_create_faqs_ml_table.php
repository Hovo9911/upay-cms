<?php

use App\DB\Migration;
use App\DB\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFaqsMlTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->getSchemaBuilder()->create('faqs_ml', function (Blueprint $table) {
            $table->integer('faq_id')->unsigned();
            $table->integer('lng_id')->unsigned();
            $table->string('question');
            $table->text('answer');
            $table->showStatus();
            $table->primary(['faq_id', 'lng_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('faqs_ml');
    }
}
