<?php

namespace App\UPay\Role;

use App\UPay\Interfaces\RoleRepositoryInterface;
use App\UPay\Admin\AdminRole;
use Illuminate\Support\Collection;

class RoleRepository implements RoleRepositoryInterface
{
    public function create($data): Role
    {
        $role = new Role($data);
        $role->save();
        return $role;
    }

    public function mock(): Role
    {
        return new Role([
            'name' => '',
            'show_status' => Role::STATUS_ACTIVE,
            'permissions' => []
        ]);
    }

    public function findOrFail($id): Role
    {
        $role = Role::where('id', $id)->notDeleted()->first();
        if (empty($role)) {
            abort(404);
        }
        return $role;
    }

    public function update($id, $data): Role
    {
        $role = $this->findOrFail($id);
        $role->update($data);
        return $role;
    }

    public function delete($ids): int
    {
        AdminRole::whereIn('role_id', $ids)->update(['show_status' => AdminRole::STATUS_DELETED]);
        return Role::whereIn('id', $ids)->update(['show_status' => Role::STATUS_DELETED]);
    }

    public function all(): Collection
    {
        return Role::notDeleted()->orderBy('name', 'asc')->get();
    }

    public function foundAll($ids): bool
    {
        return count($ids) == Role::notDeleted()->whereIn('id', $ids)->count();
    }
}
