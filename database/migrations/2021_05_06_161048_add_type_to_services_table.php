<?php

use App\DB\Migration;
use App\DB\Blueprint;
use \App\UPay\Service\Service;

class AddTypeToServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->getSchemaBuilder()->table('services', function (Blueprint $table) {
            $table->enum('type', [
                Service::TYPE_PAY,
                Service::TYPE_TRANSFER,
            ])->default(Service::TYPE_PAY);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $this->getSchemaBuilder()->table('services', function (Blueprint $table) {
            $table->dropColumn('type');
        });
    }
}
