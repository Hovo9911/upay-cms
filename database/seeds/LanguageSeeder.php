<?php

use Illuminate\Database\Seeder;

class LanguageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('languages')->insert([
            ['id' => 1, 'code' => 'hy', 'name' => 'Հայերեն', 'show_status' => '1'],
            ['id' => 2, 'code' => 'ru', 'name' => 'Русский', 'show_status' => '1'],
            ['id' => 3, 'code' => 'en', 'name' => 'English', 'show_status' => '1'],
        ]);
    }
}
