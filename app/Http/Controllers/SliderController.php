<?php

namespace App\Http\Controllers;

use App\Exceptions\InvalidDataException;
use App\Http\Requests\DeleteRequest;
use App\Http\Requests\Slider\EditRequest;
use App\Http\Requests\Slider\SearchRequest;
use App\UPay\Interfaces\LanguageRepositoryInterface;
use App\UPay\Interfaces\SliderRepositoryInterface;
use App\UPay\Slider\SliderSearch;
use DB;
use Illuminate\Http\Request;

class SliderController extends Controller
{
    private $languageRepository;
    private $sliderRepository;

    public function __construct(SliderRepositoryInterface $sliderRepository,LanguageRepositoryInterface $languageRepository)
    {
        $this->sliderRepository = $sliderRepository;
        $this->languageRepository = $languageRepository;
    }

    public function index()
    {
        return view('website.slider.list');
    }

    public function edit(Request $request, $id = null)
    {
        if ($id != null) {
            $slider = $this->sliderRepository->findOrFail($id);
            $slider->load([
                'ml',
            ]);

        } else {
            $slider = $this->sliderRepository->mock();
        }
        $slider->ml = $slider->ml->keyBy('lng_id');
        $languages = $this->languageRepository->all();

        return view('website.slider.edit')->with([
            'mode' => $id != null ? 'edit' : 'add',
            'slider' => $slider,
            'languages' => $languages,
        ]);
    }

    public function save(EditRequest $request, $id = null)
    {
        $data = $request->validated();

        if ($id != null) {
            $this->sliderRepository->findOrFail($id);
            $slider = $this->sliderRepository->update($id, $data);
        } else {
            $slider = $this->sliderRepository->create($data);
        }

        return $this->sendOK([
            'message' => trans('upay.website.slider.saved.message'),
            'edit_url' => url('/website/slider/edit/'.$slider->id),
            'list_url' => url('/website/slider/list')
        ]);
    }

    public function delete(DeleteRequest $request)
    {
        $this->sliderRepository->delete($request->validated()['ids']);
        return $this->sendOK([
            'redirect_url' => url('/website/slider/list')
        ]);
    }

    public function getList(SearchRequest $request)
    {
        $searcher = new SliderSearch($request->validated());
        return response()->json([
            'data' => $searcher->search(),
            'recordsTotal' => $searcher->totalCount(),
            'recordsFiltered' => $searcher->filteredCount(),
        ]);
    }
}
