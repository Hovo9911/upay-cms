<?php
    use App\Model;
    use App\UPay\Admin\Admin;
    $activeMenu = 'admin';
    $canEdit = \Auth::user()->hasPermission('admin.edit');
    $canDelete = \Auth::user()->hasPermission('admin.delete');
    $canResetPassword = \Auth::user()->hasPermission('admin.reset_password');
    $title = 'upay.admin.'.($canEdit ? $mode : 'view').'_title';
    $disabled = $canEdit ? '' : 'disabled';
    $jsTrans->add([
        'upay.admin.status.password_reset_pending',
        'upay.admin.reset_password.confirm_modal_title',
        'upay.admin.reset_password.confirm_modal_text'
    ]);
?>
@extends('layout')
@section('page_title') @lang($title) @stop
@section('content')
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox ">
                    <div class="ibox-title">
                        <h1>@lang($title)</h1>
                    </div>
                    <div class="ibox-content">
                        <form id="save-form" action="{{url('/admin/save/'.($admin->id ?? ''))}}" method="POST">

                            @if($mode == 'edit' && $admin->show_status == Model::STATUS_ACTIVE && $admin->password_reset_status == Admin::PASSWORD_RESET_STATUS_PENDING)
                                <div class="form-group row">
                                    <div class="text-warning p-xs b-r-sm"><i class="fa fa-clock-o"></i> @lang('upay.admin.status.password_reset_pending.info')</div>
                                </div>
                            @endif

                            <div class="form-group required row"><label class="col-lg-2 col-form-label">@lang('upay.admin.email')</label>
                                <div class="col-lg-10">
                                    <input type="text" name="email" value="{{$admin->email}}" {{$disabled}} class="form-control">
                                    <span class="form-text text-danger form-error-text form-error-email"></span>
                                </div>
                            </div>
                            <div class="form-group required row"><label class="col-lg-2 col-form-label">@lang('upay.admin.name')</label>
                                <div class="col-lg-10">
                                    <input type="text" name="name" value="{{$admin->name}}" {{$disabled}} class="form-control">
                                    <span class="form-text text-danger form-error-text form-error-name"></span>
                                </div>
                            </div>
                            <div class="form-group required row"><label class="col-lg-2 col-form-label">@lang('upay.admin.roles')</label>
                                <div class="col-lg-10">
                                    <select name="roles[]" class="form-control m-b select-picker" {{$disabled}} multiple>
                                        @foreach($roles as $role)
                                            <option value="{{$role->id}}" @if($admin->roles && $admin->roles->contains('role_id', $role->id)) selected="selected" @endif>{{$role->name}}</option>
                                        @endforeach
                                    </select>
                                    <span class="form-text text-danger form-error-text form-error-roles"></span>
                                </div>
                            </div>
                            <div class="form-group row"><label class="col-lg-2 col-form-label">@lang('upay.general.show_status')</label>
                                <div class="col-lg-10">
                                    <input type="checkbox" name="show_status" value="{{Model::STATUS_ACTIVE}}" @if($admin->show_status == Model::STATUS_ACTIVE) checked @endif {{$disabled}} data-toggle="switchbutton" data-onlabel="@lang('upay.general.show_status.active')" data-offlabel="@lang('upay.general.show_status.inactive')">
                                    <span class="form-text text-danger form-error-text form-error-show_status"></span>
                                </div>
                            </div>
                            @if($mode == 'edit' && $admin->show_status == Model::STATUS_ACTIVE && $canResetPassword)
                                <div class="form-group row"><label class="col-lg-2 col-form-label">@lang('upay.admin.reset_password')</label>
                                    <div class="col-lg-10">
                                        <button class="btn btn-warning reset-password-btn" type="button"><i class="fa fa-warning"></i> <span class="bold">@lang('upay.admin.reset_password.btn')</span></button>
                                    </div>
                                </div>
                            @endif

                            {{csrf_field()}}

                            <hr/>
                            <div class="form-group row">
                                <div class="col-lg-4 col-lg-offset-2">
                                    <a href="{{url('/admin/list')}}" class="btn btn-white">@lang('upay.general.buttons.cancel')</a>
                                    @if($mode == 'edit' && $canDelete)<button onclick="gDelete({{$admin->id}});return false;" type="button" class="btn btn-danger">@lang('upay.general.buttons.delete')</button>@endif
                                    @if($canEdit)<button class="btn btn-primary submit-btn" type="submit">@lang('upay.general.buttons.save')</button>@endif
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @if($mode == 'edit')
        @if($canDelete) @include('components.delete', ['url' => url('/admin/delete')]) @endif
        @if($admin->show_status == Model::STATUS_ACTIVE && $canResetPassword)
            <form id="reset-password-form" action="{{url('/admin/reset-password/'.$admin->id)}}" method="POST">
                {{csrf_field()}}
            </form>
        @endif
    @endif

    <script src="{{script_url('upay/js/admin.js')}}"></script>
@stop
