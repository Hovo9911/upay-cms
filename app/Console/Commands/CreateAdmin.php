<?php

namespace App\Console\Commands;

use App\UPay\Admin\Admin;
use Illuminate\Console\Command;

class CreateAdmin extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'admin:create';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new admin';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $now = date('Y-m-d H:i:s');
        $name = $this->ask('Name?');
        $email = $this->ask('Email?');
        $pwd = $this->secret('Password?');
        \DB::table('admins')->insert([
            'name' => $name,
            'email' => $email,
            'password' => bcrypt($pwd),
            'show_status' => Admin::STATUS_ACTIVE,
            'password_reset_status' => null,
            'created_at' => $now,
            'updated_at' => $now,
            'is_root' => true
        ]);
        $this->info('Account created for '.$name);
    }
}
