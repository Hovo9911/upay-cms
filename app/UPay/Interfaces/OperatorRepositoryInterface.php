<?php
namespace App\UPay\Interfaces;

use \App\UPay\Operator\Operator;
use Illuminate\Database\Eloquent\Collection;

interface OperatorRepositoryInterface
{
    public function create($data): Operator;

    public function mock(): Operator;

    public function findOrFail($id): Operator;

    public function update(Operator $user, $data): Operator;

    public function emailFound($email, $id): bool;

    public function all(bool $onlyActives = true): Collection;

    public function checkIds($ids): bool;
}
