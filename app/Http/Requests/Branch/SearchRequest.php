<?php

namespace App\Http\Requests\Branch;

use App\Http\Requests\DatatableSearchRequest;
use App\Model;

class SearchRequest extends DatatableSearchRequest
{
    public function rules()
    {
        return [
            'filters.id' => 'integer',
            'filters.city' => 'integer',
            'filters.code' => 'string|max:255',
            'filters.armsoft_code' => 'string|max:255',
            'filters.name' => 'string|max:255',
            'filters.address' => 'string|max:255',
            'filters.show_status' => 'in:'.Model::STATUS_ACTIVE.','.Model::STATUS_INACTIVE,
            'order' => 'array',
            'start' => 'integer',
        ];
    }
}
