<?php
namespace App\UPay\Interfaces;

use \App\UPay\Provider\Provider;
use Illuminate\Database\Eloquent\Collection;

interface ProviderRepositoryInterface
{
    public function create($data): Provider;

    public function mock(): Provider;

    public function findOrFail($id): Provider;

    public function update($id, $data): Provider;

    public function saveMl(Provider $provider, array $mls): void;

    public function parents(int $except = null): Collection;

    public function all(): Collection;

    public function found(int $id): bool;

    public function hasChild(int $id): bool;

    public function saveLogo($newLogo, $oldLogo = null): void;
}
