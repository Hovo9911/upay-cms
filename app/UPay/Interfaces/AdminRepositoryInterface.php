<?php
namespace App\UPay\Interfaces;

use \App\UPay\Admin\Admin;

interface AdminRepositoryInterface
{
    public function create($data): Admin;

    public function mock(): Admin;

    public function findOrFail($id): Admin;

    public function update(Admin $admin, $data): Admin;

    public function delete($ids): int;

    public function saveRoles(Admin $admin, array $roles): void;

    public function emailFound($email, $id): bool;

    public function findByEmail($email);

    public function findByHash($hash) : Admin;
}
