<?php

namespace App\UPay\Career;

use App\Model;

class Career extends Model
{
    protected $table = 'career';

    protected $fillable = [
        'image',
        'show_on_homepage',
        'career_category_id',
        'sort_order',
        'show_status',
    ];

    public function ml()
    {
        return $this->hasMany(CareerMl::class, 'career_id', 'id');
    }

    public function scopeJoinMl($query)
    {
        $query->join('career_ml as ml', function ($join) {
            $join->on('career.id', '=', 'ml.career_id')
                ->where('ml.lng_id', '=', c_lng('id'))
                ->where('ml.show_status', '!=', CareerMl::STATUS_DELETED);
        });
    }
}
