<?php
    use App\Model;
    $activeMenu = 'branch';
    $canDelete = \Auth::user()->hasPermission('branch.delete');
    $canEdit = \Auth::user()->hasPermission('branch.edit');
    $title = 'upay.branch.'.($canEdit ? $mode : 'view').'_title';
    $disabled = $canEdit ? '' : 'disabled';
?>
@extends('layout')
@section('page_title') @lang($title) @stop
@section('content')
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox ">
                    <div class="ibox-title">
                        <h1>@lang($title)</h1>
                    </div>
                    <div class="ibox-content">
                        <form id="save-form" action="{{url('/branch/save/'.($branch->id ?? ''))}}" data-disabled="{{!$canEdit}}" method="POST">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="tabs-container">
                                        <ul class="nav nav-tabs" role="tablist">
                                            <li><a class="nav-link active" data-toggle="tab" href="#tab-general">@lang('upay.tab.general')</a></li>
                                            @foreach($languages as $lng)
                                                <li><a class="nav-link" data-toggle="tab" href="#tab-lng-{{$lng->id}}">{{$lng->name}}</a></li>
                                            @endforeach
                                        </ul>
                                        <div class="tab-content">
                                            <div role="tabpanel" id="tab-general" class="tab-pane active">
                                                <div class="panel-body">
                                                    <div class="form-group required row"><label class="col-lg-2 col-form-label">@lang('upay.branch.code')</label>
                                                        <div class="col-lg-2">
                                                            <input type="text" name="code" value="{{$branch->code}}" {{$disabled}} class="form-control">
                                                            <span class="form-text text-danger form-error-text form-error-code"></span>
                                                        </div>
                                                    </div>
                                                    <div class="form-group required row"><label class="col-lg-2 col-form-label">@lang('upay.branch.armsoft_code')</label>
                                                        <div class="col-lg-2">
                                                            <input type="text" name="armsoft_code" value="{{$branch->armsoft_code}}" {{$disabled}} class="form-control">
                                                            <span class="form-text text-danger form-error-text form-error-armsoft_code"></span>
                                                        </div>
                                                    </div>
                                                    <div class="form-group required row"><label class="col-lg-2 col-form-label">@lang('upay.branch.city')</label>
                                                        <div class="col-lg-10">
                                                            <select name="city_id" {{$disabled}} class="form-control m-b select-picker">
                                                                <option value="">@lang('upay.general.dropdown.select')</option>
                                                                @foreach($cities as $city)
                                                                    <option value="{{$city->id}}" @if($branch->city_id == $city->id) selected="selected" @endif>{{$city->region.' > '.$city->name}}</option>
                                                                @endforeach
                                                            </select>
                                                            <span class="form-text text-danger form-error-text form-error-city_id"></span>
                                                        </div>
                                                    </div>

                                                    <div class="form-group required row"><label class="col-lg-2 col-form-label">@lang('upay.branch.coordinates')</label>
                                                        <div class="col-lg-10">
                                                            <input id="pac-input" class="controls form-control col-lg-5 ml-2 mt-2" type="text" placeholder="@lang('upay.branch.coordinates.placeholder')" {{$disabled}}>
                                                            <div id="map" style="width: 100%; height: 400px;"></div>
                                                            <input type="hidden" name="lat" id="address-lat" value="{{$branch->lat}}" {{$disabled}} >
                                                            <input type="hidden" name="lng" id="address-lng" value="{{$branch->lng}}" {{$disabled}} >
                                                            <span class="form-text text-danger form-error-text form-error-lat"></span>
                                                            <span class="form-text text-danger form-error-text form-error-lng"></span>
                                                        </div>
                                                    </div>

                                                    <div class="form-group row"><label class="col-lg-2 col-form-label">@lang('upay.general.sort_order')</label>
                                                        <div class="col-lg-2">
                                                            <input type="text" name="sort_order" value="{{$branch->sort_order}}" {{$disabled}} class="form-control">
                                                            <span class="form-text text-danger form-error-text form-error-sort_order"></span>
                                                        </div>
                                                    </div>

                                                    <div class="form-group row"><label class="col-lg-2 col-form-label">@lang('upay.general.show_status')</label>
                                                        <div class="col-lg-10">
                                                            <input type="checkbox" name="show_status" value="{{Model::STATUS_ACTIVE}}" {{$disabled}} @if($branch->show_status == Model::STATUS_ACTIVE) checked @endif data-toggle="switchbutton" data-onlabel="@lang('upay.general.show_status.active')" data-offlabel="@lang('upay.general.show_status.inactive')" data-width="86" data-height="33">
                                                            <span class="form-text text-danger form-error-text form-error-show_status"></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            @foreach($languages as $lng)
                                                <div role="tabpanel" id="tab-lng-{{$lng->id}}" class="tab-pane">
                                                    <div class="panel-body">
                                                        <div class="form-group required row"><label class="col-lg-2 col-form-label">@lang('upay.branch.name')</label>
                                                            <div class="col-lg-10">
                                                                <input type="text" name="ml[{{$lng->id}}][name]" @isset($branch->ml[$lng->id]) value="{{$branch->ml[$lng->id]->name}}" @endisset {{$disabled}} class="form-control">
                                                                <span class="form-text text-danger form-error-text form-error-ml_{{$lng->id}}_name"></span>
                                                            </div>
                                                        </div>
                                                        <div class="form-group required row"><label class="col-lg-2 col-form-label">@lang('upay.branch.address')</label>
                                                            <div class="col-lg-10">
                                                                <input type="text" name="ml[{{$lng->id}}][address]" @isset($branch->ml[$lng->id]) value="{{$branch->ml[$lng->id]->address}}" @endisset {{$disabled}} class="form-control address">
                                                                <span class="form-text text-danger form-error-text form-error-ml_{{$lng->id}}_address"></span>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row"><label class="col-lg-2 col-form-label">@lang('upay.general.show_status')</label>
                                                            <div class="col-lg-10">
                                                                <input type="checkbox" name="ml[{{$lng->id}}][show_status]" value="{{Model::STATUS_ACTIVE}}" {{$disabled}} @if(!isset($branch->ml[$lng->id]) || $branch->ml[$lng->id]->show_status == Model::STATUS_ACTIVE) checked @endif data-toggle="switchbutton" data-onlabel="@lang('upay.general.show_status.active')" data-offlabel="@lang('upay.general.show_status.inactive')" data-width="86" data-height="33">
                                                                <span class="form-text text-danger form-error-text form-error-ml_{{$lng->id}}_show_status"></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>

                            {{csrf_field()}}

                            <hr/>
                            <div class="form-group row">
                                <div class="col-lg-4 col-lg-offset-2">
                                    <a href="{{url('/branch/list')}}" class="btn btn-white">@lang('upay.general.buttons.cancel')</a>
                                    @if($mode == 'edit' && $canDelete)<button onclick="gDelete({{$branch->id}});return false;" type="button" class="btn btn-danger">@lang('upay.general.buttons.delete')</button>@endif
                                    @if($canEdit)<button class="btn btn-primary submit-btn" type="submit">@lang('upay.general.buttons.save')</button>@endif
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @if($mode == 'edit' && $canDelete)
        @include('components.delete', ['url' => url('/branch/delete')])
    @endif
    <script src="{{script_url('upay/js/branch.js')}}"></script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key={{env('GOOGLE_MAP_API_KEY', '')}}&libraries=places&callback=$branchMap.init"></script>
@stop
