<?php

namespace App\UPay\Language;

use App\UPay\Interfaces\LanguageRepositoryInterface;
use Illuminate\Support\Collection;

class LanguageRepository implements LanguageRepositoryInterface
{
    private $languages = null;
    private $current = null;

    public function all(): Collection
    {
        if (is_null($this->languages)) {
            $this->languages = Language::notDeleted()->get();
        }
        return $this->languages;
    }

    public function current(): Language
    {
        if (!is_null($this->current)) {
            return $this->current;
        }
        $locale = app()->getLocale();
        if (is_null($this->languages)) {
            $this->current = Language::notDeleted()->where('code', $locale)->firstOrFail();
        } else {
            $this->current = $this->languages->where('code', $locale)->first();
        }
        return $this->current;
    }
}
