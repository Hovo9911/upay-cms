<?php

namespace App\Http\Controllers;

use App\Exceptions\InvalidDataException;
use App\Http\Requests\City\EditRequest;
use App\Http\Requests\City\SearchRequest;
use App\Http\Requests\DeleteRequest;
use App\UPay\City\CitySearch;
use App\UPay\Interfaces\CityRepositoryInterface;
use App\UPay\Interfaces\LanguageRepositoryInterface;
use App\UPay\Interfaces\RegionRepositoryInterface;
use Illuminate\Http\Request;

class CityController extends Controller
{
    private $cityRepository;
    private $languageRepository;
    private $regionRepository;

    public function __construct(
        CityRepositoryInterface $cityRepository,
        LanguageRepositoryInterface $languageRepository,
        RegionRepositoryInterface $regionRepository
    )
    {
        $this->cityRepository = $cityRepository;
        $this->languageRepository = $languageRepository;
        $this->regionRepository = $regionRepository;
    }

    public function index()
    {
        $regions = $this->regionRepository->all();
        return view('city.list', [
            'regions' => $regions
        ]);
    }

    public function edit(Request $request, $id = null)
    {
        if ($id != null) {
            $city = $this->cityRepository->findOrFail($id);
            $city->load('ml');
        } else {
            $city = $this->cityRepository->mock();
        }
        $city->ml = $city->ml->keyBy('lng_id');
        $languages = $this->languageRepository->all();
        $regions = $this->regionRepository->all();

        return view('city.edit')->with([
            'mode' => $id != null ? 'edit' : 'add',
            'city' => $city,
            'languages' => $languages,
            'regions' => $regions
        ]);
    }

    public function save(EditRequest $request, $id = null)
    {
        $data = $request->validated();
        if (!empty($data['region_id']) && !$this->regionRepository->found($data['region_id'])) {
            throw InvalidDataException::withErrors(['region_id' => trans('upay.city.region_id.not_found')]);
        }
        if ($id != null) {
            $city = $this->cityRepository->update($id, $data);
        } else {
            $city = $this->cityRepository->create($data);
        }
        return $this->sendOK([
            'message' => trans('upay.city.saved.message'),
            'edit_url' => url('/city/edit/'.$city->id),
            'list_url' => url('/city/list')
        ]);
    }

    public function delete(DeleteRequest $request)
    {
        $this->cityRepository->delete($request->validated()['ids']);
        return $this->sendOK([
            'redirect_url' => url('/city/list')
        ]);
    }

    public function getList(SearchRequest $request)
    {
        $searcher = new CitySearch($request->validated());
        return response()->json([
            'data' => $searcher->search(),
            'recordsTotal' => $searcher->totalCount(),
            'recordsFiltered' => $searcher->filteredCount(),
        ]);
    }
}
