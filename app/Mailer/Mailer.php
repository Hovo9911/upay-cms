<?php

namespace App\Mailer;

class Mailer
{
    protected $template = 'default';

    protected $tryAgainStatuses = [
        451
    ];

    public function setTemplate($template)
    {
        $this->template = $template;
    }

    public function send($data)
    {
        $email = $this->saveEmail($data);
        $this->sendEmail($email);
        return $email;
    }

    public function saveEmail($data)
    {
        $emailData = [
            'priority' => isset($data['priority']) ? $data['priority'] : 0,
            'to' => $data['to'],
            'to_name' => isset($data['to_name']) ? $data['to_name'] : $data['to'],
            'from' => env('MAIL_FROM_ADDRESS', ''),
            'from_name' => isset($data['from_name']) ? $data['from_name'] : env('MAIL_FROM_ADDRESS', ''),
            'reply_to' => isset($data['reply_to']) ? $data['reply_to'] : '',
            'reply_to_name' => isset($data['reply_to_name']) ? $data['reply_to_name'] : '',
            'cc' => isset($data['cc']) ? implode(',', $data['cc']) : '',
            'bcc' => isset($data['bcc']) ? implode(',', $data['bcc']) : '',
            'subject' => isset($data['subject']) ? $data['subject'] : '',
            'body' => isset($data['body']) ? view('mailer.' . $this->template)->with([
                'body' => $data['body'],
                'to' => $data['to'],
                'toName' => isset($data['to_name']) ? $data['to_name'] : $data['to'],
            ])->render() : '',
            'attachments' => isset($data['attachments']) ? implode(',', $data['attachments']) : '',
            'add_date' => date('Y-m-d H:i:s'),
            'sent_date' => null,
            'status' => Email::STATUS_PENDING,
            'log' => ''
        ];

        $email = new Email($emailData);
        $email->save();

        return $email;
    }

    protected function sendEmail($email)
    {
        try {
            $transport = new \Swift_SmtpTransport(env('MAIL_HOST', '127.0.0.1'), env('MAIL_PORT', 25));
            $transport->setUsername(env('MAIL_USERNAME', ''));
            $transport->setPassword(env('MAIL_PASSWORD', ''));
            $transport->setEncryption(env('MAIL_ENCRYPTION', ''));
            if (php_sapi_name() == 'cli') {
                $transport->setLocalDomain(env('MAIL_LOCAL_DOMAIN', '127.0.0.1'));
            }

            $mailer = new \Swift_Mailer($transport);

            $message = new \Swift_Message($email->subject);
            $message->setFrom([$email->from => $email->from_name]);
            $message->setTo([$email->to => $email->to_name]);
            $message->setBody(strip_tags($email->body));
            $message->addPart($email->body, 'text/html');

            if (!empty($email->cc)) {
                $message->setCc(explode(',', $email->cc));
            }
            if (!empty($email->bcc)) {
                $message->setBcc(explode(',', $email->bcc));
            }
            if (!empty($email->reply_to)) {
                $name = (!empty($email->reply_to_name)) ? $email->reply_to_name : null;
                $message->setReplyTo($email->reply_to, $name);
            }
            if (!empty($email->attachments)) {
                $attachments = explode(',', $email->attachments);
                foreach ($attachments as $attachment) {
                    $message->attach(\Swift_Attachment::fromPath($attachment));
                }
            }

            $mailLogger = new \Swift_Plugins_Loggers_ArrayLogger();
            $mailer->registerPlugin(new \Swift_Plugins_LoggerPlugin($mailLogger));

            $result = $mailer->send($message);
            if ($result > 0) {
                $email->status = Email::STATUS_SENT;
            } else {
                $email->status = Email::STATUS_FAILED;
            }
        } catch (\Exception $e) {
            if (isset($mailLogger)) {
                $email->log = $mailLogger->dump().' Exception message: '.$e->getMessage();
            } else {
                $email->log = $e->getMessage();
            }

            if (in_array($e->getCode(), $this->tryAgainStatuses)) {
                $email->status = Email::STATUS_TRY_LATER;
            } else {
                $email->status = Email::STATUS_FAILED;
            }
        }

        $email->sent_date = date('Y-m-d H:i:s');
        $email->save();
    }

    public function sendEmails()
    {
        $emails = Email::where('status', Email::STATUS_PENDING)->orWhere('status', Email::STATUS_TRY_LATER)->orderBy('priority', 'desc')->get();
        foreach ($emails as $email) {
            $this->sendEmail($email);
        }
    }
}
