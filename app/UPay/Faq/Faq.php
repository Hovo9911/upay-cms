<?php

namespace App\UPay\Faq;

use App\Model;

class Faq extends Model
{
    protected $table = 'faqs';

    protected $fillable = [
        'category_id',
        'sort_order',
        'show_status',
    ];

    public function ml()
    {
        return $this->hasMany(FaqMl::class, 'faq_id', 'id');
    }

    public function scopeJoinMl($query)
    {
        $query->join('faqs_ml as ml', function ($join) {
            $join->on('faqs.id', '=', 'ml.faq_id')
                ->where('ml.lng_id', '=', c_lng('id'))
                ->where('ml.show_status', '!=', FaqMl::STATUS_DELETED);
        });
    }
}
