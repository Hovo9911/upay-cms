<?php

namespace App\UPay\Transaction;

use \App\Model;
use App\UPay\Branch\Branch;
use App\UPay\Cashbox\Cashbox;
use App\UPay\Operator\Operator;
use App\UPay\Service\Service;

class Transaction extends Model
{
    const STATUS_PENDING = 'pending';
    const STATUS_COMPLETED = 'completed';
    const STATUS_FAILED = 'failed';

    const ARMSOFT_STATUS_PENDING = 'pending';
    const ARMSOFT_STATUS_SUCCEED = 'succeed';
    const ARMSOFT_STATUS_FAILED = 'failed';

    const REPEAT_STATUS_PENDING = 'pending';
    const REPEAT_STATUS_COMPLETED = 'completed';
    const REPEAT_STATUS_SUCCEED = 'succeed';
    const REPEAT_STATUS_FAILED = 'failed';

    const PAYMENT_TYPE_CASH = 'cash';
    const PAYMENT_TYPE_NON_CASH = 'non_cash';

    const TYPE_IN = 'in';
    const TYPE_OUT = 'out';

    const SERVICE_TYPE_PAYMENT = 'payment';
    const SERVICE_TYPE_CASHBOX = 'cashbox';
    const SERVICE_TYPE_CANCEL = 'cancel';

    protected $table = 'transactions';

    protected $fillable = [
        'code',
        'payment_id',
        'receipt_id',
        'operator_id',
        'cashbox_id',
        'branch_id',
        'service_id',
        'created_at',
        'completed_at',
        'purpose',
        'amount',
        'total_amount',
        'commission',
        'debt',
        'cash',
        'details',
        'contact_number',
        'attempts_count',
        'last_attempted_at',
        'pos_auth_code',
        'status',
        'armsoft_status',
        'service_type',
        'type',
        'cashbox_state_id',
        'external_id',
        'is_cancelled',
        'repeat_status'
    ];

    protected $casts = [
        'details' => 'array'
    ];

    public function branch()
    {
        return $this->belongsTo(Branch::class, 'branch_id', 'id')->joinMl();
    }

    public function cashbox()
    {
        return $this->belongsTo(Cashbox::class, 'cashbox_id', 'id')->joinMl();
    }

    public function operator()
    {
        return $this->belongsTo(Operator::class, 'operator_id', 'id');
    }
    public function service()
    {
        return $this->belongsTo(Service::class, 'service_id', 'id')->joinMl();
    }

    public function scopeFailed($query){
        return $query->where('attempts_count', config('upay.adapter_payment_attempts.max_count'))
            ->where('service_type', Transaction::SERVICE_TYPE_PAYMENT)
            ->where('status', Transaction::STATUS_FAILED)
            ->where('repeat_status', 'is distinct from', Transaction::REPEAT_STATUS_COMPLETED);
    }
}
