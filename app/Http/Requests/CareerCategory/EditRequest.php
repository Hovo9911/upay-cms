<?php

namespace App\Http\Requests\CareerCategory;

use App\Http\Requests\Request;
use App\Model;

class EditRequest extends Request
{
    protected $module = 'career_category';

    public function rules()
    {
        $rules = [
            'sort_order' => 'required|integer',
            'show_status' => 'in:' . Model::STATUS_ACTIVE . ',' . Model::STATUS_INACTIVE,
            'ml' => 'required|array',
        ];
        $rules = $this->checkStatusForRequired($rules, request()->input('show_status'));

        $rules = $this->addMlRules($rules, [
            'title' => 'required|max:255',
            'show_status' => 'in:' . Model::STATUS_ACTIVE . ',' . Model::STATUS_INACTIVE
        ]);

        return $rules;
    }

    protected function defaultValues()
    {
        return [
            'show_status' => Model::STATUS_INACTIVE,
            'ml' => [
                'title' => '',
                'show_status' => Model::STATUS_INACTIVE,
            ]
        ];
    }

}
