<?php

namespace App\UPay\Operator;

use App\Search;

class OperatorSearch extends Search
{
    protected $orderables = [
        'id',
        'email',
        'first_name',
        'last_name'
    ];

    protected function query()
    {
        $filters = $this->filters;
        $query = Operator::when(!empty($filters['id']), function($query) use ($filters) {
                $query->where('id', $filters['id']);
            })
            ->when(!empty($filters['first_name']), function($query) use ($filters) {
                $query->where('first_name', 'ILIKE', '%'.$filters['first_name'].'%');
            })
            ->when(!empty($filters['last_name']), function($query) use ($filters) {
                $query->where('last_name', 'ILIKE', '%'.$filters['last_name'].'%');
            })
            ->when(!empty($filters['phone']), function($query) use ($filters) {
                $query->where('phone', 'ILIKE', '%'.$filters['phone'].'%');
            })
            ->when(!empty($filters['email']), function($query) use ($filters) {
                $query->where('email', 'ILIKE', '%'.$filters['email'].'%');
            })
            ->when(!empty($filters['status']), function($query) use ($filters) {
                if ($filters['status'] == Operator::PASSWORD_RESET_STATUS_PENDING) {
                    $query->where('password_reset_status', Operator::PASSWORD_RESET_STATUS_PENDING)->where('show_status', '!=', Operator::STATUS_DELETED);
                } else {
                    $query->where('password_reset_status', 'is distinct from', Operator::PASSWORD_RESET_STATUS_PENDING)
                        ->where('show_status', $filters['status']);
                }
            }, function($query) use ($filters) {
                $query->where('show_status', '!=', Operator::STATUS_DELETED);
            });
        return $query;
    }

    public function totalCount()
    {
        return Operator::notDeleted()->count();
    }
}
