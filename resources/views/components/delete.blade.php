<form id="delete-form" action="{{$url}}" method="POST">
    <input type="hidden" name="ids" value="{{$ids ?? ''}}">
    {{csrf_field()}}
</form>
