var $search = $.extend({}, $gsearch);
$search.defaultOrder = [[0, 'desc']];
$search.hasExport = true;
$search.modifyColumns = {
    cashbox: {
        render: function (data, type, row) {
            return type === 'display' ? data.name : data;
        }
    },
    operator: {
        render: function (data, type, row) {
            return type === 'display' ? (data == null ? trans('upay.report_closing.closed_automatically') : data.first_name+' '+data.last_name) : data;
        }
    },
    closing_date: {
        render: function (data, type, row) {
            return type === 'display' ? format_date(data) : data;
        }
    },
    actions: {
        render: function(data, type) {
            return type === 'display' ?
                '<a href="' + $search.searchForm.data('view-url') + '/' + data + '" class="btn btn-xs btn-success"><i class="fa fa-eye"></i></a> ' :
                data;
        }
    }
};

$search.footerCallback = function ( row, data, start, end, display ) {
    var api = this.api();
    var response = api.ajax.json();

    $("span", $( api.column( 0 ).footer() )).html(response.totalDetails.count);
    $( api.column( 3 ).footer() ).html(response.totalDetails.initial_balance);
    $( api.column( 4 ).footer() ).html(response.totalDetails.cash_in);
    $( api.column( 5 ).footer() ).html(response.totalDetails.cash_out);
    $( api.column( 6 ).footer() ).html(response.totalDetails.final_balance);
};

$search.setupCashbox = function(){
    $("#cashbox option").remove();
    $("#cashbox").closest(".form-group").hide();
    if ($("#branch").val() != '') {
        $.ajax({
            url: $("#branch").attr('data-cashbox-action')+'/'+$("#branch").val(),
            type: 'GET',
            dataType: 'json',
            xhrFields: {
                withCredentials: true
            },
            success: function(response){
                if(response.status == 'OK') {
                    for (var i in response.data) {
                        var newOption = new Option(response.data[i].name, response.data[i].id, false, false);
                        $('#cashbox').append(newOption).trigger('change');
                    }
                    $("#cashbox").closest(".form-group").show();
                }
            }
        });
    }
};

$(document).ready(function() {
    if ($($search.searchFormSelector).length) {
        $search.init();
        $("#branch").change(function(){
            $search.setupCashbox();
        });
        $search.setupCashbox();
    } else {
        $('#transactions-table').DataTable({
            searching: false,
            paging: false,
            info: false,
            dom: '<"html5buttons pb-1"B>lTg<"float-left ml-1"i>tp',
            order: [[ 1, 'desc' ]],
            buttons: [
                {
                    text: 'CSV',
                    action: function ( e, dt, node, config ) {
                        exportMe(dt, 'csv');
                    }
                },
                {
                    text: 'Excel',
                    action: function ( e, dt, node, config ) {
                        exportMe(dt, 'xlsx');
                    }
                },
            ]
        });
    }

    function exportMe( dt, format ) {
        var order = [];
        // var dataOrder = dt.order();
        // if (dataOrder.length) {
        //     for (var i in dataOrder) {
        //         order.push({
        //             column: self.columns[dataOrder[i][0]].data,
        //             dir: dataOrder[i][1]
        //         })
        //     }
        // }
        var link = $("#transactions-table").attr('export-action')+'?' + $.param({ order: order, format: format});
        window.location.href = link;
    }
});
