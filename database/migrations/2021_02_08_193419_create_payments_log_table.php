<?php

use App\DB\Migration;
use App\DB\Blueprint;

class CreatePaymentsLogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->getSchemaBuilder()->create('payments_log', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('client');
            $table->unsignedInteger('transaction_id');
            $table->text('request');
            $table->text('response');
            $table->string('log_type'); // error, info, ok
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments_log');
    }
}
