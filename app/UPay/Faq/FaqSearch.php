<?php

namespace App\UPay\Faq;

use App\Search;

class FaqSearch extends Search
{
    protected $orderables = [
        'id',
        'question',
        'sort_order',
        'show_status',
    ];

    protected function query()
    {
        $filters = $this->filters;

        return Faq::joinMl()->select(
            'faqs.id',
            'ml.question',
            'faqs.show_status'
        )
            ->when(!empty($filters['id']), function ($query) use ($filters) {
                $query->where('faqs.id', $filters['id']);
            })
            ->when(!empty($filters['question']), function ($query) use ($filters) {
                $query->where('ml.question', 'ILIKE', '%' . $filters['question'] . '%');
            })
            ->when(!empty($filters['show_status']), function ($query) use ($filters) {

                $query->where('faqs.show_status', $filters['show_status']);
            }, function ($query) use ($filters) {
                $query->where('faqs.show_status', '!=', Faq::STATUS_DELETED);
            });

    }

    public function totalCount()
    {
        return Faq::notDeleted()->count();
    }
}
