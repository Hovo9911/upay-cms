<?php

namespace App\UPay\Slider;

use App\GUploader;
use App\Model;

class Slider extends Model
{
    protected $table = 'sliders';

    protected $fillable = [
        'image',
        'sort_order',
        'show_status',
    ];

    public function ml(){
        return $this->hasMany(SliderMl::class, 'slider_id', 'id');
    }

    public function scopeJoinMl($query){
        $query->join('sliders_ml as ml', function ($join){
            $join->on('sliders.id', '=', 'ml.slider_id')
                ->where('ml.lng_id', '=', c_lng('id'))
                ->where('ml.show_status', '!=', SliderMl::STATUS_DELETED);
        });
    }

    public function getSliderImageAttribute()
    {
        $gUploader = new GUploader('website_slider_image');
        if (!empty($this->logo)) {
            return $gUploader->getFileUrl($this->image);
        }
        return '';
    }
}
