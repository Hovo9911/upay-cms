var $gsearch = {
    searchFormSelector: "#search-form",
    searchTableSelector: "#search-table",
    defaultActions: {},
    options: {},
    hasExport: false,
    table: null,
    columns: [],
    modifyColumns: {},
    defaultOrder: [[ 0, 'desc' ]],
    footerCallback: function(){},
    setupColumns() {
        var self = this;
        $("thead th", self.searchTable).each(function (i, elem) {
            var isActionCol = $(elem).attr('action-col') != undefined;
            var isCheckCol = $(elem).attr('check-col') != undefined;
            var column = {
                data: $(elem).data('col'),
                name: $(elem).data('col-name') != undefined ? $(elem).data('col-name') : $(elem).data('col'),
                orderable: $(elem).data('orderable') != undefined ? $(elem).data('orderable') : !isActionCol && !isCheckCol
            };
            if (column.name == 'id' && !isActionCol && !isCheckCol) {
                self.defaultOrder = [[i, 'desc']];
                column.width = '20px';
            } else if (column.name == 'show_status') {
                column.width = '61px';
                column.className = 'text-center';
                column.render = function (data, type) {
                    return type === 'display' ?
                        (data == '1' ? '<i title="'+trans('upay.general.show_status.active')+'" class="fa fa-check text-navy"></i>' : '<i title="'+trans('upay.general.show_status.inactive')+'" class="fa fa-minus text-muted"></i>') :
                        data;
                };
            } else if (isActionCol) {
                column.width = '61px';
                column.className = 'text-center';
                column.visible = !$.isEmptyObject(self.defaultActions);
                column.render = function (data, type) {
                    if (type === 'display') {
                        var actions = '';
                        if (self.defaultActions.view) {
                            actions += '<a href="' + self.searchForm.data('edit-url') + '/' + data + '" class="btn btn-xs btn-success"><i class="fa fa-eye"></i></a> ';
                        }
                        if (self.defaultActions.edit) {
                            actions += '<a href="' + self.searchForm.data('edit-url') + '/' + data + '" class="btn btn-xs btn-success"><i class="fa fa-pencil"></i></a> ';
                        }
                        if (self.defaultActions.delete) {
                            actions += '<a href="#" class="btn btn-xs btn-danger" onclick="gDelete(' + data + ');return false;"><i class="fa fa-remove"></i></a>' ;
                        }
                        return actions;
                    }
                    return  data;
                }
            } else if (isCheckCol) {
                column.visible = self.defaultActions.delete;
                column.width = '20px';
                column.render = function (data, type) {
                    return type === 'display' ?
                        '<input type="checkbox" class="i-checks check-col" value="'+data+'">' :
                        data;
                };
                $(this).html('<input type="checkbox" class="i-checks">');
                $(this).on('ifChecked', function(){
                    $(".check-col", self.searchTable).iCheck('check');
                });
                $(this).on('ifUnchecked', function(){
                    $(".check-col", self.searchTable).iCheck('uncheck');
                });
            }
            if (self.modifyColumns[column.name] != undefined) {
                column = Object.assign(column, self.modifyColumns[column.name]);
            }
            self.columns.push(column);
        });
    },
    init() {
        var self = this;
        self.searchForm = $(self.searchFormSelector);
        self.searchTable = $(self.searchTableSelector);
        self.defaultActions = {
            view: typeof $permissions != 'undefined' && $permissions.edit != undefined  && !$permissions.edit ? true : false,
            edit: typeof $permissions != 'undefined' && $permissions.edit != undefined  && $permissions.edit ? true : false,
            delete: typeof $permissions != 'undefined' && $permissions.delete != undefined && $permissions.delete ? true : false
        };

        self.searchForm.on('submit', function () {
            $(".submit-btn", self.searchForm).attr('disabled', 'disabled');
            self.table.draw();
            return false;
        });
        self.setupColumns();
        var options = Object.assign({
            serverSide: true,
            processing: true,
            searching: false,
            pagingType: 'full_numbers',
            pageLength: 25,
            responsive: true,
            dom: '<"html5buttons"B>lTg<"float-left ml-1"i>tp',
            buttons: [],
            columns: self.columns,
            order: self.defaultOrder,
            ajax: {
                url: self.searchForm.attr('action'),
                data: function (data) {
                    var order = [];
                    if (data.order.length) {
                        for (var i in data.order) {
                            order.push({
                                column: data.columns[data.order[i].column].data,
                                dir: data.order[i].dir
                            })
                        }
                    }
                    return self.searchForm.serialize() + '&' + $.param({
                        order: order,
                        start: data.start,
                        length: data.length
                    });
                },
            },
            drawCallback: function () {
                init_checkboxes($("tbody .i-checks", self.searchTable));
                $(".submit-btn", self.searchForm).removeAttr('disabled');
            },
            footerCallback: self.footerCallback
        }, self.options);
        if (self.hasExport) {
            options.buttons = [
                {
                    text: 'CSV',
                    action: function ( e, dt, node, config ) {
                        self.exportMe(dt, 'csv');
                    }
                },
                {
                    text: 'Excel',
                    action: function ( e, dt, node, config ) {
                        self.exportMe(dt, 'xlsx');
                    }
                },
            ];
        }
        self.table = self.searchTable.DataTable(options)
        .on('error.dt', function (e, settings, techNote, message) {
            $(".submit-btn", self.searchForm).removeAttr('disabled');
        })
        .on('xhr', function (e, settings, json, xhr) {
            $(".submit-btn", self.searchForm).removeAttr('disabled');
            if (json.status != undefined && json.status != 'OK') {
                json.aaData = [];
                json.recordsTotal = 0;
                json.recordsFiltered = 0;
                globalProcessResponse(json);
                return true;
            }
        });
    },
    refresh: function(){
        this.table.draw();
    },
    exportMe( dt, format ) {
        var self = this;
        var order = [];
        var dataOrder = dt.order();
        if (dataOrder.length) {
            for (var i in dataOrder) {
                order.push({
                    column: self.columns[dataOrder[i][0]].data,
                    dir: dataOrder[i][1]
                })
            }
        }
        var link = self.searchForm.attr('export-action')+'?'+self.searchForm.serialize() + '&' + $.param({ order: order, format: format});
        window.location.href = link;
    }

};
