<?php

use App\DB\Migration;
use App\DB\Blueprint;
use App\UPay\Service\Commission;

class CreateServiceCommissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->getSchemaBuilder()->create('service_commissions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('service_id')->unsigned();
            $table->enum('type', [Commission::TYPE_CUSTOMER, Commission::TYPE_PROVIDER]);
            $table->enum('source', [
                Commission::SOURCE_BRANCH,
                Commission::SOURCE_API,
                Commission::SOURCE_TERMINAL,
                Commission::SOURCE_ONLINE,
            ]);
            $table->showStatus();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('service_commissions');
    }
}
