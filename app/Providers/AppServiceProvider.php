<?php

namespace App\Providers;

use App\JsTrans;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(JsTrans::class, function () {
            return new JsTrans();
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        \View::share('jsTrans', $this->app->make(JsTrans::class));
//        \DB::listen(function($query) {
//            logger()->info($query->sql . print_r($query->bindings, true));
//        });
        //
    }
}
