<?php
use App\DB\Migration;
use App\DB\Blueprint;

class CreateEmailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        $this->getSchemaBuilder()->create('emails', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('priority')->default(0);
            $table->string('to');
            $table->string('to_name');
            $table->string('from');
            $table->string('from_name');
            $table->string('reply_to');
            $table->string('reply_to_name');
            $table->text('cc');
            $table->text('bcc');
            $table->string('subject', 1024);
            $table->text('body');
            $table->text('attachments');
            $table->timestamp('add_date');
            $table->timestamp('sent_date')->nullable()->default(null);
            $table->enum('status',[
                App\Mailer\Email::STATUS_PENDING,
                App\Mailer\Email::STATUS_FAILED,
                App\Mailer\Email::STATUS_SENT,
                App\Mailer\Email::STATUS_TRY_LATER
            ]);
            $table->timestamps();
            $table->index('status');
            $table->text('log');
            $table->index('priority');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('emails');
    }
}
