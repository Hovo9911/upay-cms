<?php

use App\DB\Migration;
use App\DB\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFaqCategoriesMlTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->getSchemaBuilder()->create('faq_categories_ml', function (Blueprint $table) {
            $table->integer('faq_category_id')->unsigned();
            $table->integer('lng_id')->unsigned();
            $table->string('title');
            $table->showStatus();
            $table->primary(['faq_category_id', 'lng_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('faq_categories_ml');
    }
}
