<?php

namespace App;

class Search
{
    protected $perPage = 25;
    protected $start = 0;
    protected $filters = [];
    protected $order = [
        [ 'column' => 'id', 'dir' => 'desc' ]
    ];
    protected $orderables = [
        'id',
    ];

    public function __construct($data)
    {
        if (!empty($data['filters'])) {
            $this->filters = $data['filters'];
        }
        if (!empty($data['order'])) {
            $this->order = $data['order'];
        }
        if (!empty($data['start'])) {
            $this->start = $data['start'];
        }
    }

    protected function setOrdering($query){
        foreach($this->order as $order) {
            if (empty($order['column']) || empty($order['dir']) ||
                (!empty($order['dir']) && ($order['dir'] != 'asc' && $order['dir'] != 'desc'))) {
                continue;
            }
            if (in_array($order['column'], $this->orderables)) {
                $query->orderBy($order['column'], $order['dir']);
            }
        }
    }
    protected function setLimits($query){
        $query
            ->skip($this->start)
            ->take($this->perPage);
    }

    /**
     * @return \Illuminate\Database\Query\Builder;
     */
    protected function query(){}

    public function search(){
        $query = $this->query();
        $this->setOrdering($query);
        $this->setLimits($query);
        return $query->get();
    }

    protected function totalCount(){
        return 0;
    }

    public function filteredCount(){
        $query = $this->query();
        return $query->count();
    }
}
