<?php

namespace App\UPay\Admin;

use App\MultiPKModel;
use App\UPay\Role\Role;

class AdminRole extends MultiPKModel
{
    protected $table = 'admin_roles';
    public $timestamps = false;
    protected $primaryKey = ['admin_id', 'role_id'];
    public $incrementing = false;

    protected $fillable = [
        'admin_id',
        'role_id',
        'show_status',
    ];

    public function role(){
        return $this->hasOne(Role::class, 'id', 'role_id')->select('id', 'name')->notDeleted();
    }

    public function rolePermissions(){
        return $this->hasOne(Role::class, 'id', 'role_id')->select('id', 'name', 'permissions')->active();
    }
}
