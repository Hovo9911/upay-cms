<?php

namespace App\Http\Controllers;

use App\Http\Requests\Report\TransactionSearchRequest;
use App\UPay\Interfaces\TransactionRepositoryInterface;
use App\UPay\Transaction\FailedTransactionSearch;
use Illuminate\Http\Request;

class FailedTransactionsController extends Controller
{
    public function index()
    {
        return view('failed_transactions.list');
    }

    public function getList(TransactionSearchRequest $request)
    {
        $searcher = new FailedTransactionSearch($request->validated());
        $result = $searcher->search();

        return response()->json([
            'data' => $result,
            'recordsTotal' => $searcher->totalCount(),
            'recordsFiltered' => $searcher->filteredCount(),
        ]);
    }

    public function repeat(Request $request, TransactionRepositoryInterface $transactionRepository)
    {
        $id = $request->input('id');
        if (empty($id)) {
            abort(404);
        }
        $id = intval($id);
        $transactionRepository->repeat($id);
        return $this->sendOK();
    }

    public function complete(Request $request, TransactionRepositoryInterface $transactionRepository)
    {
        $id = $request->input('id');
        if (empty($id)) {
            abort(404);
        }
        $id = intval($id);
        $transactionRepository->complete($id);
        return $this->sendOK();
    }
}
