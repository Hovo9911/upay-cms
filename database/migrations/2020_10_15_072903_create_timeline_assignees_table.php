<?php

use App\DB\Migration;
use App\DB\Blueprint;

class CreateTimelineAssigneesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->getSchemaBuilder()->create('timeline_assignees', function (Blueprint $table) {
            $table->integer('timeline_id')->unsigned();
            $table->integer('operator_id')->unsigned();
            $table->timestamp('assigned_at');
            $table->primary(['timeline_id', 'operator_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('timeline_assignees');
    }
}
