<?php

namespace App\Http\Controllers;

use App\GUploader;
use App\Http\Requests\Transaction\CancellationChangeStatusRequest;
use App\Http\Requests\Transaction\CancellationSearchRequest;
use App\UPay\Interfaces\ServiceRepositoryInterface;
use App\UPay\Transaction\TransactionCancellation;
use App\UPay\Transaction\TransactionCancellationSearch;
use App\UPay\Interfaces\TransactionRepositoryInterface;
use Illuminate\Http\Request;

class CancellationController extends Controller
{
    private $transactionRepository;

    public function __construct(TransactionRepositoryInterface $transactionRepository)
    {
        $this->transactionRepository = $transactionRepository;
    }

    public function index()
    {
        $serviceRepository = \App::make(ServiceRepositoryInterface::class);
        return view('cancellation.list')->with(['services' => $serviceRepository->all()]);
    }

    public function view(Request $request, $id)
    {
        $cancellation = $this->transactionRepository->findCancellationOrFail($id);
        if ($cancellation->type == TransactionCancellation::TYPE_CUSTOMER) {
            $gUploader = new GUploader('cancellation');
            $details = $cancellation->details;
            foreach ($details['files'] as $key => $file) {
                $details['files'][$key] = [
                    'label' => explode('/', $file)[1],
                    'url' => $gUploader->getFileUrl($file)
                ];
            }
            $serviceRepository = \App::make(ServiceRepositoryInterface::class);
            $details['service'] = $serviceRepository->findOrFail($cancellation->details['service_data']['service_id'], true);

            $cancellation->details = $details;
        }
        $cancellation->load(['history', 'transaction.cashbox', 'operator']);
        foreach ($cancellation->history as $item) {
            if ($item->status != TransactionCancellation::STATUS_NEW &&
                (($cancellation->type == TransactionCancellation::TYPE_CASHIER && $item->status != TransactionCancellation::STATUS_COMPLETED && $item->status != TransactionCancellation::STATUS_REJECTED)
                    || ($cancellation->type == TransactionCancellation::TYPE_CUSTOMER))) {
                $item->admin_did = true;
                $item->load('admin');
            } else {
                $item->load('operator');
            }
        }
        return view('cancellation.view')->with(['cancellation' => $cancellation]);
    }

    public function getList(CancellationSearchRequest $request)
    {
        $searcher = new TransactionCancellationSearch($request->validated());
        $result = $searcher->search();
        return response()->json([
            'data' => $result,
            'recordsTotal' => $searcher->totalCount(),
            'recordsFiltered' => $searcher->filteredCount(),
        ]);
    }

    public function changeStatus(CancellationChangeStatusRequest $request, $id)
    {
        $cancellation = $this->transactionRepository->findCancellationOrFail($id);
        $data = $request->validated();
        $this->transactionRepository->changeCancellationStatus($cancellation, $data);

        return $this->sendOK([
            'message' => trans('upay.cancellation.change_status.success_message'),
            'edit_url' => url('/cancellation/view/'.$cancellation->id),
            'list_url' => url('/cancellation/list')
        ]);
    }
}
