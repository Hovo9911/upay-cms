var $search = $.extend({}, $gsearch);
$search.hasExport = true;
$search.options = {
    searching: false,
    paging: false,
    info: false,
    dom: '<"html5buttons pb-1"B>lTgtp',
};
$search.defaultOrder = [[1, 'desc']];
$search.modifyColumns = {
    service: {
        render: function (data, type, row) {
            return type === 'display' ? data.name : data;
        }
    },
    amount: {
        className: 'text-right',
    },
    commission: {
        className: 'text-right',
    }
};

$(document).ready(function() {
    if ($($search.searchFormSelector).length) {
        $search.init();
    }
});
