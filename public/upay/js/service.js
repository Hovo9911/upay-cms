var $saveForm = $.extend({}, $gForm);
$saveForm.formId = "save-form";

var $approveCommissionsForm = $.extend({}, $gForm);
$approveCommissionsForm.formId = "approve-commissions-form";
$approveCommissionsForm.onSuccess = function(response) {
    swal({
        title: response.data.message,
        type: "success",
        showCancelButton: false,
        showConfirmButton: true,
        closeOnConfirm: true
    }, function(){
        window.location.reload();
    });
    $('.approve-commissions-btn').removeAttr('disabled');
};

var $search = $.extend({}, $gsearch);
$search.modifyColumns = {
    branch_account_number: {
        render: function (data, type, row) {
            if (type === 'display') {
                var str = '';
                if (data != '') {
                    str += '<b>' + trans('upay.service.branch_account_number.short') + '</b>' + data + '<br>';
                }
                if (row.branch_commission_account_number != '') {
                    str += '<b>' + trans('upay.service.branch_commission_account_number.short') + '</b>' + row.branch_commission_account_number + '<br>';
                }
                if (row.mobile_account_number != '') {
                    str += '<b>' + trans('upay.service.mobile_account_number.short') + '</b>' + row.mobile_account_number + '<br>';
                }
                if (row.mobile_commission_account_number != '') {
                    str += '<b>' + trans('upay.service.mobile_commission_account_number.short') + '</b>' + row.mobile_commission_account_number;
                }
                return str;
            }
            return data;
        }
    },
    account_min: {
        render: function (data, type, row) {
            return type === 'display' ?
                (data !== null ? (data + ' - ' + row.account_max) : ''):
                data;
        }
    },
    use_fixed_amount: {
        width: '61px',
        className: 'text-center',
        render: function (data, type) {
            return type === 'display' ?
                (data ? '<i title="' + trans('upay.service.use_fixed_amount.yes') + '" class="fa fa-check text-navy"></i>' : '<i title="' + trans('upay.service.use_fixed_amount.no') + '" class="fa fa-remove text-danger"></i>') :
                '';
        }
    },
};

var $commissions = {
    rangeIndexes: {},
    getRangeIndex: function(type, src){
        if (typeof this.rangeIndexes[type] == 'undefined') {
            this.rangeIndexes[type] = {};
        }
        if (typeof this.rangeIndexes[type][src] == 'undefined') {
            this.rangeIndexes[type][src] = 0;
        }
        return this.rangeIndexes[type][src];
    },
    setRangeIndex: function(type, src, i){
        this.rangeIndexes[type][src] = i;
    },
    removeRow: function(self){
        var table =  $(self).closest('table');
        if ($("tbody tr", table).length == 1) {
            table.addClass('d-none');
        }
        $(self).closest('tr').remove();
    },
    addRow: function(type, src, values){
        if (typeof values == 'undefined') {
            values = {
                amount: '',
                max: '',
                min: '',
                percent: '',
                type: 'fixed'
            };
        } else {
            values.amount = values.amount != null ? values.amount : '';
            values.max = values.max != null ? values.max : '';
            values.min = values.min != null ? values.min : '';
            values.percent = values.percent != null ? values.percent : '';
            values.type = values.type != null ? values.type : 'fixed';
        }
        var i = this.getRangeIndex(type, src) + 1;
        var row =
        '<tr>'+
            '<td>'+
                '<div class="form-group">' +
                    '<select style="width:100%" onchange="$commissions.typeChanged(this)" '+$formDisabled+' name="commissions_'+type+'_'+src+'['+i+'][type]" class="form-control m-b select-picker">' +
                        '<option value="fixed" '+(values.type == 'fixed' ? 'selected="selected"' : '')+'>'+trans('upay.service.commissions.type.fixed')+'</option>' +
                        '<option value="percent" '+(values.type == 'percent' ? 'selected="selected"' : '')+'>'+trans('upay.service.commissions.type.percent')+'</option>' +
                    '</select>' +
                    '<span class="form-text text-danger form-error-text form-error-commissions_'+type+'_'+src+'_'+i+'_type"></span>' +
                '</div>' +
            '</td>'+
            '<td>' +
                '<div class="form-group">' +
                    '<input type="text" value="'+values.min+'" '+$formDisabled+' name="commissions_'+type+'_'+src+'['+i+'][min]" class="form-control">' +
                    '<span class="form-text text-danger form-error-text form-error-commissions_'+type+'_'+src+'_'+i+'_min"></span>' +
                '</div>' +
            '</td>'+
            '<td>' +
                '<div class="form-group">' +
                    '<input type="text" value="'+values.max+'" '+$formDisabled+' name="commissions_'+type+'_'+src+'['+i+'][max]" class="form-control">' +
                    '<span class="form-text text-danger form-error-text form-error-commissions_'+type+'_'+src+'_'+i+'_max"></span>' +
                '</div>' +
            '</td>'+
            '<td>' +
                '<div class="form-group commission-value commission-fixed '+(values.type == 'percent' ? 'd-none' : '')+'">' +
                    '<input type="text" value="'+values.amount+'" '+$formDisabled+' name="commissions_'+type+'_'+src+'['+i+'][amount]" class="form-control">' +
                    '<span class="form-text text-danger form-error-text form-error-commissions_'+type+'_'+src+'_'+i+'_amount"></span>' +
                '</div>' +
                '<div class="form-group commission-value commission-percent '+(values.type == 'fixed' ? 'd-none' : '')+'">' +
                    '<input type="text" value="'+values.percent+'" '+$formDisabled+' name="commissions_'+type+'_'+src+'['+i+'][percent]" class="form-control">' +
                    '<span class="form-text text-danger form-error-text form-error-commissions_'+type+'_'+src+'_'+i+'_percent"></span>' +
                '</div>' +
            '</td>'+
            '<td class="align-middle text-center">' +
                '<div class="form-group">'+
                    '<button onclick="$commissions.removeRow(this)" '+$formDisabled+' type="button" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></button>'+
                '</div>' +
            '</td>'+
        '</tr>';
        row = $(row);
        $("#commission-values-"+type+"-"+src+" tbody").append(row);
        $("#commission-values-"+type+"-"+src).removeClass('d-none');
        $("#commission-values-"+type+"-"+src+" .select-picker").select2({
            minimumResultsForSearch: 15,
            width: 'resolve'
        });
        $saveForm.setupFocus(row);
        this.setRangeIndex(type, src, i);
    },
    typeChanged: function(self){
        $(".commission-value", $(self).closest('tr')).addClass('d-none');
        $(".commission-"+$(self).val(), $(self).closest('tr')).removeClass('d-none');
    }
};

$(document).ready(function() {
    if ($($search.searchFormSelector).length) {
        $search.init();
    } else {
        $saveForm.init();

        $("input[name=is_encashment]").change(function(){
            if ($(this).prop('checked')) {
                $("input[name=encashment_type]").closest('.form-group').removeClass('d-none');
            } else {
                $("input[name=encashment_type]").closest('.form-group').addClass('d-none');
            }
        });

        for (var i in $commissionsSrc) {
            for (var j in $commissionsSrc[i].commission_values) {
                $commissions.addRow($commissionsSrc[i].type, $commissionsSrc[i].source, $commissionsSrc[i].commission_values[j]);
            }
        }
        $approveCommissionsForm.init();
        $(".approve-commissions-btn").click(function(){
            $(this).attr('disabled', 'disabled');
            swal({
                title: trans('upay.service.commissions.approve.confirm_modal_title'),
                text: trans('upay.service.commissions.approve.confirm_modal_text'),
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: trans('upay.general.confirm_modal_ok'),
                cancelButtonText: trans('upay.general.confirm_modal_cancel'),
                closeOnConfirm: false
            }, function(confirmed){
                if (confirmed) {
                    $approveCommissionsForm.form().submit();
                } else {
                    $('.approve-commissions-btn').removeAttr('disabled');
                }
            });
            return false;
        });
    }
});
