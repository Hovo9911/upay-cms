<?php
use App\DB\Migration;
use App\DB\Blueprint;

class CreateServicesMlTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->getSchemaBuilder()->create('services_ml', function (Blueprint $table) {
            $table->integer('service_id')->unsigned();
            $table->integer('lng_id')->unsigned();
            $table->string('name');
            $table->string('payment_purpose');
            $table->string('invoice_note', 1024);
            $table->showStatus();
            $table->primary(['service_id', 'lng_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('services_ml');
    }
}
