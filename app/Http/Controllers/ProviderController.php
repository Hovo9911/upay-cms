<?php

namespace App\Http\Controllers;

use App\Exceptions\InvalidDataException;
use App\Http\Requests\Provider\EditRequest;
use App\Http\Requests\Provider\SearchRequest;
use App\UPay\Provider\ProviderSearch;
use App\UPay\Interfaces\ProviderRepositoryInterface;
use App\UPay\Interfaces\LanguageRepositoryInterface;
use Illuminate\Http\Request;

class ProviderController extends Controller
{
    private $providerRepository;
    private $languageRepository;

    public function __construct(ProviderRepositoryInterface $providerRepository, LanguageRepositoryInterface $languageRepository)
    {
        $this->languageRepository = $languageRepository;
        $this->providerRepository = $providerRepository;
    }

    public function index()
    {
        $parents = $this->providerRepository->parents();
        return view('provider.list')->with([
            'parents' => $parents
        ]);
    }

    public function edit(Request $request, $id = null)
    {
        if ($id != null) {
            $provider = $this->providerRepository->findOrFail($id);
            $provider->load('ml');
        } else {
            $provider = $this->providerRepository->mock();
        }
        $provider->ml = $provider->ml->keyBy('lng_id');
        $languages = $this->languageRepository->all();
        $parents = $this->providerRepository->parents($id);

        return view('provider.edit')->with([
            'mode' => $id != null ? 'edit' : 'add',
            'provider' => $provider,
            'languages' => $languages,
            'parents' => $parents
        ]);
    }

    public function save(EditRequest $request, $id = null)
    {
        $data = $request->validated();
        if (!empty($data['parent_id'])) {
            $parentFound = $this->providerRepository->found($data['parent_id']);
            if (!$parentFound || (!is_null($id) && $data['parent_id'] == $id)) {
                throw InvalidDataException::withErrors(['parent_id' => trans('upay.provider.parent_id.not_found')]);
            }
        }
        if ($id != null) {
            if (!empty($data['parent_id'])) {
                $hasChild = $this->providerRepository->hasChild($id);
                if ($hasChild) {
                    throw InvalidDataException::withErrors(['parent_id' => trans('upay.provider.parent_id.forbidden')]);
                }
            }
            $provider = $this->providerRepository->update($id, $data);
        } else {
            $provider = $this->providerRepository->create($data);
        }

        return $this->sendOK([
            'message' => trans('upay.provider.saved.message'),
            'edit_url' => url('/provider/edit/'.$provider->id),
            'list_url' => url('/provider/list')
        ]);
    }

    public function getList(SearchRequest $request)
    {
        $searcher = new ProviderSearch($request->validated());
        $result = $searcher->search();

        return response()->json([
            'data' => $result,
            'recordsTotal' => $searcher->totalCount(),
            'recordsFiltered' => $searcher->filteredCount(),
        ]);
    }
}
