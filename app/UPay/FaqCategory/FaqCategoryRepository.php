<?php

namespace App\UPay\FaqCategory;


use App\UPay\Interfaces\FaqCategoryRepositoryInterface;
use DB;
use Illuminate\Database\Eloquent\Collection;

class FaqCategoryRepository implements FaqCategoryRepositoryInterface
{
    public function create($data): FaqCategory
    {
        $faq = new FaqCategory($data);
        DB::beginTransaction();
        $faq->save();
        $this->saveMl($faq, $data['ml']);
        DB::commit();
        return $faq;
    }

    public function mock(): FaqCategory
    {
        return new FaqCategory([
            'sort_order' => '',
            'show_status' => FaqCategory::STATUS_ACTIVE,
        ]);
    }

    public function findOrFail($id, $withMl = false): FaqCategory
    {
        $faq = FaqCategory::where('id', $id)->where('faq_categories.show_status', '!=', FaqCategory::STATUS_DELETED);

        if ($withMl) {
            $faq->joinMl();
        }
        $faq = $faq->first();

        if (empty($faq)) {
            abort(404);
        }
        return $faq;
    }

    public function update($id, $data): FaqCategory
    {
        $faq = $this->findOrFail($id);

        DB::beginTransaction();
        $faq->update($data);
        $this->saveMl($faq, $data['ml']);
        DB::commit();
        return $faq;
    }

    public function delete($ids): int
    {
        DB::beginTransaction();
        FaqCategoryMl::whereIn('faq_category_id', $ids)->update(['show_status' => FaqCategoryMl::STATUS_DELETED]);
        $faq= FaqCategory::whereIn('id', $ids)->update(['show_status' => FaqCategory::STATUS_DELETED]);
        DB::commit();
        return $faq;
    }

    public function saveMl(FaqCategory $faqCategory, array $mls): void
    {
        foreach ($mls as $lngId => $ml) {
            $faqCategory->ml()->updateOrCreate(
                ['faq_category_id' => $faqCategory->id, 'lng_id' => $lngId],
                [
                    'title' => $ml['title'],
                    'show_status' => $ml['show_status'],
                ]
            );
        }
    }

    public function all(): Collection
    {
        return FaqCategory::joinMl()
            ->where('faq_categories.show_status', FaqCategory::STATUS_ACTIVE)
            ->where('ml.show_status', FaqCategory::STATUS_ACTIVE)
            ->get();
    }
}
