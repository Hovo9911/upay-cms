<?php

use App\DB\Migration;
use App\DB\Blueprint;

class CreateApplicationLanguagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->getSchemaBuilder()->create('application_languages', function (Blueprint $table) {
            $table->tinyInteger('application_id');
            $table->integer('lng_id');
            $table->enum('is_default', ['0', '1']);
            $table->sortOrder();
            $table->showStatus();
            $table->primary(['application_id', 'lng_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('application_languages');
    }
}
