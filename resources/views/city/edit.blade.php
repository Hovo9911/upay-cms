<?php
    use App\Model;
    $activeMenu = 'city';
    $canDelete = \Auth::user()->hasPermission('city.delete');
    $canEdit = \Auth::user()->hasPermission('city.edit');
    $title = 'upay.city.'.($canEdit ? $mode : 'view').'_title';
    $disabled = $canEdit ? '' : 'disabled';
?>
@extends('layout')
@section('page_title') @lang($title) @stop
@section('content')
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox ">
                    <div class="ibox-title">
                        <h1>@lang($title)</h1>
                    </div>
                    <div class="ibox-content">
                        <form id="save-form" action="{{url('/city/save/'.($city->id ?? ''))}}" method="POST">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="tabs-container">
                                        <ul class="nav nav-tabs" role="tablist">
                                            <li><a class="nav-link active" data-toggle="tab" href="#tab-general">@lang('upay.tab.general')</a></li>
                                            @foreach($languages as $lng)
                                                <li><a class="nav-link" data-toggle="tab" href="#tab-lng-{{$lng->id}}">{{$lng->name}}</a></li>
                                            @endforeach
                                        </ul>
                                        <div class="tab-content">
                                            <div role="tabpanel" id="tab-general" class="tab-pane active">
                                                <div class="panel-body">
                                                    <div class="form-group required row"><label class="col-lg-2 col-form-label">@lang('upay.city.region')</label>
                                                        <div class="col-lg-10">
                                                            <select name="region_id" {{$disabled}} class="form-control m-b select-picker">
                                                                <option value="">@lang('upay.general.dropdown.select')</option>
                                                                @foreach($regions as $region)
                                                                    <option value="{{$region->id}}" @if($city->region_id == $region->id) selected="selected" @endif>{{$region->name}}</option>
                                                                @endforeach
                                                            </select>
                                                            <span class="form-text text-danger form-error-text form-error-region_id"></span>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row"><label class="col-lg-2 col-form-label">@lang('upay.general.sort_order')</label>
                                                        <div class="col-lg-2">
                                                            <input type="text" name="sort_order" value="{{$city->sort_order}}" {{$disabled}} class="form-control">
                                                            <span class="form-text text-danger form-error-text form-error-sort_order"></span>
                                                        </div>
                                                    </div>

                                                    <div class="form-group row"><label class="col-lg-2 col-form-label">@lang('upay.general.show_status')</label>
                                                        <div class="col-lg-10">
                                                            <input type="checkbox" name="show_status" value="{{Model::STATUS_ACTIVE}}" @if($city->show_status == Model::STATUS_ACTIVE) checked @endif {{$disabled}} data-toggle="switchbutton" data-onlabel="@lang('upay.general.show_status.active')" data-offlabel="@lang('upay.general.show_status.inactive')" data-width="86" data-height="33">
                                                            <span class="form-text text-danger form-error-text form-error-show_status"></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            @foreach($languages as $lng)
                                                <div role="tabpanel" id="tab-lng-{{$lng->id}}" class="tab-pane">
                                                    <div class="panel-body">
                                                        <div class="form-group required row"><label class="col-lg-2 col-form-label">@lang('upay.city.name')</label>
                                                            <div class="col-lg-10">
                                                                <input type="text" name="ml[{{$lng->id}}][name]" @isset($city->ml[$lng->id]) value="{{$city->ml[$lng->id]->name}}" @endisset {{$disabled}} class="form-control">
                                                                <span class="form-text text-danger form-error-text form-error-ml_{{$lng->id}}_name"></span>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row"><label class="col-lg-2 col-form-label">@lang('upay.general.show_status')</label>
                                                            <div class="col-lg-10">
                                                                <input type="checkbox" name="ml[{{$lng->id}}][show_status]" value="{{Model::STATUS_ACTIVE}}" @if(!isset($city->ml[$lng->id]) || $city->ml[$lng->id]->show_status == Model::STATUS_ACTIVE) checked @endif {{$disabled}} data-toggle="switchbutton" data-onlabel="@lang('upay.general.show_status.active')" data-offlabel="@lang('upay.general.show_status.inactive')" data-width="86" data-height="33">
                                                                <span class="form-text text-danger form-error-text form-error-ml_{{$lng->id}}_show_status"></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>

                            {{csrf_field()}}

                            <hr/>
                            <div class="form-group row">
                                <div class="col-lg-4 col-lg-offset-2">
                                    <a href="{{url('/city/list')}}" class="btn btn-white">@lang('upay.general.buttons.cancel')</a>
                                    @if($mode == 'edit' && $canDelete)<button onclick="gDelete({{$city->id}});return false;" type="button" class="btn btn-danger">@lang('upay.general.buttons.delete')</button>@endif
                                    @if($canEdit)<button class="btn btn-primary submit-btn" type="submit">@lang('upay.general.buttons.save')</button>@endif
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @if($mode == 'edit' && $canDelete)
        @include('components.delete', ['url' => url('/city/delete')])
    @endif
    <script src="{{script_url('upay/js/city.js')}}"></script>
@stop
