<?php
namespace App\UPay\Interfaces;

use App\UPay\Localization\Localization;

interface LocalizationRepositoryInterface
{
    public function create($data): Localization;

    public function findOrFail($id): Localization;

    public function update($id, $data): Localization;

    public function delete($ids): int;

    public function saveMl(Localization $localization, array $mls): void;

    public function keyFound($key, $group, $id): bool;

    public function getGroups(): array;

    public function sync($group = null): void;

    public function syncBack(): void;
}
