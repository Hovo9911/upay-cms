<?php

use App\DB\Migration;
use App\DB\Blueprint;
use App\UPay\Service\CommissionValue;
use App\UPay\Service\CommissionValueApproved;

class CreateServiceCommissionValuesApprovedTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->getSchemaBuilder()->create('service_commission_values_approved', function (Blueprint $table) {
            $table->integer('commission_id')->unsigned();
            $table->enum('type', [CommissionValue::TYPE_FIXED, CommissionValue::TYPE_PERCENT]);
            $table->float('min')->nullable()->default(null);
            $table->float('max')->nullable()->default(null);
            $table->float('amount')->nullable()->default(null);
            $table->float('percent')->nullable()->default(null);
            $table->timestamp('date');
            $table->enum('status', [CommissionValueApproved::STATUS_ACTIVE, CommissionValueApproved::STATUS_ARCHIVED]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('service_commission_values_approved');
    }
}
