var $saveForm = $.extend({}, $gForm);
$saveForm.formId = "save-form";

var $resetPasswordForm = $.extend({}, $gForm);
$resetPasswordForm.formId = "reset-password-form";
$resetPasswordForm.onSuccess = function(response) {
    swal({
        title: response.data.message,
        type: "success",
        showCancelButton: false,
        showConfirmButton: true,
        closeOnConfirm: true
    }, function(){
        window.location.reload();
    });
    $('.reset-password-btn').removeAttr('disabled');
};

var $search = $.extend({}, $gsearch);
$search.modifyColumns = {
    status: {
        width: '61px',
        className: 'text-center',
        render: function(data, type, row) {
            return type === 'display' ?
                (row.password_reset_status == 'pending' ?
                '<i title="'+trans('upay.admin.status.password_reset_pending')+'" class="fa fa-clock-o text-navy text-warning"></i>' :
                (row.show_status == '1' ? '<i title="'+trans('upay.general.show_status.active')+'" class="fa fa-check text-navy"></i>' : '<i title="'+trans('upay.general.show_status.inactive')+'" class="fa fa-minus text-muted"></i>')) :
                '';
        }
    },
    roles: {
        render: function(data, type) {
            return type === 'display' ?
                (data.map(function(item){return item.role.name}).join(', ')) :
                data;
        }
    }
};

$(document).ready(function() {
    if ($($search.searchFormSelector).length) {
        $search.init();
    } else {
        $saveForm.init();
        $resetPasswordForm.init();
        $(".reset-password-btn").click(function(){
            $(this).attr('disabled', 'disabled');
            swal({
                title: trans('upay.admin.reset_password.confirm_modal_title'),
                text: trans('upay.admin.reset_password.confirm_modal_text'),
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: trans('upay.general.confirm_modal_ok'),
                cancelButtonText: trans('upay.general.confirm_modal_cancel'),
                closeOnConfirm: false
            }, function(confirmed){
                if (confirmed) {
                    $resetPasswordForm.form().submit();
                } else {
                    $('.reset-password-btn').removeAttr('disabled');
                }
            });
            return false;
        });
    }
});
