<?php

namespace App\UPay\Region;

use \App\Model;

class Region extends Model
{
    protected $table = 'regions';

    public function ml(){
        return $this->hasMany(RegionMl::class, 'region_id', 'id');
    }

    public function scopeJoinMl($query){
        $query->join('regions_ml as ml', function ($join){
            $join->on('regions.id', '=', 'ml.region_id')
                ->where('ml.lng_id', '=', c_lng('id'));
        });
    }
}
