<?php

use App\DB\Migration;
use App\DB\Blueprint;

class AddSomeFieldsToTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->getSchemaBuilder()->table('transactions', function (Blueprint $table) {
            $table->enum('service_type', ['payment', 'cashbox', 'cancel'])->default('payment');
            $table->enum('type', ['in', 'out'])->default('out');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $this->getSchemaBuilder()->table('transactions', function (Blueprint $table) {
            $table->dropColumn('service_type');
            $table->dropColumn('type');
        });
    }
}
