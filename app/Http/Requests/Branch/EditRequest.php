<?php

namespace App\Http\Requests\Branch;

use App\Http\Requests\Request;
use App\Model;

class EditRequest extends Request
{
    protected $module = 'branch';

    public function rules()
    {
        $rules = [
            'code' => 'required|regex:/^[0-9]{5}$/',
            'armsoft_code' => 'required|regex:/^[0-9]{2}$/',
            'city_id' => 'required|integer|nullable',
            'lat' => 'required|numeric|nullable',
            'lng' => 'required|numeric|nullable',
            'sort_order' => 'required|integer',
            'show_status' => 'in:'.Model::STATUS_ACTIVE.','.Model::STATUS_INACTIVE,
            'ml' => 'required|array',
        ];
        $rules = $this->checkStatusForRequired($rules, request()->input('show_status'));

        $rules = $this->addMlRules($rules, [
            'name' => 'required|max:255',
            'address' => 'required|max:255',
            'show_status' => 'in:'.Model::STATUS_ACTIVE.','.Model::STATUS_INACTIVE
        ]);

        return $rules;
    }

    protected function defaultValues()
    {
        return [
            'code' => '',
            'armsoft_code' => '',
            'city_id' => null,
            'sort_order' => 0,
            'show_status' => Model::STATUS_INACTIVE,
            'lat' => null,
            'lng' => null,
            'ml' => [
                'name' => '',
                'address' => '',
                'show_status' => Model::STATUS_INACTIVE,
            ]
        ];
    }
}
