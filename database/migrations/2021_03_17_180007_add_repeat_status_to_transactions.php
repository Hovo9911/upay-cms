<?php

use App\DB\Migration;
use App\DB\Blueprint;
use App\UPay\Transaction\Transaction;

class AddRepeatStatusToTransactions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->getSchemaBuilder()->table('transactions', function (Blueprint $table) {
            $table->enum('repeat_status', [
                Transaction::REPEAT_STATUS_PENDING,
                Transaction::REPEAT_STATUS_COMPLETED,
                Transaction::REPEAT_STATUS_SUCCEED,
                Transaction::REPEAT_STATUS_FAILED
            ])->nullable()->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $this->getSchemaBuilder()->table('transactions', function (Blueprint $table) {
            $table->dropColumn('repeat_status');
        });
    }
}
