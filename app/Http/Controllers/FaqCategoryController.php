<?php

namespace App\Http\Controllers;

use App\Http\Requests\FaqCategory\EditRequest;
use App\Http\Requests\FaqCategory\SearchRequest;
use App\Http\Requests\DeleteRequest;
use App\UPay\FaqCategory\FaqCategorySearch;
use App\UPay\Interfaces\FaqCategoryRepositoryInterface;
use App\UPay\Interfaces\LanguageRepositoryInterface;
use DB;
use Illuminate\Http\Request;

class FaqCategoryController extends Controller
{
    private $languageRepository;
    private $faqCategoryRepository;

    public function __construct(FaqCategoryRepositoryInterface $faqCategoryRepository,LanguageRepositoryInterface $languageRepository)
    {
        $this->faqCategoryRepository = $faqCategoryRepository;
        $this->languageRepository = $languageRepository;
    }

    public function index()
    {
        return view('website.faq_category.list');
    }

    public function edit(Request $request, $id = null)
    {
        if ($id != null) {
            $faqCategory = $this->faqCategoryRepository->findOrFail($id);
            $faqCategory->load([
                'ml',
            ]);

        } else {
            $faqCategory = $this->faqCategoryRepository->mock();
        }
        $faqCategory->ml = $faqCategory->ml->keyBy('lng_id');
        $languages = $this->languageRepository->all();

        return view('website.faq_category.edit')->with([
            'mode' => $id != null ? 'edit' : 'add',
            'faqCategory' => $faqCategory,
            'languages' => $languages,
        ]);
    }

    public function save(EditRequest $request, $id = null)
    {
        $data = $request->validated();

        if ($id != null) {
            $this->faqCategoryRepository->findOrFail($id);
            $faqCategory = $this->faqCategoryRepository->update($id, $data);
        } else {
            $faqCategory = $this->faqCategoryRepository->create($data);
        }

        return $this->sendOK([
            'message' => trans('upay.website.faq_category.saved.message'),
            'edit_url' => url('/website/faq-category/edit/'.$faqCategory->id),
            'list_url' => url('/website/faq-category/list')
        ]);
    }

    public function delete(DeleteRequest $request)
    {
        $this->faqCategoryRepository->delete($request->validated()['ids']);
        return $this->sendOK([
            'redirect_url' => url('/website/faq-category/list')
        ]);
    }

    public function getList(SearchRequest $request)
    {
        $searcher = new FaqCategorySearch($request->validated());

        return response()->json([
            'data' => $searcher->search(),
            'recordsTotal' => $searcher->totalCount(),
            'recordsFiltered' => $searcher->filteredCount(),
        ]);
    }
}
