<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>@lang('upay.login.page_title') @lang('upay.page_title.postfix')</title>
        {!! $jsTrans->output() !!}
        <link rel="stylesheet" href="{{mix('css/theme.css')}}"/>
    </head>
    <body class="gray-bg">
        <div class="middle-box text-center loginscreen animated fadeInDown">
            <div>
                <div>
                    <img class="gray-logo" src="{{url('upay/img/logo.png')}}" alt="UPay">
                </div>
                <h3>Welcome to UPay</h3>
                <p>Login in. To see it in action.</p>
                <form id="login-form" action="{{route('login')}}" method="POST" class="m-t" role="form">
                    <div class="form-group">
                        <input type="text" name="email" class="form-control" placeholder="@lang('upay.login.email.placeholder')">
                        <span class="form-text text-left text-danger form-error-text form-error-email"></span>
                    </div>
                    <div class="form-group">
                        <input type="password" name="password" class="form-control" placeholder="@lang('upay.login.password.placeholder')">
                        <span class="form-text text-left text-danger form-error-text form-error-password"></span>
                    </div>
                    <div class="form-group">
                        <span class="form-text text-left text-danger form-error-text form-error-login"></span>
                    </div>
                    {{csrf_field()}}
                    <button type="submit" class="btn btn-primary block full-width m-b submit-btn">@lang('upay.login.submit_btn')</button>
                    <a href="#" data-toggle="modal" data-target="#forgot-password-modal">@lang('upay.login.forgot_password')</a>
                </form>
            </div>
        </div>
        <div class="modal inmodal" id="forgot-password-modal" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content animated bounceInDown">
                    <form id="forgot-password-form" action="{{url('/forgot-password')}}" method="POST">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                            <h4 class="modal-title">@lang('upay.forgot_password.title')</h4>
                        </div>
                        <div class="modal-body">
                            <p>@lang('upay.forgot_password.description')</p>
                            <div class="form-group">
                                <label>@lang('upay.forgot_password.email')</label>
                                <input type="text" name="email" class="form-control">
                                <span class="form-text text-left text-danger form-error-text form-error-email"></span>
                            </div>
                            {{csrf_field()}}
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-white" data-dismiss="modal">@lang('upay.forgot_password.close_btn')</button>
                            <button type="submit" class="btn btn-primary submit-btn">@lang('upay.forgot_password.submit_btn')</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <script src="{{mix('js/lib.js')}}"></script>
        <script src="{{script_url('/upay/js/login.js')}}"></script>
    </body>
</html>
