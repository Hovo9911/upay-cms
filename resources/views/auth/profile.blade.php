@extends('layout')
@section('page_title') @lang('upay.profile.title') @stop
@section('content')
    <?php
    use App\UPay\Admin\Admin;
    $activeMenu = '';
    ?>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox ">
                    <div class="ibox-title">
                        <h1>@lang('upay.profile.title')</h1>
                    </div>
                    <div class="ibox-content">
                        @if($admin->password_reset_status == Admin::PASSWORD_RESET_STATUS_PENDING)
                            <div class="alert alert-danger">
                                <i class="fa fa-warning"></i> <strong>@lang('upay.admin.profile.change_password.info')</strong>
                            </div>
                        @elseif($admin->passwordExpired())
                            <div class="alert alert-danger">
                                <i class="fa fa-warning"></i> <strong>@lang('upay.admin.profile.password_expired.info')</strong>
                            </div>
                        @endif
                        <form id="change-password-form" action="{{url('/change-password')}}" method="POST">
                            <div class="form-group required row"><label class="col-lg-2 col-form-label">@lang('upay.profile.change_password.old')</label>
                                <div class="col-lg-5">
                                    <input type="password" name="old_password" class="form-control">
                                    <span class="form-text text-danger form-error-text form-error-old_password"></span>
                                </div>
                            </div>
                            <div class="form-group required row">
                                <label class="col-lg-2 col-form-label">@lang('upay.profile.change_password.new') <span class="align-self-center hint-tooltip"><i class="fa fa-lg fa-question-circle" data-toggle="tooltip" data-placement="right" title="" data-original-title="@lang('upay.change_password.password.hint')"></i></span></label>
                                <div class="col-lg-5">
                                    <input type="password" name="password" class="form-control">
                                    <span class="form-text text-danger form-error-text form-error-password"></span>
                                </div>
                            </div>
                            <div class="form-group required row"><label class="col-lg-2 col-form-label">@lang('upay.profile.change_password.confirm')</label>
                                <div class="col-lg-5">
                                    <input type="password" name="password_confirmation" class="form-control">
                                    <span class="form-text text-danger form-error-text form-error-password_confirmation"></span>
                                </div>
                            </div>
                            {{csrf_field()}}
                            <hr/>
                            <div class="form-group row">
                                <div class="col-lg-4 col-lg-offset-2">
                                    <button class="btn btn-primary submit-btn" type="submit">@lang('upay.general.buttons.save')</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="{{script_url('upay/js/profile.js')}}"></script>
@stop
