<?php

namespace App\Http\Requests\Faq;

use App\Http\Requests\Request;
use App\Model;
use App\UPay\FaqCategory\FaqCategory;

class EditRequest extends Request
{
    protected $module = 'faq';

    public function rules()
    {

        $rules = [
            'category_id' => 'required|integer|in:' . implode(',', FaqCategory::getIds()),
            'sort_order' => 'required|integer',
            'show_status' => 'in:' . Model::STATUS_ACTIVE . ',' . Model::STATUS_INACTIVE,
            'ml' => 'required|array',
        ];

        $rules = $this->checkStatusForRequired($rules, request()->input('show_status'));

        $rules = $this->addMlRules($rules, [
            'question' => 'required|max:255',
            'answer' => 'required|max:50000',
            'show_status' => 'in:' . Model::STATUS_ACTIVE . ',' . Model::STATUS_INACTIVE
        ]);

        return $rules;
    }

    protected function defaultValues()
    {
        return [
            'category_id' => null,
            'sort_order' => '',
            'show_status' => Model::STATUS_INACTIVE,
            'ml' => [
                'question' => '',
                'answer' => '',
                'show_status' => Model::STATUS_INACTIVE,
            ]
        ];
    }

}
