@extends('layout')
@section('page_title') @lang('upay.report_transactions.list_title') @stop
@section('content')
<?php
$activeMenu = 'report_transactions';
$jsTrans->add([
    'upay.transaction.payment_type.cash',
    'upay.transaction.payment_type.non_cash',
]);
?>
<script>
    var $permissions = { };
</script>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-title">
                    <h1>@lang('upay.report_transactions.list_title')</h1>
                </div>
                <div class="ibox-content">
                    <div class="ibox">
                        <div class="ibox-title">
                            <h5>@lang('upay.general.list.filters.title')</h5>
                            <div class="ibox-tools"><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></div>
                        </div>
                        <div class="ibox-content">
                            <form id="search-form" method="GET" action="{{url('/report-transactions/get-list')}}" export-action="{{url('/report-transactions/export')}}" role="form">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group row">
                                            <label class="col-lg-3 col-form-label text-right">@lang('upay.report_transactions.date')</label>
                                            <div class="col-lg-9">
                                                <div class="input-group date form-datepicker">
                                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input name="filters[date_from]" type="text" class="form-control" value="{{date('Y-m-d', strtotime('yesterday'))}}">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group row">
                                            <span class="col-lg-2 col-form-label text-right fa fa-long-arrow-right"></span>
                                            <label class="col-lg-1 col-form-label text-right"></label>
                                            <div class="col-lg-9">
                                                <div class="input-group date form-datepicker">
                                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input name="filters[date_to]" type="text" class="form-control" value="{{date('Y-m-d')}}">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group row">
                                            <label class="col-lg-3 col-form-label text-right">@lang('upay.report_transactions.amount')</label>
                                            <div class="col-lg-9">
                                                <input type="text" name="filters[amount_from]" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group row">
                                            <span class="col-lg-2 col-form-label text-right fa fa-long-arrow-right"></span>
                                            <label class="col-lg-1 col-form-label text-right"></label>
                                            <div class="col-lg-9">
                                                <input type="text" name="filters[amount_to]" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group row">
                                            <label class="col-lg-3 col-form-label text-right">@lang('upay.report_closing.branch')</label>
                                            <div class="col-lg-9">
                                                <select name="filters[branch]" class="form-control m-b select-picker">
                                                    <option value="">@lang('upay.general.dropdown.select')</option>
                                                    @foreach($branches as $branch)
                                                        <option value="{{$branch->id}}">{{$branch->name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-6">
                                        <div class="form-group row">
                                            <label class="col-lg-3 col-form-label text-right">@lang('upay.report_closing.cashbox')</label>
                                            <div class="col-lg-9">
                                                <select name="filters[cashbox]" class="form-control m-b select-picker">
                                                    <option value="">@lang('upay.general.dropdown.select')</option>
                                                    @foreach($cashboxes as $cashbox)
                                                        <option value="{{$cashbox->id}}">{{$cashbox->name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group row">
                                            <label class="col-lg-3 col-form-label text-right">@lang('upay.report_transactions.service')</label>
                                            <div class="col-lg-9">
                                                <select class="form-control m-b select-picker" name="filters[service]">
                                                    <option value="">@lang('upay.general.dropdown.select')</option>
                                                    @foreach($services as $service)
                                                        <option value="{{$service->id}}">{{$service->name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group row">
                                            <label class="col-lg-3 col-form-label text-right">@lang('upay.report_transactions.code')</label>
                                            <div class="col-lg-9">
                                                <input type="text" name="filters[code]" class="form-control">
                                            </div>
                                        </div>
                                    </div>

                                </div>

                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group row">
                                            <label class="col-lg-3 col-form-label text-right">@lang('upay.report_transactions.receipt_id')</label>
                                            <div class="col-lg-9">
                                                <input type="text" name="filters[receipt_id]" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group row">
                                            <label class="col-lg-3 col-form-label text-right">@lang('upay.report_closing.operator')</label>
                                            <div class="col-lg-9">
                                                <select name="filters[operator]" class="form-control m-b select-picker">
                                                    <option value="">@lang('upay.general.dropdown.select')</option>
                                                    @foreach($operators as $operator)
                                                        <option value="{{$operator->id}}">{{$operator->first_name}} {{$operator->last_name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>


                                </div>

                                <div class="row">
                                    <div class="col-lg-12">
                                        <button disabled class="btn btn-info float-right m-t-n-xs submit-btn" type="submit"><i class="fa fa-filter"></i> @lang('upay.general.list.filters.btn')</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table id="search-table" style="width: 100%" class="table table-striped table-bordered table-hover" >
                            <thead>
                                <tr>
                                    <th data-col="external_id" data-orderable="false">@lang('upay.report_transactions.external_id')</th>
                                    <th data-col="created_at">@lang('upay.report_transactions.date')</th>
                                    <th data-col="time" data-orderable="false">@lang('upay.report_transactions.time')</th>
                                    <th data-col="cashbox" data-orderable="false">@lang('upay.report_transactions.cashbox')</th>
                                    <th data-col="service" data-orderable="false">@lang('upay.report_transactions.service')</th>
                                    <th data-col="amount">@lang('upay.report_transactions.amount')</th>
                                    <th data-col="contact_number" data-orderable="false">@lang('upay.report_transactions.contact_number')</th>
                                    <th data-col="cash" data-orderable="false">@lang('upay.report_transactions.cash')</th>
                                    <th data-col="pos_auth_code" data-orderable="false">@lang('upay.report_transactions.pos_auth_code')</th>
                                    <th data-col="code" data-orderable="false">@lang('upay.report_transactions.code')</th>
                                    <th data-col="receipt_id" data-orderable="false">@lang('upay.report_transactions.receipt_id')</th>
                                    <th data-col="commission" data-orderable="false">@lang('upay.report_transactions.fee')</th>
                                    <th data-col="is_cancelled" data-orderable="false">@lang('upay.report_transactions.is_cancelled')</th>
                                    <th data-col="operator" data-orderable="false">@lang('upay.report_transactions.operator')</th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="{{script_url('upay/js/report/transactions.js')}}"></script>
@stop
