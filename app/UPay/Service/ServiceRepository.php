<?php

namespace App\UPay\Service;

use App\GUploader;
use App\UPay\Interfaces\ServiceRepositoryInterface;
use DB;
use Illuminate\Database\Eloquent\Collection;

class ServiceRepository implements ServiceRepositoryInterface
{
    public function create($data): Service
    {
        $service = new Service($data);
        DB::beginTransaction();
        $this->saveLogo($data['logo']);
        $service->save();
        $this->saveMl($service, $data['ml']);
        $this->saveCommissions($service, $data);
        DB::commit();
        return $service;
    }

    public function mock(): Service
    {
        return new Service([
            'provider_id' => null,
            'branch_account_number' => '',
            'branch_commission_account_number' => '',
            'mobile_account_number' => '',
            'mobile_commission_account_number' => '',
            'account_min' => '',
            'account_max' => '',
            'use_fixed_amount' => false,
            'is_encashment' => false,
            'encashment_type' => Service::ENCASHMENT_IN,
            'logo' => '',
            'hidden' => false,
            'for_mobile' => true,
            'for_ops' => true,
            'show_on_homepage' => false,
            'is_new' => false,
            'type' => Service::TYPE_PAY,
            'adapter' => '',
            'sort_order' => '',
            'show_status' => Service::STATUS_ACTIVE,
        ]);
    }

    public function findOrFail($id, $withMl = false): Service
    {
        $service = Service::where('id', $id)->where('services.show_status', '!=', Service::STATUS_DELETED);
        if ($withMl) {
            $service->joinMl();
        }
        $service = $service->first();

        if (empty($service)) {
            abort(404);
        }
        return $service;
    }

    public function update($id, $data): Service
    {
        $service = $this->findOrFail($id);
        DB::beginTransaction();
        $this->saveLogo($data['logo'], $service->logo);
        $service->update($data);
        $this->saveMl($service, $data['ml']);
        $this->saveCommissions($service, $data);
        DB::commit();
        return $service;
    }

    public function saveMl(Service $service, array $mls): void
    {
        foreach ($mls as $lngId => $ml) {
            $service->ml()->updateOrCreate(
                ['service_id' => $service->id, 'lng_id' => $lngId],
                [
                    'name' => $ml['name'],
                    'payment_purpose' => $ml['payment_purpose'],
                    'invoice_note' => $ml['invoice_note'],
                    'title' => $ml['title'],
                    'description' => $ml['description'],
                    'order_purpose_password' => $ml['order_purpose_password'],
                    'order_reason' => $ml['order_reason'],
                    'order_received' => $ml['order_received'],
                    'show_status' => $ml['show_status'],
                ]
            );
        }
    }

    public function saveCommissions(Service $service, array $data): void
    {
        $service->commissions()->update(['show_status' => Commission::STATUS_DELETED]);
        CommissionValue::whereIn('commission_id', $service->commissions()->select('id')->get()->pluck('id'))->delete();

        foreach (Commission::getTypes() as $type) {
            foreach (Commission::getSources() as $src) {
                if (!empty($data['commissions_'.$type.'_'.$src])) {
                    $commission = $service->commissions()->updateOrCreate(
                        [
                            'service_id' => $service->id,
                            'source' => $src,
                            'type' => $type,
                        ],
                        [
                            'source' => $src,
                            'type' => $type,
                            'show_status' => $data['commissions_'.$type.'_'.$src.'_show_status']
                        ]
                    );
                    $values = [];
                    foreach($data['commissions_'.$type.'_'.$src] as $key => $vals) {
                        $values []= [
                            'commission_id' => $commission->id,
                            'type' => $vals['type'],
                            'min' => $vals['min'],
                            'max' => $vals['max'],
                            'amount' => $vals['amount'],
                            'percent' => $vals['percent'],
                            'date' => date('Y-m-d H:i:s')
                        ];
                    }
                    if (!empty($values)) {
                        CommissionValue::insert($values);
                    }
                }
            }
        }
    }

    public function approveCommissions($id): void
    {
        $service = $this->findOrFail($id);
        DB::beginTransaction();
        $commissions = $service->commissions()->select('id', 'show_status')->get();
        CommissionValueApproved::whereIn('commission_id', $commissions->pluck('id'))->update(['status' => CommissionValueApproved::STATUS_ARCHIVED]);
        $values = CommissionValue::whereIn('commission_id', $commissions->where('show_status', Commission::STATUS_ACTIVE)->pluck('id'))->get();
        $inserts = [];
        foreach($values as $value) {
            $inserts[] = [
                'commission_id' => $value->commission_id,
                'type' => $value->type,
                'min' => $value->min,
                'max' => $value->max,
                'amount' => $value->amount,
                'percent' => $value->percent,
                'date' => date('Y-m-d H:i:s'),
                'status' => CommissionValueApproved::STATUS_ACTIVE,
            ];
        }
        CommissionValueApproved::insert($inserts);
        DB::commit();
    }

    public function saveLogo($newLogo, $oldLogo = null): void
    {
        if (!empty($newLogo) && (is_null($oldLogo) || (!is_null($oldLogo) && $newLogo != $oldLogo))) {
            $gUploader = new GUploader('service_logo');
            $gUploader->storePerm($newLogo);
        }
    }

    public function all(): Collection
    {
        return Service::joinMl()
            ->where('services.show_status', Service::STATUS_ACTIVE)
            ->where('ml.show_status', Service::STATUS_ACTIVE)
            ->get();
    }
}
