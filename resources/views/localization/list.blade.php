@extends('layout')
@section('page_title') @lang('upay.localization.list_title') @stop
@section('content')
<?php
    $activeMenu = 'localization';
    $canEdit = \Auth::user()->hasPermission('localization.edit');
    $canDelete = \Auth::user()->hasPermission('localization.delete');
?>
<script>
    var $permissions = { edit: {{$canEdit ? '1' : '0'}}, delete: {{$canDelete ? '1' : '0'}} };
</script>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-title">
                    <h1>@lang('upay.localization.list_title')</h1>
                    <div class="ibox-tools">
                        @if($canDelete)<button onclick="gDelete();return false;" class="btn btn-w-m btn-danger"><i class="fa fa-remove"></i> @lang('upay.general.buttons.delete')</button>@endif
                        @if($canEdit)<button onclick="addLocalization();" class="btn btn-w-m btn-primary"><i class="fa fa-plus"></i> @lang('upay.general.buttons.add_new')</button>@endif
                    </div>
                </div>
                <div class="ibox-content">
                    <div class="ibox">
                        <div class="ibox-title">
                            <h5>@lang('upay.general.list.filters.title')</h5>
                            <div class="ibox-tools"><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></div>
                        </div>
                        <div class="ibox-content">
                            <form id="search-form" method="GET" action="{{url('/localization/get-list')}}" role="form">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group row">
                                            <label class="col-lg-3 col-form-label text-right">@lang('upay.general.id')</label>
                                            <div class="col-lg-9">
                                                <input type="text" name="filters[id]" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group row">
                                            <label class="col-lg-3 col-form-label text-right">@lang('upay.localization.group')</label>
                                            <div class="col-lg-9">
                                                <select class="form-control m-b select-picker" name="filters[group]">
                                                    @foreach($groups as $group)
                                                        <option value="{{$group}}">@lang('upay.localization.group.'.$group)</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group row">
                                            <label class="col-lg-3 col-form-label text-right">@lang('upay.localization.key')</label>
                                            <div class="col-lg-9">
                                                <input type="text" name="filters[key]" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group row">
                                            <label class="col-lg-3 col-form-label text-right">@lang('upay.localization.value')</label>
                                            <div class="col-lg-9">
                                                <input type="text" name="filters[value]" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <button disabled class="btn btn-info float-right m-t-n-xs submit-btn" type="submit"><i class="fa fa-filter"></i> @lang('upay.general.list.filters.btn')</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table id="search-table" style="width: 100%" class="table table-striped table-bordered table-hover" >
                            <thead>
                                <tr>
                                    <th data-col="id" data-col-name="check" check-col></th>
                                    <th data-col="id">@lang('upay.general.id')</th>
                                    <th data-col="key">@lang('upay.localization.key')</th>
                                    <th data-col="value">@lang('upay.localization.value')</th>
                                    <th data-col="id" data-col-name="actions" action-col>@lang('upay.general.list.actions')</th>
                                    <th data-col="group" class="d-none"></th>
                                    <th data-col="ml" class="d-none"></th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal inmodal fade" id="edit-localization-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <form id="save-form" method="POST" action-url="{{url('/localization/save')}}" role="form">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 add-title="@lang('upay.localization.modal.add_title')" edit-title="@lang('upay.localization.modal.edit_title')" view-title="@lang('upay.localization.modal.view_title')" class="modal-title"></h4>
                </div>
                <div class="modal-body">
                    <div class="form-group required row">
                        <label class="col-lg-3 col-form-label text-right">@lang('upay.localization.group')</label>
                        <div class="col-lg-9">
                            <select class="form-control m-b select-picker" name="group">
                                @foreach($groups as $group)
                                    <option value="{{$group}}">@lang('upay.localization.group.'.$group)</option>
                                @endforeach
                            </select>
                            <span class="form-text text-danger form-error-text form-error-group"></span>
                        </div>
                    </div>
                    <div class="form-group required row">
                        <label class="col-lg-3 col-form-label text-right">@lang('upay.localization.key')</label>
                        <div class="col-lg-9">
                            <input type="text" name="key" class="form-control">
                            <span class="form-text text-danger form-error-text form-error-key"></span>
                        </div>
                    </div>
                    @foreach($languages as $lng)
                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label text-right">@lang('upay.localization.value') ({{$lng->name}})</label>
                            <div class="col-lg-9">
                                <textarea rows="2" name="ml[{{$lng->id}}][value]" class="form-control"></textarea>
                                <span class="form-text text-danger form-error-text form-error-ml_{{$lng->id}}_value"></span>
                            </div>
                        </div>
                    @endforeach
                </div>
                {{csrf_field()}}
                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">@lang('upay.general.popup.close')</button>
                    <button type="submit" class="btn btn-primary submit-btn">@lang('upay.general.buttons.save')</button>
                </div>
            </div>
        </form>
    </div>
</div>
@if($canDelete) @include('components.delete', ['url' => url('/localization/delete')]) @endif
<script src="{{script_url('upay/js/localization.js')}}"></script>
@stop
