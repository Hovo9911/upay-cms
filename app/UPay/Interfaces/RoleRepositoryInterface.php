<?php
namespace App\UPay\Interfaces;

use App\UPay\Role\Role;
use Illuminate\Support\Collection;

interface RoleRepositoryInterface
{
    public function create($data): Role;

    public function mock(): Role;

    public function findOrFail($id): Role;

    public function update($id, $data): Role;

    public function delete($ids): int;

    public function all(): Collection;

    public function foundAll($ids): bool;
}
