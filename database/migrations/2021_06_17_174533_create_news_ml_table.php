<?php

use App\DB\Migration;
use App\DB\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNewsMlTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->getSchemaBuilder()->create('news_ml', function (Blueprint $table) {
            $table->integer('news_id')->unsigned();
            $table->integer('lng_id')->unsigned();
            $table->string('title');
            $table->string('alias')->default('');
            $table->string('short_description');
            $table->text('body');
            $table->showStatus();
            $table->primary(['news_id', 'lng_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('news_ml');
    }
}
