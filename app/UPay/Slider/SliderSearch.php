<?php

namespace App\UPay\Slider;

use App\Search;

class SliderSearch extends Search
{
    protected $orderables = [
        'id',
        'title',
        'sort_order',
        'show_status',
    ];

    protected function query()
    {
        $filters = $this->filters;

        return  Slider::joinMl()->select(
            'sliders.id',
            'ml.title',
            'sliders.show_status'
        )
            ->when(!empty($filters['id']), function($query) use ($filters) {
                $query->where('sliders.id', $filters['id']);
            })
            ->when(!empty($filters['title']), function($query) use ($filters) {
                $query->where('ml.title', 'ILIKE', '%'.$filters['title'].'%');
            })
            ->when(!empty($filters['show_status']), function($query) use ($filters) {

                $query->where('sliders.show_status', $filters['show_status']);
            }, function($query) use ($filters) {
                $query->where('sliders.show_status', '!=', Slider::STATUS_DELETED);
            });


    }

    public function totalCount()
    {
        return Slider::notDeleted()->count();
    }
}
