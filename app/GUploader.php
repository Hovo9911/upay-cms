<?php

namespace App;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

class GUploader
{
    protected $conf;

    public function __construct($name)
    {
        $conf = config('guploader.'.$name);
        if (empty($conf)) {
            abort(404, 'Uploader is not configured');
        }
        $this->conf = $conf;
    }

    public function getConfig()
    {
        return $this->conf;
    }

    public function isMultiple()
    {
        return $this->conf['multiple'];
    }

    public function getAcceptTypes()
    {
        $types = $this->conf['allowed_types'];
        $groups = [];
        foreach ($types as $type) {
            $groups []= implode(',', $type);
        }
        return implode(',', $groups);
    }

    public function getFilename($path)
    {
        return basename($path);
    }

    public function getFileUrl($name)
    {
        $conf = $this->conf;
        if (!Storage::disk($conf['disk'])->exists($conf['perm_path'].$name)) {
            return '';
        }
        return url($conf['disk'].'/'.$conf['perm_path'].$name);
    }

    public function checkType($file)
    {
        $conf = $this->conf;
        $mimeType = $file->getMimeType();
        $attachmentType = null;
        foreach ($conf['allowed_types'] as $type => $mimeTypes) {
            if (in_array($mimeType, $mimeTypes)) {
                return $type;
                break;
            }
        }
        return false;
    }

    public function checkFilename($file)
    {
        if (!$this->conf['check_filename']) {
            return true;
        }
        $originalName = $file->getClientOriginalName();
        $filename = basename($originalName, '.' . pathinfo($originalName, PATHINFO_EXTENSION));
        return preg_match('/^[a-z0-9]+$/i', $filename);
    }

    public function checkSize($file)
    {
        $size = $file->getClientSize();
        return ($size < $this->conf['max_size']);
    }

    public function storeTemp($file)
    {
        $conf = $this->conf;
        $originalName = $file->getClientOriginalName();
        $filename = basename($originalName, '.' . pathinfo($originalName, PATHINFO_EXTENSION));
        $filename = mb_ereg_replace("([^\w\s\d\-_~,;\[\]\(\).])", '', $filename);
        $filename = str_replace(' ', '_', $filename);

        $extension = $file->getClientOriginalExtension();
        $folder = microtime(1) % 1000;
        $filePath = $this->getFilePath($this->conf['tmp_path'], $folder, $filename . '.' . $extension);
        $path = $conf['tmp_path'] . $filePath;
        Storage::disk($conf['disk'])->put($path, File::get($file));

        return [
            'file_path' => $path,
            'tmp_name' => $filePath,
            'file_name' => $filename . '.' . $extension,
        ];
    }

    public function getTempFilepath($tmpName)
    {
        $conf = $this->conf;
        if (!Storage::disk($conf['disk'])->exists($conf['tmp_path'].$tmpName)) {
            return false;
        }
        return Storage::disk($conf['disk'])->path($conf['tmp_path'].$tmpName);
    }

    private function getFilePath($pre, $folder, $file)
    {
        $filePath = $pre . $folder . '/' . $file;
        if (Storage::disk($this->conf['disk'])->exists($filePath)) {
            return $this->getFilePath($pre, $folder + 1, $file);
        }
        return $folder . '/' . $file;
    }

    public function storePerm($tmpName)
    {
        $conf = $this->conf;
        $permPath = $conf['perm_path'];
        if (!Storage::disk($conf['disk'])->exists($conf['tmp_path'].$tmpName)) {
            abort(404);
        }
        $permPath .= $tmpName;

        if (Storage::disk($conf['disk'])->exists($permPath)) {
            list($folder, $file) = explode('/', $tmpName);
            $permPath = $this->getFilePath($this->conf['perm_path'], $folder, $file);
        }
        if (!Storage::disk('uploads')->copy($conf['tmp_path'].$tmpName, $permPath)) {
            abort(500, trans('upay.general.uploader.cant_store'));
        }
    }
}
