<?php

namespace App\UPay\Operator;

use \App\Model;

class Operator extends Model {

    const PASSWORD_RESET_STATUS_PENDING = 'pending';
    const PASSWORD_RESET_STATUS_CONFIRMED = 'confirmed';

    protected $fillable = [
        'first_name',
        'last_name',
        'email',
        'phone',
        'password',
        'show_status',
        'password_reset_status'
    ];

    protected $hidden = [
        'password',
    ];

}
