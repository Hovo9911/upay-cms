<?php

return [
    'provider_logo' => [
        'allowed_types' => [
            'photo' => [
                'image/jpeg',
                'image/png',
                'image/gif',
            ]
        ],
        'max_size' => 4 * 1024 * 1024, // bytes
        'disk' => 'uploads',
        'tmp_path' => 'temp/',
        'perm_path' => 'files/',
        'multiple' => false,
        'check_filename' => false,
    ],
    'service_logo' => [
        'allowed_types' => [
            'photo' => [
                'image/jpeg',
                'image/png',
                'image/gif',
            ]
        ],
        'max_size' => 4 * 1024 * 1024, // bytes
        'disk' => 'uploads',
        'tmp_path' => 'temp/',
        'perm_path' => 'files/',
        'multiple' => false,
        'check_filename' => false,
    ],
    //this is not uploaded from cms,, just to get the url of files uploaded from api
    'cancellation' => [
        'allowed_types' => [
            'photo' => [
                'image/jpeg',
                'image/png',
            ],
            'files' => [
                'application/pdf',
            ]
        ],
        'max_size' => 5 * 1024 * 1024, // bytes
        'disk' => 'uploads',
        'tmp_path' => 'temp/',
        'perm_path' => 'files/',
        'multiple' => true,
        'check_filename' => false,
    ],
    'website_slider_image' => [
        'allowed_types' => [
            'photo' => [
                'image/jpeg',
                'image/png',
                'image/gif',
            ]
        ],
        'max_size' => 700 * 1024, // bytes,700kb
        'disk' => 'uploads',
        'tmp_path' => 'temp/',
        'perm_path' => 'files/',
        'multiple' => false,
        'check_filename' => false,
    ],
    'website_news_image' => [
        'allowed_types' => [
            'photo' => [
                'image/jpeg',
                'image/png',
                'image/gif',
            ]
        ],
        'max_size' => 4 * 1024 * 1024, // bytes ,4MB
        'disk' => 'uploads',
        'tmp_path' => 'temp/',
        'perm_path' => 'files/',
        'multiple' => false,
        'check_filename' => false,
    ],
    'website_career_image' => [
        'allowed_types' => [
            'photo' => [
                'image/jpeg',
                'image/png',
                'image/gif',
            ]
        ],
        'max_size' => 4 * 1024 * 1024, // bytes ,4MB
        'disk' => 'uploads',
        'tmp_path' => 'temp/',
        'perm_path' => 'files/',
        'multiple' => false,
        'check_filename' => false,
    ],
];
