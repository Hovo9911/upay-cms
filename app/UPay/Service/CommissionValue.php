<?php

namespace App\UPay\Service;

use \App\Model;

class CommissionValue extends Model
{
    const TYPE_FIXED = 'fixed';
    const TYPE_PERCENT = 'percent';

    protected $table = 'service_commission_values';
    public $timestamps = false;

    protected $fillable = [
        'commission_id',
        'type',
        'min',
        'max',
        'amount',
        'percent',
        'date',
    ];
}
