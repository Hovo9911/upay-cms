<?php

use App\DB\Migration;
use App\DB\Blueprint;

class AddArmsoftCodeToBranchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->getSchemaBuilder()->table('branches', function (Blueprint $table) {
            $table->string('armsoft_code')->default('');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $this->getSchemaBuilder()->table('branches', function (Blueprint $table) {
            $table->dropColumn('armsoft_code');
        });
    }
}
