<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>@lang('upay.reset_password.page_title') @lang('upay.page_title.postfix')</title>
    {!! $jsTrans->output() !!}
    <link rel="stylesheet" href="{{mix('css/theme.css')}}"/>
</head>
<body class="gray-bg">
<div class="middle-box text-center loginscreen animated fadeInDown">
    <div>
        <div>
            <img class="gray-logo" src="{{url('upay/img/logo.png')}}" alt="UPay">
        </div>
        <h3>@lang('upay.reset_password.page_title')</h3>
        <p>@lang('upay.reset_password.description')</p>
        <form id="reset-password-form" action="{{url('/forgot-password/reset/'.$hash)}}" method="POST" class="m-t" role="form">
            <div class="form-group position-relative">
                <input type="password" name="password" class="form-control" placeholder="@lang('upay.reset_password.password')">
                <span class="form-text text-left text-danger form-error-text form-error-password"></span>
                <span class="align-self-center hint-tooltip hint-tooltip-right"><i class="fa fa-lg fa-question-circle" data-toggle="tooltip" data-placement="right" title="" data-original-title="@lang('upay.change_password.password.hint')"></i></span>
            </div>
            <div class="form-group">
                <input type="password" name="password_confirmation" class="form-control" placeholder="@lang('upay.reset_password.password_confirm')">
                <span class="form-text text-left text-danger form-error-text form-error-password_confirmation"></span>
            </div>
            {{csrf_field()}}
            <button type="submit" class="btn btn-primary block full-width m-b submit-btn">@lang('upay.reset_password.submit_btn')</button>
        </form>
    </div>
</div>

<script src="{{mix('js/lib.js')}}"></script>
<script src="{{script_url('upay/js/layout.js')}}"></script>
<script src="{{script_url('/upay/js/reset_password.js')}}"></script>
</body>
</html>
