<?php

use App\DB\Migration;
use App\DB\Blueprint;
use App\UPay\Admin\Admin;

class CreateAdminsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->getSchemaBuilder()->create('admins', function (Blueprint $table) {
            $table->increments('id');
            $table->string('email');
            $table->string('name');
            $table->string('password');
            $table->enum('password_reset_status', [Admin::PASSWORD_RESET_STATUS_PENDING, Admin::PASSWORD_RESET_STATUS_CONFIRMED])->nullable()->default(null);
            $table->showStatus();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admins');
    }
}
