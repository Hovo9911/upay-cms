<?php
namespace App\Mailer;
use App\Model;

class Email extends Model
{
	const STATUS_PENDING = 'pending';
	const STATUS_SENT = 'sent';
	const STATUS_FAILED = 'failed';
	const STATUS_TRY_LATER = 'try_later';

	protected $table = 'emails';

	protected $fillable = [
		'priority',
		'to',
		'to_name',
		'from',
		'from_name',
		'reply_to',
		'reply_to_name',
		'cc',
		'bcc',
		'subject',
		'body',
		'attachments',
		'add_date',
		'sent_date',
		'status',
		'log'
	];
}
