<?php

namespace App\UPay\Provider;

use App\GUploader;
use App\UPay\Interfaces\ProviderRepositoryInterface;
use DB;
use Illuminate\Database\Eloquent\Collection;

class ProviderRepository implements ProviderRepositoryInterface
{
    public function create($data): Provider
    {
        $provider = new Provider($data);
        DB::beginTransaction();
        $this->saveLogo($data['logo']);
        $provider->save();
        $this->saveMl($provider, $data['ml']);
        DB::commit();
        return $provider;
    }

    public function mock(): Provider
    {
        return new Provider([
            'parent_id' => null,
            'logo' => '',
            'type' => Provider::TYPE_PAYMENT,
            'sort_order' => '',
            'show_status' => Provider::STATUS_ACTIVE,
        ]);
    }

    public function findOrFail($id): Provider
    {
        $provider = Provider::where('id', $id)->notDeleted()->first();
        if (empty($provider)) {
            abort(404);
        }
        return $provider;
    }

    public function update($id, $data): Provider
    {
        $provider = $this->findOrFail($id);
        DB::beginTransaction();
        $this->saveLogo($data['logo'], $provider->logo);
        $provider->update($data);
        $this->saveMl($provider, $data['ml']);
        DB::commit();
        return $provider;
    }

    public function saveMl(Provider $provider, array $mls): void
    {
        foreach ($mls as $lngId => $ml) {
            $provider->ml()->updateOrCreate(
                ['provider_id' => $provider->id, 'lng_id' => $lngId],
                [
                    'name' => $ml['name'],
                    'show_status' => $ml['show_status'],
                ]
            );
        }
    }

    public function parents(int $except = null): Collection
    {
        return Provider::joinMl()
            ->where('providers.show_status', '!=', Provider::STATUS_DELETED)
            ->whereNull('parent_id')
            ->when(!is_null($except), function($q) use ($except){
                $q->where('id', '!=', $except);
            })
            ->sorted()->get();
    }

    public function all(): Collection
    {
        return Provider::joinMl()
            ->where('providers.show_status', '!=', Provider::STATUS_DELETED)
            ->whereNull('parent_id')
            ->sorted()
            ->with('children')
            ->get();
    }

    public function found(int $id): bool
    {
        return !empty(Provider::where('id', $id)->notDeleted()->first());
    }

    public function hasChild(int $id): bool
    {
        return !empty(Provider::where('parent_id', $id)->notDeleted()->first());
    }

    public function saveLogo($newLogo, $oldLogo = null): void
    {
        if (!empty($newLogo) && (is_null($oldLogo) || (!is_null($oldLogo) && $newLogo != $oldLogo))) {
            $gUploader = new GUploader('provider_logo');
            $gUploader->storePerm($newLogo);
        }
    }
}
