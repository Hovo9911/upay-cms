<?php

namespace App\Http\Requests\Provider;

use App\Http\Requests\DatatableSearchRequest;
use App\Model;

class SearchRequest extends DatatableSearchRequest
{
    public function rules()
    {
        return [
            'filters.id' => 'integer',
            'filters.name' => 'string|max:255',
            'filters.parent' => 'integer',
            'filters.show_status' => 'in:'.Model::STATUS_ACTIVE.','.Model::STATUS_INACTIVE,
            'order' => 'array',
            'start' => 'integer',
        ];
    }
}
