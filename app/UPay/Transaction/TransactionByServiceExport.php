<?php

namespace App\UPay\Transaction;

use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithCustomValueBinder;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use PhpOffice\PhpSpreadsheet\Cell\Cell;
use PhpOffice\PhpSpreadsheet\Cell\DataType;

class TransactionByServiceExport extends TransactionByServiceSearch implements FromQuery, WithMapping, WithHeadings, WithCustomValueBinder
{
    use Exportable;

    public function query()
    {
        $query = parent::query();
        $this->setOrdering($query);
        return $query;
    }

    public function headings(): array
    {
        return [
            trans('upay.report_services.service'),
            trans('upay.report_services.amount'),
            trans('upay.report_services.commission'),
            trans('upay.report_services.count'),
        ];
    }

    public function map($item): array
    {
        return [
            $item->service->name,
            $item->amount,
            $item->commission,
            $item->count,
        ];
    }

    public function bindValue(Cell $cell, $value)
    {
        $cell->setValueExplicit($value, DataType::TYPE_STRING);
        return true;
    }
}
