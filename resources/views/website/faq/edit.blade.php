<?php
use App\Model;
$activeMenu = 'website-faq';
$canDelete = \Auth::user()->hasPermission('faq.delete');
$canEdit = \Auth::user()->hasPermission('faq.edit');
$title = 'upay.faq.' . ($canEdit ? $mode : 'view') . '_title';
$disabled = $canEdit ? '' : 'disabled';

?>
@extends('layout')
@section('page_title') @lang($title) @stop
@section('content')
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox ">
                    <div class="ibox-title">
                        <h1>@lang($title)</h1>
                    </div>
                    <div class="ibox-content">
                        <form id="save-form" action="{{url('/website/faq/save/'.($faq->id ?? ''))}}"
                              method="POST">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="tabs-container">
                                        <ul class="nav nav-tabs" role="tablist">
                                            <li><a class="nav-link active" data-toggle="tab"
                                                   href="#tab-general">@lang('upay.tab.general')</a></li>
                                            @foreach($languages as $lng)
                                                <li><a class="nav-link" data-toggle="tab"
                                                       href="#tab-lng-{{$lng->id}}">{{$lng->name}}</a></li>
                                            @endforeach
                                        </ul>
                                        <div class="tab-content">
                                            <div role="tabpanel" id="tab-general" class="tab-pane active">
                                                <div class="panel-body">

                                                    <div class="form-group required row"><label class="col-lg-2 col-form-label">@lang('upay.website.faq.category_type')</label>
                                                        <div class="col-lg-10">
                                                            <select name="category_id" {{$disabled}} class="form-control m-b select-picker">
                                                                <option value="">@lang('upay.general.dropdown.select')</option>
                                                                @foreach($faqCategories as $faqCategory)
                                                                    <option value="{{$faqCategory->id}}" @if($faq->category_id == $faqCategory->id) selected="selected" @endif>{{$faqCategory->title}}</option>
                                                                @endforeach
                                                            </select>
                                                            <span class="form-text text-danger form-error-text form-error-category_id"></span>
                                                        </div>
                                                    </div>
                                                    <div class="form-group required row"><label
                                                            class="col-lg-2 col-form-label">@lang('upay.general.sort_order')</label>
                                                        <div class="col-lg-2">
                                                            <input type="text" name="sort_order"
                                                                   value="{{$faq->sort_order}}"
                                                                   {{$disabled}} class="form-control">
                                                            <span
                                                                class="form-text text-danger form-error-text form-error-sort_order"></span>
                                                        </div>
                                                    </div>

                                                    <div class="form-group row"><label for="general-status"
                                                                                       class="col-lg-2 col-form-label">@lang('upay.general.show_status')</label>
                                                        <div class="col-lg-10">
                                                            <input id="general-status" type="checkbox"
                                                                   name="show_status" value="{{Model::STATUS_ACTIVE}}"
                                                                   @if($faq->show_status == Model::STATUS_ACTIVE) checked
                                                                   @endif {{$disabled}} data-toggle="switchbutton"
                                                                   data-onlabel="@lang('upay.general.show_status.active')"
                                                                   data-offlabel="@lang('upay.general.show_status.inactive')"
                                                                   data-width="86" data-height="33">
                                                            <span
                                                                class="form-text text-danger form-error-text form-error-show_status"></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            @foreach($languages as $lng)
                                                <div role="tabpanel" id="tab-lng-{{$lng->id}}" class="tab-pane">
                                                    <div class="panel-body">

                                                        <div class="form-group required row"><label
                                                                class="col-lg-2 col-form-label">@lang('upay.website.faq.question')</label>
                                                            <div class="col-lg-10">
                                                                <input type="text"
                                                                       name="ml[{{$lng->id}}][question]"
                                                                       @isset($faq->ml[$lng->id]) value="{{$faq->ml[$lng->id]->question}}"
                                                                       @endisset {{$disabled}} class="form-control">
                                                                <span
                                                                    class="form-text text-danger form-error-text form-error-ml_{{$lng->id}}_question"></span>
                                                            </div>
                                                        </div>
                                                        <div class="form-group required row"><label
                                                                class="col-lg-2 col-form-label">@lang('upay.website.faq.answer')</label>
                                                            <div class="col-lg-10">
                                                                <textarea rows="3" name="ml[{{$lng->id}}][answer]"
                                                                          {{$disabled}} class="form-control content">{{isset($faq->ml[$lng->id]) ? $faq->ml[$lng->id]->answer : ''}}</textarea>
                                                                <span
                                                                    class="form-text text-danger form-error-text form-error-ml_{{$lng->id}}_answer"></span>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row"><label
                                                                class="col-lg-2 col-form-label">@lang('upay.general.show_status')</label>
                                                            <div class="col-lg-10">
                                                                <input type="checkbox"
                                                                       name="ml[{{$lng->id}}][show_status]"
                                                                       value="{{Model::STATUS_ACTIVE}}"
                                                                       @if(!isset($faq->ml[$lng->id]) || $faq->ml[$lng->id]->show_status == Model::STATUS_ACTIVE) checked
                                                                       @endif {{$disabled}} data-toggle="switchbutton"
                                                                       data-onlabel="@lang('upay.general.show_status.active')"
                                                                       data-offlabel="@lang('upay.general.show_status.inactive')"
                                                                       data-width="86" data-height="33">
                                                                <span
                                                                    class="form-text text-danger form-error-text form-error-ml_{{$lng->id}}_show_status"></span>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>

                            {{csrf_field()}}

                            <hr/>
                            <div class="form-group row">
                                <div class="col-lg-4 col-lg-offset-2">
                                    <a href="{{url('/website/faq/list')}}"
                                       class="btn btn-white">@lang('upay.general.buttons.cancel')</a>
                                    @if($mode == 'edit' && $canDelete)
                                        <button onclick="gDelete({{$faq->id}});return false;" type="button"
                                                class="btn btn-danger">@lang('upay.general.buttons.delete')</button>@endif
                                    @if($canEdit)
                                        <button class="btn btn-primary submit-btn"
                                                type="submit">@lang('upay.general.buttons.save')</button>@endif
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @if($mode == 'edit' && $canDelete)
        @include('components.delete', ['url' => url('/website/faq/delete')])
    @endif
    <script src="{{script_url('upay/js/website/faq.js')}}"></script>
@stop
