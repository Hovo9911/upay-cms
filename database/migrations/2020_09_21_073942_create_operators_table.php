<?php

use App\DB\Migration;
use App\DB\Blueprint;
use App\UPay\Operator\Operator;

class CreateOperatorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->getSchemaBuilder()->create('operators', function (Blueprint $table) {
            $table->increments('id');
            $table->string('email');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('phone');
            $table->string('password');
            $table->enum('password_reset_status', [Operator::PASSWORD_RESET_STATUS_PENDING, Operator::PASSWORD_RESET_STATUS_CONFIRMED])->nullable()->default(null);
            $table->showStatus();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('operators');
    }
}
