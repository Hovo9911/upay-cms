<?php

use App\DB\Migration;
use App\DB\Blueprint;

class AddExternalIdToTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->getSchemaBuilder()->table('transactions', function (Blueprint $table) {
            $table->string('external_id')->default('');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $this->getSchemaBuilder()->table('transactions', function (Blueprint $table) {
            $table->dropColumn('external_id');
        });
    }
}
