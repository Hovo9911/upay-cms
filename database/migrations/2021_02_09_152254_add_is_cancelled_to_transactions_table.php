<?php

use App\DB\Migration;
use App\DB\Blueprint;

class AddIsCancelledToTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->getSchemaBuilder()->table('transactions', function (Blueprint $table) {
            $table->boolean('is_cancelled')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $this->getSchemaBuilder()->table('transactions', function (Blueprint $table) {
            $table->dropColumn('is_cancelled');
        });
    }
}
