@extends('layout')
@section('page_title') @lang('upay.cashbox.list_title') @stop
@section('content')
<?php
    use App\Model;
    $activeMenu = 'cashbox';
    $canDelete = \Auth::user()->hasPermission('cashbox.delete');
    $canEdit = \Auth::user()->hasPermission('cashbox.edit');
    $jsTrans->add([
        'upay.cashbox.allow_pos.yes',
        'upay.cashbox.allow_pos.no',
        'upay.cashbox.allow_close.yes',
        'upay.cashbox.allow_close.no',
    ]);
?>
<script>
    var $permissions = { edit: {{$canEdit ? '1' : '0'}}, delete: {{$canDelete ? '1' : '0'}} };
</script>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-title">
                    <h1>@lang('upay.cashbox.list_title')</h1>
                    <div class="ibox-tools">
                        @if($canDelete)<button onclick="gDelete();return false;" class="btn btn-w-m btn-danger"><i class="fa fa-remove"></i> @lang('upay.general.buttons.delete')</button>@endif
                        @if($canEdit)<a href="{{url('/cashbox/edit')}}" class="btn btn-w-m btn-primary"><i class="fa fa-plus"></i> @lang('upay.general.buttons.add_new')</a>@endif
                    </div>
                </div>
                <div class="ibox-content">
                    <div class="ibox">
                        <div class="ibox-title">
                            <h5>@lang('upay.general.list.filters.title')</h5>
                            <div class="ibox-tools"><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></div>
                        </div>
                        <div class="ibox-content">
                            <form id="search-form" method="GET" action="{{url('/cashbox/get-list')}}" data-edit-url="{{url('/cashbox/edit/')}}" role="form">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group row">
                                            <label class="col-lg-3 col-form-label text-right">@lang('upay.general.id')</label>
                                            <div class="col-lg-9">
                                                <input type="text" name="filters[id]" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group row">
                                            <label class="col-lg-3 col-form-label text-right">@lang('upay.cashbox.name')</label>
                                            <div class="col-lg-9">
                                                <input type="text" name="filters[name]" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group row">
                                            <label class="col-lg-3 col-form-label text-right">@lang('upay.cashbox.branch')</label>
                                            <div class="col-lg-9">
                                                <select class="form-control m-b select-picker" name="filters[branch]">
                                                    <option value="">@lang('upay.general.dropdown.select')</option>
                                                    @foreach($branches as $branch)
                                                        <option value="{{$branch->id}}">{{$branch->name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group row">
                                            <label class="col-lg-3 col-form-label text-right">@lang('upay.general.show_status')</label>
                                            <div class="col-lg-9">
                                                <select class="form-control m-b select-picker" name="filters[show_status]">
                                                    <option value="">@lang('upay.general.dropdown.select')</option>
                                                    <option value="{{Model::STATUS_ACTIVE}}">@lang('upay.general.show_status.active')</option>
                                                    <option value="{{Model::STATUS_INACTIVE}}">@lang('upay.general.show_status.inactive')</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group row">
                                            <label class="col-lg-3 col-form-label text-right">@lang('upay.cashbox.allow_close')</label>
                                            <div class="col-lg-9 align-self-lg-center">
                                                <select class="form-control m-b select-picker-checkbox" name="filters[allow_close]">
                                                    <option value="">@lang('upay.general.dropdown.select')</option>
                                                    <option value="1">@lang('upay.cashbox.allow_close.yes')</option>
                                                    <option value="0">@lang('upay.cashbox.allow_close.no')</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group row">
                                            <label class="col-lg-3 col-form-label text-right">@lang('upay.cashbox.allow_pos')</label>
                                            <div class="col-lg-9 align-self-lg-center">
                                                <select class="form-control m-b select-picker-checkbox" name="filters[allow_pos]">
                                                    <option value="">@lang('upay.general.dropdown.select')</option>
                                                    <option value="1">@lang('upay.cashbox.allow_pos.yes')</option>
                                                    <option value="0">@lang('upay.cashbox.allow_pos.no')</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <button disabled class="btn btn-info float-right m-t-n-xs submit-btn" type="submit"><i class="fa fa-filter"></i> @lang('upay.general.list.filters.btn')</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table id="search-table" style="width: 100%" class="table table-striped table-bordered table-hover" >
                            <thead>
                                <tr>
                                    <th data-col="id" data-col-name="check" check-col></th>
                                    <th data-col="id">@lang('upay.general.id')</th>
                                    <th data-col="name">@lang('upay.cashbox.name')</th>
                                    <th data-col="branch">@lang('upay.cashbox.branch')</th>
                                    <th data-col="account_number" data-orderable="false">@lang('upay.cashbox.account_number')</th>
                                    <th data-col="allow_close" data-orderable="false">@lang('upay.cashbox.allow_close')</th>
                                    <th data-col="allow_pos" data-orderable="false">@lang('upay.cashbox.allow_pos')</th>
                                    <th data-col="pos_account_number" data-orderable="false">@lang('upay.cashbox.pos_account_number')</th>
                                    <th data-col="show_status">@lang('upay.general.show_status')</th>
                                    <th data-col="id" action-col>@lang('upay.general.list.actions')</th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@if($canDelete) @include('components.delete', ['url' => url('/cashbox/delete')]) @endif
<script src="{{script_url('upay/js/cashbox.js')}}"></script>
@stop
