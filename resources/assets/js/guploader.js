//!!!!!!!!Don't forget to remove csrf verification for upload action!!!!!!!!!!

$gUploader = {};

$gUploader.beforeUpload = function (self) {
};
$gUploader.afterUpload = function (self) {
};

$gUploader.onX = function (self) {
    if (self == undefined) {
        var target = $(".g-upload-x");
    } else {
        var target = $(".g-upload-x", self.closest(".g-upload-container"));
    }
    target.off().on('click', function () {
        $(this).closest(".g-uploaded").remove();
    });
};

$gUploader.onSuccess = function (response, self) {
    $('.form-error-text', self.closest(".g-upload-container").closest('.form-group')).html('');
    self.closest(".g-upload-container").closest('.form-group').removeClass('has-error');
    for (var i in response.data) {
        var str = '<div class="g-uploaded border fb fs14 position-relative text-break text-wrap">' +
            '<i class="g-upload-x fa fa-2x fa-remove text-danger"></i>' +
            (response.data[i].thumbnail != '' ? '<img src="' + response.data[i].thumbnail + '"/>' : '<i class="fa fa-file-text-o fa-2x"></i>') +
            response.data[i].label +
            '<input type="hidden" name="' + self.attr('name') + '" value="' + response.data[i].file_name + '" />' +
        '</div>';
        if (typeof self.attr('multiple') != 'undefined') {
            $(".g-uploaded-list", self.closest(".g-upload-container")).append(str);
        } else {
            $(".g-uploaded-list", self.closest(".g-upload-container")).html(str);
        }
    }
};

$gUploader.onError = function (response, self) {
    for (var i in response.errors) {
        $(".form-error-" + i, self.closest(".g-upload-container")).html(response.errors[i]).fadeIn();
        $(".form-error-" + i, self.closest(".g-upload-container")).closest('.form-group').addClass('has-error');
    }
};

$gUploader.onCustomResponse = globalProcessResponse;

$gUploader.init = function () {
    var uploader = this;
    $(".g-upload").on('change', function () {
        var self = $(this);

        uploader.beforeUpload(self);
        self.attr('disabled', 'disabled');
        $(".uploader-loading", self.closest(".g-upload-box")).show();

        var data = new FormData();
        $.each(self[0].files, function (i, file) {
            data.append('files[' + i + ']', file);
        });

        data.append('conf', self.data('conf'));
        $.ajax({
            data: data,
            url: self.data('action'),
            type: 'post',
            dataType: 'json',
            cache: false,
            contentType: false,
            processData: false,
            xhrFields: {
                withCredentials: true
            },
            success: function (response) {
                $(self).removeAttr('disabled');
                $(".form-error-text", self.closest(".g-upload-container")).html('');

                if (response.status == 'OK') {
                    uploader.onSuccess(response, self);
                } else if (response.status == 'INVALID_DATA') {
                    uploader.onError(response, $(self));
                } else {
                    uploader.onCustomResponse(response, self);
                }
                self.val('');
                uploader.onX(self);
                $(".uploader-loading", self.closest(".g-upload-box")).hide();
                uploader.afterUpload(self);
            },
            error: function (response) {
                $(self).removeAttr('disabled');
                $(".uploader-loading", self.closest(".g-upload-box")).hide();
                uploader.onCustomResponse(response, self);
            }
        });

        $gUploader.onX(self);
    });
    $gUploader.onX();
};

$(document).ready(function () {
    $gUploader.init();
});
