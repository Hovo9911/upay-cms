<?php

use App\DB\Migration;
use App\DB\Blueprint;

class CreateArmsoftTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->getSchemaBuilder()->create('armsoft_transactions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->enum('transaction_type', ['transaction', 'order', 'cancellation']);
            $table->unsignedInteger('transaction_id');
            $table->timestamp('transaction_date');
            $table->float('amount');
            $table->string('account_credit');
            $table->string('account_debit');
            $table->string('outer_account_credit');
            $table->string('outer_account_debit');
            $table->string('aim', 1024);
            $table->string('branch_code');
            $table->string('receipt_id');
            $table->string('currency');
            $table->enum('status', [ 'pending', 'succeed', 'failed' ]);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('armsoft_transactions');
    }
}
