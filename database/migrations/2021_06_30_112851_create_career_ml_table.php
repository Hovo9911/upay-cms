<?php

use App\DB\Migration;
use App\DB\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCareerMlTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->getSchemaBuilder()->create('career_ml', function (Blueprint $table) {
            $table->integer('career_id')->unsigned();
            $table->integer('lng_id')->unsigned();
            $table->string('title');
            $table->string('description');
            $table->showStatus();
            $table->primary(['career_id', 'lng_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('career_ml');
    }
}
