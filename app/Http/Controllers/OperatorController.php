<?php

namespace App\Http\Controllers;

use App\Exceptions\InvalidDataException;
use App\Http\Requests\Operator\EditRequest;
use App\Http\Requests\Operator\SearchRequest;
use App\UPay\Interfaces\ResetPasswordRepositoryInterface;
use App\UPay\Interfaces\OperatorRepositoryInterface;
use App\UPay\Operator\Operator;
use App\UPay\Operator\OperatorSearch;
use Illuminate\Http\Request;
use DB;

class OperatorController extends Controller
{
    private $operatorRepository;
    private $resetPasswordRepository;

    public function __construct(OperatorRepositoryInterface $operatorRepository, ResetPasswordRepositoryInterface $resetPasswordRepository)
    {
        $this->operatorRepository = $operatorRepository;
        $this->resetPasswordRepository = $resetPasswordRepository;
    }

    public function index()
    {
        return view('operator.list');
    }

    public function edit(Request $request, $id = null)
    {
        if ($id != null) {
            $operator = $this->operatorRepository->findOrFail($id);
        } else {
            $operator = $this->operatorRepository->mock();
        }
        return view('operator.edit')->with([
            'mode' => $id != null ? 'edit' : 'add',
            'operator' => $operator,
        ]);
    }

    public function save(EditRequest $request, $id = null)
    {
        $data = $request->validated();
        DB::beginTransaction();
        if ($id != null) {
            $operator = $this->operatorRepository->findOrFail($id);
            $resetPassword = ($operator->show_status == Operator::STATUS_INACTIVE && $data['show_status'] == Operator::STATUS_ACTIVE);
            unset($data['email']);
            $operator = $this->operatorRepository->update($operator, $data);
        } else {
            if ($this->operatorRepository->emailFound($data['email'], $id)) {
                throw InvalidDataException::withErrors(['email' => trans('upay.operator.email.exists')]);
            }
            $resetPassword = ($data['show_status'] == Operator::STATUS_ACTIVE);
            $operator = $this->operatorRepository->create($data);
        }
        if ($resetPassword) {
            $this->resetPasswordRepository->sendOperatorPassword($operator);
        }
        DB::commit();

        return $this->sendOK([
            'message' => trans('upay.operator.saved.message'),
            'edit_url' => url('/operator/edit/'.$operator->id),
            'list_url' => url('/operator/list')
        ]);
    }

    public function getList(SearchRequest $request)
    {
        $searcher = new OperatorSearch($request->validated());
        return response()->json([
            'data' => $searcher->search(),
            'recordsTotal' => $searcher->totalCount(),
            'recordsFiltered' => $searcher->filteredCount(),
        ]);
    }

    public function resetPassword(Request $request, $id)
    {
        $operator = $this->operatorRepository->findOrFail($id);
        $this->resetPasswordRepository->sendOperatorPassword($operator);
        return $this->sendOK([
            'message' => trans('upay.operator.reset_password.success')
        ]);
    }
}
