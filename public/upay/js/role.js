var $saveForm = $.extend({}, $gForm);
$saveForm.formId = "save-form";

var $search = $.extend({}, $gsearch);
$search.modifyColumns = {
    permissions: {
        render: function(data, type) {
            return type === 'display' ?
                (data == null ? '' : Object.keys(data).join(', ')) :
                data;
        }
    }
};

$(document).ready(function() {
    if ($($search.searchFormSelector).length) {
        $search.init();
    } else {
        $(".permissions-table input[value=edit], .permissions-table input[value=delete]").on('ifChecked', function(){
            $("input[value=view]", $(this).closest('tr')).iCheck('check');
        });
        $(".permissions-table input[value=view]").on('ifUnchecked', function(){
            $("input[value=edit]", $(this).closest('tr')).iCheck('uncheck');
            $("input[value=delete]", $(this).closest('tr')).iCheck('uncheck');
        });
        $saveForm.init();
    }
});
