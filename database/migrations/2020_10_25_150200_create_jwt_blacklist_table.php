<?php

use App\DB\Migration;
use App\DB\Blueprint;

class CreateJwtBlacklistTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->getSchemaBuilder()->create('jwt_blacklist', function (Blueprint $table) {
            $table->increments('id');
            $table->text('token');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jwt_blacklist');
    }
}
