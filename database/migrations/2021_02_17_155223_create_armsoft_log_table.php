<?php

use App\DB\Migration;
use App\DB\Blueprint;

class CreateArmsoftLogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->getSchemaBuilder()->create('armsoft_log', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('armsoft_transaction_id');
            $table->text('data');
            $table->text('result');
            $table->timestamps();
        });
    }

    /**x
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('armsoft_log');
    }
}
