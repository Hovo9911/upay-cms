<?php

namespace App\UPay\Transaction;

use App\Search;

class TransactionSearch extends Search
{
    protected $orderables = [
        'id',
        'created_at',
        'amount',
    ];

    public function query()
    {
        $filters = $this->filters;
        $query = Transaction::with(['cashbox', 'operator', 'service'])
            ->where('service_type', '!=', Transaction::SERVICE_TYPE_CANCEL)
            ->when(!empty($filters['receipt_id']), function($query) use ($filters) {
                $query->where('receipt_id', $filters['receipt_id']);
            })
            ->when(!empty($filters['code']), function($query) use ($filters) {
                $query->where('code', $filters['code']);
            })
            ->when(!empty($filters['service']), function($query) use ($filters) {
                $query->where('service_id', $filters['service']);
            })
            ->when(!empty($filters['branch']), function($query) use ($filters) {
                $query->where('branch_id', $filters['branch']);
            })
            ->when(!empty($filters['cashbox']), function($query) use ($filters) {
                $query->where('cashbox_id', $filters['cashbox']);
            })
            ->when(!empty($filters['operator']), function($query) use ($filters) {
                $query->where('operator_id', $filters['operator']);
            })
            ->when(!empty($filters['date_from']), function($query) use ($filters) {
                $query->where('created_at', '>=', $filters['date_from'].' 00:00:00');
            })
            ->when(!empty($filters['date_to']), function($query) use ($filters) {
                $query->where('created_at', '<=', $filters['date_to'].' 23:59:59');
            })
            ->when(!empty($filters['amount_from']), function($query) use ($filters) {
                $query->where('amount', '>=', $filters['amount_from']);
            })
            ->when(!empty($filters['amount_to']), function($query) use ($filters) {
                $query->where('amount', '<=', $filters['amount_to']);
            });
        return $query;
    }

    public function totalCount()
    {
        return Transaction::where('service_type', '!=', Transaction::SERVICE_TYPE_CANCEL)->count();
    }
}
