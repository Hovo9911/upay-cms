<?php

use App\DB\Migration;
use App\DB\Blueprint;

class AddAdapterToServices extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->getSchemaBuilder()->table('services', function (Blueprint $table) {
            $table->string('adapter')->default('');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $this->getSchemaBuilder()->table('services', function (Blueprint $table) {
            $table->dropColumn('adapter');
        });
    }
}
