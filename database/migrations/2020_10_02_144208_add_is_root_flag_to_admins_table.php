<?php

use App\DB\Migration;
use App\DB\Blueprint;

class AddIsRootFlagToAdminsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->getSchemaBuilder()->table('admins', function (Blueprint $table) {
            $table->boolean('is_root')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $this->getSchemaBuilder()->table('admins', function (Blueprint $table) {
            $table->dropColumn('is_root');
        });
    }
}
