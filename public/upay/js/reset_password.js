var $resetPassword = $.extend({}, $gForm);
$resetPassword.formId = "reset-password-form";
$resetPassword.onSuccess = function(response) {
    swal({
        title: response.data.message,
        type: "success",
        showCancelButton: false,
        showConfirmButton: true,
        closeOnConfirm: true
    }, function(){
        window.location.href = response.data.redirect_url;
    });
};

$(document).ready(function() {
    $resetPassword.init();
});
