<?php

namespace App\UPay\Role;

use App\Search;

class RoleSearch extends Search
{
    protected $orderables = [
        'id',
        'show_status',
        'name'
    ];

    protected function query()
    {
        $filters = $this->filters;
        return Role::when(!empty($filters['id']), function($query) use ($filters) {
                $query->where('id', $filters['id']);
            })
            ->when(!empty($filters['name']), function($query) use ($filters) {
                $query->where('name', 'ILIKE', '%'.$filters['name'].'%');
            })
            ->when(!empty($filters['show_status']), function($query) use ($filters) {
                $query->where('show_status', $filters['show_status']);
            }, function($query) use ($filters) {
                $query->where('show_status', '!=', Role::STATUS_DELETED);
            });
    }

    public function totalCount()
    {
        return Role::notDeleted()->count();
    }
}
