<?php

namespace App\UPay\Service;

use \App\Model;

class Commission extends Model
{
    const TYPE_CUSTOMER = 'customer';
    const TYPE_PROVIDER = 'provider';
    const SOURCE_BRANCH = 'branch';
    const SOURCE_API = 'api';
    const SOURCE_TERMINAL = 'terminal';
    const SOURCE_ONLINE = 'online';

    protected $table = 'service_commissions';
    public $timestamps = false;

    protected $fillable = [
        'service_id',
        'source',
        'type',
        'show_status',
    ];

    public function commissionValues(){
        return $this->hasMany(CommissionValue::class, 'commission_id', 'id');
    }

    public function commissionValuesApproved(){
        return $this->hasMany(CommissionValueApproved::class, 'commission_id', 'id')->orderBy('date')->orderBy('order');
    }

    public static function getTypes()
    {
        return [
            self::TYPE_CUSTOMER,
            self::TYPE_PROVIDER
        ];
    }

    public static function getSources()
    {
        return [
            self::SOURCE_BRANCH,
            self::SOURCE_API,
            self::SOURCE_TERMINAL,
            self::SOURCE_ONLINE,
        ];
    }
}
