@extends('layout')
@section('page_title') @lang('upay.cancellation.list_title') @stop
@section('content')
<?php
use App\UPay\Transaction\TransactionCancellation;
$activeMenu = 'cancellation';

foreach(TransactionCancellation::statuses() as $status) {
    $jsTrans->add('upay.cancellation.status.'.$status);
}
$jsTrans->add([
    'upay.cancellation.cashier.status.completed',
    'upay.cancellation.cashier.status.rejected',
    'upay.cancellation.customer.status.completed',
    'upay.cancellation.customer.status.rejected',
    'upay.cancellation.type.cashier',
    'upay.cancellation.type.customer',
]);
?>
<script>
    var $permissions = { };
</script>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-title">
                    <h1>@lang('upay.cancellation.list_title')</h1>
                </div>
                <div class="ibox-content">
                    <div class="ibox">
                        <div class="ibox-title">
                            <h5>@lang('upay.general.list.filters.title')</h5>
                            <div class="ibox-tools"><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></div>
                        </div>
                        <div class="ibox-content">
                            <form id="search-form" method="GET" action="{{url('/cancellation/get-list')}}" data-view-url="{{url('/cancellation/view/')}}" role="form">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group row">
                                            <label class="col-lg-3 col-form-label text-right">@lang('upay.cancellation.transaction_date')</label>
                                            <div class="col-lg-9">
                                                <div class="input-group date form-datepicker">
                                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input name="filters[transaction_date_from]" type="text" class="form-control" value="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group row">
                                            <span class="col-lg-2 col-form-label text-right fa fa-long-arrow-right"></span>
                                            <label class="col-lg-1 col-form-label text-right"></label>
                                            <div class="col-lg-9">
                                                <div class="input-group date form-datepicker">
                                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input name="filters[transaction_date_to]" type="text" class="form-control" value="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group row">
                                            <label class="col-lg-3 col-form-label text-right">@lang('upay.cancellation.cancellation_date')</label>
                                            <div class="col-lg-9">
                                                <div class="input-group date form-datepicker">
                                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input name="filters[cancellation_date_from]" type="text" class="form-control" value="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group row">
                                            <span class="col-lg-2 col-form-label text-right fa fa-long-arrow-right"></span>
                                            <label class="col-lg-1 col-form-label text-right"></label>
                                            <div class="col-lg-9">
                                                <div class="input-group date form-datepicker">
                                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input name="filters[cancellation_date_to]" type="text" class="form-control" value="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group row">
                                            <label class="col-lg-3 col-form-label text-right">@lang('upay.cancellation.amount')</label>
                                            <div class="col-lg-9">
                                                <input type="text" name="filters[amount_from]" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group row">
                                            <span class="col-lg-2 col-form-label text-right fa fa-long-arrow-right"></span>
                                            <label class="col-lg-1 col-form-label text-right"></label>
                                            <div class="col-lg-9">
                                                <input type="text" name="filters[amount_to]" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group row">
                                            <label class="col-lg-3 col-form-label text-right">@lang('upay.cancellation.service')</label>
                                            <div class="col-lg-9">
                                                <select class="form-control m-b select-picker" name="filters[service_id]">
                                                    <option value="">@lang('upay.general.dropdown.select')</option>
                                                    @foreach($services as $service)
                                                        <option value="{{$service->id}}">{{$service->name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group row">
                                            <label class="col-lg-3 col-form-label text-right">@lang('upay.cancellation.code')</label>
                                            <div class="col-lg-9">
                                                <input type="text" name="filters[code]" class="form-control">
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group row">
                                            <label class="col-lg-3 col-form-label text-right">@lang('upay.cancellation.receipt_id')</label>
                                            <div class="col-lg-9">
                                                <input type="text" name="filters[receipt_id]" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group row">
                                            <label class="col-lg-3 col-form-label text-right">@lang('upay.cancellation.status')</label>
                                            <div class="col-lg-9">
                                                <select class="form-control m-b select-picker" name="filters[status]">
                                                    <option value="">@lang('upay.general.dropdown.select')</option>
                                                    @foreach(TransactionCancellation::statuses() as $status)
                                                        <option value="{{$status}}" @if($status == TransactionCancellation::STATUS_NEW) selected @endif>@lang('upay.cancellation.status.'.$status)</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group row">
                                            <label class="col-lg-3 col-form-label text-right">@lang('upay.cancellation.type')</label>
                                            <div class="col-lg-9">
                                                <select class="form-control m-b select-picker" name="filters[type]">
                                                    <option value="">@lang('upay.general.dropdown.select')</option>
                                                    <option value="{{TransactionCancellation::TYPE_CASHIER}}">@lang('upay.cancellation.type.'.TransactionCancellation::TYPE_CASHIER)</option>
                                                    <option value="{{TransactionCancellation::TYPE_CUSTOMER}}">@lang('upay.cancellation.type.'.TransactionCancellation::TYPE_CUSTOMER)</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <button disabled class="btn btn-info float-right m-t-n-xs submit-btn" type="submit"><i class="fa fa-filter"></i> @lang('upay.general.list.filters.btn')</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table id="search-table" style="width: 100%" class="table table-striped table-bordered table-hover" >
                            <thead>
                                <tr>
                                    <th data-col="service" data-orderable="false">@lang('upay.cancellation.service')</th>
                                    <th data-col="transaction_date">@lang('upay.cancellation.transaction_date')</th>
                                    <th data-col="cancellation_date">@lang('upay.cancellation.cancellation_date')</th>
                                    <th data-col="receipt_id">@lang('upay.cancellation.receipt_id')</th>
                                    <th data-col="cashbox" data-orderable="false">@lang('upay.cancellation.cashbox')</th>
                                    <th data-col="total_amount">@lang('upay.cancellation.amount')</th>
                                    <th data-col="code" data-orderable="false">@lang('upay.cancellation.code')</th>
                                    <th data-col="type" data-orderable="false">@lang('upay.cancellation.type')</th>
                                    <th data-col="operator" data-orderable="false">@lang('upay.cancellation.operator')</th>
                                    <th data-col="status" data-orderable="false">@lang('upay.cancellation.status')</th>
                                    <th data-col="id" data-col-name="actions" action-col>@lang('upay.general.list.actions')</th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="{{script_url('upay/js/cancellation.js')}}"></script>
@stop
