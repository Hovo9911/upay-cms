<?php

namespace App\UPay\Branch;

use App\UPay\Cashbox\Cashbox;
use App\UPay\Interfaces\BranchRepositoryInterface;
use DB;
use Illuminate\Database\Eloquent\Collection;

class BranchRepository implements BranchRepositoryInterface
{
    public function create($data): Branch
    {
        $branch = new Branch($data);
        DB::beginTransaction();
        $branch->save();
        $this->saveMl($branch, $data['ml']);
        DB::commit();
        return $branch;
    }

    public function mock(): Branch
    {
        return new Branch([
            'code' => '',
            'armsoft_code' => '',
            'city_id' => null,
            'lat' => '',
            'lng' => '',
            'sort_order' => '',
            'show_status' => Branch::STATUS_ACTIVE,
        ]);
    }

    public function findOrFail($id): Branch
    {
        $branch = Branch::where('id', $id)->notDeleted()->first();
        if (empty($branch)) {
            abort(404);
        }
        return $branch;
    }

    public function update($id, $data): Branch
    {
        $branch = $this->findOrFail($id);
        DB::beginTransaction();
        $branch->update($data);
        $this->saveMl($branch, $data['ml']);
        DB::commit();
        return $branch;
    }

    public function delete($ids): int
    {
        DB::beginTransaction();
        BranchMl::whereIn('branch_id', $ids)->update(['show_status' => BranchMl::STATUS_DELETED]);
        $cnt = Branch::whereIn('id', $ids)->update(['show_status' => Branch::STATUS_DELETED]);
        DB::commit();
        return $cnt;
    }

    public function saveMl(Branch $branch, array $mls): void
    {
        foreach ($mls as $lngId => $ml) {
            $branch->ml()->updateOrCreate(
                ['branch_id' => $branch->id, 'lng_id' => $lngId],
                [
                    'name' => $ml['name'],
                    'address' => $ml['address'],
                    'show_status' => $ml['show_status'],
                ]
            );
        }
    }

    public function codeFound($code, $id): bool
    {
        if (empty($code)) {
            return false;
        }
        $query = Branch::where('code', $code)->notDeleted();
        if (!empty($id)) {
            $query->where('id', '!=', $id);
        }
        return !empty($query->first());
    }

    public function armsoftCodeFound($code, $id): bool
    {
        if (empty($code)) {
            return false;
        }
        $query = Branch::where('armsoft_code', $code)->notDeleted();
        if (!empty($id)) {
            $query->where('id', '!=', $id);
        }
        return !empty($query->first());
    }

    public function all(): Collection
    {
        return Branch::joinMl()->where('branches.show_status', '!=', Branch::STATUS_DELETED)
                ->sorted()->get();
    }

    public function found(int $id): bool
    {
        return !empty(Branch::where('id', $id)->notDeleted()->first());
    }

    public function getCashboxes(int $id)
    {
        return Cashbox::joinMl()->where('cashboxes.show_status', '!=', Cashbox::STATUS_DELETED)->where('branch_id', $id)->get();
    }
}
