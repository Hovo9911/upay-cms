<?php
namespace App\UPay\Interfaces;

use \App\UPay\Branch\Branch;
use Illuminate\Database\Eloquent\Collection;

interface BranchRepositoryInterface
{
    public function create($data): Branch;

    public function mock(): Branch;

    public function findOrFail($id): Branch;

    public function update($id, $data): Branch;

    public function delete($ids): int;

    public function saveMl(Branch $branch, array $mls): void;

    public function codeFound($code, $id): bool;

    public function armsoftCodeFound($code, $id): bool;

    public function all(): Collection;

    public function found(int $id): bool;

    public function getCashboxes(int $id);
}
