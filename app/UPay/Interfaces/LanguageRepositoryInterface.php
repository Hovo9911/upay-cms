<?php
namespace App\UPay\Interfaces;

use App\UPay\Language\Language;
use Illuminate\Support\Collection;

interface LanguageRepositoryInterface
{
    public function all(): Collection;

    public function current(): Language;
}
