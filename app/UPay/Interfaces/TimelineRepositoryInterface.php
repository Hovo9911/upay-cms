<?php
namespace App\UPay\Interfaces;

use Illuminate\Database\Eloquent\Collection;

interface TimelineRepositoryInterface
{
    public function save($data): void;

    public function getTimeline($year, $month);

    public function all(): Collection;

    public function checkOperatorsForDay($cashboxId, $date, $operators): bool;
}
