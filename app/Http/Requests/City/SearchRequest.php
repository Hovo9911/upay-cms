<?php

namespace App\Http\Requests\City;

use App\Http\Requests\DatatableSearchRequest;
use App\Model;

class SearchRequest extends DatatableSearchRequest
{
    public function rules()
    {
        return [
            'filters.id' => 'integer',
            'filters.region' => 'integer',
            'filters.name' => 'string|max:255',
            'filters.show_status' => 'in:'.Model::STATUS_ACTIVE.','.Model::STATUS_INACTIVE,
            'order' => 'array',
            'start' => 'integer',
        ];
    }
}
