<?php

namespace App\UPay\Service;

use App\Search;

class ServiceSearch extends Search
{
    protected $orderables = [
        'id',
        'name',
        'provider',
        'show_status',
    ];

    protected function query()
    {
        $filters = $this->filters;
        $query = Service::joinMl()->select(
            'services.id',
            'ml.name',
            'provider_ml.name as provider',
            'services.branch_account_number',
            'services.branch_commission_account_number',
            'services.mobile_account_number',
            'services.mobile_commission_account_number',
            'services.account_min',
            'services.account_max',
            'services.use_fixed_amount',
            'services.show_status'
        )
        ->joinProvider()
        ->when(!empty($filters['id']), function($query) use ($filters) {
            $query->where('services.id', $filters['id']);
        })
        ->when(!empty($filters['name']), function($query) use ($filters) {
            $query->where('ml.name', 'ILIKE', '%'.$filters['name'].'%');
        })
        ->when(!empty($filters['provider']), function($query) use ($filters) {
            $query->where('services.provider_id', $filters['provider']);
        })
        ->when(!empty($filters['show_status']), function($query) use ($filters) {
            $query->where('services.show_status', $filters['show_status']);
        }, function($query) use ($filters) {
            $query->where('services.show_status', '!=', Service::STATUS_DELETED);
        });
        return $query;
    }

    public function totalCount()
    {
        return Service::notDeleted()->count();
    }
}
