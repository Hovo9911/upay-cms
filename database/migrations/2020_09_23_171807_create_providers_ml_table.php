<?php
use App\DB\Migration;
use App\DB\Blueprint;

class CreateProvidersMlTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->getSchemaBuilder()->create('providers_ml', function (Blueprint $table) {
            $table->integer('provider_id')->unsigned();
            $table->integer('lng_id')->unsigned();
            $table->string('name');
            $table->showStatus();
            $table->primary(['provider_id', 'lng_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('providers_ml');
    }
}
