<?php

namespace App\UPay\City;

use App\UPay\Interfaces\CityRepositoryInterface;
use DB;
use Illuminate\Database\Eloquent\Collection;

class CityRepository implements CityRepositoryInterface
{
    public function create($data): City
    {
        $city = new City($data);
        DB::beginTransaction();
        $city->save();
        $this->saveMl($city, $data['ml']);
        DB::commit();
        return $city;
    }

    public function mock(): City
    {
        return new City([
            'region_id' => null,
            'sort_order' => '',
            'show_status' => City::STATUS_ACTIVE,
        ]);
    }

    public function findOrFail($id): City
    {
        $city = City::where('id', $id)->notDeleted()->first();
        if (empty($city)) {
            abort(404);
        }
        return $city;
    }

    public function update($id, $data): City
    {
        $city = $this->findOrFail($id);
        DB::beginTransaction();
        $city->update($data);
        $this->saveMl($city, $data['ml']);
        DB::commit();
        return $city;
    }

    public function delete($ids): int
    {
        DB::beginTransaction();
        CityMl::whereIn('city_id', $ids)->update(['show_status' => CityMl::STATUS_DELETED]);
        $cnt = City::whereIn('id', $ids)->update(['show_status' => City::STATUS_DELETED]);
        DB::commit();
        return $cnt;
    }

    public function saveMl(City $city, array $mls): void
    {
        foreach ($mls as $lngId => $ml) {
            $city->ml()->updateOrCreate(
                ['city_id' => $city->id, 'lng_id' => $lngId],
                [
                    'name' => $ml['name'],
                    'show_status' => $ml['show_status'],
                ]
            );
        }
    }

    public function allWithRegions(): Collection
    {
        return City::select(
            'cities.id',
            'ml.name',
            'region_ml.name as region'
        )
        ->joinMl()->joinRegion()
        ->where('cities.show_status', '!=', City::STATUS_DELETED)
        ->orderBy('regions.sort_order', 'asc')
        ->orderBy('cities.sort_order', 'asc')
        ->orderBy('cities.id', 'desc')
        ->get();
    }

    public function found(int $id): bool
    {
        return !empty(City::where('id', $id)->notDeleted()->first());
    }
}
