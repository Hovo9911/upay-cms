<?php
namespace App\UPay\Interfaces;

use \App\UPay\News\News;
use Illuminate\Database\Eloquent\Collection;

interface NewsRepositoryInterface
{
    public function create($data): News;

    public function mock(): News;

    public function findOrFail($id, $withMl = false): News;

    public function update($id, $data): News;

    public function delete($ids): int;

    public function saveMl(News $slider, array $mls): void;

    public function saveImage($newImage, $oldImage = null): void;

    public function all(): Collection;
}
