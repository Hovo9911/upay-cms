<?php

use App\DB\Migration;
use App\DB\Blueprint;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->getSchemaBuilder()->create('transactions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('code');
            $table->unsignedInteger('payment_id')->nullable()->default(null); // for payments from mobile
            $table->string('receipt_id')->default('');
            $table->unsignedInteger('operator_id')->nullable()->default(null); //for payments from mobile there will be no operator
            $table->unsignedInteger('cashbox_id')->nullable()->default(null); //for payments from mobile there will be no cashbox
            $table->unsignedInteger('branch_id')->nullable()->default(null); //for payments from mobile there will be no branch
            $table->unsignedInteger('service_id');
            $table->timestamp('completed_at')->nullable()->default(null);
            $table->string('purpose');
            $table->float('amount');
            $table->float('total_amount');
            $table->float('commission');
            $table->float('debt');
            $table->boolean('cash');
            $table->jsonb('details');
            $table->string('contact_number');
            $table->smallInteger('attempts_count');
            $table->timestamp('last_attempted_at')->nullable()->default(null);
            $table->string('pos_auth_code');
            $table->enum('status', [ 'pending', 'completed', 'failed' ]);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
