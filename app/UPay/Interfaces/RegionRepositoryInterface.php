<?php
namespace App\UPay\Interfaces;

use Illuminate\Database\Eloquent\Collection;

interface RegionRepositoryInterface
{
    public function all(): Collection;

    public function found(int $id): bool;
}
