<?php

namespace App\Http\Requests\News;

use App\Http\Requests\Request;
use App\Model;

class EditRequest extends Request
{
    protected $module = 'news';

    public function rules()
    {
        $rules = [
            'image' => 'required|max:255',
            'show_on_homepage' => 'required|boolean',
            'publish_date' => 'required|date',
            'date' => 'required|date',
            'sort_order' => 'required|integer',
            'show_status' => 'in:' . Model::STATUS_ACTIVE . ',' . Model::STATUS_INACTIVE,
            'ml' => 'required|array',
        ];
        $rules = $this->checkStatusForRequired($rules, request()->input('show_status'));

        $rules = $this->addMlRules($rules, [
            'title' => 'required|max:255',
            'alias' => 'required|max:255',
            'short_description' => 'nullable|max:250',
            'body' => 'required|max:50000',
            'show_status' => 'in:' . Model::STATUS_ACTIVE . ',' . Model::STATUS_INACTIVE
        ]);

        return $rules;
    }

    protected function defaultValues()
    {
        return [
            'image' => '',
            'show_on_homepage' => false,
            'show_status' => Model::STATUS_INACTIVE,
            'ml' => [
                'title' => '',
                'alias' => '',
                'short_description' => '',
                'body' => '',
                'show_status' => Model::STATUS_INACTIVE,
            ]
        ];
    }

}
