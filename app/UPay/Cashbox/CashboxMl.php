<?php

namespace App\UPay\Cashbox;

use App\MultiPKModel;

class CashboxMl extends MultiPKModel
{
    protected $table = 'cashboxes_ml';
    public $timestamps = false;
    protected $primaryKey = ['cashbox_id', 'lng_id'];
    public $incrementing = false;

    protected $fillable = [
        'cashbox_id',
        'lng_id',
        'name',
        'show_status',
    ];
}
