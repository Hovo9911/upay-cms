<?php

namespace App\Http\Requests\Timeline;

use App\Http\Requests\Request;

class EditRequest extends Request
{
    protected $module = 'timeline';

    public function rules()
    {
        $rules = [
            'year' => 'required|integer',
            'month' => 'required|integer',
            'branch_id' => 'required|integer',
            'assignees' => 'array',
            'assignees.*.cashbox_id' => 'required|integer',
            'assignees.*.operators' => 'array',
            'assignees.*.operators.*' => 'required|integer',
            'assignees.*.day' => 'required|integer',
        ];
        return $rules;
    }
}
