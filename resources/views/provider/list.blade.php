@extends('layout')
@section('page_title') @lang('upay.provider.list_title') @stop
@section('content')
<?php
    use App\Model;
    $activeMenu = 'provider';
    $canEdit = \Auth::user()->hasPermission('provider.edit');
?>
<script>
    var $permissions = { edit: {{$canEdit ? '1' : '0'}} };
</script>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-title">
                    <h1>@lang('upay.provider.list_title')</h1>
                    <div class="ibox-tools">
                        @if($canEdit)<a href="{{url('/provider/edit')}}" class="btn btn-w-m btn-primary"><i class="fa fa-plus"></i> @lang('upay.general.buttons.add_new')</a>@endif
                    </div>
                </div>
                <div class="ibox-content">
                    <div class="ibox">
                        <div class="ibox-title">
                            <h5>@lang('upay.general.list.filters.title')</h5>
                            <div class="ibox-tools"><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></div>
                        </div>
                        <div class="ibox-content">
                            <form id="search-form" method="GET" action="{{url('/provider/get-list')}}" data-edit-url="{{url('/provider/edit/')}}" role="form">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group row">
                                            <label class="col-lg-3 col-form-label text-right">@lang('upay.general.id')</label>
                                            <div class="col-lg-9">
                                                <input type="text" name="filters[id]" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group row">
                                            <label class="col-lg-3 col-form-label text-right">@lang('upay.provider.name')</label>
                                            <div class="col-lg-9">
                                                <input type="text" name="filters[name]" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group row">
                                            <label class="col-lg-3 col-form-label text-right">@lang('upay.provider.parent')</label>
                                            <div class="col-lg-9">
                                                <select class="form-control m-b select-picker" name="filters[parent]">
                                                    <option value="">@lang('upay.general.dropdown.select')</option>
                                                    @foreach($parents as $parent)
                                                        <option value="{{$parent->id}}">{{$parent->name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group row">
                                            <label class="col-lg-3 col-form-label text-right">@lang('upay.general.show_status')</label>
                                            <div class="col-lg-9">
                                                <select class="form-control m-b select-picker" name="filters[show_status]">
                                                    <option value="">@lang('upay.general.dropdown.select')</option>
                                                    <option value="{{Model::STATUS_ACTIVE}}">@lang('upay.general.show_status.active')</option>
                                                    <option value="{{Model::STATUS_INACTIVE}}">@lang('upay.general.show_status.inactive')</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <button disabled class="btn btn-info float-right m-t-n-xs submit-btn" type="submit"><i class="fa fa-filter"></i> @lang('upay.general.list.filters.btn')</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table id="search-table" style="width: 100%" class="table table-striped table-bordered table-hover" >
                            <thead>
                                <tr>
                                    <th data-col="id">@lang('upay.general.id')</th>
                                    <th data-col="name">@lang('upay.provider.name')</th>
                                    <th data-col="parent">@lang('upay.provider.parent')</th>
                                    <th data-col="show_status">@lang('upay.general.show_status')</th>
                                    <th data-col="id" data-col-name="actions" action-col>@lang('upay.general.list.actions')</th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="{{script_url('upay/js/provider.js')}}"></script>
@stop
