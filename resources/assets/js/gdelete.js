var $deleteForm = $.extend({}, $gForm);
$deleteForm.formId = "delete-form";
$deleteForm.onSuccess = function(response){
    swal({
        title: trans('upay.general.deleted_title'),
        text: trans('upay.general.deleted_text'),
        type: "success",
    }, function() {
        if ($search != undefined && $search.table != null) {
            $search.table.draw();
        } else if (response.data.redirect_url != undefined) {
            window.location.href = response.data.redirect_url;
        } else {
            window.location.reload();
        }
    });
};
function gDelete(ids){
    if (ids != undefined) {
        $("#"+$deleteForm.formId+" input[name=ids]").val(ids);
    } else if ($search != undefined) {
        var ids = [];
        $(".check-col:checked", $search.searchTable).each(function(key, item){
            ids.push($(item).val());
        });
        $("#"+$deleteForm.formId+" input[name=ids]").val(ids.join(','));
    }
    swal({
        title: trans('upay.general.delete_modal_title'),
        text: trans('upay.general.delete_modal_text'),
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: trans('upay.general.delete_modal_ok'),
        cancelButtonText: trans('upay.general.delete_modal_cancel'),
        closeOnConfirm: false
    }, function () {
        $deleteForm.form().submit();
    });
    return false;
}

$(document).ready(function() {
    $deleteForm.init();
});
