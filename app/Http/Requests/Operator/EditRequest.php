<?php

namespace App\Http\Requests\Operator;

use App\Http\Requests\Request;
use App\Model;

class EditRequest extends Request
{
    protected $module = 'operator';

    public function rules()
    {
        $emailReq = empty(\Route::getCurrentRoute()->parameters()['id']);
        return $this->checkStatusForRequired([
            'email' => ($emailReq ? 'required|' : '').'email|ends_with:@upay.am',
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'phone' => 'required|regex:/^[0-9]{8}$/',
            'show_status' => 'in:'.Model::STATUS_ACTIVE.','.Model::STATUS_INACTIVE,
        ], request()->input('show_status'));
    }

    protected function defaultValues()
    {
        return [
            'email' => '',
            'first_name' => '',
            'last_name' => '',
            'phone' => '',
            'show_status' => Model::STATUS_INACTIVE,
        ];
    }

    public function messages()
    {
        $messages = parent::messages();
        return array_merge([
            'email.ends_with' => trans('upay.'.$this->module.'.email.not_upay'),
        ], $messages);
    }
}
