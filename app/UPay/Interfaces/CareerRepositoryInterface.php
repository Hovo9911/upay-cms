<?php
namespace App\UPay\Interfaces;

use \App\UPay\Career\Career;
use Illuminate\Database\Eloquent\Collection;

interface CareerRepositoryInterface
{
    public function create($data): Career;

    public function mock(): Career;

    public function findOrFail($id, $withMl = false): Career;

    public function update($id, $data): Career;

    public function delete($ids): int;

    public function saveMl(Career $careerCategory, array $mls): void;

    public function saveImage($newImage, $oldImage = null): void;

    public function all(): Collection;
}
