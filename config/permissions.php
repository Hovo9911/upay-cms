<?php

return [
    'localization' => [ 'view', 'edit', 'delete' ],
    'role' => [ 'view', 'edit', 'delete' ],
    'admin' => [ 'view', 'edit', 'delete', 'reset_password' ],
    'operator' => [ 'view', 'edit', 'reset_password' ],
    'city' => [ 'view', 'edit', 'delete' ],
    'branch' => [ 'view', 'edit', 'delete' ],
    'cashbox' => [ 'view', 'edit', 'delete' ],
    'provider' => [ 'view', 'edit' ],
    'service' => [ 'view', 'edit', 'approve_commissions' ],
    'timeline' => [ 'view', 'edit' ],
    'cancellation' => [ 'view', 'edit' ],
    'report_closing' => [ 'view' ],
    'report_transactions' => [ 'view' ],
    'report_services' => [ 'view' ],
    'failed_transactions' => [ 'view', 'repeat', 'complete' ],
    'slider' => [ 'view', 'edit', 'delete' ],
    'news' => [ 'view', 'edit', 'delete' ],
];
