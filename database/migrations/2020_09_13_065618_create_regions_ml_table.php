<?php
use App\DB\Migration;
use App\DB\Blueprint;

class CreateRegionsMlTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->getSchemaBuilder()->create('regions_ml', function (Blueprint $table) {
            $table->integer('region_id')->unsigned();
            $table->integer('lng_id')->unsigned();
            $table->string('name');
            $table->primary(['region_id', 'lng_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('regions_ml');
    }
}
