<?php

use App\DB\Migration;
use App\DB\Blueprint;

class CreateServiceCheckLogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       $this->getSchemaBuilder()->create('service_check_log', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('service_id');
            $table->string('code');
            $table->jsonb('details');
            $table->timestamp('date');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('service_check_log');
    }
}
