<?php

namespace App\Http\Requests\Report;

use App\Http\Requests\DatatableSearchRequest;

class TransactionSearchRequest extends DatatableSearchRequest
{
    public function rules()
    {
        return [
            'filters.receipt_id' => 'string',
            'filters.code' => 'string',
            'filters.service' => 'integer',
            'filters.cashbox' => 'integer',
            'filters.branch' => 'integer',
            'filters.operator' => 'integer',
            'filters.amount_from' => 'integer',
            'filters.amount_to' => 'integer',
            'filters.date_from' => 'date',
            'filters.date_to' => 'date',
            'order' => 'array',
            'start' => 'integer',
            'format' => 'string'
        ];
    }
}
