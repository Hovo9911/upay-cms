<?php

namespace App\Http\Requests;

use App\Exceptions\GeneralErrorException;
use Illuminate\Contracts\Validation\Validator;

class DeleteRequest extends Request
{
    public function rules()
    {
        return [
            'ids' => 'required|array',
            'ids.*' => 'required|integer'
        ];
    }

    protected function prepareForValidation()
    {
        $this->merge([
            'ids' => isset($this->ids) ? explode(',', $this->ids) : [],
        ]);
    }

    public function messages()
    {
        return [
            'ids.required' => trans('upay.general.delete_empty_error'),
            'ids.*.required' => trans('upay.general.delete_empty_error'),
            'ids.*.*' => trans('upay.general.delete_invalid_error'),
        ];
    }

    protected function failedValidation(Validator $validator) {
        $messages = $validator->errors()->getMessages();
        throw new GeneralErrorException($messages[array_key_first($messages)][0]);
    }
}
