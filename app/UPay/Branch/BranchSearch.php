<?php

namespace App\UPay\Branch;

use App\Search;

class BranchSearch extends Search
{
    protected $orderables = [
        'id',
        'code',
        'armsoft_code',
        'name',
        'address',
        'show_status',
    ];

    protected function query()
    {
        $filters = $this->filters;
        $query = Branch::joinMl()->select(
            'branches.id',
            'branches.code',
            'branches.armsoft_code',
            'city_ml.name as city',
            'ml.name',
            'ml.address',
            'branches.show_status'
        )
        ->joinCity()
        ->when(!empty($filters['id']), function($query) use ($filters) {
            $query->where('branches.id', $filters['id']);
        })
        ->when(!empty($filters['city']), function($query) use ($filters) {
            $query->where('branches.city_id', $filters['city']);
        })
        ->when(!empty($filters['code']), function($query) use ($filters) {
            $query->where('branches.code', $filters['code']);
        })
        ->when(!empty($filters['armsoft_code']), function($query) use ($filters) {
            $query->where('branches.armsoft_code', $filters['armsoft_code']);
        })
        ->when(!empty($filters['name']), function($query) use ($filters) {
            $query->where('ml.name', 'ILIKE', '%'.$filters['name'].'%');
        })
        ->when(!empty($filters['address']), function($query) use ($filters) {
            $query->where('ml.address', 'ILIKE', '%'.$filters['address'].'%');
        })
        ->when(!empty($filters['show_status']), function($query) use ($filters) {
            $query->where('branches.show_status', $filters['show_status']);
        }, function($query) use ($filters) {
            $query->where('branches.show_status', '!=', Branch::STATUS_DELETED);
        });
        return $query;
    }

    public function totalCount()
    {
        return Branch::notDeleted()->count();
    }
}
