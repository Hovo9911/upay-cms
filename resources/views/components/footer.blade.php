<div class="footer">
    <div class="float-right">
        Website by <strong>Helix</strong>
    </div>
    <div>
        <strong>&copy; Copyright</strong> Ucom CJSC, 2020-{{date('Y')}}. All rights reserved.
    </div>
</div>
