<?php

namespace App\UPay\Slider;

use App\GUploader;
use App\UPay\Interfaces\SliderRepositoryInterface;
use DB;
use Illuminate\Database\Eloquent\Collection;

class SliderRepository implements SliderRepositoryInterface
{
    public function create($data): Slider
    {
        $slider = new Slider($data);
        DB::beginTransaction();
        $this->saveImage($data['image']);
        $slider->save();
        $this->saveMl($slider, $data['ml']);
        DB::commit();
        return $slider;
    }

    public function mock(): Slider
    {
        return new Slider([
            'image' => '',
            'sort_order' => '',
            'show_status' => Slider::STATUS_ACTIVE,
        ]);
    }

    public function findOrFail($id, $withMl = false): Slider
    {
        $slider = Slider::where('id', $id)->where('sliders.show_status', '!=', Slider::STATUS_DELETED);

        if ($withMl) {
            $slider->joinMl();
        }
        $slider = $slider->first();

        if (empty($slider)) {
            abort(404);
        }
        return $slider;
    }

    public function update($id, $data): Slider
    {
        $slider = $this->findOrFail($id);

        DB::beginTransaction();
        $this->saveImage($data['image'], $slider->image);
        $slider->update($data);
        $this->saveMl($slider, $data['ml']);
        DB::commit();
        return $slider;
    }

    public function delete($ids): int
    {
        DB::beginTransaction();
        SliderMl::whereIn('slider_id', $ids)->update(['show_status' => SliderMl::STATUS_DELETED]);
        $slider = Slider::whereIn('id', $ids)->update(['show_status' => Slider::STATUS_DELETED]);
        DB::commit();
        return $slider;
    }

    public function saveMl(Slider $slider, array $mls): void
    {
        foreach ($mls as $lngId => $ml) {
            $slider->ml()->updateOrCreate(
                ['slider_id' => $slider->id, 'lng_id' => $lngId],
                [
                    'title' => $ml['title'],
                    'description' => $ml['description'],
                    'button_text_1' => $ml['button_text_1'],
                    'button_url_1' => $ml['button_url_1'],
                    'button_text_2' => $ml['button_text_2'],
                    'button_url_2' => $ml['button_url_2'],
                    'show_status' => $ml['show_status'],
                ]
            );
        }
    }

    public function saveImage($newImage, $oldImage = null): void
    {
        if (!empty($newImage) && (is_null($oldImage) || (!is_null($oldImage) && $newImage != $oldImage))) {
            $gUploader = new GUploader('website_slider_image');
            $gUploader->storePerm($newImage);
        }
    }

    public function all(): Collection
    {
        return Slider::joinMl()
            ->where('sliders.show_status', Slider::STATUS_ACTIVE)
            ->where('ml.show_status', Slider::STATUS_ACTIVE)
            ->get();
    }
}
