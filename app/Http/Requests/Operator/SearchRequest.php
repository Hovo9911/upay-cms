<?php

namespace App\Http\Requests\Operator;

use App\Http\Requests\DatatableSearchRequest;
use App\Model;
use App\UPay\Operator\Operator;

class SearchRequest extends DatatableSearchRequest
{
    public function rules()
    {
        return [
            'filters.id' => 'integer',
            'filters.email' => 'string|max:255',
            'filters.first_name' => 'string|max:255',
            'filters.last_name' => 'string|max:255',
            'filters.phone' => 'string|max:255',
            'filters.status' => 'in:'.Model::STATUS_ACTIVE.','.Model::STATUS_INACTIVE.','.Operator::PASSWORD_RESET_STATUS_PENDING,
            'order' => 'array',
            'start' => 'integer',
        ];
    }
}
