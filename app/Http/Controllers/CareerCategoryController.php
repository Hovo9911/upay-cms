<?php

namespace App\Http\Controllers;

use App\Http\Requests\CareerCategory\EditRequest;
use App\Http\Requests\CareerCategory\SearchRequest;
use App\Http\Requests\DeleteRequest;
use App\UPay\CareerCategory\CareerCategorySearch;
use App\UPay\Interfaces\CareerCategoryRepositoryInterface;
use App\UPay\Interfaces\LanguageRepositoryInterface;
use DB;
use Illuminate\Http\Request;

class CareerCategoryController extends Controller
{
    private $languageRepository;
    private $careerCategoryRepository;

    public function __construct(CareerCategoryRepositoryInterface $careerCategoryRepository,LanguageRepositoryInterface $languageRepository)
    {
        $this->careerCategoryRepository = $careerCategoryRepository;
        $this->languageRepository = $languageRepository;
    }

    public function index()
    {
        return view('website.career_category.list');
    }

    public function edit(Request $request, $id = null)
    {
        if ($id != null) {
            $careerCategory = $this->careerCategoryRepository->findOrFail($id);
            $careerCategory->load([
                'ml',
            ]);

        } else {
            $careerCategory = $this->careerCategoryRepository->mock();
        }
        $careerCategory->ml = $careerCategory->ml->keyBy('lng_id');
        $languages = $this->languageRepository->all();

        return view('website.career_category.edit')->with([
            'mode' => $id != null ? 'edit' : 'add',
            'careerCategory' => $careerCategory,
            'languages' => $languages,
        ]);
    }

    public function save(EditRequest $request, $id = null)
    {
        $data = $request->validated();

        if ($id != null) {
            $this->careerCategoryRepository->findOrFail($id);
            $careerCategory = $this->careerCategoryRepository->update($id, $data);
        } else {
            $careerCategory = $this->careerCategoryRepository->create($data);
        }

        return $this->sendOK([
            'message' => trans('upay.website.career_category.saved.message'),
            'edit_url' => url('/website/career-category/edit/'.$careerCategory->id),
            'list_url' => url('/website/career-category/list')
        ]);
    }

    public function delete(DeleteRequest $request)
    {
        $this->careerCategoryRepository->delete($request->validated()['ids']);
        return $this->sendOK([
            'redirect_url' => url('/website/career-category/list')
        ]);
    }

    public function getList(SearchRequest $request)
    {
        $searcher = new CareerCategorySearch($request->validated());

        return response()->json([
            'data' => $searcher->search(),
            'recordsTotal' => $searcher->totalCount(),
            'recordsFiltered' => $searcher->filteredCount(),
        ]);
    }
}
