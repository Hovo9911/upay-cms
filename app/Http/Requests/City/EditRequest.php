<?php

namespace App\Http\Requests\City;

use App\Http\Requests\Request;
use App\Model;

class EditRequest extends Request
{
    protected $module = 'city';

    public function rules()
    {
        $rules = [
            'region_id' => 'required|integer|nullable',
            'sort_order' => 'required|integer',
            'show_status' => 'in:'.Model::STATUS_ACTIVE.','.Model::STATUS_INACTIVE,
            'ml' => 'required|array',
        ];
        $rules = $this->checkStatusForRequired($rules, request()->input('show_status'));

        $rules = $this->addMlRules($rules, [
            'name' => 'required|max:255',
            'show_status' => 'in:'.Model::STATUS_ACTIVE.','.Model::STATUS_INACTIVE
        ]);

        return $rules;
    }

    protected function defaultValues()
    {
        return [
            'region_id' => null,
            'sort_order' => 0,
            'show_status' => Model::STATUS_INACTIVE,
            'ml' => [
                'name' => '',
                'show_status' => Model::STATUS_INACTIVE,
            ]
        ];
    }
}
