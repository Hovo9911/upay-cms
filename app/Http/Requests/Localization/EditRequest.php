<?php

namespace App\Http\Requests\Localization;

use App\Http\Requests\Request;
use App\UPay\Localization\Localization;

class EditRequest extends Request
{
    protected $module = 'localization';

    public function rules()
    {
        $rules = [
            'group' => 'required|in:' . Localization::GROUP_CMS . ',' . Localization::GROUP_OPERATOR_WWW . ',' . Localization::GROUP_OPERATOR_API,
            'key' => 'required|max:255',
            'ml' => 'required|array',
        ];
        $rules = $this->addMlRules($rules, [
            'value' => 'string|max:30000',
        ]);
        return $rules;
    }

    protected function defaultValues()
    {
        return [
            'ml' => [
                'value' => '',
            ]
        ];
    }
}
