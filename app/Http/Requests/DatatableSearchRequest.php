<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

abstract class DatatableSearchRequest extends Request
{
    protected function failedValidation(Validator $validator) {
        throw new HttpResponseException(response()->json([
            'data' => [],
            'recordsTotal' => 0,
            'recordsFiltered' => 0,
        ], 200));
    }
}
