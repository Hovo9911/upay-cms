<?php
use App\DB\Migration;
use App\DB\Blueprint;

class CreateCitiesMlTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->getSchemaBuilder()->create('cities_ml', function (Blueprint $table) {
            $table->integer('city_id')->unsigned();
            $table->integer('lng_id')->unsigned();
            $table->string('name');
            $table->showStatus();
            $table->primary(['city_id', 'lng_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cities_ml');
    }
}
