<?php

namespace App\Http\Controllers;

use App\Http\Requests\Report\TransactionSearchRequest;
use App\UPay\Interfaces\BranchRepositoryInterface;
use App\UPay\Interfaces\CashboxRepositoryInterface;
use App\UPay\Interfaces\OperatorRepositoryInterface;
use App\UPay\Interfaces\ServiceRepositoryInterface;
use App\UPay\Transaction\TransactionExport;
use App\UPay\Transaction\TransactionSearch;

class ReportTransactionsController extends Controller
{
    public function index(BranchRepositoryInterface $branchRepository, OperatorRepositoryInterface $operatorRepository,
                          CashboxRepositoryInterface $cashboxRepository, ServiceRepositoryInterface $serviceRepository)
    {
        return view('report.transactions.list')->with([
            'branches' => $branchRepository->all(),
            'cashboxes' => $cashboxRepository->all(),
            'operators' => $operatorRepository->all(),
            'services' => $serviceRepository->all()
        ]);
    }

    public function getList(TransactionSearchRequest $request)
    {
        $searcher = new TransactionSearch($request->validated());
        $result = $searcher->search();
        return response()->json([
            'data' => $result,
            'recordsTotal' => $searcher->totalCount(),
            'recordsFiltered' => $searcher->filteredCount(),
        ]);
    }

    public function export(TransactionSearchRequest $request)
    {
        ini_set('memory_limit', '-1');
        $params = $request->validated();
        $format = !empty($params['format']) && $params['format'] == 'csv' ? 'csv' : 'xlsx';
        $type = $format == 'csv' ? \Maatwebsite\Excel\Excel::CSV : \Maatwebsite\Excel\Excel::XLSX;
        return (new TransactionExport($params))->download(trans('upay.report_transactions.export_file_name').'.'.$format, $type);
    }
}
