<?php
namespace App\UPay\Interfaces;

use \App\UPay\City\City;
use Illuminate\Database\Eloquent\Collection;

interface CityRepositoryInterface
{
    public function create($data): City;

    public function mock(): City;

    public function findOrFail($id): City;

    public function update($id, $data): City;

    public function delete($ids): int;

    public function saveMl(City $city, array $mls): void;

    public function allWithRegions(): Collection;

    public function found(int $id): bool;
}
