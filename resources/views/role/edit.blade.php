<?php
    use App\Model;
    $activeMenu = 'role';
    $canDelete = \Auth::user()->hasPermission('role.delete');
    $canEdit = \Auth::user()->hasPermission('role.edit');
    $title = 'upay.role.'.($canEdit ? $mode : 'view').'_title';
    $disabled = $canEdit ? '' : 'disabled';
?>
@extends('layout')
@section('page_title') @lang($title) @stop
@section('content')
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox ">
                    <div class="ibox-title">
                        <h1>@lang($title)</h1>
                    </div>
                    <div class="ibox-content">
                        <form id="save-form" action="{{url('/role/save/'.($role->id ?? ''))}}" method="POST">
                            <div class="form-group required row"><label class="col-lg-2 col-form-label">@lang('upay.role.name')</label>
                                <div class="col-lg-5">
                                    <input type="text" name="name" value="{{$role->name}}" {{$disabled}} class="form-control">
                                    <span class="form-text text-danger form-error-text form-error-name"></span>
                                </div>
                            </div>
                            <div class="form-group row"><label for="general-status" class="col-lg-2 col-form-label">@lang('upay.general.show_status')</label>
                                <div class="col-lg-10">
                                    <input id="general-status" type="checkbox" name="show_status" value="{{Model::STATUS_ACTIVE}}" @if($role->show_status == Model::STATUS_ACTIVE) checked @endif {{$disabled}} data-toggle="switchbutton" data-onlabel="@lang('upay.general.show_status.active')" data-offlabel="@lang('upay.general.show_status.inactive')">
                                    <span class="form-text text-danger form-error-text form-error-show_status"></span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="ibox ">
                                        <div class="ibox-title">
                                            <h5>@lang('upay.role.permissions')</h5>
                                            <div class="ibox-tools"><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></div>
                                        </div>
                                        <div class="ibox-content">
                                            <div class="table-responsive">
                                                <table class="table table-striped permissions-table">
                                                    <thead>
                                                        <tr>
                                                            <th>@lang('upay.role.permissions.module')</th>
                                                            <th>@lang('upay.role.permissions.view')</th>
                                                            <th>@lang('upay.role.permissions.edit')</th>
                                                            <th>@lang('upay.role.permissions.delete')</th>
                                                            <th>@lang('upay.role.permissions.other')</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @foreach($permissions as $module => $options)
                                                        <tr>
                                                            <td>@lang('upay.role.permissions.module.'.$module)</td>
                                                            <td>@if(in_array('view', $options))<input type="checkbox" name="permissions[{{$module}}][]" @if(isset($role->permissions[$module]) && in_array('view', $role->permissions[$module])) checked @endif value="view" {{$disabled}} class="i-checks">@endif</td>
                                                            <td>@if(in_array('edit', $options))<input type="checkbox" name="permissions[{{$module}}][]" @if(isset($role->permissions[$module]) && in_array('edit', $role->permissions[$module])) checked @endif value="edit" {{$disabled}} class="i-checks">@endif</td>
                                                            <td>@if(in_array('delete', $options))<input type="checkbox" name="permissions[{{$module}}][]" @if(isset($role->permissions[$module]) && in_array('delete', $role->permissions[$module])) checked @endif value="delete" {{$disabled}} class="i-checks">@endif</td>
                                                            <td>
                                                                @foreach($options as $option)
                                                                    @if($option == 'view' || $option == 'edit' || $option == 'delete') @continue @endif
                                                                    <label for="other-{{$module}}-{{$option}}" class="checkbox-inline"><input type="checkbox" name="permissions[{{$module}}][]" @if(isset($role->permissions[$module]) && in_array($option, $role->permissions[$module])) checked @endif {{$disabled}} class="i-checks" value="{{$option}}" id="other-{{$module}}-{{$option}}"> @lang('upay.role.permissions.'.$option)</label>
                                                                @endforeach
                                                            </td>
                                                        </tr>
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-lg-12">
                                                    <span class="form-text text-danger form-error-text form-error-permissions"></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            {{csrf_field()}}
                            <hr/>
                            <div class="form-group row">
                                <div class="col-lg-4 col-lg-offset-2">
                                    <a href="{{url('/role/list')}}" class="btn btn-white">@lang('upay.general.buttons.cancel')</a>
                                    @if($mode == 'edit' && $canDelete)<button onclick="gDelete({{$role->id}});return false;" type="button" class="btn btn-danger">@lang('upay.general.buttons.delete')</button>@endif
                                    @if($canEdit)<button class="btn btn-primary submit-btn" type="submit">@lang('upay.general.buttons.save')</button>@endif
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @if($mode == 'edit' && $canDelete)
        @include('components.delete', ['url' => url('/role/delete')])
    @endif
    <script src="{{script_url('upay/js/role.js')}}"></script>
@stop
