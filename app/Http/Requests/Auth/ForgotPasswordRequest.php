<?php

namespace App\Http\Requests\Auth;

use App\Http\Requests\Request;

class ForgotPasswordRequest extends Request
{
    protected $module = 'forgot_password';

    public function rules()
    {
        return [
            'email' => 'required|email|ends_with:@upay.am',
        ];
    }

    public function messages()
    {
        $messages = parent::messages();
        return array_merge([
            'email.ends_with' => trans('upay.'.$this->module.'.email.not_upay'),
        ], $messages);
    }
}
