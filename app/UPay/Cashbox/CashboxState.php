<?php

namespace App\UPay\Cashbox;

use \App\Model;
use App\UPay\Operator\Operator;
use App\UPay\Transaction\Transaction;

class CashboxState extends Model
{
    protected $table = 'cashbox_states';

    protected $fillable = [
        'cashbox_id',
        'is_closed',
        'closing_date',
        'operator_id',
        'initial_balance',
        'cash_in',
        'cash_out',
        'non_cash',
        'final_balance',
    ];
    public function cashbox(){
        return $this->belongsTo(Cashbox::class, 'cashbox_id', 'id')->joinMl();
    }
    public function operator(){
        return $this->belongsTo(Operator::class, 'operator_id', 'id');
    }
    public function transactions(){
        return $this->hasMany(Transaction::class, 'cashbox_state_id', 'id');
    }
}
