const mix = require('laravel-mix');

mix.styles([
    './resources/assets/inspinia/css/bootstrap.min.css',
    './resources/assets/inspinia/font-awesome/css/font-awesome.css',
    './resources/assets/inspinia/css/plugins/iCheck/custom.css',
    './resources/assets/inspinia/css/animate.css',
    './resources/assets/inspinia/css/style.css',
    './resources/assets/inspinia/css/plugins/dataTables/datatables.min.css',
    './resources/assets/inspinia/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css',
    './resources/assets/inspinia/css/plugins/bootstrap-switch-button/bootstrap-switch-button.min.css',
    './resources/assets/inspinia/css/plugins/sweetalert/sweetalert.css',
    './resources/assets/inspinia/css/plugins/datapicker/datepicker3.css',
    './resources/assets/css/general.css',
    './resources/assets/css/select2.min.css',
    './resources/assets/css/datetimepicker.min.css',
],  'public/css/theme.css').version();

mix.copy('resources/assets/inspinia/fonts/', 'public/fonts/');
mix.copy('resources/assets/inspinia/font-awesome/fonts/', 'public/fonts/');
mix.copy('resources/assets/inspinia/css/patterns/', 'public/css/patterns/');
mix.copy(['resources/assets/inspinia/css/plugins/iCheck/green.png', 'resources/assets/inspinia/css/plugins/iCheck/green@2x.png'], 'public/css/');

mix.scripts([
    './resources/assets/inspinia/js/jquery-3.1.1.min.js',
    './resources/assets/inspinia/js/popper.min.js',
    './resources/assets/inspinia/js/bootstrap.js',
    './resources/assets/inspinia/js/plugins/bootstrap-switch-button/bootstrap-switch-button.min.js',
    './resources/assets/inspinia/js/plugins/metisMenu/jquery.metisMenu.js',
    './resources/assets/inspinia/js/plugins/slimscroll/jquery.slimscroll.min.js',
    './resources/assets/inspinia/js/plugins/dataTables/datatables.min.js',
    './resources/assets/inspinia/js/plugins/dataTables/dataTables.bootstrap4.min.js',
    './resources/assets/inspinia/js/inspinia.js',
    './resources/assets/inspinia/js/plugins/iCheck/icheck.min.js',
    './resources/assets/inspinia/js/plugins/pace/pace.min.js',
    './resources/assets/inspinia/js/plugins/sweetalert/sweetalert.min.js',
    './resources/assets/inspinia/js/plugins/datapicker/bootstrap-datepicker.js',
    './resources/assets/js/helpers.js',
    './resources/assets/js/guploader.js',
    './resources/assets/js/gform.js',
    './resources/assets/js/gdelete.js',
    './resources/assets/js/gsearch.js',
    './resources/assets/js/select2.min.js',
    './resources/assets/js/jquery.inputmask.js',
    './resources/assets/js/datetimepicker.min.js',
    './resources/assets/js/tinymce.min.js',
],  'public/js/lib.js').babel(['public/js/lib.js'], 'public/js/lib.js').version();

