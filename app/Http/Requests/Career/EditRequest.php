<?php

namespace App\Http\Requests\Career;

use App\Http\Requests\Request;
use App\Model;
use App\UPay\CareerCategory\CareerCategory;

class EditRequest extends Request
{
    protected $module = 'career';

    public function rules()
    {

        $rules = [
            'image' => 'required|max:255',
            'show_on_homepage' => 'required|boolean',
            'career_category_id' => 'nullable|integer|in:' . implode(',', CareerCategory::getIds()),
            'sort_order' => 'required|integer',
            'show_status' => 'in:' . Model::STATUS_ACTIVE . ',' . Model::STATUS_INACTIVE,
            'ml' => 'required|array',
        ];

        $rules = $this->checkStatusForRequired($rules, request()->input('show_status'));

        $rules = $this->addMlRules($rules, [
            'title' => 'required|max:255',
            'description' => 'required|max:50000',
            'show_status' => 'in:' . Model::STATUS_ACTIVE . ',' . Model::STATUS_INACTIVE
        ]);

        return $rules;
    }

    protected function defaultValues()
    {
        return [
            'image' => '',
            'show_on_homepage' => false,
            'career_category_id' => null,
            'sort_order' => 0,
            'show_status' => Model::STATUS_INACTIVE,
            'ml' => [
                'title' => '',
                'description' => '',
                'show_status' => Model::STATUS_INACTIVE,
            ]
        ];
    }

}
