<?php

use App\DB\Migration;
use App\DB\Blueprint;

class CreateTimelinesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->getSchemaBuilder()->create('timelines', function (Blueprint $table) {
            $table->increments('id');
            $table->date('date');
            $table->integer('cashbox_id')->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('timelines');
    }
}
