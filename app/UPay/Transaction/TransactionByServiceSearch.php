<?php

namespace App\UPay\Transaction;

use App\Search;

class TransactionByServiceSearch extends Search
{
    protected $orderables = [
        'amount',
        'commission',
        'count',
    ];

    protected function query()
    {
        $filters = $this->filters;
        $query = Transaction::with(['service'])->select(
            'service_id',
            \DB::raw('SUM(amount) as amount'),
            \DB::raw('SUM(commission) as commission'),
            \DB::raw('COUNT(*) as count')
        )
        ->where('is_cancelled', false)
        ->where('service_type', '!=', Transaction::SERVICE_TYPE_CANCEL)
        ->when(!empty($filters['service']), function($query) use ($filters) {
            $query->where('service_id', $filters['service']);
        })
        ->when(!empty($filters['date_from']), function($query) use ($filters) {
            $query->where('created_at', '>=', $filters['date_from'].' 00:00:00');
        })
        ->when(!empty($filters['date_to']), function($query) use ($filters) {
            $query->where('created_at', '<=', $filters['date_to'].' 23:59:59');
        })
        ->groupBy('service_id');
        return $query;
    }
}
