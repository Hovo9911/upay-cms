<?php

namespace App\UPay\Cashbox;

use App\Search;

class CashboxSearch extends Search
{
    protected $orderables = [
        'id',
        'name',
        'branch',
        'show_status',
    ];

    protected function query()
    {
        $filters = $this->filters;
        $query = Cashbox::joinMl()->select(
            'cashboxes.id',
            'ml.name',
            'branch_ml.name as branch',
            'cashboxes.account_number',
            'cashboxes.allow_close',
            'cashboxes.allow_pos',
            'cashboxes.pos_account_number',
            'cashboxes.show_status'
        )
        ->joinBranch()
        ->when(!empty($filters['id']), function($query) use ($filters) {
            $query->where('cashboxes.id', $filters['id']);
        })
        ->when(!empty($filters['name']), function($query) use ($filters) {
            $query->where('ml.name', 'ILIKE', '%'.$filters['name'].'%');
        })
        ->when(!empty($filters['branch']), function($query) use ($filters) {
            $query->where('cashboxes.branch_id', $filters['branch']);
        })
        ->when((isset($filters['allow_close']) && $filters['allow_close'] != ''), function($query) use ($filters) {
            $query->where('cashboxes.allow_close', $filters['allow_close']);
        })
        ->when((isset($filters['allow_pos']) && $filters['allow_pos'] != ''), function($query) use ($filters) {
            $query->where('cashboxes.allow_pos', $filters['allow_pos']);
        })
        ->when(!empty($filters['show_status']), function($query) use ($filters) {
            $query->where('cashboxes.show_status', $filters['show_status']);
        }, function($query) use ($filters) {
            $query->where('cashboxes.show_status', '!=', Cashbox::STATUS_DELETED);
        });
        return $query;
    }

    public function totalCount()
    {
        return Cashbox::notDeleted()->count();
    }
}
