<?php

namespace App\UPay\Region;

use App\MultiPKModel;

class RegionMl extends MultiPKModel
{
    protected $table = 'regions_ml';
    public $timestamps = false;
    protected $primaryKey = ['region_id', 'lng_id'];
    public $incrementing = false;
}
