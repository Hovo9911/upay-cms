<?php

namespace App\UPay\Timeline;

use App\UPay\Interfaces\TimelineRepositoryInterface;
use DB;
use Illuminate\Database\Eloquent\Collection;

class TimelineRepository implements TimelineRepositoryInterface
{
    public function save($data): void
    {
        DB::beginTransaction();
        $today = date('Y-m-d');
        $nextMonth = date('Y-m-t', strtotime('next month'));
        //@FIXME maybe you can think of better wheres..
        $timelineIds = Timeline::select('id')
            ->where('date', '>=', $today)
            ->where('date', '<=', $nextMonth)
            ->whereYear('date', '=', $data['year'])
            ->whereMonth('date', lead0($data['month']))
            ->whereIn('cashbox_id', $data['cashbox_ids'])
            ->get()->pluck('id');

        TimelineAssignee::whereIn('timeline_id', $timelineIds)->delete();
        foreach ($data['assignees'] as $assignee) {
            $assigneeDay = $data['year'] . '-' . lead0($data['month']). '-' . lead0($assignee['day']);
            if ($assigneeDay < $today || $assigneeDay > $nextMonth) {
                continue;
            }
            $keys = [
                'date' => $assigneeDay,
                'cashbox_id' => $assignee['cashbox_id'],
            ];
            $timeline = Timeline::where($keys)->first();
            if (empty($timeline) && !empty($assignee['operators'])) {
                $timeline = new Timeline($keys);
                $timeline->save();
            } elseif (!empty($timeline) && !empty($assignee['operators'])) {
                $timeline->touch();
            } elseif (!empty($timeline) && empty($assignee['operators'])) {
                $timeline->delete();
            }

            if (!empty($assignee['operators'])) {
                $operators = [];
                foreach ($assignee['operators'] as $operatorId) {
                    $operators[] = new TimelineAssignee([
                        'timeline_id' => $timeline->id,
                        'operator_id' => $operatorId,
                        'assigned_at' => date('Y-m-d H:i:s')
                    ]);
                }
                $timeline->assignees()->saveMany($operators);
            }
        }
        DB::commit();
    }

    public function getTimeline($year, $month)
    {
        return Timeline::whereYear('date', '=', $year)->whereMonth('date', $month)->with('assignees')->get();
    }

    public function all(): Collection
    {
        return Timeline::selectRaw('extract( year from "date") as "year", extract( month from "date") as "month"')->orderBy('year', 'desc')->get();
    }

    public function checkOperatorsForDay($cashboxId, $date, $operators): bool
    {
        $ids = Timeline::where('date', $date)->where('cashbox_id', '!=', $cashboxId)->get()->pluck('id');
        return empty(TimelineAssignee::whereIn('operator_id', $operators)->whereIn('timeline_id', $ids)->first());
    }
}
