<?php

namespace App\DB;

use Illuminate\Database\Schema\Blueprint as IlluminateBlueprint;
use DB;

class Blueprint extends IlluminateBlueprint
{
    public function showStatus()
    {
        return $this->addColumn('showStatus', 'show_status')->default('1');
    }

//    @FIXME remove this if not necessary
//    public function adminInfo()
//    {
//        $this->integer('created_admin_id')->unsigned()->default(0);
//        $this->string('created_admin_ip')->default('');
//        $this->integer('updated_admin_id')->unsigned()->default(0);
//        $this->string('updated_admin_ip')->default('');
//        $this->integer('deleted_admin_id')->unsigned()->default(0);
//        $this->string('deleted_admin_ip')->default('');
//    }

    public function sortOrder()
    {
        $this->integer('sort_order')->unsigned()->default(0);
    }

    public function getSchemaBuilder()
    {
        DB::connection()->setSchemaGrammar(new Grammar());
        $schema = DB::connection()->getSchemaBuilder();
        $schema->blueprintResolver(function ($table, $callback) {
            return new Blueprint($table, $callback);
        });

        return $schema;
    }

    public function getDB()
    {
        return DB::connection();
    }
}
