@extends('layout')
@section('page_title') @lang('upay.timeline.title') @stop
@section('content')
<?php
    $activeMenu = 'timeline';
    $canEdit = \Auth::user()->hasPermission('timeline.edit');
    use App\UPay\Operator\Operator;
    $jsTrans->add([
        'upay.timeline.assignees'
    ]);
?>
<script>
    var $operators = {!! json_encode($operators) !!};
    var $timeline = {!! json_encode($timeline) !!};
</script>
<div id="save-form-error-scope" class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-title">
                    <h1>@lang('upay.timeline.title')</h1>
                </div>
                <div class="ibox-content">
                    <form id="search-form" method="GET" action="{{url('/timeline')}}" role="form">
                        <div class="row">
                            <div class="col-lg-4">
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label text-right">@lang('upay.timeline.month')</label>
                                    <div class="col-lg-9">
                                        <select class="form-control m-b select-picker" name="m">
                                            @for($i=1;$i<=12;$i++)
                                                <option @if($i == $month) selected @endif value="{{$i}}">{{strftime('%B', strtotime(date('Y').'-'.$i.'-01'))}}</option>
                                            @endfor
                                        </select>
                                        <span class="form-text text-danger form-error-text form-error-month"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label text-right">@lang('upay.timeline.year')</label>
                                    <div class="col-lg-9">
                                        <select class="form-control m-b select-picker" name="y">
                                            @foreach($timelineYears as $y)
                                                <option @if($y == $year) selected @endif value="{{$y}}">{{$y}}</option>
                                            @endforeach
                                        </select>
                                        <span class="form-text text-danger form-error-text form-error-year"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <button class="btn btn-info submit-btn" type="submit">@lang('upay.timeline.select_date')</button>
                            </div>
                        </div>
                    </form>

                    @if($yearMonth > date('Y-m', strtotime('next month')))
                        <h3 class="text-center text-danger mt-5 mb-5"><i class="fa fa-exclamation-triangle"></i> @lang('upay.timeline.max_date_error')</h3>
                    @elseif($yearMonth < date('Y-m') && $timeline->isEmpty())
                        <h3 class="text-center mt-5 mb-5"><i class="fa fa-exclamation-circle"></i> @lang('upay.timeline.empty_error')</h3>
                    @else
                        <form id="save-form" action="{{url('timeline/save')}}">
                            <div class="position-relative">
                                <div class="timeline-table-scroller">
                                    <div class="table-responsive">
                                        <table class="table table-striped">
                                            <tr>
                                                <th class="timeline-table-sticky timeline-table-sticky-col"><div class="timeline-table-sticky-cell">@lang('upay.timeline.edit')</div></th>
                                                <th class="timeline-table-sticky timeline-table-sticky-col1"><div class="timeline-table-sticky-cell">@lang('upay.timeline.branch')</div></th>
                                                <th class="timeline-table-sticky timeline-table-sticky-col2"><div class="timeline-table-sticky-cell">@lang('upay.timeline.cashbox')</div></th>
                                                @for($i=1;$i<=date('t', strtotime($yearMonth.'-01'));$i++)
                                                    <th @if($yearMonth.'-'.lead0($i) == date('Y-m-d')) id="today" class="today" @endif>{{$i.'/'.$month.'/'.substr($year, -2)}}</th>
                                                @endfor
                                            </tr>
                                            @foreach($cashboxes as $branchId => $cashboxList)
                                                @foreach($cashboxList as $cashbox)
                                                    <tr data-branch="{{$branchId}}">
                                                        @if($loop->first)
                                                            <td class="timeline-table-sticky timeline-table-sticky-col" rowspan="{{$cashboxList->count()}}">
                                                                <div class="timeline-table-sticky-cell">
                                                                @if($canEdit && $yearMonth >= date('Y-m'))
                                                                    <button class="btn btn-primary submit-btn save-branch-btn d-none" type="submit">@lang('upay.general.buttons.save')</button>
                                                                    <button data-id="{{$branchId}}" class="btn btn-default edit-branch-btn" type="button">@lang('upay.general.buttons.edit')</button>
                                                                @endif
                                                                </div>
                                                            </td>
                                                            <td class="timeline-table-sticky timeline-table-sticky-col1" rowspan="{{$cashboxList->count()}}">
                                                                <div class="timeline-table-sticky-cell">{{$cashboxList->first()->branch}}</div>
                                                            </td>
                                                        @endif
                                                        <td class="timeline-table-sticky timeline-table-sticky-col2">
                                                            <div class="timeline-table-sticky-cell">
                                                                {{$cashbox->name}}
                                                                <div class="form-group">
                                                                    <span class="form-text text-danger form-error-text form-error-assignees_{{$cashbox->id}}"></span>
                                                                </div>
                                                            </div>
                                                        </td>
                                                        @for($i=1;$i<=date('t', strtotime($yearMonth.'-01'));$i++)
                                                            <?php $disabled = (!$canEdit || $yearMonth.'-'.lead0($i) < date('Y-m-d')) ? 'disabled="disabled"' : ''; ?>
                                                            <td @if($yearMonth.'-'.lead0($i) == date('Y-m-d')) id="today" class="today" @endif>
                                                                <div class="form-group invisible">
                                                                    <div class="preview">
                                                                        @foreach($operators as $operator)
                                                                            @if(isset($timeline[$cashbox->id.'_'.$i]) && $timeline[$cashbox->id.'_'.$i]->assignees->contains('operator_id', $operator->id)) <span>{{username($operator->email)}}</span><br/> @endif
                                                                        @endforeach
                                                                    </div>
                                                                    <div class="day-cell" data-disabled="{{$disabled}}" data-day="{{$i}}" data-cashbox="{{$cashbox->id}}"></div>
                                                                </div>
                                                            </td>
                                                        @endfor
                                                    </tr>
                                                @endforeach
                                            @endforeach
                                        </table>
                                    </div>
                                </div>
                            </div>

                        {{csrf_field()}}
                        <input type="hidden" name="year" value="{{$year}}" />
                        <input type="hidden" name="month" value="{{$month}}" />
                        <input type="hidden" name="branch_id" value="" />

                        <hr/>
                        <div class="form-group row">
                            <div class="col-lg-4 col-lg-offset-2">
                                <a href="{{url('timeline/export?y='.$year.'&m='.$month)}}" target="_blank" class="btn btn-success"><i class="fa fa-download"></i> @lang('upay.general.buttons.export')</a>
                            </div>
                        </div>
                    </form>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
<script src="{{script_url('upay/js/timeline.js')}}"></script>
@stop
