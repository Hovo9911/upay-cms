var $login = $.extend({}, $gForm);
$login.formId = "login-form";

var $forgotPassword = $.extend({}, $gForm);
$forgotPassword.formId = "forgot-password-form";
$forgotPassword.onSuccess = function(response) {
    $("#forgot-password-modal").modal('hide');
    swal({
        title: response.data.title,
        text: response.data.message,
        type: "success",
        showCancelButton: false,
        showConfirmButton: true,
        closeOnConfirm: true
    });
};

$(document).ready(function() {
    $login.init();
    $forgotPassword.init();
    $("#forgot-password-modal").modal({show: false});
    $("#forgot-password-modal").on('hidden.bs.modal', function(){
        $("#"+$forgotPassword.formId+" input[name=email]").val('');
        $forgotPassword.showErrors([]);
    });
});
