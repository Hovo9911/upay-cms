<?php
namespace App\UPay\Interfaces;

use App\UPay\Transaction\Transaction;
use App\UPay\Transaction\TransactionCancellation;

interface TransactionRepositoryInterface
{
    public function findCancellationOrFail($id) : TransactionCancellation;

    public function changeCancellationStatus(TransactionCancellation $cancellation, $data) : void;

    public function cancellationStatusChangeAllowed($cancellation, $from, $to) : bool;

    public function copyToArmsoft(Transaction $transaction);

    public function repeat($id);

    public function complete($id);
}
