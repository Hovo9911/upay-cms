<?php
use App\DB\Migration;
use App\DB\Blueprint;

class AddMobileCheckboxesToServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->getSchemaBuilder()->table('services', function (Blueprint $table) {
            $table->boolean('show_on_homepage')->default(false);
            $table->boolean('is_new')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $this->getSchemaBuilder()->table('services', function (Blueprint $table) {
            $table->dropColumn('show_on_homepage');
            $table->dropColumn('is_new');
        });
    }
}
