<?php

use App\DB\Migration;
use App\DB\Blueprint;

class AddCashboxIdToCancellationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->getSchemaBuilder()->table('transaction_cancellations', function (Blueprint $table) {
            $table->unsignedInteger('cashbox_id')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $this->getSchemaBuilder()->table('transaction_cancellations', function (Blueprint $table) {
            $table->dropColumn('cashbox_id');
        });
    }
}
