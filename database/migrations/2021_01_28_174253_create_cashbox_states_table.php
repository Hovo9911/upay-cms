<?php

use App\DB\Migration;
use App\DB\Blueprint;

class CreateCashboxStatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->getSchemaBuilder()->create('cashbox_states', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('cashbox_id');
            $table->boolean('is_closed');
            $table->timestamp('closing_date')->nullable();
            $table->unsignedInteger('operator_id')->nullable();
            $table->float('initial_balance');
            $table->float('cash_in');
            $table->float('cash_out');
            $table->float('non_cash');
            $table->float('final_balance');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cashbox_states');
    }
}
