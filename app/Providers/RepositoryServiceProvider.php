<?php

namespace App\Providers;

use App\UPay\Branch\BranchRepository;
use App\UPay\Career\CareerRepository;
use App\UPay\CareerCategory\CareerCategoryRepository;
use App\UPay\Cashbox\CashboxRepository;
use App\UPay\City\CityRepository;
use App\UPay\Faq\FaqRepository;
use App\UPay\FaqCategory\FaqCategoryRepository;
use App\UPay\Interfaces\BranchRepositoryInterface;
use App\UPay\Interfaces\CareerCategoryRepositoryInterface;
use App\UPay\Interfaces\CareerRepositoryInterface;
use App\UPay\Interfaces\CashboxRepositoryInterface;
use App\UPay\Interfaces\CityRepositoryInterface;
use App\UPay\Interfaces\FaqCategoryRepositoryInterface;
use App\UPay\Interfaces\FaqRepositoryInterface;
use App\UPay\Interfaces\LanguageRepositoryInterface;
use App\UPay\Interfaces\LocalizationRepositoryInterface;
use App\UPay\Interfaces\NewsRepositoryInterface;
use App\UPay\Interfaces\OperatorRepositoryInterface;
use App\UPay\Interfaces\ProviderRepositoryInterface;
use App\UPay\Interfaces\RegionRepositoryInterface;
use App\UPay\Interfaces\ResetPasswordRepositoryInterface;
use App\UPay\Interfaces\ServiceRepositoryInterface;
use App\UPay\Interfaces\SliderRepositoryInterface;
use App\UPay\Interfaces\TimelineRepositoryInterface;
use App\UPay\Interfaces\TransactionRepositoryInterface;
use App\UPay\Interfaces\AdminRepositoryInterface;
use App\UPay\Interfaces\RoleRepositoryInterface;
use App\UPay\Language\LanguageRepository;
use App\UPay\Localization\LocalizationRepository;
use App\UPay\News\NewsRepository;
use App\UPay\Operator\OperatorRepository;
use App\UPay\Provider\ProviderRepository;
use App\UPay\Region\RegionRepository;
use App\UPay\ResetPasswordRepository;
use App\UPay\Service\ServiceRepository;
use App\UPay\Slider\SliderRepository;
use App\UPay\Timeline\TimelineRepository;
use App\UPay\Transaction\TransactionRepository;
use App\UPay\Admin\AdminRepository;
use App\UPay\Role\RoleRepository;
use Illuminate\Support\ServiceProvider;

/**
 * Class RepositoryServiceProvider
 * @package App\Providers
 */
class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(AdminRepositoryInterface::class, AdminRepository::class);
        $this->app->bind(RoleRepositoryInterface::class, RoleRepository::class);
        $this->app->bind(BranchRepositoryInterface::class, BranchRepository::class);
        $this->app->bind(CityRepositoryInterface::class, CityRepository::class);
        $this->app->bind(RegionRepositoryInterface::class, RegionRepository::class);
        $this->app->bind(OperatorRepositoryInterface::class, OperatorRepository::class);
        $this->app->bind(CashboxRepositoryInterface::class, CashboxRepository::class);
        $this->app->bind(ProviderRepositoryInterface::class, ProviderRepository::class);
        $this->app->bind(ServiceRepositoryInterface::class, ServiceRepository::class);
        $this->app->bind(TimelineRepositoryInterface::class, TimelineRepository::class);
        $this->app->bind(SliderRepositoryInterface::class, SliderRepository::class);
        $this->app->bind(NewsRepositoryInterface::class, NewsRepository::class);
        $this->app->bind(CareerCategoryRepositoryInterface::class, CareerCategoryRepository::class);
        $this->app->bind(CareerRepositoryInterface::class, CareerRepository::class);
        $this->app->bind(FaqCategoryRepositoryInterface::class, FaqCategoryRepository::class);
        $this->app->bind(FaqRepositoryInterface::class, FaqRepository::class);
        $this->app->singleton(ResetPasswordRepositoryInterface::class, ResetPasswordRepository::class);
        $this->app->singleton(LanguageRepositoryInterface::class, LanguageRepository::class);
        $this->app->singleton(LocalizationRepositoryInterface::class, LocalizationRepository::class);
        $this->app->singleton(TransactionRepositoryInterface::class, TransactionRepository::class);
    }
}
