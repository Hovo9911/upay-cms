<?php

namespace App\UPay\FaqCategory;

use App\MultiPKModel;

class FaqCategoryMl extends MultiPKModel
{
    protected $table = 'faq_categories_ml';
    public $timestamps = false;
    protected $primaryKey = ['faq_category_id', 'lng_id'];
    public $incrementing = false;

    protected $fillable = [
        'faq_category_id',
        'lng_id',
        'title',
        'show_status',
    ];
}
