<?php

namespace App\Http\Requests\Service;

use App\Http\Requests\Request;
use App\Model;
use App\UPay\Service\Commission;
use App\UPay\Service\CommissionValue;
use App\UPay\Service\Service;

class EditRequest extends Request
{
    protected $module = 'service';

    public function rules()
    {
        $rules = [
            'provider_id' => 'required|integer|nullable',
            'branch_account_number' => 'required|max:255|regex:/^[0-9]+$/',
            'branch_commission_account_number' => 'required|max:255|regex:/^[0-9]+$/',
            'mobile_account_number' => 'max:255|regex:/^[0-9]+$/',
            'mobile_commission_account_number' => 'max:255|regex:/^[0-9]+$/',
            'account_min' => 'required_with:account_max|integer|nullable|min:1',
            'account_max' => 'required_with:account_min|integer|nullable|min:1|gte:account_min',
            'use_fixed_amount' => 'boolean',
            'is_encashment' => 'boolean',
            'encashment_type' => 'required_if:is_encashment,1|nullable|in:'.Service::ENCASHMENT_IN.','.Service::ENCASHMENT_OUT,
            'logo' => 'required|max:255',
            'hidden' => 'boolean',
            'type' => 'required|in:'.Service::TYPE_TRANSFER.','.Service::TYPE_PAY,
            'for_mobile' => 'boolean',
            'for_ops' => 'boolean',
            'show_on_homepage' => 'boolean',
            'is_new' => 'boolean',
            'adapter' => 'required|max:255',
            'sort_order' => 'required|integer',
            'show_status' => 'in:'.Model::STATUS_ACTIVE.','.Model::STATUS_INACTIVE,
            'ml' => 'required|array',
        ];

        $rules = $this->checkStatusForRequired($rules, request()->input('show_status'));

        foreach (Commission::getTypes() as $type) {
            foreach (Commission::getSources() as $src) {
                $commRules = [
                    'commissions_'.$type.'_'.$src => 'required|array',
                    'commissions_'.$type.'_'.$src.'_show_status' => 'in:'.Model::STATUS_ACTIVE.','.Model::STATUS_INACTIVE,
                    'commissions_'.$type.'_'.$src.'.*.type' => 'required|in:'.CommissionValue::TYPE_FIXED.','.CommissionValue::TYPE_PERCENT,
                    'commissions_'.$type.'_'.$src.'.*.min' => 'required_with:commissions_'.$type.'_'.$src.'.*.max|numeric|nullable|min:0',
                    'commissions_'.$type.'_'.$src.'.*.max' => 'numeric|nullable|min:0',
                    'commissions_'.$type.'_'.$src.'.*.amount' => 'required_if:commissions_'.$type.'_'.$src.'.*.type,'.CommissionValue::TYPE_FIXED.'|numeric|min:0|nullable',
                    'commissions_'.$type.'_'.$src.'.*.percent' => 'required_if:commissions_'.$type.'_'.$src.'.*.type,'.CommissionValue::TYPE_PERCENT.'|numeric|min:0|max:100|nullable',
                ];
                $commRules = $this->checkStatusForRequired($commRules, request()->input('commissions_'.$type.'_'.$src.'_show_status'));
                $rules = array_merge($rules, $commRules);
            }
        }

        $rules = $this->addMlRules($rules, [
            'name' => 'required|max:255',
            'payment_purpose' => 'required|max:255',
            'invoice_note' => 'string|max:1024',
            'title' => 'string|max:255',
            'description' => 'string|max:1024',
            'order_purpose_password' => 'string|max:1024',
            'order_received' => 'string|max:1024',
            'order_reason' => 'string|max:1024',
            'show_status' => 'in:'.Model::STATUS_ACTIVE.','.Model::STATUS_INACTIVE
        ]);
        return $rules;
    }

    protected function defaultValues()
    {
        $values = [
            'provider_id' => null,
            'branch_account_number' => '',
            'branch_commission_account_number' => '',
            'mobile_account_number' => '',
            'mobile_commission_account_number' => '',
            'account_min' => null,
            'account_max' => null,
            'use_fixed_amount' => 0,
            'is_encashment' => 0,
            'encashment_type' => null,
            'hidden' => false,
            'type' => null,
            'for_mobile' => false,
            'for_ops' => false,
            'show_on_homepage' => false,
            'is_new' => false,
            'logo' => '',
            'adapter' => '',
            'sort_order' => 0,
            'show_status' => Model::STATUS_INACTIVE,
            'ml' => [
                'name' => '',
                'payment_purpose' => '',
                'invoice_note' => '',
                'title' => '',
                'description' => '',
                'order_purpose_password' => '',
                'order_received' => '',
                'order_reason' => '',
                'show_status' => Model::STATUS_INACTIVE,
            ]
        ];
        foreach (Commission::getTypes() as $type) {
            foreach (Commission::getSources() as $src) {
                $values['commissions_'.$type.'_'.$src.'_show_status'] = Model::STATUS_INACTIVE;
                $values['commissions_'.$type.'_'.$src.'_id'] = null;
                $values['commissions_'.$type.'_'.$src] = [
                    'type' => CommissionValue::TYPE_FIXED,
                    'min' => null,
                    'max' => null,
                    'amount' => null,
                    'percent' => null,
                ];
            }
        }
        return $values;
    }

    public function messages()
    {
        $messages = parent::messages();
        $messages = array_merge([
            'account_min.required_with' => trans('upay.'.$this->module.'.account_min.required_with_max'),
            'account_max.required_with' => trans('upay.'.$this->module.'.account_max.required_with_min'),
            'account_max.gt' => trans('upay.'.$this->module.'.account_max.lt_min'),
            'encashment_type.required_if' => trans('upay.'.$this->module.'.encashment_type.required'),
        ], $messages);
        foreach (Commission::getTypes() as $type) {
            foreach (Commission::getSources() as $src) {
                //@FIXME find a better way
                unset($messages['commissions_'.$type.'_'.$src.'.*.type.*']);
                unset($messages['commissions_'.$type.'_'.$src.'.*.min.*']);
                unset($messages['commissions_'.$type.'_'.$src.'.*.max.*']);
                unset($messages['commissions_'.$type.'_'.$src.'.*.amount.*']);
                unset($messages['commissions_'.$type.'_'.$src.'.*.percent.*']);
                unset($messages['commissions_'.$type.'_'.$src.'.*']);
                unset($messages['commissions_'.$type.'_'.$src.'.required']);
                $messages = array_merge([
                    'commissions_'.$type.'_'.$src.'.required' => trans('upay.'.$this->module.'.commissions.ranges.required'),
                    'commissions_'.$type.'_'.$src.'_show_status.*' => trans('upay.general.show_status.invalid'),
                    'commissions_'.$type.'_'.$src.'.*.min.required_with' => trans('upay.'.$this->module.'.commissions.min.required_with_max'),
                    'commissions_'.$type.'_'.$src.'.*.amount.required' => trans('upay.'.$this->module.'.commissions.value.required'),
                    'commissions_'.$type.'_'.$src.'.*.amount.required_if' => trans('upay.'.$this->module.'.commissions.value.required'),
                    'commissions_'.$type.'_'.$src.'.*.percent.required' => trans('upay.'.$this->module.'.commissions.value.required'),
                    'commissions_'.$type.'_'.$src.'.*.percent.required_if' => trans('upay.'.$this->module.'.commissions.value.required'),
                    'commissions_'.$type.'_'.$src.'.*.type.required' => trans('upay.'.$this->module.'.commissions.type.required'),
                    'commissions_'.$type.'_'.$src.'.*.type.*' => trans('upay.'.$this->module.'.commissions.type.invalid'),
                    'commissions_'.$type.'_'.$src.'.*.min.*' => trans('upay.'.$this->module.'.commissions.min.invalid'),
                    'commissions_'.$type.'_'.$src.'.*.max.*' => trans('upay.'.$this->module.'.commissions.max.invalid'),
                    'commissions_'.$type.'_'.$src.'.*.amount.*' => trans('upay.'.$this->module.'.commissions.amount.invalid'),
                    'commissions_'.$type.'_'.$src.'.*.percent.*' => trans('upay.'.$this->module.'.commissions.percent.invalid'),
                ], $messages);
            }
        }
        return $messages;
    }

}
