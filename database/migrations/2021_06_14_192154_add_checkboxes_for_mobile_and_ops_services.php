<?php
use App\DB\Migration;
use App\DB\Blueprint;

class AddCheckboxesForMobileAndOpsServices extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->getSchemaBuilder()->table('services', function (Blueprint $table) {
            $table->boolean('for_mobile')->default(true);
            $table->boolean('for_ops')->default(true);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $this->getSchemaBuilder()->table('services', function (Blueprint $table) {
            $table->dropColumn('for_mobile');
            $table->dropColumn('for_ops');
        });
    }
}
