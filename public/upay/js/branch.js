var $saveForm = $.extend({}, $gForm);
$saveForm.formId = "save-form";

var $search = $.extend({}, $gsearch);

var $branchMap = {
    marker: null,
    geocoder: null,
    map: null,
    autocomplete: null,
    placeMarker: function(position) {
        var self = this;
        var marker = new google.maps.Marker({
            position: position,
            map: self.map
        });
        $("#address-lat").val(position.lat());
        $("#address-lng").val(position.lng());
        self.marker = marker;
        self.map.panTo(position);
    },
    init: function(){
        var self = this;
        var center = {lat: 40.1800511, lng: 44.5056414};
        var disableMap = $("#"+$saveForm.formId).data('disabled');
        self.map = new google.maps.Map(document.getElementById('map'), {
            zoom: 15,
            center: center,
            disableDefaultUI: disableMap
        });
        self.geocoder = new google.maps.Geocoder();
        if ($("#address-lat").val() != '' && $("#address-lng").val() != '') {
            self.placeMarker(new google.maps.LatLng($("#address-lat").val(), $("#address-lng").val()));
        }
        if (!disableMap) {
            self.map.addListener('click', function(e) {
                self.googleMapAddMarker(e);
            });
        }

        // Create the search box and link it to the UI element.
        var input = document.getElementById('pac-input');
        var searchBox = new google.maps.places.SearchBox(input);
        self.map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

        // Bias the SearchBox results towards current map's viewport.
        self.map.addListener('bounds_changed', function() {
            searchBox.setBounds(self.map.getBounds());
        });

        // Listen for the event fired when the user selects a prediction and retrieve
        // more details for that place.
        self.autocomplete = new google.maps.places.Autocomplete(input);
        google.maps.event.addListener(self.autocomplete, 'place_changed', self.autoCompleteCallback );
        searchBox.addListener('places_changed', function(e) {
            var places = searchBox.getPlaces();
            if (places.length == 0) {
                return;
            }
            // For each place, get the icon, name and location.
            var bounds = new google.maps.LatLngBounds();
            var place = places[0];
            if (!place.geometry) {
                console.log("Returned place contains no geometry");
                return;
            }
            self.placeMarker(place.geometry.location);

            if (place.geometry.viewport) {
                // Only geocodes have viewport.
                bounds.union(place.geometry.viewport);
            } else {
                bounds.extend(place.geometry.location);
            }
            self.map.fitBounds(bounds);
        });
        $("#pac-input").bind("keydown", function (e) {
            if (e.keyCode == 13) {
                return false;
            }
        });
    },
    autoCompleteCallback: function() {
        var self = $branchMap;
        var place = self.autocomplete.getPlace();

        if (place.geometry.viewport) {
            self.map.fitBounds(place.geometry.viewport);
        } else {
            self.map.setCenter(place.geometry.location);
            self.map.setZoom(15);
        }
        if (self.marker !== null) {
            self.marker.setMap(null);
        }
        var cPosition = place.geometry.location;
        $("#address-lat").val(cPosition.lat());
        $("#address-lng").val(cPosition.lng());
        self.marker = new google.maps.Marker({position: cPosition, map: self.map});
        self.geocodePosition(self.marker.getPosition());
    },
    googleMapAddMarker: function(event){
        var self = this;
        var startLocation = event.latLng;

        if (self.marker !== null){
            self.marker.setMap(null);
        }
        self.marker = new google.maps.Marker({position: startLocation, map: self.map});
        self.geocodePosition(self.marker.getPosition());

        $("#address-lat").val(self.marker.getPosition().lat());
        $("#address-lng").val(self.marker.getPosition().lng());
    },
    geocodePosition: function(pos) {
        var self = this;
        self.geocoder.geocode({
            latLng: pos
        }, function(responses) {
            if (responses && responses.length > 0) {
                return self.updateAddress(responses[0]);
            } else {
                return self.updateAddress('Cannot determine address at this location.', true);
            }
        });
    },
    updateAddress: function(address, placeHolder){
        var tmp = $('.address');
        if(placeHolder){
            tmp.attr("placeholder", "Cannot determine address at this location.");
            return;
        }
        tmp.val(address.formatted_address);
    }
};

$(document).ready(function() {
    if ($($search.searchFormSelector).length) {
        $search.init();
    } else {
        $saveForm.init();
    }
});
