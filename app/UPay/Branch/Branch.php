<?php

namespace App\UPay\Branch;

use \App\Model;
use App\UPay\City\City;

class Branch extends Model
{
    protected $table = 'branches';

    protected $fillable = [
        'code',
        'armsoft_code',
        'city_id',
        'lat',
        'lng',
        'sort_order',
        'show_status',
    ];

    public function ml(){
        return $this->hasMany(BranchMl::class, 'branch_id', 'id');
    }

    public function scopeJoinMl($query, $notDeleted = true){
        $query->join('branches_ml as ml', function ($join) use($notDeleted) {
            $join->on('branches.id', '=', 'ml.branch_id')
                ->where('ml.lng_id', '=', c_lng('id'));
            if ($notDeleted) {
                $join->where('ml.show_status', '!=', BranchMl::STATUS_DELETED);
            }
        });
    }

    public function scopeJoinCity($query){
        $query->leftJoin('cities', function ($join){
            $join->on('branches.city_id', '=', 'cities.id')
                ->where('cities.show_status', '!=', City::STATUS_DELETED);
        })->leftJoin('cities_ml as city_ml', function ($join){
            $join->on('cities.id', '=', 'city_ml.city_id')
                ->where('city_ml.lng_id', '=', c_lng('id'));
        });
    }
}
