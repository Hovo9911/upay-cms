<?php

namespace App\UPay\Career;

use App\Search;

class CareerSearch extends Search
{
    protected $orderables = [
        'id',
        'title',
        'sort_order',
        'show_status',
    ];

    protected function query()
    {
        $filters = $this->filters;

        return  Career::joinMl()->select(
            'career.id',
            'ml.title',
            'career.show_status'
        )
            ->when(!empty($filters['id']), function($query) use ($filters) {
                $query->where('career.id', $filters['id']);
            })
            ->when(!empty($filters['title']), function($query) use ($filters) {
                $query->where('ml.title', 'ILIKE', '%'.$filters['title'].'%');
            })
            ->when(!empty($filters['show_status']), function($query) use ($filters) {

                $query->where('career.show_status', $filters['show_status']);
            }, function($query) use ($filters) {
                $query->where('career.show_status', '!=', Career::STATUS_DELETED);
            });

    }

    public function totalCount()
    {
        return Career::notDeleted()->count();
    }
}
