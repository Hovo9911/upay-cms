var $saveForm = $.extend({}, $gForm);
$saveForm.formId = "save-form";
$saveForm.onSuccess = function(){
    $("#edit-localization-modal").modal('hide');
    $search.refresh();
};

var $search = $.extend({}, $gsearch);
$search.modifyColumns = {
    group: {
        visible: false
    },
    ml: {
        visible: false
    },
    actions: {
        render: function (data, type, row, meta) {
            if (type === 'display') {
                var actions = '';
                if ($search.defaultActions.view) {
                    actions += '<button class="btn btn-xs btn-success" onclick="viewLocalization(' + meta.row + ')"><i class="fa fa-eye"></i></button> ';
                }
                if ($search.defaultActions.edit) {
                    actions += '<button class="btn btn-xs btn-success" onclick="editLocalization(' + meta.row + ')"><i class="fa fa-pencil"></i></button> ';
                }
                if ($search.defaultActions.delete) {
                    actions += '<button class="btn btn-xs btn-danger" onclick="gDelete(' + data + ');return false;"><i class="fa fa-remove"></i></a>';
                }
                return actions;
            }
            return  data;
        }
    }
};

function editLocalization(rowIndex) {
    $saveForm.showErrors([]);
    $("#edit-localization-modal .modal-title").text($("#edit-localization-modal .modal-title").attr('edit-title'));
    var localization = $search.table.row(rowIndex).data();
    $saveForm.form().attr('action', $saveForm.form().attr('action-url')+'/'+localization.id);
    $("#edit-localization-modal [name=group]").val(localization.group).change();
    $("#edit-localization-modal [name=key]").val(localization.key).attr('readonly', 'readonly');
    for (var i in localization.ml) {
        $("#edit-localization-modal [name='ml["+localization.ml[i].lng_id+"][value]']").val(localization.ml[i].value);
    }
    $("#edit-localization-modal .submit-btn").show();
    $("#edit-localization-modal").modal('show');
}

function viewLocalization(rowIndex) {
    $saveForm.showErrors([]);
    $("#edit-localization-modal .modal-title").text($("#edit-localization-modal .modal-title").attr('view-title'));
    var localization = $search.table.row(rowIndex).data();
    $("#edit-localization-modal [name=group]").val(localization.group).change().attr('disabled', 'disabled');
    $("#edit-localization-modal [name=key]").val(localization.key).attr('readonly', 'readonly');
    for (var i in localization.ml) {
        $("#edit-localization-modal [name='ml["+localization.ml[i].lng_id+"][value]']").val(localization.ml[i].value);
    }
    $("#edit-localization-modal textarea").attr('disabled', 'disabled');
    $("#edit-localization-modal .submit-btn").hide();
    $("#edit-localization-modal").modal('show');
}

function addLocalization() {
    $saveForm.showErrors([]);
    $("#edit-localization-modal .modal-title").text($("#edit-localization-modal .modal-title").attr('add-title'));
    $saveForm.form().attr('action', $saveForm.form().attr('action-url'));
    $("#edit-localization-modal input[type=text], #edit-localization-modal textarea").val('');
    $("#edit-localization-modal [name=key]").removeAttr('readonly');
    $("#edit-localization-modal").modal('show');
}

$(document).ready(function() {
    $search.init();
    $saveForm.init();
    $("#edit-localization-modal").modal({show: false});
});
