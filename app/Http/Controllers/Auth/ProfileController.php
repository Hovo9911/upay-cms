<?php

namespace App\Http\Controllers\Auth;

use App\Exceptions\InvalidDataException;
use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\ChangePasswordRequest;
use App\UPay\Interfaces\AdminRepositoryInterface;
use App\UPay\Admin\Admin;
use Illuminate\Support\Facades\Hash;

class ProfileController extends Controller
{
    private $adminRepository;

    public function __construct(AdminRepositoryInterface $adminRepository)
    {
        $this->adminRepository = $adminRepository;
    }

    public function index()
    {
        return view('auth.profile')->with([
            'admin' => \Auth::user()
        ]);
    }

    public function changePassword(ChangePasswordRequest $request)
    {
        $admin = \Auth::user();
        $data = $request->validated();
        if (!Hash::check($data['old_password'], $admin->password)) {
            throw InvalidDataException::withErrors(['old_password' => trans('upay.change_password.old_password.wrong')]);
        }
        $this->adminRepository->update($admin, [
            'password' => bcrypt($data['password']),
            'password_reset_status' => Admin::PASSWORD_RESET_STATUS_CONFIRMED,
            'password_updated_at' => date('Y-m-d H:i:s')
        ]);

        return $this->sendOK([
            'message' => trans('upay.change_password.success'),
            'redirect_url' => url('/')
        ]);
    }

}
