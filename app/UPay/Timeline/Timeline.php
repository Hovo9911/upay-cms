<?php

namespace App\UPay\Timeline;

use \App\Model;
use App\UPay\Cashbox\Cashbox;

class Timeline extends Model
{
    protected $table = 'timelines';

    protected $fillable = [
        'date',
        'cashbox_id',
    ];

    public function assignees(){
        return $this->hasMany(TimelineAssignee::class, 'timeline_id', 'id');
    }

    public function cashbox(){
        return $this->belongsTo(Cashbox::class, 'cashbox_id', 'id')->joinMl(false);
    }

}
