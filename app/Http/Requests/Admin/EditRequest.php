<?php

namespace App\Http\Requests\Admin;

use App\Http\Requests\Request;
use App\Model;

class EditRequest extends Request
{
    protected $module = 'admin';

    public function rules()
    {
        return $this->checkStatusForRequired([
            'email' => 'required|email|ends_with:@upay.am',
            'name' => 'required|max:255',
            'show_status' => 'in:'.Model::STATUS_ACTIVE.','.Model::STATUS_INACTIVE,
            'roles' => 'required|array',
            'roles.*' => 'integer'
        ], request()->input('show_status'));
    }

    protected function defaultValues()
    {
        return [
            'email' => '',
            'name' => '',
            'roles' => [],
            'show_status' => Model::STATUS_INACTIVE,
        ];
    }

    public function messages()
    {
        $messages = parent::messages();
        return array_merge([
            'email.ends_with' => trans('upay.'.$this->module.'.email.not_upay'),
        ], $messages);
    }
}
