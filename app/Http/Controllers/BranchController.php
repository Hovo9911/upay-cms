<?php

namespace App\Http\Controllers;

use App\Exceptions\InvalidDataException;
use App\Http\Requests\Branch\EditRequest;
use App\Http\Requests\Branch\SearchRequest;
use App\Http\Requests\DeleteRequest;
use App\UPay\Branch\BranchSearch;
use App\UPay\Interfaces\BranchRepositoryInterface;
use App\UPay\Interfaces\CityRepositoryInterface;
use App\UPay\Interfaces\LanguageRepositoryInterface;
use Illuminate\Http\Request;

class BranchController extends Controller
{
    private $branchRepository;
    private $languageRepository;
    private $cityRepository;

    public function __construct(BranchRepositoryInterface $branchRepository, LanguageRepositoryInterface $languageRepository, CityRepositoryInterface $cityRepository)
    {
        $this->branchRepository = $branchRepository;
        $this->languageRepository = $languageRepository;
        $this->cityRepository = $cityRepository;
    }

    public function index()
    {
        $cities = $this->cityRepository->allWithRegions();
        return view('branch.list')->with([
            'cities' => $cities
        ]);
    }

    public function edit(Request $request, $id = null)
    {
        if ($id != null) {
            $branch = $this->branchRepository->findOrFail($id);
            $branch->load('ml');
        } else {
            $branch = $this->branchRepository->mock();
        }
        $branch->ml = $branch->ml->keyBy('lng_id');
        $languages = $this->languageRepository->all();
        $cities = $this->cityRepository->allWithRegions();
        return view('branch.edit')->with([
            'mode' => $id != null ? 'edit' : 'add',
            'branch' => $branch,
            'languages' => $languages,
            'cities' => $cities
        ]);
    }

    public function save(EditRequest $request, $id = null)
    {
        $data = $request->validated();
        if (!empty($data['city_id']) && !$this->cityRepository->found($data['city_id'])) {
            throw InvalidDataException::withErrors(['city_id' => trans('upay.branch.city_id.not_found')]);
        }
        if ($this->branchRepository->codeFound($data['code'], $id)) {
            throw InvalidDataException::withErrors(['code' => trans('upay.branch.code.exists')]);
        }
        if ($this->branchRepository->armsoftCodeFound($data['armsoft_code'], $id)) {
            throw InvalidDataException::withErrors(['armsoft_code' => trans('upay.branch.armsoft_code.exists')]);
        }
        if ($id != null) {
            $branch = $this->branchRepository->update($id, $data);
        } else {
            $branch = $this->branchRepository->create($data);
        }
        return $this->sendOK([
            'message' => trans('upay.branch.saved.message'),
            'edit_url' => url('/branch/edit/'.$branch->id),
            'list_url' => url('/branch/list')
        ]);
    }

    public function delete(DeleteRequest $request)
    {
        $this->branchRepository->delete($request->validated()['ids']);
        return $this->sendOK([
            'redirect_url' => url('/branch/list')
        ]);
    }

    public function getCashboxes(Request $request, $id)
    {
        return $this->sendOK($this->branchRepository->getCashboxes($id));
    }

    public function getList(SearchRequest $request)
    {
        $searcher = new BranchSearch($request->validated());
        return response()->json([
            'data' => $searcher->search(),
            'recordsTotal' => $searcher->totalCount(),
            'recordsFiltered' => $searcher->filteredCount(),
        ]);
    }
}
