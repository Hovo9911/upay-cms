<?php

namespace App\Http\Controllers;

use App\Exceptions\InvalidDataException;
use App\Http\Requests\DeleteRequest;
use App\Http\Requests\Role\EditRequest;
use App\Http\Requests\Role\SearchRequest;
use App\UPay\Interfaces\RoleRepositoryInterface;
use App\UPay\Role\RoleSearch;
use Illuminate\Http\Request;

class RoleController extends Controller
{
    private $roleRepository;

    public function __construct(RoleRepositoryInterface $roleRepository)
    {
        $this->roleRepository = $roleRepository;
    }

    public function index()
    {
        return view('role.list');
    }

    public function edit(Request $request, $id = null)
    {
        if ($id != null) {
            $role = $this->roleRepository->findOrFail($id);
        } else {
            $role = $this->roleRepository->mock();
        }
        $permissions = config('permissions');
        return view('role.edit')->with([
            'mode' => $id != null ? 'edit' : 'add',
            'role' => $role,
            'permissions' => $permissions
        ]);
    }

    public function save(EditRequest $request, $id = null)
    {
        $data = $request->validated();
        $data['permissions'] = $this->validatePermissions($data['permissions']);
        if ($id != null) {
            $role = $this->roleRepository->update($id, $data);
        } else {
            $role = $this->roleRepository->create($data);
        }
        return $this->sendOK([
            'message' => trans('upay.role.saved.message'),
            'edit_url' => url('/role/edit/'.$role->id),
            'list_url' => url('/role/list')
        ]);
    }

    public function delete(DeleteRequest $request)
    {
        $this->roleRepository->delete($request->validated()['ids']);
        return $this->sendOK([
            'redirect_url' => url('/role/list')
        ]);
    }

    public function getList(SearchRequest $request)
    {
        $searcher = new RoleSearch($request->validated());
        return response()->json([
            'data' => $searcher->search(),
            'recordsTotal' => $searcher->totalCount(),
            'recordsFiltered' => $searcher->filteredCount(),
        ]);
    }

    private function validatePermissions($data){
        $allPermissions = config('permissions');
        foreach ($data as $module => $permissions) {
            if (!isset($allPermissions[$module])) {
                throw InvalidDataException::withErrors([
                    'permissions' => trans('upay.role.permissions.invalid')
                ]);
            }
            foreach ($permissions as $permission) {
                if (!in_array($permission, $allPermissions[$module])) {
                    throw InvalidDataException::withErrors([
                        'permissions' => trans('upay.role.permissions.invalid')
                    ]);
                }
                if (($permission == 'edit' || $permission == 'delete') && !in_array('view', $permissions)) {
                    $data[$module][] = 'view';
                }
            }
        }
        return $data;
    }
}
