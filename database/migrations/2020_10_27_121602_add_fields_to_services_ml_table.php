<?php

use App\DB\Migration;
use App\DB\Blueprint;

class AddFieldsToServicesMlTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->getSchemaBuilder()->table('services_ml', function (Blueprint $table) {
            $table->string('title')->default('');
            $table->string('description', 1024)->default('');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $this->getSchemaBuilder()->table('services_ml', function (Blueprint $table) {
            $table->dropColumn('title');
            $table->dropColumn('description');
        });
    }
}
