<?php

use App\UPay\Interfaces\LocalizationRepositoryInterface;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\App;

class LocalizationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('localizations')->truncate();
        \DB::table('localizations_ml')->truncate();
        $locRepo = App::make(LocalizationRepositoryInterface::class);
        $locRepo->syncBack();
    }
}
