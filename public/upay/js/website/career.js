var $saveForm = $.extend({}, $gForm);
$saveForm.formId = "save-form";

var $search = $.extend({}, $gsearch);

$(document).ready(function() {
    if ($($search.searchFormSelector).length) {
        $search.init();
    } else {
        $saveForm.init();
    }
});
