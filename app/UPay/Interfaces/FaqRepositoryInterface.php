<?php
namespace App\UPay\Interfaces;

use \App\UPay\Faq\Faq;
use Illuminate\Database\Eloquent\Collection;

interface FaqRepositoryInterface
{
    public function create($data): Faq;

    public function mock(): Faq;

    public function findOrFail($id, $withMl = false): Faq;

    public function update($id, $data): Faq;

    public function delete($ids): int;

    public function saveMl(Faq $faq, array $mls): void;

    public function all(): Collection;
}
