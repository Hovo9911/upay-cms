<?php

namespace App\UPay\CareerCategory;

use App\Model;

class CareerCategory extends Model
{
    protected $table = 'career_categories';

    protected $fillable = [
        'sort_order',
        'show_status',
    ];

    public function ml()
    {
        return $this->hasMany(CareerCategoryMl::class, 'career_category_id', 'id');
    }

    public function scopeJoinMl($query)
    {
        $query->join('career_categories_ml as ml', function ($join) {
            $join->on('career_categories.id', '=', 'ml.career_category_id')
                ->where('ml.lng_id', '=', c_lng('id'))
                ->where('ml.show_status', '!=', CareerCategoryMl::STATUS_DELETED);
        });
    }

    public static function getIds()
    {
        return self::where('show_status', Model::STATUS_ACTIVE)->pluck('id')->all();
    }
}
