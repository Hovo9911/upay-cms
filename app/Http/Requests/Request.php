<?php

namespace App\Http\Requests;

use App\Model;
use App\UPay\Interfaces\LanguageRepositoryInterface;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Support\Facades\App;

abstract class Request extends FormRequest
{
    protected $module = 'general';

    protected $generalFields = [
        'show_status',
        'sort_order'
    ];

    public function authorize()
    {
        return true;
    }

    public function validationData()
    {
        $data = parent::validationData();
        if ($this->isJson()) {
            $json = json_decode($this->getContent(), 1);
            $data = !is_null($json) ? $json : $data;
        }
        return $data;
    }

    protected function checkStatusForRequired($rules, $status)
    {
        if ($status != Model::STATUS_ACTIVE) {
            foreach ($rules as $key => $value) {
                if (is_array($value)) {
                    unset($value['required']);
                    $rules[$key] = $value;
                } else {
                    $rules[$key] = preg_replace("/required.*?\|/", "", $value);
                }
            }
        }
        return $rules;
    }

    protected function addMlRules($rules, $mlRules)
    {
        $languageRepository = App::make(LanguageRepositoryInterface::class);
        $languages = $languageRepository->all();
        foreach ($languages as $lng) {
            $mlRule = [];
            foreach ($mlRules as $field => $rule) {
                $mlRule['ml.'.$lng->id.'.'.$field] = $rule;
            }
            if (isset($mlRules['show_status'])) {
                $mlRule = $this->checkStatusForRequired($mlRule, request()->input('ml.'.$lng->id.'.show_status'));
            }
            $rules = array_merge($rules, $mlRule);
        }
        return $rules;
    }

    protected function failedValidation(Validator $validator) {
        $errors = [];
        foreach ($validator->errors()->getMessages() as $key => $error) {
            $key = str_replace(".", "_", $key);
            $errors[$key] = implode(', ', $error);
        }
        $errors = $this->processErrors($errors);
        throw new HttpResponseException(response()->json([
            'status' => 'INVALID_DATA',
            'errors' => $errors
        ], 200));
    }

    protected function processErrors($errors){
        return $errors;
    }

    protected function defaultValues()
    {
        return [];
    }

    protected function prepareForValidation()
    {
        $inputs = [];
        foreach ($this->defaultValues() as $field => $value) {
            if (is_null($value) || is_int($value)) {
                $inputs[$field] = !empty($this->$field) || $this->$field === 0 || $this->$field === '0' ? $this->$field : $value;
            } else if (is_array($value) && !empty($value)) {
                $subs = [];
                if (!empty($this->$field)) {
                    foreach ($this->$field as $key => $values) {
                        $subs[$key] = [];
                        foreach ($value as $subKey => $subVal) {
                            if (is_null($subVal) || is_int($subVal)) {
                                $subs[$key][$subKey] = !empty($values[$subKey]) || $values[$subKey] === 0 || $values[$subKey] === '0' ? $values[$subKey] : $subVal;
                            } else {
                                $subs[$key][$subKey] = isset($values[$subKey]) ? $values[$subKey] : $subVal;
                            }
                        }
                    }
                }
                $this->merge([$field => $subs]);
            } else {
                $inputs[$field] = isset($this->$field) ? $this->$field : $value;
            }
        }
        $this->merge($inputs);
    }

    public function messages()
    {
        $messages = [];
        foreach ($this->rules() as $field => $rule) {
            $key = preg_replace('/(.+)\.(.+)\.(.+)/i', '$1.*.$3', $field);
            $field = strrpos($key, '.') !== false ? substr($key, strrpos($key, '.') + 1) : $key;
            $module = in_array($field, $this->generalFields) ? 'general' : $this->module;
            $messages[$key.'.required'] = trans('upay.'.$module.'.'.$field.'.required');
            $messages[$key.'.*'] = trans('upay.'.$module.'.'.$field.'.invalid');
        }
        return $messages;
    }
}
