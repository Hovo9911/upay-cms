<?php

namespace App\UPay;

use App\Mailer\Mailer;
use App\UPay\Interfaces\ResetPasswordRepositoryInterface;
use App\UPay\Operator\Operator;
use App\UPay\Admin\Admin;

class ResetPasswordRepository implements ResetPasswordRepositoryInterface
{
    public function sendAdminPassword(Admin $admin): void
    {
        $password = generate_password();
        $admin->update([
            'password' => bcrypt($password),
            'password_reset_status' => Admin::PASSWORD_RESET_STATUS_PENDING
        ]);
        $mailer = new Mailer();
        $mailer->send([
            'to' => $admin->email,
            'subject' => trans('upay.admin.reset_password.email.subject'),
            'body' => $password
        ]);
    }

    public function sendOperatorPassword(Operator $operator): void
    {
        $password = generate_password(8);
        $operator->update([
            'password' => bcrypt($password),
            'password_reset_status' => Operator::PASSWORD_RESET_STATUS_PENDING
        ]);
        $mailer = new Mailer();
        $mailer->send([
            'to' => $operator->email,
            'subject' => trans('upay.operator.reset_password.email.subject'),
            'body' => $password
        ]);
    }

    public function sendAdminLink(Admin $admin): void
    {
        $hash = $this->generateForgotPasswordHash();
        $admin->update([
            'forgot_password_hash' => $hash,
            'forgot_password_hash_exp_date' => date('Y-m-d H:i:s', strtotime('+1 day'))
        ]);
        $mailer = new Mailer();
        $mailer->send([
            'to' => $admin->email,
            'subject' => trans('upay.forgot_password.email.subject'),
            'body' => url('/forgot-password/reset/'.$hash)
        ]);
    }

    private function generateForgotPasswordHash() : string
    {
        $hash = \Str::random(40);
        if (empty(Admin::where('forgot_password_hash', $hash)->first())) {
            return $hash;
        }
        return $this->generateForgotPasswordHash();
    }
}
