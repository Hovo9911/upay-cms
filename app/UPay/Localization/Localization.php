<?php

namespace App\UPay\Localization;

use \App\Model;

class Localization extends Model
{
    const GROUP_CMS = 'cms';
    const GROUP_OPERATOR_WWW = 'operator_www';
    const GROUP_OPERATOR_API = 'operator_api';
    protected $table = 'localizations';

    protected $fillable = [
        'group',
        'key',
    ];

    public function ml(){
        return $this->hasMany(LocalizationMl::class, 'localization_id', 'id');
    }

    public function scopeJoinMl($query){
        $query->join('localizations_ml as ml', function ($join){
            $join->on('localizations.id', '=', 'ml.localization_id')
                ->where('ml.lng_id', '=', c_lng('id'));
        });
    }

}
