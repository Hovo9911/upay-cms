var $saveForm = $.extend({}, $gForm);
$saveForm.formId = "save-form";
$saveForm.errorsScope = $("#save-form-error-scope");
$saveForm.onSuccess = function(response){
    swal({
        title: trans('upay.general.saved_modal.title'),
        text: response.data.message,
        type: "success",
        showCancelButton: false,
        showConfirmButton: true,
        closeOnConfirm: true
    }, function() {
        window.location.reload();
    });
};

function quitEditMode(){
    $("input[name=branch_id]").val('');
    $(".preview").show();
    $(".day-cell").html('');
    $(".edit-branch-btn").show();
    $(".save-branch-btn").addClass('d-none');
}

function scrollToToday()
{
    $('.timeline-table-scroller .table-responsive').animate({scrollLeft: $('.timeline-table-scroller .table-responsive').scrollLeft()+$('#today').position().left-450/* width of first 2 sticky cols*/}, 500);
}

$(document).ready(function() {
    $saveForm.init();
    $(".invisible").removeClass('invisible');
    setTimeout(scrollToToday, 100);
    $('.timeline-table-scroller .table-responsive').css('max-height', window.innerHeight-330);

    $(".edit-branch-btn").click(function(){
        quitEditMode();
        var container = $("tr[data-branch="+$(this).data('id')+"]");
        $("input[name=branch_id]").val($(this).data('id'));
        $(".preview", container).hide();
        $(".day-cell", container).each(function() {
            var operatorOptions = '';
            for(var i in $operators) {
                operatorOptions += '<option '+(typeof $timeline[$(this).data('cashbox')+'_'+$(this).data('day')] != 'undefined' && $timeline[$(this).data('cashbox')+'_'+$(this).data('day')].assignees.filter(function(item){return item.operator_id == $operators[i].id}).length ? 'selected="selected"' : '')+ ' ' +($operators[i].show_status != '1' ? 'disabled="disabled"' : '')+' value="'+$operators[i].id+'">'+($operators[i].email.substr(0, $operators[i].email.indexOf('@')))+'</option>';
            }
            $(this).html(
                '<select '+$(this).data('disabled')+' name="assignees['+$(this).data('cashbox')+'_'+$(this).data('day')+'][operators][]" style="width: auto;" data-placeholder="'+trans('upay.timeline.assignees')+'" class="form-control m-b operator-select-picker" multiple>' +
                    operatorOptions +
                '</select>' +
                '<input '+$(this).data('disabled')+' type="hidden" name="assignees['+$(this).data('cashbox')+'_'+$(this).data('day')+'][day]" value="'+$(this).data('day')+'" />' +
                '<input '+$(this).data('disabled')+' type="hidden" name="assignees['+$(this).data('cashbox')+'_'+$(this).data('day')+'][cashbox_id]" value="'+$(this).data('cashbox')+'" />' +
                '<span class="form-text text-danger form-error-text form-error-assignees_'+$(this).data('cashbox')+'_'+$(this).data('day')+'_day"></span>' +
                '<span class="form-text text-danger form-error-text form-error-assignees_'+$(this).data('cashbox')+'_'+$(this).data('day')+'_operators"></span>' +
                '<span class="form-text text-danger form-error-text form-error-assignees_'+$(this).data('cashbox')+'_'+$(this).data('day')+'_cashbox_id"></span>'
            )
        });
        $(".operator-select-picker", container).select2({
            minimumResultsForSearch: 15,
            dropdownAutoWidth : true,
            maximumSelectionLength: 10
        });
        $(this).hide();
        $(".save-branch-btn", $(this).closest('td')).removeClass('d-none');
        scrollToToday();
        return false;
    });
});
