<?php
use App\DB\Migration;
use App\DB\Blueprint;

class CreateCashboxesMlTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->getSchemaBuilder()->create('cashboxes_ml', function (Blueprint $table) {
            $table->integer('cashbox_id')->unsigned();
            $table->integer('lng_id')->unsigned();
            $table->string('name');
            $table->showStatus();
            $table->primary(['cashbox_id', 'lng_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cashboxes_ml');
    }
}
