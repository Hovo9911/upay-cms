<?php

namespace App\UPay\City;

use \App\Model;
use App\UPay\Region\Region;

class City extends Model
{
    protected $table = 'cities';

    protected $fillable = [
        'region_id',
        'sort_order',
        'show_status',
    ];

    public function ml(){
        return $this->hasMany(CityMl::class, 'city_id', 'id');
    }

    public function scopeJoinMl($query){
        $query->join('cities_ml as ml', function ($join){
            $join->on('cities.id', '=', 'ml.city_id')
                ->where('ml.lng_id', '=', c_lng('id'))
                ->where('ml.show_status', '!=', CityMl::STATUS_DELETED);
        });
    }

    public function scopeJoinRegion($query){
        $query->leftJoin('regions', function ($join){
            $join->on('cities.region_id', '=', 'regions.id')
                ->where('regions.show_status', '!=', Region::STATUS_DELETED);
        })->leftJoin('regions_ml as region_ml', function ($join){
            $join->on('regions.id', '=', 'region_ml.region_id')
                ->where('region_ml.lng_id', '=', c_lng('id'));
        });
    }
}
