@extends('layout')
@section('page_title') @lang('upay.home.title') @stop
@section('content')
    <div class="wrapper wrapper-content h-100">
        <div class="d-flex justify-content-center align-items-center h-100">
            <div class="animated bounce"><h1>Welcome! <i class="fa fa-smile-o animated bounce infinite"></i></h1></div>
        </div>
    </div>
@stop
