<?php

use App\DB\Migration;
use App\DB\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSlidersMlTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->getSchemaBuilder()->create('sliders_ml', function (Blueprint $table) {
            $table->integer('slider_id')->unsigned();
            $table->integer('lng_id')->unsigned();
            $table->string('title');
            $table->string('description');
            $table->string('button_text_1');
            $table->string('button_url_1');
            $table->string('button_text_2');
            $table->string('button_url_2');
            $table->showStatus();
            $table->primary(['slider_id', 'lng_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sliders_ml');
    }
}
