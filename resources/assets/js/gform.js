var $gForm = {
    gLocked: false,
    formId: "form-data",
    errorsScope: null,
    preventSubmit: false,
    form: function(){
        return $("#"+this.formId)
    },
    beforeSubmit: function(){},
    onSuccess: function(response) {
        if (response.data && response.data.redirect_url != undefined) {
            window.location.href = response.data.redirect_url;
        } else {
            swal({
                title: trans('upay.general.saved_modal.title'),
                text: (response.data && response.data.message != undefined ? response.data.message : ''),
                type: "success",
                showCancelButton: true,
                confirmButtonText: trans('upay.general.saved_modal.to_edit'),
                cancelButtonText: trans('upay.general.saved_modal.to_list'),
                closeOnConfirm: false,
                closeOnCancel: false
            }, function(confirmed) {
                if (confirmed && response.data.edit_url != undefined) {
                    window.location.href = response.data.edit_url;
                } else if (!confirmed && response.data.list_url != undefined) {
                    window.location.href = response.data.list_url;
                }
            });
        }
    },
    onError: function(response){
        this.showErrors(response.errors);
    },
    onProcessResponse: function(){},
    cleanError:  function(){
        $(this).closest(".form-group.has-error").removeClass("has-error");
        $(".form-error-text", $(this).closest(".form-group")).hide().html('');
    },
    showErrors: function(errors){
        $(".form-error-text", this.errorsScope).hide().html('');
        $(".form-group", this.errorsScope).removeClass('has-error');
        for(var i in errors){
            $(".form-error-" + i, this.errorsScope).html(errors[i]).fadeIn();
            $(".form-error-" + i, this.errorsScope).closest('.form-group').addClass('has-error');
        }
        $('.nav-link').removeClass('text-danger');
        var tab1 = $(".form-group.has-error").first().closest('.tab-pane');
        $('.nav-link[href="#'+tab1.attr('id')+'"]').addClass('text-danger');
        tab1.parents('.tab-pane').each(function(){
            $('.nav-link[href="#'+$(this).attr('id')+'"]').addClass('text-danger');
        })
    },
    processResponse: globalProcessResponse,
    enableForm: function(){
        var self = this;
        $(".submit-btn", $("#" + self.formId)).removeAttr('disabled').removeClass('loading');
    },
    disableForm: function(){
        var self = this;
        $(".submit-btn", $("#" + self.formId)).attr('disabled', 'disabled').addClass('loading');
    },
    init: function(){
        var self = this;
        if (self.errorsScope == null) {
            self.errorsScope = $("#" + self.formId);
        }

        $("#"+self.formId).off().on('submit', function(){

            self.beforeSubmit();
            if (self.preventSubmit) {
                return false;
            }

            if (self.gLocked) {
                return false;
            }
            self.gLocked = true;
            self.disableForm();

            var method = $("#"+self.formId).attr('method');
            method = (typeof method == 'undefined') ? 'post' : method;
            $.ajax({
                url: $("#"+self.formId).attr('action'),
                type: method,
                data: $("#"+self.formId).serialize(),
                dataType: 'json',
                xhrFields: {
                    withCredentials: true
                },
                success: function(response){
                    if (!response.data || typeof response.data.redirect_url == 'undefined') {
                        self.enableForm();
                    }
                    if(response.status == 'OK'){
                        self.showErrors([]);
                        self.onSuccess(response);
                    } else if(response.status == 'INVALID_DATA'){
                        self.onError(response);
                    } else {
                        self.showErrors([]);
                        self.onProcessResponse(response);
                        self.processResponse(response);
                    }
                    self.gLocked = false;
                },
                error: function(response){
                    self.enableForm();
                    self.gLocked = false;
                    self.onProcessResponse(response.responseJSON);
                    self.processResponse(response.responseJSON);
                }
            });
            return false;
        });
        self.setupFocus();
    },
    setupFocus: function(context){
        if (typeof context == 'undefined') {
            context = $("#"+this.formId);
        }
        $("input", context).on("focus", this.cleanError);
        $("select", context).on("select2:opening", this.cleanError);
        $("textarea", context).on("focus", this.cleanError);
    }
};
