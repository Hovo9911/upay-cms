<?php

namespace App\UPay\Transaction;

use \App\Model;

class ArmsoftTransaction extends Model
{
    const STATUS_PENDING = 'pending';
    const STATUS_SUCCEED = 'succeed';
    const STATUS_FAILED = 'failed';

    const TYPE_TRANSACTION = 'transaction';
    const TYPE_ORDER = 'order';
    const TYPE_CANCELLATION = 'cancellation';

    protected $table = 'armsoft_transactions';

    protected $fillable = [
        'transaction_type',
        'transaction_id',
        'transaction_date',
        'amount',
        'account_credit',
        'account_debit',
        'outer_account_credit',
        'outer_account_debit',
        'aim',
        'branch_code',
        'receipt_id',
        'status',
        'currency',
    ];
}
