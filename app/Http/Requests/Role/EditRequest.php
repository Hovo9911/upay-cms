<?php

namespace App\Http\Requests\Role;

use App\Http\Requests\Request;
use App\Model;

class EditRequest extends Request
{
    protected $module = 'role';

    public function rules()
    {
        return $this->checkStatusForRequired([
            'name' => 'required|max:255',
            'show_status' => 'in:'.Model::STATUS_ACTIVE.','.Model::STATUS_INACTIVE,
            'permissions' => 'array'
        ], request()->input('show_status'));
    }

    protected function defaultValues()
    {
        return [
            'name' => '',
            'show_status' => Model::STATUS_INACTIVE,
            'permissions' => []
        ];
    }
}
