<?php

namespace App\Exceptions;

use App\Providers\RouteServiceProvider;
use Exception;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Session\TokenMismatchException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\HttpException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        InvalidDataException::class,
        GeneralErrorException::class
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Exception  $exception
     * @return void
     *
     * @throws \Exception
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws \Exception
     */
    public function render($request, Exception $exception)
    {
        if ($request->expectsJson()) {
            if ($exception instanceof InvalidDataException) {
                return response()->json([
                    'status' => 'INVALID_DATA',
                    'errors' => $exception->getErrors()
                ]);
            } elseif ($exception instanceof GeneralErrorException) {
                return response()->json([
                    'status' => 'ERROR',
                    'message' => $exception->getMessage()
                ]);
            } elseif ($exception instanceof NotFoundHttpException ||
                $exception instanceof ModelNotFoundException ||
                $exception instanceof MethodNotAllowedHttpException) {
                return response()->json([
                    'status' => 'NOT_FOUND',
                    'message' => trans('upay.general.errors.not_found_error_message')
                ]);
            } elseif ($exception instanceof TokenMismatchException) {
                return response()->json([
                    'status' => 'TOKEN_MISMATCH',
                    'message' => trans('upay.general.errors.token_mismatch_error_message')
                ]);
            } elseif ($exception instanceof AuthenticationException) {
                return response()->json([
                    'status' => 'UNAUTHORIZED',
                    'message' => trans('upay.general.errors.auth_error_message'),
                    'redirect_url' => RouteServiceProvider::HOME
                ]);
            } elseif ($exception instanceof HttpException) {
                if ($exception->getStatusCode() == 403) {
                    return response()->json([
                        'status' => 'FORBIDDEN',
                        'message' => trans('upay.general.errors.access_denied')
                    ]);
                }
            }
            return response()->json([
                'status' => 'APP_ERROR',
                'message' => trans('upay.general.errors.app_error_message')
            ]);
        }
        return parent::render($request, $exception);
    }
}
