<?php

namespace App\Http\Requests\Report;

use App\Http\Requests\DatatableSearchRequest;

class ClosingSearchRequest extends DatatableSearchRequest
{
    public function rules()
    {
        return [
            'filters.cashbox' => 'integer',
            'filters.operator' => 'integer',
            'filters.auto_closed' => 'bool',
            'filters.date_from' => 'date',
            'filters.date_to' => 'date',
            'order' => 'array',
            'start' => 'integer',
            'format' => 'string',
        ];
    }
}
