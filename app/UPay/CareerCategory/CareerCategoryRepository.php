<?php

namespace App\UPay\CareerCategory;


use App\UPay\Interfaces\CareerCategoryRepositoryInterface;
use DB;
use Illuminate\Database\Eloquent\Collection;

class CareerCategoryRepository implements CareerCategoryRepositoryInterface
{
    public function create($data): CareerCategory
    {
        $career = new CareerCategory($data);
        DB::beginTransaction();
        $career->save();
        $this->saveMl($career, $data['ml']);
        DB::commit();
        return $career;
    }

    public function mock(): CareerCategory
    {
        return new CareerCategory([
            'sort_order' => '',
            'show_status' => CareerCategory::STATUS_ACTIVE,
        ]);
    }

    public function findOrFail($id, $withMl = false): CareerCategory
    {
        $career = CareerCategory::where('id', $id)->where('career_categories.show_status', '!=', CareerCategory::STATUS_DELETED);

        if ($withMl) {
            $career->joinMl();
        }
        $career = $career->first();

        if (empty($career)) {
            abort(404);
        }
        return $career;
    }

    public function update($id, $data): CareerCategory
    {
        $career = $this->findOrFail($id);

        DB::beginTransaction();
        $career->update($data);
        $this->saveMl($career, $data['ml']);
        DB::commit();
        return $career;
    }

    public function delete($ids): int
    {
        DB::beginTransaction();
        CareerCategoryMl::whereIn('career_category_id', $ids)->update(['show_status' => CareerCategoryMl::STATUS_DELETED]);
        $career= CareerCategory::whereIn('id', $ids)->update(['show_status' => CareerCategory::STATUS_DELETED]);
        DB::commit();
        return $career;
    }

    public function saveMl(CareerCategory $careerCategory, array $mls): void
    {
        foreach ($mls as $lngId => $ml) {
            $careerCategory->ml()->updateOrCreate(
                ['career_category_id' => $careerCategory->id, 'lng_id' => $lngId],
                [
                    'title' => $ml['title'],
                    'show_status' => $ml['show_status'],
                ]
            );
        }
    }

    public function all(): Collection
    {
        return CareerCategory::joinMl()
            ->where('career_categories.show_status', CareerCategory::STATUS_ACTIVE)
            ->where('ml.show_status', CareerCategory::STATUS_ACTIVE)
            ->get();
    }
}
