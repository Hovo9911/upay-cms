<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>@yield('page_title') @lang('upay.page_title.postfix')</title>

    <link rel="stylesheet" href="{{mix('css/theme.css')}}"/>
    {!! $jsTrans->output() !!}
    <script src="{{mix('js/lib.js')}}"></script>
</head>

<body>

<div id="wrapper">

    <nav class="navbar-default navbar-static-side" role="navigation">
        <div class="sidebar-collapse">
            <ul class="nav metismenu" id="side-menu">
                <li class="nav-header">
                    <div class="dropdown profile-element">
                        <i class="align-ce fa fa-3x fa-user-circle text-white-50"></i>
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <span class="block m-t-xs font-bold text-wrap text-break=">{{\Auth::user()->name}}</span>
                            <span class="text-muted text-xs block text-wrap text-break">{{\Auth::user()->email}}<b
                                    class="caret"></b></span>
                        </a>
                        <ul class="dropdown-menu animated fadeInRight m-t-xs">
                            <li><a class="dropdown-item" href="{{url('/profile')}}">@lang('upay.menu.profile')</a></li>
                            <li class="dropdown-divider"></li>
                            <li><a class="dropdown-item logout-btn"><i class="fa fa-sign-out"></i> @lang('upay.logout')
                                </a></li>
                        </ul>
                    </div>
                </li>
                <?php $menuLevels = isset($activeMenu) ? explode('-', $activeMenu) : ['']; ?>
                @foreach(config('menu') as $menu)
                    @if(!empty($menu['subs']) && \Auth::user()->hasMenuPermission($menu['alias']))
                        <li @if(isset($activeMenu) && $menu['alias'] == $menuLevels[0]) class="active" @endif>
                            <a href="#"><i class="fa fa-{{$menu['icon']}}"></i> <span
                                    class="nav-label">@lang('upay.menu.'.$menu['alias'])</span> <span
                                    class="fa arrow"></span></a>
                            <ul class="nav nav-second-level collapse">
                                @foreach($menu['subs'] as $sub)
                                    @if(\Auth::user()->hasPermission($menu['permission']))

                                        <li @if(isset($activeMenu)  && $menu['alias'] == $menuLevels[0] && $sub['alias'] == $menuLevels[1]) class="active" @endif>
                                            <a href="{{url($sub['url'])}}">
                                                <i class="fa fa-{{$sub['icon']}}"></i>
                                                <span
                                                    class="nav-label">@lang('upay.menu.'.$menu['alias'].'.'.$sub['alias'])</span>
                                            </a>
                                        </li>
                                    @endif
                                @endforeach
                            </ul>
                        </li>
                    @else
                        @if(\Auth::user()->hasPermission($menu['permission']))
                            <li @if(isset($activeMenu) && $menu['alias'] == $activeMenu) class="active" @endif>
                                <a href="{{url($menu['url'])}}"><i class="fa fa-{{$menu['icon']}}"></i> <span
                                        class="nav-label">@lang('upay.menu.'.$menu['alias'])</span></a>
                            </li>
                        @endif
                    @endif
                @endforeach
            </ul>

        </div>
    </nav>

    <div id="page-wrapper" class="gray-bg">
        @include('components.header')

        @yield('content')

        @include('components.footer')

    </div>
</div>

<form id="logout-form" method="POST" action="{{route('logout')}}">
    {{csrf_field()}}
</form>
<script src="{{script_url('upay/js/layout.js')}}"></script>

</body>

</html>
