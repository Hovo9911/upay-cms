<?php

use App\UPay\Interfaces\LanguageRepositoryInterface;
use App\UPay\Transaction\TransactionCancellation;

function generate_password($length = 10)
{
    //this all is for 8 and 10 lengths

    $l = 'abcdefghijklmnopqrstuvwxyz';
    $u = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $d = '0123456789';
    $s = '!"#$%&\'()*+,-./:;<=>?@[\]^_`{|}~';

    $ls = sub_rand($l, 4);
    $us = sub_rand($u, $length - 4 - 2);
    $ds = $d[random_int(0, strlen($d)-1)];
    $ss = $s[random_int(0, strlen($s)-1)];

    $password = str_shuffle($ls.$us.$ds.$ss);
    return $password;
}

function sub_rand($set, $size) {
    $set = str_split($set);
    $result = '';
    for ($i = 0; $i < $size; $i++) {
        $result .= $set[random_int(0, count($set)-1)];
    }
    return $result;
}


function c_lng($key = null)
{
    $languageRepository = app(LanguageRepositoryInterface::class);
    $cLng = $languageRepository->current();
    if ($key != null) {
        return $cLng->{$key};
    }
    return $cLng;
}

function script_url($url)
{
    return url($url.'?v='.env('SCRIPT_VERSION', 1));
}

function style_url($url)
{
    return url($url.'?v='.env('STYLE_VERSION', 1));
}

function lead0($number)
{
    return $number < 10 ? '0'.$number : $number;
}

function username($email)
{
    return substr($email, 0, strpos($email, '@'));
}

function operator_www_url($uri, $lngCode = null)
{
    $lngCode = is_null($lngCode) ? c_lng('code') : $lngCode;
    return env('OPERATOR_WWW_URL').$lngCode.'/'.$uri;
}

function getCancellationStatusColor($status)
{
    $statusColor = 'badge-primary';
    switch($status) {
        case TransactionCancellation::STATUS_NEW:
        case TransactionCancellation::STATUS_PROCESSING:
            $statusColor = 'badge-warning';
            break;
        case TransactionCancellation::STATUS_ABORTED:
        case TransactionCancellation::STATUS_REJECTED:
        case TransactionCancellation::STATUS_PRE_DECLINED:
        case TransactionCancellation::STATUS_FINALLY_DECLINED:
            $statusColor = 'badge-danger';
            break;
    }
    return $statusColor;
}

function format_date($date, $time = true)
{
    if ($time) {
        return date('d-m-Y H:i:s', strtotime($date));
    }
    return date('d-m-Y', strtotime($date));
}
