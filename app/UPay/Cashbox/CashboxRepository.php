<?php

namespace App\UPay\Cashbox;

use App\UPay\Branch\Branch;
use App\UPay\Branch\BranchMl;
use App\UPay\Interfaces\CashboxRepositoryInterface;
use App\UPay\Transaction\Transaction;
use DB;
use Illuminate\Database\Eloquent\Collection;

class CashboxRepository implements CashboxRepositoryInterface
{
    public function create($data): Cashbox
    {
        $cashbox = new Cashbox($data);
        DB::beginTransaction();
        $cashbox->save();
        $this->saveMl($cashbox, $data['ml']);
        DB::commit();
        return $cashbox;
    }

    public function mock(): Cashbox
    {
        return new Cashbox([
            'branch_id' => null,
            'account_number' => '',
            'certificate_id' => '',
            'allow_close' => false,
            'allow_pos' => false,
            'pos_account_number' => '',
            'sort_order' => '',
            'show_status' => Cashbox::STATUS_ACTIVE,
        ]);
    }

    public function findOrFail($id): Cashbox
    {
        $cashbox = Cashbox::where('id', $id)->notDeleted()->first();
        if (empty($cashbox)) {
            abort(404);
        }
        return $cashbox;
    }

    public function update($id, $data): Cashbox
    {
        $cashbox = $this->findOrFail($id);
        DB::beginTransaction();
        $cashbox->update($data);
        $this->saveMl($cashbox, $data['ml']);
        DB::commit();
        return $cashbox;
    }

    public function delete($ids): int
    {
        DB::beginTransaction();
        CashboxMl::whereIn('cashbox_id', $ids)->update(['show_status' => CashboxMl::STATUS_DELETED]);
        $cnt = Cashbox::whereIn('id', $ids)->update(['show_status' => Cashbox::STATUS_DELETED]);
        DB::commit();
        return $cnt;
    }

    public function saveMl(Cashbox $cashbox, array $mls): void
    {
        foreach ($mls as $lngId => $ml) {
            $cashbox->ml()->updateOrCreate(
                ['cashbox_id' => $cashbox->id, 'lng_id' => $lngId],
                [
                    'name' => $ml['name'],
                    'show_status' => $ml['show_status'],
                ]
            );
        }
    }

    public function activesWithBranches(): Collection
    {
        return Cashbox::select([
            'cashboxes.*',
            'ml.*',
            'branch_ml.name as branch'
        ])
            ->joinMl()->joinBranch()
            ->where('cashboxes.show_status', '=', Cashbox::STATUS_ACTIVE)
            ->where('ml.show_status', '=', CashboxMl::STATUS_ACTIVE)
            ->where('branches.show_status', '=', Branch::STATUS_ACTIVE)
            ->where('branch_ml.show_status', '=', BranchMl::STATUS_ACTIVE)
            ->orderBy('cashboxes.sort_order', 'asc')
            ->orderBy('cashboxes.id', 'desc')
            ->get();
    }

    public function withBranches($ids, bool $withActives): Collection
    {
        return Cashbox::select([
            'cashboxes.*',
            'ml.*',
            'branch_ml.name as branch'
        ])
            ->joinMl(false)->joinBranch(false)
            ->where(function($query) use($ids, $withActives){
                $query->whereIn('cashboxes.id', $ids);
                if ($withActives) {
                    $query->orWhere(function($q){
                        $q->where('cashboxes.show_status', '=', Cashbox::STATUS_ACTIVE)
                            ->where('ml.show_status', '=', CashboxMl::STATUS_ACTIVE)
                            ->where('branches.show_status', '=', Branch::STATUS_ACTIVE)
                            ->where('branch_ml.show_status', '=', BranchMl::STATUS_ACTIVE);
                    });
                }
            })
            ->orderBy('cashboxes.sort_order', 'asc')
            ->orderBy('cashboxes.id', 'desc')
            ->get();
    }

    public function found(int $id): bool
    {
        return !empty(Cashbox::where('id', $id)->active()->first());
    }

    public function getCashboxStateDetails(int $id)
    {
        $cashboxState = CashboxState::where('id', $id)->with(['cashbox', 'operator'])->firstOrFail();
        $transactions = Transaction::where('cashbox_state_id', $cashboxState->id)
        ->selectRaw(
            'service_id,
            SUM(
                CASE 
                    WHEN cash = true AND service_type = \''.Transaction::SERVICE_TYPE_CANCEL.'\' AND type = \'in\' THEN total_amount
                    WHEN cash = true AND service_type = \''.Transaction::SERVICE_TYPE_CANCEL.'\' AND type = \'out\' THEN 0 - total_amount
                    WHEN cash = true THEN total_amount
                    ELSE 0
                END
            ) as cash,
            SUM(
               CASE 
                    WHEN cash = false AND service_type = \''.Transaction::SERVICE_TYPE_CANCEL.'\' AND type = \'in\' THEN total_amount
                    WHEN cash = false AND service_type = \''.Transaction::SERVICE_TYPE_CANCEL.'\' AND type = \'out\' THEN 0 - total_amount
                    WHEN cash = false THEN total_amount
                    ELSE 0
                END
            ) as non_cash,
            COUNT(*) as count'
        )->groupBy('service_id')->orderBy('cash', 'desc')->with('service')->get();

        return [
            'cashboxState' => $cashboxState,
            'transactions' => $transactions
        ];
    }

    public function all(): Collection
    {
        return Cashbox::joinMl()->where('cashboxes.show_status', '!=', Cashbox::STATUS_DELETED)
            ->sorted()->get();
    }

    public function getByBranch($branchId): Collection
    {
        return Cashbox::notDeleted()->where('branch_id', $branchId)->get();
    }
}
