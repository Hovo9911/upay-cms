<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('/login', 'Auth\LoginController@login');
Route::post('/forgot-password', 'Auth\ForgotPasswordController@forgot');
Route::get('/forgot-password/reset/{hash}', 'Auth\ForgotPasswordController@showResetForm');
Route::post('/forgot-password/reset/{hash}', 'Auth\ForgotPasswordController@reset');

Route::group(['middleware' => ['auth']], function () {
    Route::post('/logout', 'Auth\LoginController@logout')->name('logout');
    Route::get('/profile', 'Auth\ProfileController@index')->name('profile');
    Route::post('/change-password', 'Auth\ProfileController@changePassword');

    Route::group(['middleware' => ['checkForResetPassword']], function () {
        Route::get('/', 'IndexController@index');
        Route::post('/g-uploader/upload', 'GUploaderController@upload')->name('gupload');

        Route::group(['prefix' => 'role'], function () {
            Route::get('/list', 'RoleController@index')->middleware('permission:role.view');
            Route::get('/get-list', 'RoleController@getList')->middleware('permission:role.view');
            Route::get('/edit/{id?}', 'RoleController@edit')->middleware('permission:role.view');
            Route::post('/save/{id?}', 'RoleController@save')->middleware('permission:role.edit');
            Route::post('/delete', 'RoleController@delete')->middleware('permission:role.delete');
        });
        Route::group(['prefix' => 'admin'], function () {
            Route::get('/list', 'AdminController@index')->middleware('permission:admin.view');
            Route::get('/get-list', 'AdminController@getList')->middleware('permission:admin.view');
            Route::get('/edit/{id?}', 'AdminController@edit')->middleware('permission:admin.view');
            Route::post('/save/{id?}', 'AdminController@save')->middleware('permission:admin.edit');
            Route::post('/delete', 'AdminController@delete')->middleware('permission:admin.delete');
            Route::post('/reset-password/{id}', 'AdminController@resetPassword')->middleware('permission:admin.reset_password');
        });
        Route::group(['prefix' => 'branch'], function () {
            Route::get('/list', 'BranchController@index')->middleware('permission:branch.view');
            Route::get('/get-list', 'BranchController@getList')->middleware('permission:branch.view');
            Route::get('/edit/{id?}', 'BranchController@edit')->middleware('permission:branch.view');
            Route::post('/save/{id?}', 'BranchController@save')->middleware('permission:branch.edit');
            Route::post('/delete', 'BranchController@delete')->middleware('permission:branch.delete');
            Route::get('/get-cashboxes/{id}', 'BranchController@getCashboxes');
        });
        Route::group(['prefix' => 'city'], function () {
            Route::get('/list', 'CityController@index')->middleware('permission:city.view');
            Route::get('/get-list', 'CityController@getList')->middleware('permission:city.view');
            Route::get('/edit/{id?}', 'CityController@edit')->middleware('permission:city.view');
            Route::post('/save/{id?}', 'CityController@save')->middleware('permission:city.edit');
            Route::post('/delete', 'CityController@delete')->middleware('permission:city.delete');
        });
        Route::group(['prefix' => 'localization'], function () {
            Route::get('/list', 'LocalizationController@index')->middleware('permission:localization.view');
            Route::get('/get-list', 'LocalizationController@getList')->middleware('permission:localization.view');
            Route::post('/save/{id?}', 'LocalizationController@save')->middleware('permission:localization.edit');
            Route::post('/delete', 'LocalizationController@delete')->middleware('permission:localization.delete');
        });
        Route::group(['prefix' => 'operator'], function () {
            Route::get('/list', 'OperatorController@index')->middleware('permission:operator.view');
            Route::get('/get-list', 'OperatorController@getList')->middleware('permission:operator.view');
            Route::get('/edit/{id?}', 'OperatorController@edit')->middleware('permission:operator.view');
            Route::post('/save/{id?}', 'OperatorController@save')->middleware('permission:operator.edit');
            Route::post('/reset-password/{id}', 'OperatorController@resetPassword')->middleware('permission:operator.reset_password');
        });
        Route::group(['prefix' => 'cashbox'], function () {
            Route::get('/list', 'CashboxController@index')->middleware('permission:cashbox.view');
            Route::get('/get-list', 'CashboxController@getList')->middleware('permission:cashbox.view');
            Route::get('/edit/{id?}', 'CashboxController@edit')->middleware('permission:cashbox.view');
            Route::post('/save/{id?}', 'CashboxController@save')->middleware('permission:cashbox.edit');
            Route::post('/delete', 'CashboxController@delete')->middleware('permission:cashbox.delete');
        });
        Route::group(['prefix' => 'provider'], function () {
            Route::get('/list', 'ProviderController@index')->middleware('permission:provider.view');
            Route::get('/get-list', 'ProviderController@getList')->middleware('permission:provider.view');
            Route::get('/edit/{id?}', 'ProviderController@edit')->middleware('permission:provider.view');
            Route::post('/save/{id?}', 'ProviderController@save')->middleware('permission:provider.edit');
        });
        Route::group(['prefix' => 'service'], function () {
            Route::get('/list', 'ServiceController@index')->middleware('permission:service.view');
            Route::get('/get-list', 'ServiceController@getList')->middleware('permission:service.view');
            Route::get('/edit/{id?}', 'ServiceController@edit')->middleware('permission:service.view');
            Route::post('/save/{id?}', 'ServiceController@save')->middleware('permission:service.edit');
            Route::post('/approve-commissions/{id?}', 'ServiceController@approveCommissions')->middleware('permission:service.approve_commissions');
        });
        Route::group(['prefix' => 'timeline'], function () {
            Route::get('/', 'TimelineController@index')->middleware('permission:timeline.view');
            Route::post('/save', 'TimelineController@save')->middleware('permission:timeline.edit');
            Route::get('/export', 'TimelineController@export')->middleware('permission:timeline.view');
        });
        Route::group(['prefix' => 'cancellation'], function () {
            Route::get('/list', 'CancellationController@index')->middleware('permission:cancellation.view');
            Route::get('/get-list', 'CancellationController@getList')->middleware('permission:cancellation.view');
            Route::get('/view/{id}', 'CancellationController@view')->middleware('permission:cancellation.view');
            Route::post('/change-status/{id}', 'CancellationController@changeStatus')->middleware('permission:cancellation.edit');
        });
        Route::group(['prefix' => 'report-closing'], function () {
            Route::get('/list', 'ReportClosingController@index')->middleware('permission:report_closing.view');
            Route::get('/get-list', 'ReportClosingController@getList')->middleware('permission:report_closing.view');
            Route::get('/view/{id}', 'ReportClosingController@view')->middleware('permission:report_closing.view');
            Route::get('/export', 'ReportClosingController@export')->middleware('permission:report_closing.view');
            Route::get('/export/{id}', 'ReportClosingController@exportItem')->middleware('permission:report_closing.view');
        });
        Route::group(['prefix' => 'report-transactions'], function () {
            Route::get('/list', 'ReportTransactionsController@index')->middleware('permission:report_transactions.view');
            Route::get('/get-list', 'ReportTransactionsController@getList')->middleware('permission:report_transactions.view');
            Route::get('/export', 'ReportTransactionsController@export')->middleware('permission:report_transactions.view');
        });
        Route::group(['prefix' => 'report-services'], function () {
            Route::get('/list', 'ReportServicesController@index')->middleware('permission:report_services.view');
            Route::get('/get-list', 'ReportServicesController@getList')->middleware('permission:report_services.view');
            Route::get('/export', 'ReportServicesController@export')->middleware('permission:report_services.view');
        });
        Route::group(['prefix' => 'failed-transactions'], function () {
            Route::get('/list', 'FailedTransactionsController@index')->middleware('permission:failed_transactions.view');
            Route::get('/get-list', 'FailedTransactionsController@getList')->middleware('permission:failed_transactions.view');
            Route::post('/repeat', 'FailedTransactionsController@repeat')->middleware('permission:failed_transactions.repeat');
            Route::post('/complete', 'FailedTransactionsController@complete')->middleware('permission:failed_transactions.complete');
        });
        Route::group(['prefix' => 'website'], function () {
            Route::group(['prefix' => 'slider'], function () {
                Route::get('/list', 'SliderController@index')->middleware('permission:slider.view');
                Route::get('/get-list', 'SliderController@getList')->middleware('permission:slider.view');
                Route::get('/edit/{id?}', 'SliderController@edit')->middleware('permission:slider.view');
                Route::post('/save/{id?}', 'SliderController@save')->middleware('permission:slider.edit');
                Route::post('/delete', 'SliderController@delete')->middleware('permission:slider.delete');
            });
            Route::group(['prefix' => 'news'], function () {
                Route::get('/list', 'NewsController@index')->middleware('permission:news.view');
                Route::get('/get-list', 'NewsController@getList')->middleware('permission:news.view');
                Route::get('/edit/{id?}', 'NewsController@edit')->middleware('permission:news.view');
                Route::post('/save/{id?}', 'NewsController@save')->middleware('permission:news.edit');
                Route::post('/delete', 'NewsController@delete')->middleware('permission:news.delete');
            });
            Route::group(['prefix' => 'career'], function () {
                Route::get('/list', 'CareerController@index')->middleware('permission:career.view');
                Route::get('/get-list', 'CareerController@getList')->middleware('permission:career.view');
                Route::get('/edit/{id?}', 'CareerController@edit')->middleware('permission:career.view');
                Route::post('/save/{id?}', 'CareerController@save')->middleware('permission:career.edit');
                Route::post('/delete', 'CareerController@delete')->middleware('permission:career.delete');
            });
            Route::group(['prefix' => 'career-category'], function () {
                Route::get('/list', 'CareerCategoryController@index')->middleware('permission:career_category.view');
                Route::get('/get-list', 'CareerCategoryController@getList')->middleware('permission:career_category.view');
                Route::get('/edit/{id?}', 'CareerCategoryController@edit')->middleware('permission:career_category.view');
                Route::post('/save/{id?}', 'CareerCategoryController@save')->middleware('permission:career_category.edit');
                Route::post('/delete', 'CareerCategoryController@delete')->middleware('permission:career_category.delete');
            });
            Route::group(['prefix' => 'faq'], function () {
                Route::get('/list', 'FaqController@index')->middleware('permission:faq.view');
                Route::get('/get-list', 'FaqController@getList')->middleware('permission:faq.view');
                Route::get('/edit/{id?}', 'FaqController@edit')->middleware('permission:faq.view');
                Route::post('/save/{id?}', 'FaqController@save')->middleware('permission:faq.edit');
                Route::post('/delete', 'FaqController@delete')->middleware('permission:faq.delete');
            });
            Route::group(['prefix' => 'faq-category'], function () {
                Route::get('/list', 'FaqCategoryController@index')->middleware('permission:faq_category.view');
                Route::get('/get-list', 'FaqCategoryController@getList')->middleware('permission:faq_category.view');
                Route::get('/edit/{id?}', 'FaqCategoryController@edit')->middleware('permission:faq_category.view');
                Route::post('/save/{id?}', 'FaqCategoryController@save')->middleware('permission:faq_category.edit');
                Route::post('/delete', 'FaqCategoryController@delete')->middleware('permission:faq_category.delete');
            });
        });
    });
});
