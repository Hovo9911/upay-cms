<?php
namespace App\UPay\Interfaces;

use App\UPay\Operator\Operator;
use App\UPay\Admin\Admin;

interface ResetPasswordRepositoryInterface
{
    public function sendAdminPassword(Admin $admin): void;

    public function sendOperatorPassword(Operator $operator): void;

    public function sendAdminLink(Admin $admin): void;
}
