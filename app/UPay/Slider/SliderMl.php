<?php

namespace App\UPay\Slider;

use App\MultiPKModel;

class SliderMl extends MultiPKModel
{
    protected $table = 'sliders_ml';
    public $timestamps = false;
    protected $primaryKey = ['slider_id', 'lng_id'];
    public $incrementing = false;

    protected $fillable = [
        'slider_id',
        'lng_id',
        'title',
        'description',
        'button_text_1',
        'button_url_1',
        'button_text_2',
        'button_url_2',
        'show_status',
    ];
}
