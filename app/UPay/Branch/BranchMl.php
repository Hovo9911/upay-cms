<?php

namespace App\UPay\Branch;

use App\MultiPKModel;

class BranchMl extends MultiPKModel
{
    protected $table = 'branches_ml';
    public $timestamps = false;
    protected $primaryKey = ['branch_id', 'lng_id'];
    public $incrementing = false;

    protected $fillable = [
        'branch_id',
        'lng_id',
        'name',
        'address',
        'show_status',
    ];
}
