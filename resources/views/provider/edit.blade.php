<?php
    use App\Model;
    use App\UPay\Provider\Provider;
    $activeMenu = 'provider';
    $canEdit = \Auth::user()->hasPermission('provider.edit');
    $title = 'upay.provider.'.($canEdit ? $mode : 'view').'_title';
    $disabled = $canEdit ? '' : 'disabled';
    $gUploader = new \App\GUploader('provider_logo');
?>
@extends('layout')
@section('page_title') @lang($title) @stop
@section('content')
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox ">
                    <div class="ibox-title">
                        <h1>@lang($title)</h1>
                    </div>
                    <div class="ibox-content">
                        <form id="save-form" action="{{url('/provider/save/'.($provider->id ?? ''))}}" method="POST">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="tabs-container">
                                        <ul class="nav nav-tabs" role="tablist">
                                            <li><a class="nav-link active" data-toggle="tab" href="#tab-general">@lang('upay.tab.general')</a></li>
                                            @foreach($languages as $lng)
                                                <li><a class="nav-link" data-toggle="tab" href="#tab-lng-{{$lng->id}}">{{$lng->name}}</a></li>
                                            @endforeach
                                        </ul>
                                        <div class="tab-content">
                                            <div role="tabpanel" id="tab-general" class="tab-pane active">
                                                <div class="panel-body">
                                                    <div class="form-group row"><label class="col-lg-2 col-form-label">@lang('upay.provider.parent')</label>
                                                        <div class="col-lg-10">
                                                            <select name="parent_id" {{$disabled}} class="form-control m-b select-picker">
                                                                <option value="">@lang('upay.general.dropdown.select')</option>
                                                                @foreach($parents as $parent)
                                                                    <option value="{{$parent->id}}" @if($provider->parent_id == $parent->id) selected="selected" @endif>{{$parent->name}}</option>
                                                                @endforeach
                                                            </select>
                                                            <span class="form-text text-danger form-error-text form-error-parent_id"></span>
                                                        </div>
                                                    </div>

                                                    <div class="form-group required row"><label class="col-lg-2 col-form-label">@lang('upay.provider.logo') <span class="align-self-center hint-tooltip"><i class="fa fa-lg fa-question-circle" data-toggle="tooltip" data-placement="right" title="" data-original-title="@lang('upay.provider.logo.hint')"></i></span></label>
                                                        <div class="col-lg-3">
                                                            <div class="g-upload-container">
                                                                @if(empty($disabled))
                                                                <div class="g-upload-box">
                                                                    <input id="logo" type="file" class="d-none g-upload" name="logo" <?php echo $gUploader->isMultiple() ? 'multiple' : ''; ?> data-conf="provider_logo" data-action="<?php echo url('/g-uploader/upload'); ?>" accept="<?php echo $gUploader->getAcceptTypes(); ?>">
                                                                    <label for="logo" class="btn btn-success">@lang('upay.general.uploader.label')</label>
                                                                </div>
                                                                @endif

                                                                <div class="g-uploaded-list">
                                                                    @if(!empty($provider->logo))
                                                                        <div class="g-uploaded border fb fs14 position-relative text-break text-wrap">
                                                                            <img src="{{$gUploader->getFileUrl($provider->logo)}}"/>
                                                                            {{$gUploader->getFilename($provider->logo)}} <i class="g-upload-x fa fa-2x fa-remove text-danger"></i>
                                                                            <input type="hidden" name="logo" value="{{$provider->logo}}" />
                                                                        </div>
                                                                    @endif
                                                                </div>

                                                                <div class="form-text text-danger form-error-text form-error-logo"></div>
                                                                <div class="form-text text-danger form-error-text form-error-files"></div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="form-group row"><label for="p-type" class="col-lg-2 col-form-label">@lang('upay.provider.type')</label>
                                                        <div class="col-lg-10">
                                                            <input type="checkbox" id="p-type" name="type" value="{{Provider::TYPE_CASHBOX}}" @if($provider->type == Provider::TYPE_CASHBOX) checked @endif {{$disabled}} data-toggle="switchbutton" data-onlabel="@lang('upay.provider.type.cashbox')" data-offlabel="@lang('upay.provider.type.payment')" data-width="90" data-height="33">
                                                            <span class="form-text text-danger form-error-text form-error-type"></span>
                                                        </div>
                                                    </div>

                                                    <div class="form-group row"><label class="col-lg-2 col-form-label">@lang('upay.general.sort_order')</label>
                                                        <div class="col-lg-2">
                                                            <input type="text" name="sort_order" value="{{$provider->sort_order}}" {{$disabled}} class="form-control">
                                                            <span class="form-text text-danger form-error-text form-error-sort_order"></span>
                                                        </div>
                                                    </div>

                                                    <div class="form-group row"><label for="general-status" class="col-lg-2 col-form-label">@lang('upay.general.show_status')</label>
                                                        <div class="col-lg-10">
                                                            <input id="general-status" type="checkbox" name="show_status" value="{{Model::STATUS_ACTIVE}}" @if($provider->show_status == Model::STATUS_ACTIVE) checked @endif {{$disabled}} data-toggle="switchbutton" data-onlabel="@lang('upay.general.show_status.active')" data-offlabel="@lang('upay.general.show_status.inactive')" data-width="86" data-height="33">
                                                            <span class="form-text text-danger form-error-text form-error-show_status"></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            @foreach($languages as $lng)
                                                <div role="tabpanel" id="tab-lng-{{$lng->id}}" class="tab-pane">
                                                    <div class="panel-body">
                                                        <div class="form-group required row"><label class="col-lg-2 col-form-label">@lang('upay.provider.name')</label>
                                                            <div class="col-lg-10">
                                                                <input type="text" name="ml[{{$lng->id}}][name]" @isset($provider->ml[$lng->id]) value="{{$provider->ml[$lng->id]->name}}" @endisset {{$disabled}} class="form-control">
                                                                <span class="form-text text-danger form-error-text form-error-ml_{{$lng->id}}_name"></span>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row"><label for="ml-status-{{$lng->id}}" class="col-lg-2 col-form-label">@lang('upay.general.show_status')</label>
                                                            <div class="col-lg-10">
                                                                <input id="ml-status-{{$lng->id}}" type="checkbox" name="ml[{{$lng->id}}][show_status]" value="{{Model::STATUS_ACTIVE}}" @if(!isset($provider->ml[$lng->id]) || $provider->ml[$lng->id]->show_status == Model::STATUS_ACTIVE) checked @endif {{$disabled}} data-toggle="switchbutton" data-onlabel="@lang('upay.general.show_status.active')" data-offlabel="@lang('upay.general.show_status.inactive')" data-width="86" data-height="33">
                                                                <span class="form-text text-danger form-error-text form-error-ml_{{$lng->id}}_show_status"></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>

                            {{csrf_field()}}

                            <hr/>
                            <div class="form-group row">
                                <div class="col-lg-4 col-lg-offset-2">
                                    <a href="{{url('/provider/list')}}" class="btn btn-white">@lang('upay.general.buttons.cancel')</a>
                                    @if($canEdit)<button class="btn btn-primary submit-btn" type="submit">@lang('upay.general.buttons.save')</button>@endif
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="{{script_url('upay/js/provider.js')}}"></script>
@stop
