<?php

namespace App\UPay\Role;

use \App\Model;
use App\UPay\Admin\Admin;
use App\UPay\Admin\AdminRole;

class Role extends Model
{
    protected $table = 'roles';

    protected $fillable = [
        'name',
        'show_status',
        'permissions'
    ];

    protected $casts = [
        'permissions' => 'array'
    ];

    public function admins()
    {
        return $this->belongsToMany(Admin::class)->using(AdminRole::class);
    }
}
