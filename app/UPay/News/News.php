<?php

namespace App\UPay\News;

use App\GUploader;
use App\Model;

class News extends Model
{
    protected $table = 'news';

    protected $fillable = [
        'image',
        'show_on_homepage',
        'publish_date',
        'date',
        'sort_order',
        'show_status',
    ];

    public function ml(){
        return $this->hasMany(NewsMl::class, 'news_id', 'id');
    }

    public function scopeJoinMl($query){
        $query->join('news_ml as ml', function ($join){
            $join->on('news.id', '=', 'ml.news_id')
                ->where('ml.lng_id', '=', c_lng('id'))
                ->where('ml.show_status', '!=', NewsMl::STATUS_DELETED);
        });
    }

    public function getSliderImageAttribute()
    {
        $gUploader = new GUploader('website_news_image');
        if (!empty($this->logo)) {
            return $gUploader->getFileUrl($this->image);
        }
        return '';
    }
}
