<?php

namespace App\Providers;

use Illuminate\Translation\FileLoader;

class TranslationServiceProvider extends \Illuminate\Translation\TranslationServiceProvider
{
    /**
     * Register the translation line loader.
     *
     * @return void
     */
    protected function registerLoader()
    {
        $this->app->singleton('translation.loader', function ($app) {
            return new FileLoader($app['files'], !empty(config('upay.lang.path')) ? config('upay.lang.path').'/'. config('upay.lang.group') : $app['path.lang']);
        });
    }

}
