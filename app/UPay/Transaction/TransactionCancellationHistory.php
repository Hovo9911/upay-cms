<?php

namespace App\UPay\Transaction;

use \App\Model;
use App\UPay\Operator\Operator;
use App\UPay\Admin\Admin;

class TransactionCancellationHistory extends Model
{
    protected $table = 'transaction_cancellation_history';

    public $timestamps = false;

    protected $fillable = [
        'cancellation_id',
        'doer_id',
        'date',
        'status',
        'notes',
    ];

    public function admin()
    {
        return $this->belongsTo(Admin::class, 'doer_id', 'id');
    }
    public function operator()
    {
        return $this->belongsTo(Operator::class, 'doer_id', 'id');
    }

}
