<?php

namespace App\UPay\News;

use App\GUploader;
use App\UPay\Interfaces\NewsRepositoryInterface;
use DB;
use Illuminate\Database\Eloquent\Collection;

class NewsRepository implements NewsRepositoryInterface
{
    public function create($data): News
    {
        $news = new News($data);
        DB::beginTransaction();
        $this->saveImage($data['image']);
        $news->save();
        $this->saveMl($news, $data['ml']);
        DB::commit();
        return $news;
    }

    public function mock(): News
    {
        return new News([
            'image' => '',
            'show_on_homepage' => false,
            'publish_date' => '',
            'date' => '',
            'sort_order' => '',
            'show_status' => News::STATUS_ACTIVE,
        ]);
    }

    public function findOrFail($id, $withMl = false): News
    {
        $news = News::where('id', $id)->where('news.show_status', '!=', News::STATUS_DELETED);

        if ($withMl) {
            $news->joinMl();
        }
        $news = $news->first();

        if (empty($news)) {
            abort(404);
        }
        return $news;
    }

    public function update($id, $data): News
    {
        $news = $this->findOrFail($id);

        DB::beginTransaction();
        $this->saveImage($data['image'], $news->image);
        $news->update($data);
        $this->saveMl($news, $data['ml']);
        DB::commit();
        return $news;
    }

    public function delete($ids): int
    {
        DB::beginTransaction();
        NewsMl::whereIn('news_id', $ids)->update(['show_status' => NewsMl::STATUS_DELETED]);
        $news = News::whereIn('id', $ids)->update(['show_status' => News::STATUS_DELETED]);
        DB::commit();
        return $news;
    }

    public function saveMl(News $news, array $mls): void
    {
        foreach ($mls as $lngId => $ml) {
            $news->ml()->updateOrCreate(
                ['news_id' => $news->id, 'lng_id' => $lngId],
                [
                    'title' => $ml['title'],
                    'alias' => $ml['alias'],
                    'short_description' => $ml['short_description'],
                    'body' => $ml['body'],
                    'show_status' => $ml['show_status'],
                ]
            );
        }
    }

    public function saveImage($newImage, $oldImage = null): void
    {
        if (!empty($newImage) && (is_null($oldImage) || (!is_null($oldImage) && $newImage != $oldImage))) {
            $gUploader = new GUploader('website_news_image');
            $gUploader->storePerm($newImage);
        }
    }

    public function all(): Collection
    {
        return News::joinMl()
            ->where('news.show_status', News::STATUS_ACTIVE)
            ->where('ml.show_status', News::STATUS_ACTIVE)
            ->get();
    }
}
