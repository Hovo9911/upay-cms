<?php

namespace App\UPay\Service;

use App\GUploader;
use \App\Model;
use App\UPay\Provider\Provider;

class Service extends Model
{
    const ENCASHMENT_IN = 'in';
    const ENCASHMENT_OUT = 'out';

    const TYPE_TRANSFER = 'transfer';
    const TYPE_PAY = 'pay';

    protected $table = 'services';

    protected $appends = ['service_logo'];

    protected $fillable = [
        'provider_id',
        'branch_account_number',
        'branch_commission_account_number',
        'mobile_account_number',
        'mobile_commission_account_number',
        'account_min',
        'account_max',
        'use_fixed_amount',
        'is_encashment',
        'encashment_type',
        'logo',
        'hidden',
        'adapter',
        'sort_order',
        'show_status',
        'type',
        'for_mobile',
        'for_ops',
        'is_new',
        'show_on_homepage',
    ];

    public function ml(){
        return $this->hasMany(ServiceMl::class, 'service_id', 'id');
    }

    public function provider(){
        return $this->belongsTo(Provider::class, 'provider_id', 'id');
    }

    public function commissions(){
        return $this->hasMany(Commission::class, 'service_id', 'id');
    }

    public function commissionsHistory(){
        return $this->commissions()->whereHas('commissionValuesApproved')->with('commissionValuesApproved');
    }

    public function scopeJoinMl($query){
        $query->join('services_ml as ml', function ($join){
            $join->on('services.id', '=', 'ml.service_id')
                ->where('ml.lng_id', '=', c_lng('id'))
                ->where('ml.show_status', '!=', ServiceMl::STATUS_DELETED);
        });
    }

    public function scopeJoinProvider($query){
        $query->leftJoin('providers', function ($join){
            $join->on('services.provider_id', '=', 'providers.id')
                ->where('providers.show_status', '!=', Provider::STATUS_DELETED);
        })->leftJoin('providers_ml as provider_ml', function ($join){
            $join->on('providers.id', '=', 'provider_ml.provider_id')
                ->where('provider_ml.lng_id', '=', c_lng('id'));
        });
    }

    public function getServiceLogoAttribute()
    {
        $gUploader = new GUploader('service_logo');
        if (!empty($this->logo)) {
            return $gUploader->getFileUrl($this->logo);
        }
        return '';
    }
}
