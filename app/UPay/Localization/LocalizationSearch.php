<?php

namespace App\UPay\Localization;

use App\Search;

class LocalizationSearch extends Search
{
    protected $orderables = [
        'id',
        'key',
        'value'
    ];

    protected function query()
    {
        $filters = $this->filters;
        $query = Localization::joinMl()->select(
            'id',
            'group',
            'key',
            'value'
        )
        ->when(!empty($filters['id']), function($query) use ($filters) {
            $query->where('id', $filters['id']);
        })
        ->when(!empty($filters['key']), function($query) use ($filters) {
            $query->where('key', 'ILIKE', '%'.$filters['key'].'%');
        })
        ->when(!empty($filters['value']), function($query) use ($filters) {
            $query->where('value', 'ILIKE', '%'.$filters['value'].'%');
        })
        ->when(!empty($filters['group']), function($query) use ($filters) {
            $query->where('group', $filters['group']);
        })
        ->with('ml');
        return $query;
    }

    public function totalCount()
    {
        return Localization::count();
    }
}
