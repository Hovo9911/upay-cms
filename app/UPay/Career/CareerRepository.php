<?php

namespace App\UPay\Career;

use App\GUploader;
use App\UPay\Interfaces\CareerRepositoryInterface;
use DB;
use Illuminate\Database\Eloquent\Collection;

class CareerRepository implements CareerRepositoryInterface
{
    public function create($data): Career
    {
        $career = new Career($data);
        DB::beginTransaction();
        $this->saveImage($data['image']);
        $career->save();
        $this->saveMl($career, $data['ml']);
        DB::commit();
        return $career;
    }

    public function mock(): Career
    {
        return new Career([
            'image' => '',
            'show_on_homepage' => false,
            'career_category_id' => null,
            'sort_order' => '',
            'show_status' => Career::STATUS_ACTIVE,
        ]);
    }

    public function findOrFail($id, $withMl = false): Career
    {
        $career = Career::where('id', $id)->where('career.show_status', '!=', Career::STATUS_DELETED);

        if ($withMl) {
            $career->joinMl();
        }
        $career = $career->first();

        if (empty($career)) {
            abort(404);
        }
        return $career;
    }

    public function update($id, $data): Career
    {
        $career = $this->findOrFail($id);

        DB::beginTransaction();
        $this->saveImage($data['image'], $career->image);
        $career->update($data);
        $this->saveMl($career, $data['ml']);
        DB::commit();
        return $career;
    }

    public function delete($ids): int
    {
        DB::beginTransaction();
        CareerMl::whereIn('career_id', $ids)->update(['show_status' => CareerMl::STATUS_DELETED]);
        $career = Career::whereIn('id', $ids)->update(['show_status' => Career::STATUS_DELETED]);
        DB::commit();
        return $career;
    }

    public function saveMl(Career $career, array $mls): void
    {
        foreach ($mls as $lngId => $ml) {
            $career->ml()->updateOrCreate(
                ['career_id' => $career->id, 'lng_id' => $lngId],
                [
                    'title' => $ml['title'],
                    'description' => $ml['description'],
                    'show_status' => $ml['show_status'],
                ]
            );
        }
    }

    public function saveImage($newImage, $oldImage = null): void
    {
        if (!empty($newImage) && (is_null($oldImage) || (!is_null($oldImage) && $newImage != $oldImage))) {
            $gUploader = new GUploader('website_career_image');
            $gUploader->storePerm($newImage);
        }
    }

    public function all(): Collection
    {
        return Career::joinMl()
            ->where('career.show_status', Career::STATUS_ACTIVE)
            ->where('ml.show_status', Career::STATUS_ACTIVE)
            ->get();
    }
}

