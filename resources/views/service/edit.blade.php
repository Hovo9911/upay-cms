<?php
    use App\Model;
    use App\UPay\Service\Service;
    use App\UPay\Service\Commission;
    use App\UPay\Service\CommissionValue;
    use App\UPay\Service\CommissionValueApproved;
    $activeMenu = 'service';
    $canEdit = \Auth::user()->hasPermission('service.edit');
    $canApproveCommissions = \Auth::user()->hasPermission('service.approve_commissions');
    $title = 'upay.service.'.($canEdit ? $mode : 'view').'_title';
    $disabled = $canEdit ? '' : 'disabled';
    $jsTrans->add([
        'upay.service.use_fixed_amount.yes',
        'upay.service.use_fixed_amount.no',
        'upay.service.commissions.type.fixed',
        'upay.service.commissions.type.percent',
        'upay.service.commissions.approve.confirm_modal_title',
        'upay.service.commissions.approve.confirm_modal_text',
    ]);
    $commissionsSrc = !empty($service->commissions) ? $service->commissions->keyBy(function($item){return $item->type.'_'.$item->source;}) : [];
    $gUploader = new \App\GUploader('service_logo');
?>
@extends('layout')
@section('page_title') @lang($title) @stop
@section('content')
    <script>
        var $commissionsSrc = {!! $commissionsSrc->toJson() !!};
        var $formDisabled = '{{$disabled}}';
    </script>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox">
                    <div class="ibox-title">
                        <h1 class="float-left">@lang($title)</h1>
                        @if($service->show_status == Model::STATUS_ACTIVE)<a class="btn btn-info mt-2 ml-2" target="_blank" href="{{operator_www_url('provider/payment/'.(!empty($service->provider->parent_id) ? $service->provider->parent_id.'/' : '').$service->provider_id.'/service/'.$service->id, 'hy')}}"><i class="fa fa-eye"></i> @lang('upay.service.preview_btn')</a>@endif
                        <div class="clearfix"></div>
                    </div>
                    <div class="ibox-content">
                        <form id="save-form" action="{{url('/service/save/'.($service->id ?? ''))}}" method="POST">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="tabs-container">
                                        <ul class="nav nav-tabs" role="tablist">
                                            <li><a class="nav-link active" data-toggle="tab" href="#tab-general">@lang('upay.tab.general')</a></li>
                                            @foreach($languages as $lng)
                                                <li><a class="nav-link" data-toggle="tab" href="#tab-lng-{{$lng->id}}">{{$lng->name}}</a></li>
                                            @endforeach
                                            <li><a class="nav-link" data-toggle="tab" href="#tab-commissions">@lang('upay.service.tab.commissions')</a></li>
                                            @if($mode == 'edit' && !$service->commissionsHistory->isEmpty())
                                            <li><a class="nav-link" data-toggle="tab" href="#tab-commissions-history">@lang('upay.service.tab.commissions_history')</a></li>
                                            @endif
                                        </ul>
                                        <div class="tab-content">
                                            <div role="tabpanel" id="tab-general" class="tab-pane active">
                                                <div class="panel-body">
                                                    <div class="form-group row required"><label class="col-lg-2 col-form-label">@lang('upay.service.adapter')</label>
                                                        <div class="col-lg-10">
                                                            <input type="text" name="adapter" value="{{$service->adapter}}" {{$disabled}} class="form-control">
                                                            <span class="form-text text-danger form-error-text form-error-adapter"></span>
                                                        </div>
                                                    </div>

                                                    <div class="form-group required row"><label class="col-lg-2 col-form-label">@lang('upay.service.provider')</label>
                                                        <div class="col-lg-10">
                                                            <select name="provider_id" {{$disabled}} class="form-control m-b select-picker">
                                                                <option value="">@lang('upay.general.dropdown.select')</option>
                                                                @foreach($providers as $provider)
                                                                    <option value="{{$provider->id}}" @if($service->provider_id == $provider->id) selected="selected" @endif>{{$provider->name}}</option>
                                                                    @foreach($provider->children as $child)
                                                                        <option value="{{$child->id}}" @if($service->provider_id == $child->id) selected="selected" @endif>{{$provider->name.' > '.$child->name}}</option>
                                                                    @endforeach
                                                                @endforeach
                                                            </select>
                                                            <span class="form-text text-danger form-error-text form-error-provider_id"></span>
                                                        </div>
                                                    </div>

                                                    <div class="form-group required row"><label class="col-lg-2 col-form-label">@lang('upay.service.logo') <span class="align-self-center hint-tooltip"><i class="fa fa-lg fa-question-circle" data-toggle="tooltip" data-placement="right" title="" data-original-title="@lang('upay.service.logo.hint')"></i></span></label>
                                                        <div class="col-lg-3">
                                                            <div class="g-upload-container">
                                                                @if(empty($disabled))
                                                                    <div class="g-upload-box">
                                                                        <input id="logo" type="file" class="d-none g-upload" name="logo" <?php echo $gUploader->isMultiple() ? 'multiple' : ''; ?> data-conf="service_logo" data-action="<?php echo url('/g-uploader/upload'); ?>" accept="<?php echo $gUploader->getAcceptTypes(); ?>">
                                                                        <label for="logo" class="btn btn-success">@lang('upay.general.uploader.label')</label>
                                                                    </div>
                                                                @endif

                                                                <div class="g-uploaded-list">
                                                                    @if(!empty($service->logo))
                                                                        <div class="g-uploaded border fb fs14 position-relative text-break text-wrap">
                                                                            <img src="{{$gUploader->getFileUrl($service->logo)}}"/>
                                                                            {{$gUploader->getFilename($service->logo)}} <i class="g-upload-x fa fa-2x fa-remove text-danger"></i>
                                                                            <input type="hidden" name="logo" value="{{$service->logo}}" />
                                                                        </div>
                                                                    @endif
                                                                </div>

                                                                <div class="form-text text-danger form-error-text form-error-logo"></div>
                                                                <div class="form-text text-danger form-error-text form-error-files"></div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="form-group required row"><label class="col-lg-2 col-form-label">@lang('upay.service.branch_account_number')</label>
                                                        <div class="col-lg-10">
                                                            <input type="text" name="branch_account_number" value="{{$service->branch_account_number}}" {{$disabled}} class="form-control">
                                                            <span class="form-text text-danger form-error-text form-error-branch_account_number"></span>
                                                        </div>
                                                    </div>

                                                    <div class="form-group required row"><label class="col-lg-2 col-form-label">@lang('upay.service.branch_commission_account_number')</label>
                                                        <div class="col-lg-10">
                                                            <input type="text" name="branch_commission_account_number" value="{{$service->branch_commission_account_number}}" {{$disabled}} class="form-control">
                                                            <span class="form-text text-danger form-error-text form-error-branch_commission_account_number"></span>
                                                        </div>
                                                    </div>

                                                    <div class="form-group row"><label class="col-lg-2 col-form-label">@lang('upay.service.mobile_account_number')</label>
                                                        <div class="col-lg-10">
                                                            <input type="text" name="mobile_account_number" value="{{$service->mobile_account_number}}" {{$disabled}} class="form-control">
                                                            <span class="form-text text-danger form-error-text form-error-mobile_account_number"></span>
                                                        </div>
                                                    </div>

                                                    <div class="form-group row"><label class="col-lg-2 col-form-label">@lang('upay.service.mobile_commission_account_number')</label>
                                                        <div class="col-lg-10">
                                                            <input type="text" name="mobile_commission_account_number" value="{{$service->mobile_commission_account_number}}" {{$disabled}} class="form-control">
                                                            <span class="form-text text-danger form-error-text form-error-mobile_commission_account_number"></span>
                                                        </div>
                                                    </div>

                                                    <div class="form-group row"><label class="col-lg-2 col-form-label">@lang('upay.service.account_min')</label>
                                                        <div class="col-lg-10">
                                                            <input type="text" name="account_min" value="{{$service->account_min}}" {{$disabled}} class="form-control">
                                                            <span class="form-text text-danger form-error-text form-error-account_min"></span>
                                                        </div>
                                                    </div>

                                                    <div class="form-group row"><label class="col-lg-2 col-form-label">@lang('upay.service.account_max')</label>
                                                        <div class="col-lg-10">
                                                            <input type="text" name="account_max" value="{{$service->account_max}}" {{$disabled}} class="form-control">
                                                            <span class="form-text text-danger form-error-text form-error-account_max"></span>
                                                        </div>
                                                    </div>

                                                    <div class="form-group row"><label class="col-lg-2 col-form-label">@lang('upay.service.use_fixed_amount')</label>
                                                        <div class="col-lg-10">
                                                            <input type="checkbox" name="use_fixed_amount" value="1" @if($service->use_fixed_amount) checked @endif {{$disabled}} data-toggle="switchbutton" data-onlabel="@lang('upay.service.use_fixed_amount.yes')" data-offlabel="@lang('upay.service.use_fixed_amount.no')" data-width="86" data-height="33">
                                                            <span class="form-text text-danger form-error-text form-error-use_fixed_amount"></span>
                                                        </div>
                                                    </div>

                                                    <div class="form-group row"><label for="is-encashment" class="col-lg-2 col-form-label">@lang('upay.service.is_encashment')</label>
                                                        <div class="col-lg-10">
                                                            <input id="is-encashment" type="checkbox" name="is_encashment" value="1" @if($service->is_encashment) checked @endif {{$disabled}} data-toggle="switchbutton" data-onlabel="@lang('upay.service.is_encashment.yes')" data-offlabel="@lang('upay.service.is_encashment.no')" data-width="86" data-height="33">
                                                            <span class="form-text text-danger form-error-text form-error-is_encashment"></span>
                                                        </div>
                                                    </div>

                                                    <div class="form-group row required @if(!$service->is_encashment) d-none @endif"><label class="col-lg-2 col-form-label">@lang('upay.service.encashment_type')</label>
                                                        <div class="col-lg-2 align-self-lg-center">
                                                            <div class="i-checks float-left mr-1"><label for="encashment-type-in"> <input id="encashment-type-in" type="radio" {{$disabled}} value="{{Service::ENCASHMENT_IN}}" name="encashment_type" @if(!$service->is_encashment || $service->encashment_type == Service::ENCASHMENT_IN) checked @endif> <i></i> @lang('upay.service.encashment_type.in')</label></div>
                                                            <div class="i-checks"><label for="encashment-type-out"> <input id="encashment-type-out" type="radio" {{$disabled}} value="{{Service::ENCASHMENT_OUT}}" name="encashment_type" @if($service->encashment_type == Service::ENCASHMENT_OUT) checked @endif> <i></i> @lang('upay.service.encashment_type.out')</label></div>
                                                            <span class="form-text text-danger form-error-text form-error-encashment_type"></span>
                                                        </div>
                                                    </div>

                                                    <div class="form-group row"><label for="is-hidden" class="col-lg-2 col-form-label">@lang('upay.service.hidden')</label>
                                                        <div class="col-lg-10">
                                                            <input id="is-hidden" type="checkbox" name="hidden" value="1" @if($service->hidden) checked @endif {{$disabled}} data-toggle="switchbutton" data-onlabel="@lang('upay.service.hidden.yes')" data-offlabel="@lang('upay.service.hidden.no')" data-width="86" data-height="33">
                                                            <span class="form-text text-danger form-error-text form-error-hidden"></span>
                                                        </div>
                                                    </div>

                                                    <div class="form-group row required"><label class="col-lg-2 col-form-label">@lang('upay.service.type')</label>
                                                        <div class="col-lg-2 align-self-lg-center">
                                                            <div class="i-checks float-left mr-1"><label for="type-pay"> <input id="type-pay" type="radio" {{$disabled}} value="{{Service::TYPE_PAY}}" name="type" @if($service->type == Service::TYPE_PAY) checked @endif> <i></i> @lang('upay.service.type.pay')</label></div>
                                                            <div class="i-checks"><label for="type-transfer"> <input id="type-transfer" type="radio" {{$disabled}} value="{{Service::TYPE_TRANSFER}}" name="type" @if($service->type == Service::TYPE_TRANSFER) checked @endif> <i></i> @lang('upay.service.type.transfer')</label></div>
                                                            <span class="form-text text-danger form-error-text form-error-type"></span>
                                                        </div>
                                                    </div>

                                                    <div class="form-group row"><label for="for-mobile" class="col-lg-2 col-form-label">@lang('upay.service.for_mobile')</label>
                                                        <div class="col-lg-10">
                                                            <input id="for-mobile" type="checkbox" name="for_mobile" value="1" @if($service->for_mobile) checked @endif {{$disabled}} data-toggle="switchbutton" data-onlabel="@lang('upay.service.for_mobile.yes')" data-offlabel="@lang('upay.service.for_mobile.no')" data-width="86" data-height="33">
                                                            <br><span class="form-text text-danger form-error-text form-error-for_mobile"></span>
                                                        </div>
                                                    </div>

                                                    <div class="form-group row"><label for="for-ops" class="col-lg-2 col-form-label">@lang('upay.service.for_ops')</label>
                                                        <div class="col-lg-10">
                                                            <input id="for-ops" type="checkbox" name="for_ops" value="1" @if($service->for_ops) checked @endif {{$disabled}} data-toggle="switchbutton" data-onlabel="@lang('upay.service.for_ops.yes')" data-offlabel="@lang('upay.service.for_ops.no')" data-width="86" data-height="33">
                                                            <br><span class="form-text text-danger form-error-text form-error-for_ops"></span>
                                                        </div>
                                                    </div>

                                                    <div class="form-group row"><label for="show-on-homepage" class="col-lg-2 col-form-label">@lang('upay.service.show_on_homepage')</label>
                                                        <div class="col-lg-10">
                                                            <input id="show-on-homepage" type="checkbox" name="show_on_homepage" value="1" @if($service->show_on_homepage) checked @endif {{$disabled}} data-toggle="switchbutton" data-onlabel="@lang('upay.service.show_on_homepage.yes')" data-offlabel="@lang('upay.service.show_on_homepage.no')" data-width="86" data-height="33">
                                                            <br><span class="form-text text-danger form-error-text form-error-show_on_homepage"></span>
                                                        </div>
                                                    </div>

                                                    <div class="form-group row"><label for="is-new" class="col-lg-2 col-form-label">@lang('upay.service.is_new')</label>
                                                        <div class="col-lg-10">
                                                            <input id="is-new" type="checkbox" name="is_new" value="1" @if($service->is_new) checked @endif {{$disabled}} data-toggle="switchbutton" data-onlabel="@lang('upay.service.is_new.yes')" data-offlabel="@lang('upay.service.is_new.no')" data-width="86" data-height="33">
                                                            <br><span class="form-text text-danger form-error-text form-error-is_new"></span>
                                                        </div>
                                                    </div>

                                                    <div class="form-group row"><label class="col-lg-2 col-form-label">@lang('upay.general.sort_order')</label>
                                                        <div class="col-lg-2">
                                                            <input type="text" name="sort_order" value="{{$service->sort_order}}" {{$disabled}} class="form-control">
                                                            <span class="form-text text-danger form-error-text form-error-sort_order"></span>
                                                        </div>
                                                    </div>

                                                    <div class="form-group row"><label for="general-status" class="col-lg-2 col-form-label">@lang('upay.general.show_status')</label>
                                                        <div class="col-lg-10">
                                                            <input id="general-status" type="checkbox" name="show_status" value="{{Model::STATUS_ACTIVE}}" @if($service->show_status == Model::STATUS_ACTIVE) checked @endif {{$disabled}} data-toggle="switchbutton" data-onlabel="@lang('upay.general.show_status.active')" data-offlabel="@lang('upay.general.show_status.inactive')" data-width="86" data-height="33">
                                                            <span class="form-text text-danger form-error-text form-error-show_status"></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            @foreach($languages as $lng)
                                                <div role="tabpanel" id="tab-lng-{{$lng->id}}" class="tab-pane">
                                                    <div class="panel-body">
                                                        <div class="form-group required row"><label class="col-lg-2 col-form-label">@lang('upay.service.name')</label>
                                                            <div class="col-lg-10">
                                                                <input type="text" name="ml[{{$lng->id}}][name]" @isset($service->ml[$lng->id]) value="{{$service->ml[$lng->id]->name}}" @endisset {{$disabled}} class="form-control">
                                                                <span class="form-text text-danger form-error-text form-error-ml_{{$lng->id}}_name"></span>
                                                            </div>
                                                        </div>

                                                        <div class="form-group required row"><label class="col-lg-2 col-form-label">@lang('upay.service.payment_purpose')</label>
                                                            <div class="col-lg-10">
                                                                <input type="text" name="ml[{{$lng->id}}][payment_purpose]" @isset($service->ml[$lng->id]) value="{{$service->ml[$lng->id]->payment_purpose}}" @endisset {{$disabled}} class="form-control">
                                                                <span class="form-text text-danger form-error-text form-error-ml_{{$lng->id}}_payment_purpose"></span>
                                                            </div>
                                                        </div>

                                                        <div class="form-group row"><label class="col-lg-2 col-form-label">@lang('upay.service.invoice_note')</label>
                                                            <div class="col-lg-10">
                                                                <textarea rows="3" name="ml[{{$lng->id}}][invoice_note]" {{$disabled}} class="form-control">{{isset($service->ml[$lng->id]) ? $service->ml[$lng->id]->invoice_note : ''}}</textarea>
                                                                <span class="form-text text-danger form-error-text form-error-ml_{{$lng->id}}_invoice_note"></span>
                                                            </div>
                                                        </div>

                                                        <div class="form-group row"><label class="col-lg-2 col-form-label">@lang('upay.service.order_purpose_password')</label>
                                                            <div class="col-lg-10">
                                                                <textarea rows="3" name="ml[{{$lng->id}}][order_purpose_password]" {{$disabled}} class="form-control">{{isset($service->ml[$lng->id]) ? $service->ml[$lng->id]->order_purpose_password : ''}}</textarea>
                                                                <span class="form-text text-danger form-error-text form-error-ml_{{$lng->id}}_order_purpose_password"></span>
                                                            </div>
                                                        </div>

                                                        <div class="form-group row"><label class="col-lg-2 col-form-label">@lang('upay.service.order_received')</label>
                                                            <div class="col-lg-10">
                                                                <textarea rows="3" name="ml[{{$lng->id}}][order_received]" {{$disabled}} class="form-control">{{isset($service->ml[$lng->id]) ? $service->ml[$lng->id]->order_received : ''}}</textarea>
                                                                <span class="form-text text-danger form-error-text form-error-ml_{{$lng->id}}_order_received"></span>
                                                            </div>
                                                        </div>

                                                        <div class="form-group row"><label class="col-lg-2 col-form-label">@lang('upay.service.order_reason')</label>
                                                            <div class="col-lg-10">
                                                                <textarea rows="3" name="ml[{{$lng->id}}][order_reason]" {{$disabled}} class="form-control">{{isset($service->ml[$lng->id]) ? $service->ml[$lng->id]->order_reason : ''}}</textarea>
                                                                <span class="form-text text-danger form-error-text form-error-ml_{{$lng->id}}_order_reason"></span>
                                                            </div>
                                                        </div>

                                                        <div class="form-group row"><label class="col-lg-2 col-form-label">@lang('upay.service.title')</label>
                                                            <div class="col-lg-10">
                                                                <input type="text" name="ml[{{$lng->id}}][title]" @isset($service->ml[$lng->id]) value="{{$service->ml[$lng->id]->title}}" @endisset {{$disabled}} class="form-control">
                                                                <span class="form-text text-danger form-error-text form-error-ml_{{$lng->id}}_title"></span>
                                                            </div>
                                                        </div>

                                                        <div class="form-group row"><label class="col-lg-2 col-form-label">@lang('upay.service.description')</label>
                                                            <div class="col-lg-10">
                                                                <textarea rows="3" name="ml[{{$lng->id}}][description]" {{$disabled}} class="form-control">{{isset($service->ml[$lng->id]) ? $service->ml[$lng->id]->description : ''}}</textarea>
                                                                <span class="form-text text-danger form-error-text form-error-ml_{{$lng->id}}_description"></span>
                                                            </div>
                                                        </div>

                                                        <div class="form-group row"><label for="ml-status-{{$lng->id}}" class="col-lg-2 col-form-label">@lang('upay.general.show_status')</label>
                                                            <div class="col-lg-10">
                                                                <input id="ml-status-{{$lng->id}}" type="checkbox" name="ml[{{$lng->id}}][show_status]" value="{{Model::STATUS_ACTIVE}}" @if(!isset($service->ml[$lng->id]) || $service->ml[$lng->id]->show_status == Model::STATUS_ACTIVE) checked @endif {{$disabled}} data-toggle="switchbutton" data-onlabel="@lang('upay.general.show_status.active')" data-offlabel="@lang('upay.general.show_status.inactive')" data-width="86" data-height="33">
                                                                <span class="form-text text-danger form-error-text form-error-ml_{{$lng->id}}_show_status"></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endforeach
                                            <div role="tabpanel" id="tab-commissions" class="tab-pane">
                                                <div class="panel-body">
                                                    <div class="tabs-container">
                                                        <div class="tabs-right">
                                                            <ul class="nav nav-tabs">
                                                                @foreach(Commission::getTypes() as $i => $type)
                                                                    <li><a class="nav-link @if($i == 0) active @endif" data-toggle="tab" href="#tab-comm-{{$type}}">@lang('upay.service.tab.commissions.'.$type)</a></li>
                                                                @endforeach
                                                            </ul>
                                                            <div class="tab-content">
                                                                @foreach(Commission::getTypes() as $i => $type)
                                                                    <div id="tab-comm-{{$type}}" class="tab-pane @if($i == 0) active @endif">
                                                                        <div class="panel-body">
                                                                            <div class="mb-2">@lang('upay.service.commissions.description')</div>
                                                                            <div class="tabs-container commission-src-tabs-container">
                                                                                <ul class="nav nav-tabs" role="tablist">
                                                                                    @foreach(Commission::getSources() as $j => $src)
                                                                                        <li><a class="nav-link @if($j == 0) active @endif" data-toggle="tab" href="#tab-comm-{{$type}}-{{$src}}">@lang('upay.service.tab.commissions.'.$src)</a></li>
                                                                                    @endforeach
                                                                                </ul>
                                                                                <div class="tab-content">
                                                                                    @foreach(Commission::getSources() as $k => $src)
                                                                                        <div role="tabpanel" id="tab-comm-{{$type}}-{{$src}}" class="tab-pane @if($k == 0) active @endif">
                                                                                            <div class="form-group row"><label class="col-lg-2 col-form-label">@lang('upay.service.commissions.ranges')</label>
                                                                                            <div class="col-lg-10">
                                                                                                <button onclick="$commissions.addRow('{{$type}}', '{{$src}}')" {{$disabled}} type="button" class="btn btn-success"><i class="fa fa-plus"></i> @lang('upay.service.commissions.ranges.add')</button>
                                                                                                <div class="form-text text-danger form-error-text form-error-commissions_{{$type}}_{{$src}}"></div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="form-group row"><label class="col-lg-2 col-form-label"></label>
                                                                                            <div class="col-lg-10">
                                                                                                <table id="commission-values-{{$type}}-{{$src}}" class="table d-none">
                                                                                                    <thead>
                                                                                                        <tr>
                                                                                                            <th>@lang('upay.service.commissions.type')</th>
                                                                                                            <th>@lang('upay.service.commissions.min')</th>
                                                                                                            <th>@lang('upay.service.commissions.max')</th>
                                                                                                            <th>@lang('upay.service.commissions.value')</th>
                                                                                                            <th>@lang('upay.service.commissions.remove')</th>
                                                                                                        </tr>
                                                                                                    </thead>
                                                                                                    <tbody>
                                                                                                    </tbody>
                                                                                                </table>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="form-group row"><label for="comm-status-{{$type}}_{{$src}}" class="col-lg-2 col-form-label">@lang('upay.general.show_status')</label>
                                                                                            <div class="col-lg-10">
                                                                                                <input id="comm-status-{{$type}}_{{$src}}" type="checkbox" name="commissions_{{$type}}_{{$src}}_show_status" value="{{Model::STATUS_ACTIVE}}" @if($src == Commission::SOURCE_BRANCH && (!isset($commissionsSrc[$type.'_'.$src]) && $mode == 'add') || (isset($commissionsSrc[$type.'_'.$src]) && $commissionsSrc[$type.'_'.$src]->show_status == Model::STATUS_ACTIVE)) checked @endif {{$disabled}} data-toggle="switchbutton" data-onlabel="@lang('upay.general.show_status.active')" data-offlabel="@lang('upay.general.show_status.inactive')" data-width="86" data-height="33">
                                                                                                <span class="form-text text-danger form-error-text form-error-commissions_{{$type}}_{{$src}}_show_status"></span>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    @endforeach
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                @endforeach
                                                            </div>
                                                        </div>
                                                    </div>

                                                    @if($mode == 'edit' && $canApproveCommissions)
                                                        <div class="mt-2">
                                                            <button type="button" class="btn btn-danger approve-commissions-btn"><i class="fa fa-check"></i> @lang('upay.service.commissions.approve')</button>
                                                        </div>
                                                    @endif
                                                </div>
                                            </div>
                                            @if($mode == 'edit' && !$service->commissionsHistory->isEmpty())
                                            <div role="tabpanel" id="tab-commissions-history" class="tab-pane">
                                                <div class="panel-body">
                                                    <div class="tabs-container">
                                                        <div class="tabs-right">
                                                            <ul class="nav nav-tabs">
                                                                @foreach($service->commissionsHistory as $type => $sources)
                                                                    <li><a class="nav-link @if($loop->first) active @endif" data-toggle="tab" href="#tab-comm-history-{{$type}}">@lang('upay.service.tab.commissions.'.$type)</a></li>
                                                                @endforeach
                                                            </ul>
                                                            <div class="tab-content">
                                                                @foreach($service->commissionsHistory as $type => $sources)
                                                                    <div id="tab-comm-history-{{$type}}" class="tab-pane @if($loop->first) active @endif">
                                                                        <div class="panel-body">
                                                                            <div class="tabs-container commission-src-tabs-container">
                                                                                <ul class="nav nav-tabs" role="tablist">
                                                                                    @foreach($sources as $src => $itemsBySource)
                                                                                        <li><a class="nav-link @if($loop->first) active @endif" data-toggle="tab" href="#tab-comm-history-{{$type}}-{{$src}}">@lang('upay.service.tab.commissions.'.$src)</a></li>
                                                                                    @endforeach
                                                                                </ul>
                                                                                <div class="tab-content">
                                                                                    @foreach($sources as $src => $itemsBySource)
                                                                                        <div role="tabpanel" id="tab-comm-history-{{$type}}-{{$src}}" class="tab-pane @if($loop->first) active @endif">
                                                                                            <table class="table commission-history-table">
                                                                                                <thead>
                                                                                                    <tr>
                                                                                                        <th class="bg-info edge" colspan="4">@lang('upay.service.commissions.history.old')</th>
                                                                                                        <th class="bg-danger" colspan="4">@lang('upay.service.commissions.history.new')</th>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <th>@lang('upay.service.commissions.type')</th>
                                                                                                        <th>@lang('upay.service.commissions.min')</th>
                                                                                                        <th>@lang('upay.service.commissions.max')</th>
                                                                                                        <th class="edge">@lang('upay.service.commissions.value')</th>
                                                                                                        <th>@lang('upay.service.commissions.type')</th>
                                                                                                        <th>@lang('upay.service.commissions.min')</th>
                                                                                                        <th>@lang('upay.service.commissions.max')</th>
                                                                                                        <th>@lang('upay.service.commissions.value')</th>
                                                                                                    </tr>
                                                                                                </thead>
                                                                                                <?php
                                                                                                    $ranges = $itemsBySource->commissionValuesApproved->groupBy('date');
                                                                                                    $dates = $ranges->keys();
                                                                                                ?>
                                                                                                @foreach($ranges as $date => $current)
                                                                                                    <?php
                                                                                                        $next = !empty($dates[$loop->index+1]) ? $ranges[$dates[$loop->index+1]] : null;
                                                                                                        $longest = max($current->count(), is_null($next) ? 0 : $next->count());
                                                                                                        $activeValuesCount = $itemsBySource->commissionValuesApproved->where('status', CommissionValueApproved::STATUS_ACTIVE)->count();
                                                                                                    ?>
                                                                                                    <tr class="table-th">
                                                                                                        <td colspan="4" class="edge"><b>{{date('d-m-Y H:i:s', strtotime($date))}}</b></td>
                                                                                                        <td colspan="4"><b>@if(!is_null($next)) {{date('d-m-Y H:i:s', strtotime($dates[$loop->index+1]))}} @endif</b></td>
                                                                                                    </tr>
                                                                                                    @for($ind = 0; $ind < $longest; $ind++)
                                                                                                        <tr>
                                                                                                            @if(isset($current[$ind]))
                                                                                                                <td>@lang('upay.service.commissions.type.'.$current[$ind]->type)</td>
                                                                                                                <td>{{$current[$ind]->min}}</td>
                                                                                                                <td>{{$current[$ind]->max}}</td>
                                                                                                                <td class="edge">{{$current[$ind]->type == CommissionValue::TYPE_FIXED ? $current[$ind]->amount.trans('upay.currency.amd') : $current[$ind]->percent.'%'}}</td>
                                                                                                            @else
                                                                                                                <td colspan="4" class="edge"></td>
                                                                                                            @endif
                                                                                                            @if(!is_null($next) && isset($next[$ind]))
                                                                                                                <td @if(!isset($current[$ind]) || $current[$ind]->type != $next[$ind]->type) class="text-danger" @endif>@lang('upay.service.commissions.type.'.$next[$ind]->type)</td>
                                                                                                                <td @if(!isset($current[$ind]) || $current[$ind]->min != $next[$ind]->min) class="text-danger" @endif>{{$next[$ind]->min}}</td>
                                                                                                                <td @if(!isset($current[$ind]) || $current[$ind]->max != $next[$ind]->max) class="text-danger" @endif>{{$next[$ind]->max}}</td>
                                                                                                                <td @if(!isset($current[$ind]) || ($next[$ind]->type == CommissionValue::TYPE_FIXED && $current[$ind]->amount != $next[$ind]->amount) || ($next[$ind]->type == CommissionValue::TYPE_PERCENT && $current[$ind]->percent != $next[$ind]->percent)) class="text-danger" @endif>{{$next[$ind]->type == CommissionValue::TYPE_FIXED ? $next[$ind]->amount.trans('upay.currency.amd') : $next[$ind]->percent.'%'}}</td>
                                                                                                            @else
                                                                                                                @for($n = 0; $n < 4; $n++)<td class="text-danger">@if($ranges->count() > 1 || $activeValuesCount == 0)<i class="fa fa-minus"></i>@endif</td>@endfor
                                                                                                            @endif
                                                                                                        </tr>
                                                                                                    @endfor
                                                                                                    @if($loop->index == $ranges->count() - 2 && $activeValuesCount > 0) @break @endif
                                                                                                @endforeach
                                                                                            </table>
                                                                                        </div>
                                                                                    @endforeach
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                @endforeach
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>

                            {{csrf_field()}}

                            <hr/>
                            <div class="form-group row">
                                <div class="col-lg-4 col-lg-offset-2">
                                    <a href="{{url('/service/list')}}" class="btn btn-white">@lang('upay.general.buttons.cancel')</a>
                                    @if($canEdit)<button class="btn btn-primary submit-btn" type="submit">@lang('upay.general.buttons.save')</button>@endif
                                </div>
                            </div>
                        </form>
                        @if($mode == 'edit' && $canApproveCommissions)
                            <form id="approve-commissions-form" action="{{url('/service/approve-commissions/'.$service->id)}}" method="POST">
                                {{csrf_field()}}
                            </form>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="{{script_url('upay/js/service.js')}}"></script>
@stop
