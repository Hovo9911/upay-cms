<?php

namespace App;

class Model extends \Illuminate\Database\Eloquent\Model
{
    const STATUS_ACTIVE = '1';
    const STATUS_INACTIVE = '2';
    const STATUS_DELETED = '0';

    public function scopeActive($query){
        return $query->where('show_status', self::STATUS_ACTIVE);
    }

    public function scopeNotDeleted($query){
        return $query->where('show_status', '!=', self::STATUS_DELETED);
    }

    public function setAsDeleted(){
        $this->update(['show_status' => self::STATUS_DELETED]);
    }

    public function scopeSorted($query){
        return $query->orderBy('sort_order', 'asc')->orderBy('id', 'desc');
    }
}
