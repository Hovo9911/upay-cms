<?php

namespace App\UPay\Transaction;

use App\Search;

class FailedTransactionSearch extends Search
{
    protected $orderables = [
        'id',
    ];

    public function query()
    {
        return Transaction::with(['cashbox', 'service'])->failed();
    }

    public function totalCount()
    {
        return Transaction::failed()->count();
    }


}
