<?php

namespace App\UPay\Admin;

use App\UPay\Interfaces\AdminRepositoryInterface;

class AdminRepository implements AdminRepositoryInterface
{
    public function create($data): Admin
    {
        $data['password'] = '';
        $data['is_root'] = false;
        $data['password_updated_at'] = date('Y-m-d H:i:s');
        $admin = new Admin($data);
        $admin->save();
        return $admin;
    }

    public function mock(): Admin
    {
        return new Admin([
            'name' => '',
            'email' => '',
            'show_status' => Admin::STATUS_ACTIVE,
            'password_reset_status' => null
        ]);
    }

    public function findOrFail($id): Admin
    {
        $admin = Admin::where('id', $id)->notDeleted()->isNotRoot()->first();
        if (empty($admin)) {
            abort(404);
        }
        return $admin;
    }

    public function update(Admin $admin, $data): Admin
    {
        $admin->fill($data);
        $admin->save();
        return $admin;
    }

    public function delete($ids): int
    {
        AdminRole::whereIn('admin_id', $ids)->update(['show_status' => Admin::STATUS_DELETED]);
        return Admin::whereIn('id', $ids)->isNotRoot()->update(['show_status' => Admin::STATUS_DELETED]);
    }

    public function saveRoles(Admin $admin, array $roles): void
    {
        $admin->roles()->update(['show_status' => AdminRole::STATUS_DELETED]);
        foreach ($roles as $role) {
            $admin->roles()->updateOrCreate(['admin_id' => $admin->id, 'role_id' => $role], ['show_status' => AdminRole::STATUS_ACTIVE]);
        }
    }

    public function emailFound($email, $id): bool
    {
        if (empty($email)) {
            return false;
        }
        $query = Admin::where('email', 'ILIKE', $email)->notDeleted();
        if (!empty($id)) {
            $query->where('id', '!=', $id);
        }
        return !empty($query->first());
    }

    public function findByEmail($email)
    {
        return Admin::where('email', $email)->active()->first();
    }

    public function findByHash($hash) : Admin
    {
        $admin = Admin::where('forgot_password_hash', $hash)
            ->where('forgot_password_hash_exp_date', '>=', date('Y-m-d H:i:s'))
            ->where('password_reset_status', '!=', Admin::PASSWORD_RESET_STATUS_PENDING)
            ->active()->first();
        if (empty($admin)) {
            abort(404);
        }
        return $admin;
    }
}
