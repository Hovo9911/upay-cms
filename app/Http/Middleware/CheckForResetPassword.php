<?php

namespace App\Http\Middleware;

use App\UPay\Admin\Admin;
use Closure;
use Illuminate\Support\Facades\Auth;

class CheckForResetPassword
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::user()->password_reset_status == Admin::PASSWORD_RESET_STATUS_PENDING) {
            return redirect(route('profile'));
        }
        if (Auth::user()->passwordExpired()) {
            return redirect(route('profile'));
        }

        return $next($request);
    }
}
