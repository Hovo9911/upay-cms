<?php

use App\DB\Migration;
use App\DB\Blueprint;

class AddCashboxStateToTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->getSchemaBuilder()->table('transactions', function (Blueprint $table) {
            $table->unsignedInteger('cashbox_state_id')->nullable()->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $this->getSchemaBuilder()->table('transactions', function (Blueprint $table) {
            $table->dropColumn('cashbox_state_id');
        });
    }
}
