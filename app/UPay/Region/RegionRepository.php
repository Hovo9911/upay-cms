<?php

namespace App\UPay\Region;

use App\UPay\Interfaces\RegionRepositoryInterface;
use Illuminate\Database\Eloquent\Collection;

class RegionRepository implements RegionRepositoryInterface
{
    public function all(): Collection
    {
        return Region::joinMl()->notDeleted()->sorted()->get();
    }

    public function found(int $id): bool
    {
        return !empty(Region::where('id', $id)->notDeleted()->first());
    }
}
