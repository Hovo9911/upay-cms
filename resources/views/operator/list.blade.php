@extends('layout')
@section('page_title') @lang('upay.operator.list_title') @stop
@section('content')
<?php
    use App\Model;
    use App\UPay\Operator\Operator;
    $activeMenu = 'operator';
    $canEdit = \Auth::user()->hasPermission('operator.edit');
    $jsTrans->add([
        'upay.operator.status.password_reset_pending',
    ]);
?>
<script>
    var $permissions = { edit: {{$canEdit ? '1' : '0'}} };
</script>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-title">
                    <h1>@lang('upay.operator.list_title')</h1>
                    <div class="ibox-tools">
                        @if($canEdit)<a href="{{url('/operator/edit')}}" class="btn btn-w-m btn-primary"><i class="fa fa-plus"></i> @lang('upay.general.buttons.add_new')</a>@endif
                    </div>
                </div>
                <div class="ibox-content">
                    <div class="ibox">
                        <div class="ibox-title">
                            <h5>@lang('upay.general.list.filters.title')</h5>
                            <div class="ibox-tools"><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></div>
                        </div>
                        <div class="ibox-content">
                            <form id="search-form" method="GET" action="{{url('/operator/get-list')}}" data-edit-url="{{url('/operator/edit/')}}" role="form">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group row">
                                            <label class="col-lg-3 col-form-label text-right">@lang('upay.general.id')</label>
                                            <div class="col-lg-9">
                                                <input type="text" name="filters[id]" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group row">
                                            <label class="col-lg-3 col-form-label text-right">@lang('upay.operator.email')</label>
                                            <div class="col-lg-9">
                                                <input type="text" name="filters[email]" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group row">
                                            <label class="col-lg-3 col-form-label text-right">@lang('upay.operator.first_name')</label>
                                            <div class="col-lg-9">
                                                <input type="text" name="filters[first_name]" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group row">
                                            <label class="col-lg-3 col-form-label text-right">@lang('upay.operator.last_name')</label>
                                            <div class="col-lg-9">
                                                <input type="text" name="filters[last_name]" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group row">
                                            <label class="col-lg-3 col-form-label text-right">@lang('upay.operator.phone')</label>
                                            <div class="col-lg-9">
                                                <input type="text" name="filters[phone]" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group row">
                                            <label class="col-lg-3 col-form-label text-right">@lang('upay.operator.status')</label>
                                            <div class="col-lg-9">
                                                <select class="form-control m-b select-picker" name="filters[status]">
                                                    <option value="">@lang('upay.general.dropdown.select')</option>
                                                    <option value="{{Model::STATUS_ACTIVE}}">@lang('upay.general.show_status.active')</option>
                                                    <option value="{{Model::STATUS_INACTIVE}}">@lang('upay.general.show_status.inactive')</option>
                                                    <option value="{{Operator::PASSWORD_RESET_STATUS_PENDING}}">@lang('upay.operator.status.password_reset_pending')</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <button disabled class="btn btn-info float-right m-t-n-xs submit-btn" type="submit"><i class="fa fa-filter"></i> @lang('upay.general.list.filters.btn')</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table id="search-table" style="width: 100%" class="table table-striped table-bordered table-hover" >
                            <thead>
                                <tr>
                                    <th data-col="id">@lang('upay.general.id')</th>
                                    <th data-col="first_name">@lang('upay.operator.first_name')</th>
                                    <th data-col="last_name">@lang('upay.operator.last_name')</th>
                                    <th data-col="email">@lang('upay.operator.email')</th>
                                    <th data-col="phone" data-orderable="false">@lang('upay.operator.phone')</th>
                                    <th data-col="status" data-orderable="false">@lang('upay.operator.status')</th>
                                    <th data-col="id" data-col-name="actions" action-col>@lang('upay.general.list.actions')</th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="{{script_url('upay/js/operator.js')}}"></script>
@stop
