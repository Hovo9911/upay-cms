<?php

namespace App\Http\Controllers;

use App\Http\Requests\Report\ClosingSearchRequest;
use App\UPay\Cashbox\ClosingSearch;
use App\UPay\Interfaces\BranchRepositoryInterface;
use App\UPay\Interfaces\CashboxRepositoryInterface;
use App\UPay\Interfaces\OperatorRepositoryInterface;
use Illuminate\Http\Request;

class ReportClosingController extends Controller
{
    public function index(BranchRepositoryInterface $branchRepository, OperatorRepositoryInterface $operatorRepository)
    {
        return view('report.closing.list')->with([
            'branches' => $branchRepository->all(),
            'operators' => $operatorRepository->all()
        ]);
    }

    public function view(Request $request, $id, CashboxRepositoryInterface $cashboxRepository)
    {
        return view('report.closing.view')->with($cashboxRepository->getCashboxStateDetails($id));
    }

    public function getList(ClosingSearchRequest $request)
    {
        $searcher = new ClosingSearch($request->validated());
        $result = $searcher->search();
        return response()->json([
            'data' => $result,
            'recordsTotal' => $searcher->totalCount(),
            'recordsFiltered' => $searcher->filteredCount(),
            'totalDetails' => $searcher->totalDetails()
        ]);
    }

    public function export(ClosingSearchRequest $request)
    {
        ini_set('memory_limit', '-1');
        $params = $request->validated();
        $format = !empty($params['format']) && $params['format'] == 'csv' ? 'csv' : 'xlsx';
        $type = $format == 'csv' ? \Maatwebsite\Excel\Excel::CSV : \Maatwebsite\Excel\Excel::XLSX;
        return (new \App\UPay\Cashbox\ClosingExport($params))->download(trans('upay.report_closing.export_file_name').'.'.$format, $type);
    }

    public function exportItem(Request $request, $id, CashboxRepositoryInterface $cashboxRepository)
    {
        ini_set('memory_limit', '-1');
        $itemDetails = $cashboxRepository->getCashboxStateDetails($id);
        $format = $request->input('format');
        $format = !empty($format) && $format == 'csv' ? 'csv' : 'xlsx';
        $type = $format == 'csv' ? \Maatwebsite\Excel\Excel::CSV : \Maatwebsite\Excel\Excel::XLSX;
        return (new \App\UPay\Cashbox\ClosingItemExport($itemDetails))->download(trans('upay.report_closing.export_file_name').'.'.$format, $type);
    }
}
