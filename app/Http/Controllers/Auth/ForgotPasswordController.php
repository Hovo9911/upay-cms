<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\ForgotPasswordRequest;
use App\Http\Requests\Auth\ResetPasswordRequest;
use App\UPay\Interfaces\ResetPasswordRepositoryInterface;
use App\UPay\Interfaces\AdminRepositoryInterface;
use App\UPay\Admin\Admin;
use Illuminate\Support\Facades\Request;

class ForgotPasswordController extends Controller
{
    private $adminRepository;
    private $resetPasswordRepository;

    public function __construct(AdminRepositoryInterface $adminRepository, ResetPasswordRepositoryInterface $resetPasswordRepository)
    {
        $this->adminRepository = $adminRepository;
        $this->resetPasswordRepository = $resetPasswordRepository;
    }
    public function forgot(ForgotPasswordRequest $request)
    {
        $data = $request->validated();

        $admin = $this->adminRepository->findByEmail($data['email']);
        if (!empty($admin) && $admin->password_reset_status != Admin::PASSWORD_RESET_STATUS_PENDING) {
            $this->resetPasswordRepository->sendAdminLink($admin);
        }
        return $this->sendOK([
            'title' => trans('upay.forgot_password.success_title'),
            'message' => trans('upay.forgot_password.success_message'),
            'admin' => $admin
        ]);
    }
    public function showResetForm(Request $request, $hash)
    {
        $this->adminRepository->findByHash($hash);
        return view('auth.reset_password')->with([
            'hash' => $hash
        ]);
    }
    public function reset(ResetPasswordRequest $request, $hash)
    {
        $data = $request->validated();
        $admin = $this->adminRepository->findByHash($hash);
        $this->adminRepository->update($admin, [
            'password' => bcrypt($data['password']),
            'forgot_password_hash' => null,
            'password_updated_at' => date('Y-m-d H:i:s')
        ]);

        return $this->sendOK([
            'message' => trans('upay.reset_password.success_message'),
            'redirect_url' => route('login')
        ]);
    }
}
