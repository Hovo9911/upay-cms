<?php

use App\DB\Migration;
use App\DB\Blueprint;

class AddFieldsToServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->getSchemaBuilder()->table('services', function (Blueprint $table) {
            $table->boolean('hidden')->default(false);
            $table->string('logo')->default('');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $this->getSchemaBuilder()->table('services', function (Blueprint $table) {
            $table->dropColumn('hidden');
            $table->dropColumn('logo');
        });
    }
}
