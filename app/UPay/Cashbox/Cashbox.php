<?php

namespace App\UPay\Cashbox;

use \App\Model;
use App\UPay\Branch\Branch;
use App\UPay\Timeline\Timeline;

class Cashbox extends Model
{
    protected $table = 'cashboxes';

    protected $fillable = [
        'branch_id',
        'account_number',
        'certificate_id',
        'allow_close',
        'allow_pos',
        'pos_account_number',
        'sort_order',
        'show_status',
    ];

    public function ml(){
        return $this->hasMany(CashboxMl::class, 'cashbox_id', 'id');
    }

    public function branch(){
        return $this->belongsTo(Branch::class, 'branch_id', 'id')->joinMl(false);
    }

    public function scopeJoinMl($query, $notDeleted = true){
        $query->join('cashboxes_ml as ml', function ($join) use($notDeleted) {
            $join->on('cashboxes.id', '=', 'ml.cashbox_id')
                ->where('ml.lng_id', '=', c_lng('id'));
            if ($notDeleted) {
                $join->where('ml.show_status', '!=', CashboxMl::STATUS_DELETED);
            }
        });
    }

    public function scopeJoinBranch($query, $notDeleted = true){
        $query->leftJoin('branches', function ($join) use($notDeleted) {
            $join->on('cashboxes.branch_id', '=', 'branches.id');
            if ($notDeleted) {
                $join->where('branches.show_status', '!=', Branch::STATUS_DELETED);
            }
        })->leftJoin('branches_ml as branch_ml', function ($join){
            $join->on('branches.id', '=', 'branch_ml.branch_id')
                ->where('branch_ml.lng_id', '=', c_lng('id'));
        });
    }

    public function timeline(){
        return $this->hasOne(Timeline::class, 'cashbox_id', 'id')->where('date', date('Y-m-d'));
    }
}
