<?php

namespace App\UPay\Localization;

use App\UPay\Interfaces\LanguageRepositoryInterface;
use App\UPay\Interfaces\LocalizationRepositoryInterface;
use DB;
use Illuminate\Support\Facades\Storage;

class LocalizationRepository implements LocalizationRepositoryInterface
{
    protected $languageRepository = null;

    public function __construct(LanguageRepositoryInterface $languageRepository)
    {
        $this->languageRepository = $languageRepository;
    }

    public function create($data): Localization
    {
        $localization = new Localization($data);
        DB::beginTransaction();
        $localization->save();
        $this->saveMl($localization, $data['ml']);
        DB::commit();
        return $localization;
    }

    public function findOrFail($id): Localization
    {
        $localization = Localization::where('id', $id)->first();
        if (empty($localization)) {
            abort(404);
        }
        return $localization;
    }

    public function update($id, $data): Localization
    {
        $localization = $this->findOrFail($id);
        DB::beginTransaction();
        $localization->update($data);
        $this->saveMl($localization, $data['ml']);
        DB::commit();

        return $localization;
    }

    public function delete($ids): int
    {
        DB::beginTransaction();
        LocalizationMl::whereIn('localization_id', $ids)->delete();
        $cnt = Localization::whereIn('id', $ids)->delete();
        DB::commit();
        return $cnt;
    }

    public function saveMl(Localization $localization, array $mls): void
    {
        foreach ($mls as $lngId => $ml) {
            $localization->ml()->updateOrCreate(
                ['localization_id' => $localization->id, 'lng_id' => $lngId],
                ['value' => $ml['value']]
            );
        }
    }

    public function keyFound($key, $group, $id): bool
    {
        $query = Localization::where('key', $key)->where('group', $group);
        if (!empty($id)) {
            $query->where('id', '!=', $id);
        }
        return !empty($query->first());
    }

    public function getGroups(): array
    {
        return [
            Localization::GROUP_CMS,
            Localization::GROUP_OPERATOR_WWW,
            Localization::GROUP_OPERATOR_API,
        ];
    }

    //from DB -> to files
    public function sync($group = null): void
    {
        $groups = !is_null($group) ? [$group] : $this->getGroups();
        $languages = $this->languageRepository->all()->keyBy('id');
        foreach ($groups as $g) {
            $localizations = Localization::join('localizations_ml as ml', function ($join) {
                $join->on('localizations.id', '=', 'ml.localization_id');
            })->where('group', $g)->get()->groupBy('lng_id');

            foreach ($localizations as $lngId => $localization) {
                $filename = $g.'/'.$languages[$lngId]->code.'.json';
                $content = $localization->keyBy('key')->transform(function ($item) { return $item->value; });
                Storage::disk('lang')->put($filename, $content->toJson());
            }
        }
    }

    //from files -> to DB
    public function syncBack(): void
    {
        $groups = $this->getGroups();
        $languages = $this->languageRepository->all()->keyBy('id');
        foreach ($groups as $g) {
            foreach ($languages as $lngId => $lng) {
                $filepath = $g.'/'.$languages[$lngId]->code.'.json';
                if (Storage::disk('lang')->exists($filepath)) {
                    $content = Storage::disk('lang')->get($filepath);
                    if (!empty($content)) {
                        $fileTranses = json_decode($content, true);
                        foreach ($fileTranses as $key => $value) {
                            $loc = Localization::firstOrCreate(['key' => $key, 'group' => $g]);
                            $loc->save();
                            LocalizationMl::insert(['localization_id' => $loc->id, 'lng_id' => $lngId, 'value' => $value]);
                        }
                    }
                }
            }
        }
    }
}
