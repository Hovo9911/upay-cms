<?php

namespace App\UPay\CareerCategory;

use App\Search;

class CareerCategorySearch extends Search
{
    protected $orderables = [
        'id',
        'title',
        'sort_order',
        'show_status',
    ];

    protected function query()
    {
        $filters = $this->filters;

        return  CareerCategory::joinMl()->select(
            'career_categories.id',
            'ml.title',
            'career_categories.show_status'
        )
            ->when(!empty($filters['id']), function($query) use ($filters) {
                $query->where('career_categories.id', $filters['id']);
            })
            ->when(!empty($filters['title']), function($query) use ($filters) {
                $query->where('ml.title', 'ILIKE', '%'.$filters['title'].'%');
            })
            ->when(!empty($filters['show_status']), function($query) use ($filters) {

                $query->where('career_categories.show_status', $filters['show_status']);
            }, function($query) use ($filters) {
                $query->where('career_categories.show_status', '!=', CareerCategory::STATUS_DELETED);
            });

    }

    public function totalCount()
    {
        return CareerCategory::notDeleted()->count();
    }
}
