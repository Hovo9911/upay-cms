<?php
namespace App\UPay\Interfaces;

use \App\UPay\Cashbox\Cashbox;
use Illuminate\Database\Eloquent\Collection;

interface CashboxRepositoryInterface
{
    public function create($data): Cashbox;

    public function mock(): Cashbox;

    public function findOrFail($id): Cashbox;

    public function update($id, $data): Cashbox;

    public function delete($ids): int;

    public function saveMl(Cashbox $cashbox, array $mls): void;

    public function activesWithBranches(): Collection;

    public function withBranches($ids, bool $withActives): Collection;

    public function found(int $id): bool;

    public function getCashboxStateDetails(int $id);

    public function all(): Collection;

    public function getByBranch($branchId): Collection;
}
