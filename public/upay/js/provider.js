var $saveForm = $.extend({}, $gForm);
$saveForm.formId = "save-form";

var $search = $.extend({}, $gsearch);
$search.modifyColumns = {
    name: {
        render: function (data, type, row) {
            return type === 'display' ?
                (row.provider_logo != '' ? '<img class="table-img" src="'+row.provider_logo+'" /> ' : ' ') + data :
                data;
        }
    }
};

$(document).ready(function() {
    if ($($search.searchFormSelector).length) {
        $search.init();
    } else {
        $saveForm.init();
    }
});
