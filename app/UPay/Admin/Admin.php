<?php

namespace App\UPay\Admin;

use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Auth\Authenticatable;
use \App\Model;

class Admin extends Model implements AuthenticatableContract, AuthorizableContract
{
    use Authenticatable, Authorizable;

    const PASSWORD_RESET_STATUS_PENDING = 'pending';
    const PASSWORD_RESET_STATUS_CONFIRMED = 'confirmed';

    protected $fillable = [
        'name',
        'email',
        'password',
        'show_status',
        'password_reset_status',
        'is_root',
        'forgot_password_hash',
        'forgot_password_hash_exp_date',
        'password_updated_at'
    ];

    protected $hidden = [
        'password',
        'is_root',
        'forgot_password_hash',
        'password_updated_at'
    ];

    public function roles(){
        return $this->hasMany(AdminRole::class, 'admin_id', 'id');
    }

    public function permissions(){
        return $this->hasMany(AdminRole::class)
            ->where('admin_roles.show_status', AdminRole::STATUS_ACTIVE)
            ->with('rolePermissions');
    }

    public function scopeIsNotRoot($query){
        return $query->where('is_root', false);
    }

    public function hasPermission($permission) {
        if ($this->is_root) {
            return true;
        }
        //@FIXME optimize this
        $parts = explode('.', $permission);
        $module = $parts[0];
        $action = isset($parts[1]) ? $parts[1] : null;
        $this->permissions();
        foreach ($this->permissions as $perm) {
            if (!empty($perm->rolePermissions)) {
                foreach ($perm->rolePermissions->permissions as $mod => $acts) {
                    if ($mod == $module) {
                        if (is_null($action)) {
                            return true;
                        }
                        if (in_array($action, $acts)) {
                            return true;
                        }
                    }
                }
            }
        }
        return false;
//        $permissions = collect();
//        $this->permissions->map(function($item) use (& $permissions) {
//            $permissions = $permissions->merge(array_values($item->rolePermissions->permissions));
//        });
//        return $permissions;

    }

    public function hasMenuPermission($menu) {
        if ($this->is_root) {
            return true;
        }
        $menuConf = config('menu.'.$menu);
        if (!empty($menuConf) && !empty($menuConf['subs'])) {
            foreach ($menuConf['subs'] as $sub) {
                if ($this->hasPermission($sub['permission'])) {
                    return true;
                }
            }
        }
        return false;
    }

    public function passwordExpired()
    {
        return strtotime($this->password_updated_at.' + '.config('upay.admin.password_active_time')) < time();
    }
}
