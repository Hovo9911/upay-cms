<?php

namespace App\Http\Controllers;

use App\Http\Requests\Career\EditRequest;
use App\Http\Requests\Career\SearchRequest;
use App\Http\Requests\DeleteRequest;
use App\UPay\Career\CareerSearch;
use App\UPay\CareerCategory\CareerCategoryRepository;
use App\UPay\Interfaces\CareerRepositoryInterface;
use App\UPay\Interfaces\LanguageRepositoryInterface;
use DB;
use Illuminate\Http\Request;

class CareerController extends Controller
{
    private $languageRepository;
    private $careerRepository;
    private $careerCategoryRepository;

    public function __construct(CareerRepositoryInterface $careerRepository,CareerCategoryRepository $careerCategoryRepository,LanguageRepositoryInterface $languageRepository)
    {
        $this->careerRepository = $careerRepository;
        $this->careerCategoryRepository = $careerCategoryRepository;
        $this->languageRepository = $languageRepository;
    }

    public function index()
    {
        $careerCategories = $this->careerCategoryRepository->all();

        return view('website.career.list', [
            'careerCategories' => $careerCategories
        ]);
    }

    public function edit(Request $request, $id = null)
    {
        if ($id != null) {
            $career = $this->careerRepository->findOrFail($id);
            $career->load([
                'ml',
            ]);

        } else {
            $career = $this->careerRepository->mock();
        }

        $career->ml = $career->ml->keyBy('lng_id');

        $careerCategories = $this->careerCategoryRepository->all();
        $languages = $this->languageRepository->all();

        return view('website.career.edit')->with([
            'mode' => $id != null ? 'edit' : 'add',
            'career' => $career,
            'careerCategories' => $careerCategories,
            'languages' => $languages,
        ]);
    }

    public function save(EditRequest $request, $id = null)
    {
        $data = $request->validated();

        if ($id != null) {
            $this->careerRepository->findOrFail($id);
            $career = $this->careerRepository->update($id, $data);
        } else {
            $career = $this->careerRepository->create($data);
        }

        return $this->sendOK([
            'message' => trans('upay.website.career.saved.message'),
            'edit_url' => url('/website/career/edit/'.$career->id),
            'list_url' => url('/website/career/list')
        ]);
    }

    public function delete(DeleteRequest $request)
    {
        $this->careerRepository->delete($request->validated()['ids']);
        return $this->sendOK([
            'redirect_url' => url('/website/career/list')
        ]);
    }

    public function getList(SearchRequest $request)
    {
        $searcher = new CareerSearch($request->validated());

        return response()->json([
            'data' => $searcher->search(),
            'recordsTotal' => $searcher->totalCount(),
            'recordsFiltered' => $searcher->filteredCount(),
        ]);
    }
}
