<?php

namespace App\DB;

use Illuminate\Database\Migrations\Migration as LaravelMigration;
use DB;

class Migration extends LaravelMigration
{
    public function getSchemaBuilder()
    {
        DB::connection()->setSchemaGrammar(new Grammar());
        $schema = DB::connection()->getSchemaBuilder();
        $schema->blueprintResolver(function ($table, $callback) {
            return new Blueprint($table, $callback);
        });

        return $schema;
    }

    public function getDB()
    {
        return DB::connection();
    }
}
