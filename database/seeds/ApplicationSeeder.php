<?php

use Illuminate\Database\Seeder;

class ApplicationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('applications')->truncate();
        \DB::table('application_languages')->truncate();

        \DB::table('applications')->insert([
            ['id' => 1, 'name' => 'cms'],
            ['id' => 2, 'name' => 'operator_api'],
            ['id' => 3, 'name' => 'operator_www'],
        ]);
        \DB::table('application_languages')->insert([
            [
                'application_id' => 1,
                'lng_id'    => 3,
                'is_default'     => '1',
                'sort_order'     => 0,
                'show_status'    => '1'
            ],
            [
                'application_id' => 2,
                'lng_id'    => 1,
                'is_default'     => '1',
                'sort_order'     => 0,
                'show_status'    => '1'
            ],
            [
                'application_id' => 2,
                'lng_id'    => 2,
                'is_default'     => '0',
                'sort_order'     => 1,
                'show_status'    => '1'
            ],
            [
                'application_id' => 2,
                'lng_id'    => 3,
                'is_default'     => '0',
                'sort_order'     => 2,
                'show_status'    => '1'
            ],
            [
                'application_id' => 3,
                'lng_id'    => 1,
                'is_default'     => '1',
                'sort_order'     => 0,
                'show_status'    => '1'
            ],
            [
                'application_id' => 3,
                'lng_id'    => 2,
                'is_default'     => '0',
                'sort_order'     => 1,
                'show_status'    => '1'
            ],
            [
                'application_id' => 3,
                'lng_id'    => 3,
                'is_default'     => '0',
                'sort_order'     => 2,
                'show_status'    => '1'
            ]
        ]);
    }
}
