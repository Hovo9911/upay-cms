var $saveForm = $.extend({}, $gForm);
$saveForm.formId = "save-form";

var $search = $.extend({}, $gsearch);
$search.modifyColumns = {
    allow_close: {
        width: '61px',
        className: 'text-center',
        render: function (data, type) {
            return type === 'display' ?
                (data ? '<i title="' + trans('upay.cashbox.allow_close.yes') + '" class="fa fa-check text-navy"></i>' : '<i title="' + trans('upay.cashbox.allow_close.no') + '" class="fa fa-remove text-danger"></i>') :
                '';
        }
    },
    allow_pos: {
        width: '61px',
        className: 'text-center',
        render: function (data, type) {
            return type === 'display' ?
                (data ? '<i title="' + trans('upay.cashbox.allow_pos.yes') + '" class="fa fa-check text-navy"></i>' : '<i title="' + trans('upay.cashbox.allow_pos.no') + '" class="fa fa-remove text-danger"></i>') :
                '';
        }
    }
};

$(document).ready(function() {
    if ($($search.searchFormSelector).length) {
        $(".select-picker-checkbox").select2({
            minimumResultsForSearch: 15,
            templateResult: function (state) {
                if (!state.id) {
                    return state.text;
                }
                return $(state.id == 1 ? '<span><i class="fa fa-check text-navy"></i> ' + state.text + '</span>' : '<span><i class="fa fa-remove text-danger"></i> ' + state.text + '</span>');
            }
        });
        $search.init();
    } else {
        $saveForm.init();
        $("input[name=allow_pos]").on('ifChecked', function(){
            $("input[name=pos_account_number]").closest('.form-group').removeClass('d-none');
        }).on('ifUnchecked', function(){
            $("input[name=pos_account_number]").closest('.form-group').addClass('d-none');
        });
    }
});
