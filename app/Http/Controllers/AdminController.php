<?php

namespace App\Http\Controllers;

use App\Exceptions\InvalidDataException;
use App\Http\Requests\DeleteRequest;
use App\Http\Requests\Admin\EditRequest;
use App\Http\Requests\Admin\SearchRequest;
use App\UPay\Interfaces\ResetPasswordRepositoryInterface;
use App\UPay\Interfaces\RoleRepositoryInterface;
use App\UPay\Interfaces\AdminRepositoryInterface;
use App\UPay\Admin\Admin;
use App\UPay\Admin\AdminSearch;
use Illuminate\Http\Request;
use DB;

class AdminController extends Controller
{
    private $adminRepository;
    private $roleRepository;
    private $resetPasswordRepository;

    public function __construct(AdminRepositoryInterface $adminRepository, RoleRepositoryInterface $roleRepository, ResetPasswordRepositoryInterface $resetPasswordRepository)
    {
        $this->adminRepository = $adminRepository;
        $this->roleRepository = $roleRepository;
        $this->resetPasswordRepository = $resetPasswordRepository;
    }

    public function index()
    {
        $roles = $this->roleRepository->all();
        return view('admin.list', [
            'roles' => $roles
        ]);
    }

    public function edit(Request $request, $id = null)
    {
        if ($id != null) {
            $admin = $this->adminRepository->findOrFail($id);
            $admin->load(['roles' => function($query){ $query->notDeleted(); }]);
        } else {
            $admin = $this->adminRepository->mock();
        }
        $roles = $this->roleRepository->all();
        return view('admin.edit')->with([
            'mode' => $id != null ? 'edit' : 'add',
            'admin' => $admin,
            'roles' => $roles
        ]);
    }

    public function save(EditRequest $request, $id = null)
    {
        $data = $request->validated();
        if (!$this->roleRepository->foundAll($data['roles'])) {
            throw InvalidDataException::withErrors(['roles' => trans('upay.admin.roles.not_found')]);
        }
        if ($this->adminRepository->emailFound($data['email'], $id)) {
            throw InvalidDataException::withErrors(['email' => trans('upay.admin.email.exists')]);
        }
        DB::beginTransaction();
        if ($id != null) {
            $admin = $this->adminRepository->findOrFail($id);
            $resetPassword = ($admin->show_status == Admin::STATUS_INACTIVE && $data['show_status'] == Admin::STATUS_ACTIVE);
            $admin = $this->adminRepository->update($admin, $data);
        } else {
            $resetPassword = ($data['show_status'] == Admin::STATUS_ACTIVE);
            $admin = $this->adminRepository->create($data);
        }
        $this->adminRepository->saveRoles($admin, $data['roles']);
        if ($resetPassword) {
            $this->resetPasswordRepository->sendAdminPassword($admin);
        }
        DB::commit();

        return $this->sendOK([
            'message' => trans('upay.admin.saved.message'),
            'edit_url' => url('/admin/edit/'.$admin->id),
            'list_url' => url('/admin/list')
        ]);
    }

    public function delete(DeleteRequest $request)
    {
        $this->adminRepository->delete($request->validated()['ids']);
        return $this->sendOK([
            'redirect_url' => url('/admin/list')
        ]);
    }

    public function getList(SearchRequest $request)
    {
        $searcher = new AdminSearch($request->validated());
        return response()->json([
            'data' => $searcher->search(),
            'recordsTotal' => $searcher->totalCount(),
            'recordsFiltered' => $searcher->filteredCount(),
        ]);
    }

    public function resetPassword(Request $request, $id)
    {
        $admin = $this->adminRepository->findOrFail($id);
        $this->resetPasswordRepository->sendAdminPassword($admin);
        return $this->sendOK([
            'message' => trans('upay.admin.reset_password.success')
        ]);
    }
}
