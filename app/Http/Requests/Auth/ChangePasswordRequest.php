<?php

namespace App\Http\Requests\Auth;

use App\Http\Requests\Request;

class ChangePasswordRequest extends Request
{
    protected $module = 'change_password';

    public function rules()
    {
        return [
            'old_password' => 'required|string',
            'password' => [
                'required',
                'confirmed',
                'different:old_password',
                'min:10',             // must be at least 10 characters in length
                'regex:/[a-z]/',      // must contain at least one lowercase letter
                'regex:/[A-Z]/',      // must contain at least one uppercase letter
                'regex:/[0-9]/',      // must contain at least one digit
                'regex:/[\\\\!"#\$%&\'\(\)\*\+,-\.\/:;<=>\?@\[\]\^_`{\|}~]/', // must contain a special character
            ]
        ];
    }

    public function messages()
    {
        $messages = parent::messages();
        return array_merge([
            'password.confirmed' => trans('upay.'.$this->module.'.password.not_confirmed'),
            'password.different' => trans('upay.'.$this->module.'.password.the_same'),
        ], $messages);
    }
}
