<?php
namespace App\UPay\Interfaces;

use \App\UPay\Slider\Slider;
use Illuminate\Database\Eloquent\Collection;

interface SliderRepositoryInterface
{
    public function create($data): Slider;

    public function mock(): Slider;

    public function findOrFail($id, $withMl = false): Slider;

    public function update($id, $data): Slider;

    public function delete($ids): int;

    public function saveMl(Slider $slider, array $mls): void;

    public function saveImage($newImage, $oldImage = null): void;

    public function all(): Collection;
}
