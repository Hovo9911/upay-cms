function format_date(ymdhis, time) {
    time = typeof time != "undefined" ? time : true;
    var parts = ymdhis.split(' ');
    var dparts = parts[0].split('-');
    return dparts[2] + '-' + dparts[1] + '-' + dparts[0] + (time ? (' ' + parts[1]) : '');
}

$(document).ready(function () {
    $('.logout-btn').click(function () {
        $("#logout-form").submit();
        return false;
    });

    init_checkboxes();

    $(".select-picker").select2({
        minimumResultsForSearch: 15
    });

    $(".form-datepicker").datepicker({
        todayBtn: "linked",
        keyboardNavigation: false,
        forceParse: false,
        calendarWeeks: true,
        autoclose: true,
        format: "yyyy-mm-dd"
    });

    $('.datetimepicker').each(function () {
        $(this).datetimepicker({
            footer: true,
            modal: true,
            format: "yyyy-mm-dd HH:MM:ss"
        });
    });

    tinymce.init({
        selector: 'textarea.content',
        plugins: "link autolink pagebreak wordcount image paste media hr code fullscreen table visualblocks",
        height: '250px',
        code_dialog_width: 800,
        code_dialog_height: 450,
        toolbar: 'undo redo styleselect | fontsizeselect sub sup forecolor backcolor forecolorpicker backcolorpicker | bold italic alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
        branding: false,
        setup: function (editor) {
            editor.on('change', function () {
                editor.save();
            });
        }
    });

    $('.nav-tabs a').on('click', function () {
        setTimeout(function () {
            $('.tab-pane.active .form-group:first input').focus();
        }, 10);
    });

    $('.hint-tooltip').tooltip({
        template: '<div class="tooltip bs-tooltip-right hint-tooltip-box" role="tooltip">' +
            '<div class="arrow"></div>' +
            '<div class="tooltip-inner">' +
            '</div>' +
            '</div>',
        selector: "[data-toggle=tooltip]",
        container: "body"
    });
});
