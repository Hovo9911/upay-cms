<?php

namespace App\UPay\FaqCategory;

use App\Search;

class FaqCategorySearch extends Search
{
    protected $orderables = [
        'id',
        'title',
        'sort_order',
        'show_status',
    ];

    protected function query()
    {
        $filters = $this->filters;

        return  FaqCategory::joinMl()->select(
            'faq_categories.id',
            'ml.title',
            'faq_categories.show_status'
        )
            ->when(!empty($filters['id']), function($query) use ($filters) {
                $query->where('faq_categories.id', $filters['id']);
            })
            ->when(!empty($filters['title']), function($query) use ($filters) {
                $query->where('ml.title', 'ILIKE', '%'.$filters['title'].'%');
            })
            ->when(!empty($filters['show_status']), function($query) use ($filters) {

                $query->where('faq_categories.show_status', $filters['show_status']);
            }, function($query) use ($filters) {
                $query->where('faq_categories.show_status', '!=', FaqCategory::STATUS_DELETED);
            });
    }

    public function totalCount()
    {
        return FaqCategory::notDeleted()->count();
    }
}
