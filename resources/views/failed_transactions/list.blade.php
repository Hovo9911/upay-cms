@extends('layout')
@section('page_title') @lang('upay.failed_transactions.list_title') @stop
@section('content')
<?php
$activeMenu = 'failed_transactions';
$canRepeat = \Auth::user()->hasPermission('failed_transactions.repeat');
$canComplete = \Auth::user()->hasPermission('failed_transactions.complete');
?>
<script>
    var $permissions = { repeat: {{$canRepeat ? '1' : '0'}}, complete: {{$canComplete ? '1' : '0'}} };
</script>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-title">
                    <h1>@lang('upay.failed_transactions.list_title')</h1>
                </div>
                <div class="ibox-content">
                    <div class="ibox d-none">
                        <div class="ibox-title">
                            <h5>@lang('upay.general.list.filters.title')</h5>
                            <div class="ibox-tools"><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></div>
                        </div>
                        <div class="ibox-content">
                            <form id="search-form" method="GET" action="{{url('/failed-transactions/get-list')}}" role="form">
                            </form>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table id="search-table" style="width: 100%" class="table table-striped table-bordered table-hover" >
                            <thead>
                                <tr>
                                    <th data-col="service" data-orderable="false">@lang('upay.failed_transactions.service')</th>
                                    <th data-col="cashbox" data-orderable="false">@lang('upay.failed_transactions.cashbox')</th>
                                    <th data-col="code" data-orderable="false">@lang('upay.failed_transactions.code')</th>
                                    <th data-col="receipt_id" data-orderable="false">@lang('upay.failed_transactions.receipt_id')</th>
                                    <th data-col="amount" data-orderable="false">@lang('upay.failed_transactions.amount')</th>
                                    <th data-col="created_at" data-orderable="false">@lang('upay.failed_transactions.date')</th>
                                    <th data-col="repeat" data-orderable="false">@lang('upay.failed_transactions.repeat')</th>
                                    <th data-col="complete" data-orderable="false">@lang('upay.failed_transactions.complete')</th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@if($canRepeat)
<form id="repeat-form" action="{{url('/failed-transactions/repeat')}}">
    <input type="hidden" name="id" value="">
    {{csrf_field()}}
</form>
@endif

@if($canComplete)
<form id="complete-form" action="{{url('/failed-transactions/complete')}}">
    <input type="hidden" name="id" value="">
    {{csrf_field()}}
</form>
@endif

<script src="{{script_url('upay/js/failed_transactions.js')}}"></script>
@stop
