<?php

namespace App\UPay\Faq;

use App\MultiPKModel;

class FaqMl extends MultiPKModel
{
    protected $table = 'faqs_ml';
    public $timestamps = false;
    protected $primaryKey = ['faq_id', 'lng_id'];
    public $incrementing = false;

    protected $fillable = [
        'faq_id',
        'lng_id',
        'question',
        'answer',
        'show_status',
    ];
}
