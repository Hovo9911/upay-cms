<?php
namespace App\UPay\Interfaces;

use \App\UPay\FaqCategory\FaqCategory;
use Illuminate\Database\Eloquent\Collection;

interface FaqCategoryRepositoryInterface
{
    public function create($data): FaqCategory;

    public function mock(): FaqCategory;

    public function findOrFail($id, $withMl = false): FaqCategory;

    public function update($id, $data): FaqCategory;

    public function delete($ids): int;

    public function saveMl(FaqCategory $faqCategory, array $mls): void;

    public function all(): Collection;
}
