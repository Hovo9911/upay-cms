<?php
    $activeMenu = 'report_closing';
?>
@extends('layout')
@section('page_title') @lang('upay.report_closing.view_title') @stop
@section('content')
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox">
                    <div class="ibox-title">
                        <h1>@lang('upay.report_closing.view_title')</h1>
                    </div>
                    <div class="ibox-content">
                        <div class="row">
                            <div class="col-lg-6">
                                <table class="table table-bordered">
                                    <tbody>
                                    <tr>
                                        <td>@lang('upay.report_closing.initial_balance')</td>
                                        <td><b>{{$cashboxState->initial_balance}}</b></td>
                                    </tr>
                                    <tr>
                                        <td>@lang('upay.report_closing.cash_in')</td>
                                        <td><b>{{$cashboxState->cash_in}}</b></td>
                                    </tr>
                                    <tr>
                                        <td>@lang('upay.report_closing.cash_out')</td>
                                        <td><b>{{$cashboxState->cash_out}}</b></td>
                                    </tr>
                                    <tr>
                                        <td>@lang('upay.report_closing.non_cash')</td>
                                        <td><b>{{$cashboxState->non_cash}}</b></td>
                                    </tr>
                                    <tr>
                                        <td>@lang('upay.report_closing.final_balance')</td>
                                        <td><b>{{$cashboxState->final_balance}}</b></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-lg-6">
                                <table class="table table-bordered">
                                    <tbody>
                                    <tr>
                                        <td>@lang('upay.report_closing.closing_date')</td>
                                        <td><b>{{format_date($cashboxState->closing_date)}}</b></td>
                                    </tr>
                                    <tr>
                                        <td>@lang('upay.report_closing.cashbox')</td>
                                        <td><b>{{$cashboxState->cashbox->name}}</b></td>
                                    </tr>
                                    <tr>
                                        <td>@lang('upay.report_closing.operator')</td>
                                        @if($cashboxState->operator != null)
                                            <td><b>{{$cashboxState->operator->first_name}} {{$cashboxState->operator->last_name}}</b></td>
                                        @else
                                            <td><b>@lang('upay.report_closing.closed_automatically')</b></td>
                                        @endif
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <hr>

                        <table id="transactions-table" export-action="{{url('/report-closing/export/'.$cashboxState->id)}}" class="table table-bordered dataTable">
                            <thead>
                            <tr>
                                <th>@lang('upay.report_closing.details.service')</th>
                                <th class="text-right">@lang('upay.report_closing.details.cash')</th>
                                <th class="text-right">@lang('upay.report_closing.details.non_cash')</th>
                                <th class="text-right">@lang('upay.report_closing.details.count')</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($transactions as $row)
                                <tr>
                                    <td>{{$row->service->name}}</td>
                                    <td class="text-right">{{$row->cash}}</td>
                                    <td class="text-right">{{$row->non_cash}}</td>
                                    <td class="text-right">{{$row->count}}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                        <hr/>

                        <div class="form-group row">
                            <div class="col-lg-4 col-lg-offset-2">
                                <a href="{{url('/report-closing/list')}}" class="btn btn-white">@lang('upay.general.buttons.cancel')</a>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="{{script_url('upay/js/report/closing.js')}}"></script>
@stop
