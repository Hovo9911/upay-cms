@extends('layout')
@section('page_title') @lang('upay.report_closing.list_title') @stop
@section('content')
<?php
$activeMenu = 'report_closing';
$jsTrans->add('upay.report_closing.closed_automatically');
?>
<style>
    tfoot th {
        background-color: #b2e1d673;
    }
</style>
<script>
    var $permissions = { };
</script>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-title">
                    <h1>@lang('upay.report_closing.list_title')</h1>
                </div>
                <div class="ibox-content">
                    <div class="ibox">
                        <div class="ibox-title">
                            <h5>@lang('upay.general.list.filters.title')</h5>
                            <div class="ibox-tools"><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></div>
                        </div>
                        <div class="ibox-content">
                            <form id="search-form" method="GET" action="{{url('/report-closing/get-list')}}" data-view-url="{{url('/report-closing/view/')}}" export-action="{{url('/report-closing/export')}}" role="form">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group row">
                                            <label class="col-lg-3 col-form-label text-right">@lang('upay.report_closing.closing_date')</label>
                                            <div class="col-lg-9">
                                                <div class="input-group date form-datepicker">
                                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input name="filters[date_from]" type="text" class="form-control" value="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group row">
                                            <span class="col-lg-2 col-form-label text-right fa fa-long-arrow-right"></span>
                                            <label class="col-lg-1 col-form-label text-right"></label>
                                            <div class="col-lg-9">
                                                <div class="input-group date form-datepicker">
                                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input name="filters[date_to]" type="text" class="form-control" value="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group row">
                                            <label class="col-lg-3 col-form-label text-right">@lang('upay.report_closing.branch')</label>
                                            <div class="col-lg-9">
                                                <select id="branch" data-cashbox-action="{{url('branch/get-cashboxes')}}" class="form-control m-b select-picker">
                                                    <option value="">@lang('upay.general.dropdown.select')</option>
                                                    @foreach($branches as $branch)
                                                        <option value="{{$branch->id}}">{{$branch->name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-6">
                                        <div class="form-group row">
                                            <label class="col-lg-3 col-form-label text-right">@lang('upay.report_closing.cashbox')</label>
                                            <div class="col-lg-9">
                                                <select id="cashbox" name="filters[cashbox]" style="width: 100%" class="form-control m-b select-picker">
                                                    <option value="">@lang('upay.general.dropdown.select')</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group row">
                                            <label class="col-lg-3 col-form-label text-right">@lang('upay.report_closing.operator')</label>
                                            <div class="col-lg-9">
                                                <select name="filters[operator]" class="form-control m-b select-picker">
                                                    <option value="">@lang('upay.general.dropdown.select')</option>
                                                    @foreach($operators as $operator)
                                                        <option value="{{$operator->id}}">{{$operator->first_name}} {{$operator->last_name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-6">
                                        <div class="form-group row">
                                            <label class="col-lg-3 col-form-label text-right"></label>
                                            <div class="col-lg-9">
                                                <input id="closed-auto" type="checkbox" name="filters[auto_closed]" value="1" class="i-checks">
                                                <label for="closed-auto" class="col-form-label">@lang('upay.report_closing.closed_automatically')</label>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                                <div class="row">
                                    <div class="col-lg-12">
                                        <button disabled class="btn btn-info float-right m-t-n-xs submit-btn" type="submit"><i class="fa fa-filter"></i> @lang('upay.general.list.filters.btn')</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table id="search-table" style="width: 100%" class="table table-striped table-bordered table-hover" >
                            <thead>
                                <tr>
                                    <th data-col="closing_date">@lang('upay.report_closing.closing_date')</th>
                                    <th data-col="cashbox" data-orderable="false">@lang('upay.report_closing.cashbox')</th>
                                    <th data-col="operator" data-orderable="false">@lang('upay.report_closing.operator')</th>
                                    <th data-col="initial_balance" data-orderable="false">@lang('upay.report_closing.initial_balance')</th>
                                    <th data-col="cash_in" data-orderable="false">@lang('upay.report_closing.cash_in')</th>
                                    <th data-col="cash_out" data-orderable="false">@lang('upay.report_closing.cash_out')</th>
                                    <th data-col="final_balance">@lang('upay.report_closing.final_balance')</th>
                                    <th data-col="id" data-col-name="actions" action-col>@lang('upay.general.list.actions')</th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                            <tfoot>
                                <tr>
                                    <th colspan="3">@lang('upay.report_closing.total_info') <span></span> @lang('upay.report_closing.total_info.count')</th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="{{script_url('upay/js/report/closing.js')}}"></script>
@stop
