<?php

namespace App\Http\Requests\Slider;

use App\Http\Requests\Request;
use App\Model;

class EditRequest extends Request
{
    protected $module = 'slider';

    public function rules()
    {
        $rules = [
            'image' => 'required|max:255',
            'sort_order' => 'required|integer',
            'show_status' => 'in:'.Model::STATUS_ACTIVE.','.Model::STATUS_INACTIVE,
            'ml' => 'required|array',
        ];
        $rules = $this->checkStatusForRequired($rules, request()->input('show_status'));

        $rules = $this->addMlRules($rules, [
            'title' => 'required|max:255',
            'description' => 'required|max:500',
            'button_text_1' => 'nullable|max:100',
            'button_url_1' => 'nullable|url',
            'button_text_2' => 'nullable|max:100',
            'button_url_2' => 'nullable|url',
            'show_status' => 'in:'.Model::STATUS_ACTIVE.','.Model::STATUS_INACTIVE
        ]);

        return $rules;
    }

    protected function defaultValues()
    {
        return [
            'image' => '',
            'show_status' => Model::STATUS_INACTIVE,
            'ml' => [
                'title' => '',
                'description' => '',
                'button_text_1' => '',
                'button_url_1' => '',
                'button_text_2' => '',
                'button_url_2' => '',
                'show_status' => Model::STATUS_INACTIVE,
            ]
        ];
    }

}
