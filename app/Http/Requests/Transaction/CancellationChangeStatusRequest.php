<?php

namespace App\Http\Requests\Transaction;

use App\Http\Requests\Request;
use App\UPay\Transaction\TransactionCancellation;

class CancellationChangeStatusRequest extends Request
{
    protected $module = 'cancellation_change_status';

    public function rules()
    {
        return [
            'status' => 'required|string|in:'.implode(',',TransactionCancellation::statuses()),
            'notes' => 'string|max:1024',
        ];
    }

    protected function defaultValues()
    {
        return [
            'status' => '',
            'notes' => '',
        ];
    }
}
