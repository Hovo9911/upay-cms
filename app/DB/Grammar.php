<?php

namespace App\DB;

use Illuminate\Database\Schema\Grammars\PostgresGrammar;
use Illuminate\Support\Fluent;

class Grammar extends PostgresGrammar
{
    public function __construct()
    {
        $this->tablePrefix = \DB::getTablePrefix();
    }

    protected function typeShowStatus(Fluent $column)
    {
        return "show_status ";
    }
}
