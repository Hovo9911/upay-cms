@extends('layout')
@section('page_title') @lang('upay.service.list_title') @stop
@section('content')
<?php
    use App\Model;
    $activeMenu = 'service';
    $canEdit = \Auth::user()->hasPermission('service.edit');
    $jsTrans->add([
        'upay.service.use_fixed_amount.yes',
        'upay.service.use_fixed_amount.no',
        'upay.service.account_number.no',
        'upay.service.mobile_commission_account_number.short',
        'upay.service.mobile_account_number.short',
        'upay.service.branch_commission_account_number.short',
        'upay.service.branch_account_number.short'
    ]);
?>
<script>
    var $permissions = { edit: {{$canEdit ? '1' : '0'}} };
</script>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-title">
                    <h1>@lang('upay.service.list_title')</h1>
                    <div class="ibox-tools">
                        @if($canEdit)<a href="{{url('/service/edit')}}" class="btn btn-w-m btn-primary"><i class="fa fa-plus"></i> @lang('upay.general.buttons.add_new')</a>@endif
                    </div>
                </div>
                <div class="ibox-content">
                    <div class="ibox">
                        <div class="ibox-title">
                            <h5>@lang('upay.general.list.filters.title')</h5>
                            <div class="ibox-tools"><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></div>
                        </div>
                        <div class="ibox-content">
                            <form id="search-form" method="GET" action="{{url('/service/get-list')}}" data-edit-url="{{url('/service/edit/')}}" role="form">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group row">
                                            <label class="col-lg-3 col-form-label text-right">@lang('upay.general.id')</label>
                                            <div class="col-lg-9">
                                                <input type="text" name="filters[id]" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group row">
                                            <label class="col-lg-3 col-form-label text-right">@lang('upay.service.name')</label>
                                            <div class="col-lg-9">
                                                <input type="text" name="filters[name]" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group row">
                                            <label class="col-lg-3 col-form-label text-right">@lang('upay.service.provider')</label>
                                            <div class="col-lg-9">
                                                <select class="form-control m-b select-picker" name="filters[provider]">
                                                    <option value="">@lang('upay.general.dropdown.select')</option>
                                                    @foreach($providers as $provider)
                                                        <option value="{{$provider->id}}">{{(!empty($provider->parent) ? $provider->parent.' > ' : '').$provider->name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group row">
                                            <label class="col-lg-3 col-form-label text-right">@lang('upay.general.show_status')</label>
                                            <div class="col-lg-9">
                                                <select class="form-control m-b select-picker" name="filters[show_status]">
                                                    <option value="">@lang('upay.general.dropdown.select')</option>
                                                    <option value="{{Model::STATUS_ACTIVE}}">@lang('upay.general.show_status.active')</option>
                                                    <option value="{{Model::STATUS_INACTIVE}}">@lang('upay.general.show_status.inactive')</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <button disabled class="btn btn-info float-right m-t-n-xs submit-btn" type="submit"><i class="fa fa-filter"></i> @lang('upay.general.list.filters.btn')</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table id="search-table" style="width: 100%" class="table table-striped table-bordered table-hover" >
                            <thead>
                                <tr>
                                    <th data-col="id">@lang('upay.general.id')</th>
                                    <th data-col="name">@lang('upay.service.name')</th>
                                    <th data-col="provider">@lang('upay.service.provider')</th>
                                    <th data-col="branch_account_number" data-orderable="false">@lang('upay.service.account_numbers')</th>
                                    <th data-col="account_min" data-orderable="false">@lang('upay.service.account_range')</th>
                                    <th data-col="use_fixed_amount" data-orderable="false">@lang('upay.service.use_fixed_amount')</th>
                                    <th data-col="show_status">@lang('upay.general.show_status')</th>
                                    <th data-col="id" data-col-name="actions" action-col>@lang('upay.general.list.actions')</th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="{{script_url('upay/js/service.js')}}"></script>
@stop
