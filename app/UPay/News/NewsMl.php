<?php

namespace App\UPay\News;

use App\MultiPKModel;

class NewsMl extends MultiPKModel
{
    protected $table = 'news_ml';
    public $timestamps = false;
    protected $primaryKey = ['news_id', 'lng_id'];
    public $incrementing = false;

    protected $fillable = [
        'news_id',
        'lng_id',
        'alias',
        'title',
        'short_description',
        'body',
        'show_status',
    ];
}
