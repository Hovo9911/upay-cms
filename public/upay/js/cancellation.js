var $saveForm = $.extend({}, $gForm);
$saveForm.formId = "change-status-form";

var $search = $.extend({}, $gsearch);
$search.defaultOrder = [[2, 'desc']];
$search.modifyColumns = {
    status: {
        width: '200px',
        render: function(data, type, row) {
            var color = 'badge-primary';
            switch(data) {
                case 'new':
                case 'processing':
                    color = 'badge-warning';
                    break;
                case 'aborted':
                case 'rejected':
                case 'pre_declined':
                case 'finally_declined':
                    color = 'badge-danger';
                    break;
            }
            return type === 'display' ?
            '<span class="badge '+color+'">'+trans('upay.cancellation.'+(row.status == 'rejected' || row.status == 'completed' ? row.type+'.' : '')+'status.'+data)+'</span> '
                 :
                data;
        }
    },
    service: {
        render: function (data, type, row) {
            return type === 'display' ?
                (data.service_logo != '' ? '<img class="table-img" src="'+data.service_logo+'" /> ' : ' ') + data.name :
                data;
        }
    },
    cashbox: {
        render: function (data, type, row) {
            return type === 'display' ? data.name : data;
        }
    },
    operator: {
        render: function (data, type, row) {
            return type === 'display' ? data.first_name+' '+data.last_name : data;
        }
    },
    transaction_date: {
        render: function (data, type, row) {
            return type === 'display' ? format_date(data) : data;
        }
    },
    type: {
        render: function (data, type, row) {
            return type === 'display' ? trans('upay.cancellation.type.'+data) : data;
        }
    },
    actions: {
        render: function(data, type) {
            return type === 'display' ?
                '<a href="' + $search.searchForm.data('view-url') + '/' + data + '" class="btn btn-xs btn-success"><i class="fa fa-eye"></i></a> ' :
                data;
        }
    }
};

$saveForm.changeStatus = function(status, elem) {
    $("input[name=status]", $saveForm.form()).val(status);
    if ($(elem).data('confirm')) {
        swal({
            title: trans('upay.cancellation_change_status.confirm_modal_title'),
            text: trans('upay.cancellation_change_status.confirm_modal_text'),
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: trans('upay.cancellation_change_status.confirm_modal_ok'),
            cancelButtonText: trans('upay.cancellation_change_status.confirm_modal_cancel'),
            closeOnConfirm: false
        }, function () {
            $saveForm.form().submit();
        });
    } else {
        $saveForm.form().submit();
    }
};

$(document).ready(function() {
    if ($($search.searchFormSelector).length) {
        $search.init();
    } else {
        $saveForm.init();
    }
});
