<?php

use Illuminate\Database\Seeder;

class RegionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $src = [
            [1 => 'Երևան', 2 => 'Ереван', 3 => 'Yerevan'],
            [1 => 'Արագածոտն', 2 => 'Арагацотн', 3 => 'Aragatsotn'],
            [1 => 'Արարատ', 2 => 'Арарат', 3 => 'Ararat'],
            [1 => 'Արմավիր', 2 => 'Армавир', 3 => 'Armavir'],
            [1 => 'Գեղարքունիք', 2 => 'Гегаркуник', 3 => 'Gegharkunik'],
            [1 => 'Լոռի', 2 => 'Лори', 3 => 'Lori'],
            [1 => 'Կոտայք', 2 => 'Котайк', 3 => 'Kotayk'],
            [1 => 'Շիրակ', 2 => 'Ширак', 3 => 'Shirak'],
            [1 => 'Սյունիք', 2 => 'Сюник', 3 => 'Syunik'],
            [1 => 'Վայոց ձոր', 2 => 'Вайоц Дзор', 3 => 'Vayots Dzor'],
            [1 => 'Տավուշ', 2 => 'Тавуш', 3 => 'Tavush'],
        ];
        $regions = [];
        $regionMls = [];
        foreach ($src as $i => $mls) {
            $regions [] = [
                'id' => ($i + 1),
                'sort_order' => ($i + 1),
                'show_status' => '1'
            ];
            foreach ($mls as $lngId => $name) {
                $regionMls [] = [
                    'region_id' => ($i + 1),
                    'lng_id' => $lngId,
                    'name' => $name
                ];
            }
        }
        \DB::table('regions')->insert($regions);
        \DB::table('regions_ml')->insert($regionMls);
    }
}
