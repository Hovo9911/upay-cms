<?php

namespace App\Http\Requests\Auth;

use App\Http\Requests\Request;

class ResetPasswordRequest extends Request
{
    protected $module = 'reset_password';

    public function rules()
    {
        return [
            'password' => [
                'required',
                'confirmed',
                'min:10',             // must be at least 10 characters in length
                'regex:/[a-z]/',      // must contain at least one lowercase letter
                'regex:/[A-Z]/',      // must contain at least one uppercase letter
                'regex:/[0-9]/',      // must contain at least one digit
                'regex:/[\\\\!"#\$%&\'\(\)\*\+,-\.\/:;<=>\?@\[\]\^_`{\|}~]/', // must contain a special character
            ]
        ];
    }

    public function messages()
    {
        $messages = parent::messages();
        return array_merge([
            'password.confirmed' => trans('upay.'.$this->module.'.password.not_confirmed'),
        ], $messages);
    }
}
