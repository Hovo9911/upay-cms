<?php

namespace App\Http\Requests\Slider;

use App\Http\Requests\DatatableSearchRequest;
use App\Model;
use App\UPay\Operator\Operator;

class SearchRequest extends DatatableSearchRequest
{
    public function rules()
    {
        return [
            'filters.id' => 'integer',
            'filters.title' => 'string|max:255',
            'filters.show_status' => 'in:'.Model::STATUS_ACTIVE.','.Model::STATUS_INACTIVE,
            'order' => 'array',
            'start' => 'integer',
        ];
    }
}
