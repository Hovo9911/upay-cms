<?php

namespace App\UPay\Service;

use App\MultiPKModel;

class CommissionValueApproved extends MultiPKModel
{
    const STATUS_ACTIVE = 'active';
    const STATUS_ARCHIVED = 'archived';

    protected $table = 'service_commission_values_approved';
    public $timestamps = false;
    public $incrementing = false;

    protected $fillable = [
        'commission_id',
        'type',
        'min',
        'max',
        'amount',
        'percent',
        'date',
        'status',
    ];
}
