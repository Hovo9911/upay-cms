<?php
    use App\Model;
    $activeMenu = 'cashbox';
    $canDelete = \Auth::user()->hasPermission('cashbox.delete');
    $canEdit = \Auth::user()->hasPermission('cashbox.edit');
    $title = 'upay.cashbox.'.($canEdit ? $mode : 'view').'_title';
    $disabled = $canEdit ? '' : 'disabled';
?>
@extends('layout')
@section('page_title') @lang($title) @stop
@section('content')
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox ">
                    <div class="ibox-title">
                        <h1>@lang($title)</h1>
                    </div>
                    <div class="ibox-content">
                        <form id="save-form" action="{{url('/cashbox/save/'.($cashbox->id ?? ''))}}" method="POST">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="tabs-container">
                                        <ul class="nav nav-tabs" role="tablist">
                                            <li><a class="nav-link active" data-toggle="tab" href="#tab-general">@lang('upay.tab.general')</a></li>
                                            @foreach($languages as $lng)
                                                <li><a class="nav-link" data-toggle="tab" href="#tab-lng-{{$lng->id}}">{{$lng->name}}</a></li>
                                            @endforeach
                                            @if($mode == 'edit')
                                                <li><a class="nav-link" data-toggle="tab" href="#tab-assignees">@lang('upay.cashbox.tab.assignees')</a></li>
                                            @endif
                                        </ul>
                                        <div class="tab-content">
                                            <div role="tabpanel" id="tab-general" class="tab-pane active">
                                                <div class="panel-body">
                                                    <div class="form-group required row"><label class="col-lg-2 col-form-label">@lang('upay.cashbox.branch')</label>
                                                        <div class="col-lg-10">
                                                            <select name="branch_id" {{$disabled}} class="form-control m-b select-picker">
                                                                <option value="">@lang('upay.general.dropdown.select')</option>
                                                                @foreach($branches as $branch)
                                                                    <option value="{{$branch->id}}" @if($cashbox->branch_id == $branch->id) selected="selected" @endif>{{$branch->name}}</option>
                                                                @endforeach
                                                            </select>
                                                            <span class="form-text text-danger form-error-text form-error-branch_id"></span>
                                                        </div>
                                                    </div>
                                                    <div class="form-group required row"><label class="col-lg-2 col-form-label">@lang('upay.cashbox.account_number')</label>
                                                        <div class="col-lg-10">
                                                            <input type="text" name="account_number" value="{{$cashbox->account_number}}" {{$disabled}} class="form-control">
                                                            <span class="form-text text-danger form-error-text form-error-account_number"></span>
                                                        </div>
                                                    </div>

{{--                                                    <div class="form-group row"><label class="col-lg-2 col-form-label">@lang('upay.cashbox.certificate_id')</label>--}}
{{--                                                        <div class="col-lg-10">--}}
{{--                                                            <input type="text" name="certificate_id" value="{{$cashbox->certificate_id}}" {{$disabled}} class="form-control">--}}
{{--                                                            <span class="form-text text-danger form-error-text form-error-certificate_id"></span>--}}
{{--                                                        </div>--}}
{{--                                                    </div>--}}

                                                    <div class="form-group row"><label for="allow-close" class="col-lg-2 col-form-label">@lang('upay.cashbox.allow_close') <span class="align-self-center hint-tooltip"><i class="fa fa-lg fa-question-circle" data-toggle="tooltip" data-placement="right" data-original-title="@lang('upay.cashbox.allow_close.hint')"></i></span></label>
                                                        <div class="col-lg-10 align-self-sm-center">
                                                            <input id="allow-close" type="checkbox" name="allow_close" value="1" @if($cashbox->allow_close) checked @endif {{$disabled}} class="i-checks">
                                                            <div class="form-text text-danger form-error-text form-error-allow_close"></div>
                                                        </div>
                                                    </div>

                                                    <div class="form-group row"><label for="allow-pos" class="col-lg-2 col-form-label">@lang('upay.cashbox.allow_pos') <span class="align-self-center hint-tooltip"><i class="fa fa-lg fa-question-circle" data-toggle="tooltip" data-placement="right" data-original-title="@lang('upay.cashbox.allow_pos.hint')"></i></span></label>
                                                        <div class="col-lg-10 align-self-sm-center">
                                                            <input id="allow-pos" type="checkbox" name="allow_pos" value="1" @if($cashbox->allow_pos) checked @endif {{$disabled}} class="i-checks">
                                                            <div class="form-text text-danger form-error-text form-error-allow_pos"></div>
                                                        </div>
                                                    </div>

                                                    <div class="form-group required row @if(!$cashbox->allow_pos) d-none @endif"><label class="col-lg-2 col-form-label">@lang('upay.cashbox.pos_account_number')</label>
                                                        <div class="col-lg-10">
                                                            <input type="text" name="pos_account_number" value="{{$cashbox->pos_account_number}}" {{$disabled}} class="form-control">
                                                            <span class="form-text text-danger form-error-text form-error-pos_account_number"></span>
                                                        </div>
                                                    </div>

                                                    <div class="form-group row"><label class="col-lg-2 col-form-label">@lang('upay.general.sort_order')</label>
                                                        <div class="col-lg-2">
                                                            <input type="text" name="sort_order" value="{{$cashbox->sort_order}}" {{$disabled}} class="form-control">
                                                            <span class="form-text text-danger form-error-text form-error-sort_order"></span>
                                                        </div>
                                                    </div>

                                                    <div class="form-group row"><label for="general-status" class="col-lg-2 col-form-label">@lang('upay.general.show_status')</label>
                                                        <div class="col-lg-10">
                                                            <input id="general-status" type="checkbox" name="show_status" value="{{Model::STATUS_ACTIVE}}" @if($cashbox->show_status == Model::STATUS_ACTIVE) checked @endif {{$disabled}} data-toggle="switchbutton" data-onlabel="@lang('upay.general.show_status.active')" data-offlabel="@lang('upay.general.show_status.inactive')" data-width="86" data-height="33">
                                                            <span class="form-text text-danger form-error-text form-error-show_status"></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            @foreach($languages as $lng)
                                                <div role="tabpanel" id="tab-lng-{{$lng->id}}" class="tab-pane">
                                                    <div class="panel-body">
                                                        <div class="form-group required row"><label class="col-lg-2 col-form-label">@lang('upay.cashbox.name')</label>
                                                            <div class="col-lg-10">
                                                                <input type="text" name="ml[{{$lng->id}}][name]" @isset($cashbox->ml[$lng->id]) value="{{$cashbox->ml[$lng->id]->name}}" @endisset {{$disabled}} class="form-control">
                                                                <span class="form-text text-danger form-error-text form-error-ml_{{$lng->id}}_name"></span>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row"><label for="ml-status-{{$lng->id}}" class="col-lg-2 col-form-label">@lang('upay.general.show_status')</label>
                                                            <div class="col-lg-10">
                                                                <input id="ml-status-{{$lng->id}}" type="checkbox" name="ml[{{$lng->id}}][show_status]" value="{{Model::STATUS_ACTIVE}}" @if(!isset($cashbox->ml[$lng->id]) || $cashbox->ml[$lng->id]->show_status == Model::STATUS_ACTIVE) checked @endif {{$disabled}} data-toggle="switchbutton" data-onlabel="@lang('upay.general.show_status.active')" data-offlabel="@lang('upay.general.show_status.inactive')" data-width="86" data-height="33">
                                                                <span class="form-text text-danger form-error-text form-error-ml_{{$lng->id}}_show_status"></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endforeach
                                            @if($mode == 'edit')
                                                <div role="tabpanel" id="tab-assignees" class="tab-pane">
                                                    <div class="panel-body">
                                                        @if(empty($cashbox->timeline) || $cashbox->timeline->assignees->isEmpty())
                                                            <p class="text-center"><i class="fa fa-exclamation-circle"></i> @lang('upay.cashbox.timeline.empty')</p>
                                                        @else
                                                            <table class="table">
                                                                <thead>
                                                                    <tr>
                                                                        <th>@lang('upay.general.id')</th>
                                                                        <th>@lang('upay.operator.first_name')</th>
                                                                        <th>@lang('upay.operator.last_name')</th>
                                                                        <th>@lang('upay.operator.email')</th>
                                                                        <th>@lang('upay.operator.phone')</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    @foreach($cashbox->timeline->assignees as $assigee)
                                                                        <tr>
                                                                            <td>{{$assigee->operator->id}}</td>
                                                                            <td>{{$assigee->operator->first_name}}</td>
                                                                            <td>{{$assigee->operator->last_name}}</td>
                                                                            <td>{{$assigee->operator->email}}</td>
                                                                            <td>{{$assigee->operator->phone}}</td>
                                                                        </tr>
                                                                    @endforeach
                                                                </tbody>
                                                            </table>
                                                        @endif
                                                    </div>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>

                            {{csrf_field()}}

                            <hr/>
                            <div class="form-group row">
                                <div class="col-lg-4 col-lg-offset-2">
                                    <a href="{{url('/cashbox/list')}}" class="btn btn-white">@lang('upay.general.buttons.cancel')</a>
                                    @if($mode == 'edit' && $canDelete)<button onclick="gDelete({{$cashbox->id}});return false;" type="button" class="btn btn-danger">@lang('upay.general.buttons.delete')</button>@endif
                                    @if($canEdit)<button class="btn btn-primary submit-btn" type="submit">@lang('upay.general.buttons.save')</button>@endif
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @if($mode == 'edit' && $canDelete)
        @include('components.delete', ['url' => url('/cashbox/delete')])
    @endif
    <script src="{{script_url('upay/js/cashbox.js')}}"></script>
@stop
