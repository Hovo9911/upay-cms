<?php

namespace App\Http\Controllers;

use App\Http\Requests\DeleteRequest;
use App\Http\Requests\News\EditRequest;
use App\Http\Requests\News\SearchRequest;
use App\UPay\Interfaces\LanguageRepositoryInterface;
use App\UPay\Interfaces\NewsRepositoryInterface;
use App\UPay\News\NewsSearch;
use DB;
use Illuminate\Http\Request;

class NewsController extends Controller
{
    private $languageRepository;
    private $newsRepository;

    public function __construct(NewsRepositoryInterface $newsRepository,LanguageRepositoryInterface $languageRepository)
    {
        $this->newsRepository = $newsRepository;
        $this->languageRepository = $languageRepository;
    }

    public function index()
    {
        return view('website.news.list');
    }

    public function edit(Request $request, $id = null)
    {
        if ($id != null) {
            $news = $this->newsRepository->findOrFail($id);
            $news->load([
                'ml',
            ]);

        } else {
            $news = $this->newsRepository->mock();
        }

        $news->ml = $news->ml->keyBy('lng_id');
        $languages = $this->languageRepository->all();

        return view('website.news.edit')->with([
            'mode' => $id != null ? 'edit' : 'add',
            'news' => $news,
            'languages' => $languages,
        ]);
    }

    public function save(EditRequest $request, $id = null)
    {
        $data = $request->validated();

        if ($id != null) {
            $this->newsRepository->findOrFail($id);
            $news = $this->newsRepository->update($id, $data);
        } else {
            $news = $this->newsRepository->create($data);
        }

        return $this->sendOK([
            'message' => trans('upay.website.news.saved.message'),
            'edit_url' => url('/website/news/edit/'.$news->id),
            'list_url' => url('/website/news/list')
        ]);
    }

    public function delete(DeleteRequest $request)
    {
        $this->newsRepository->delete($request->validated()['ids']);
        return $this->sendOK([
            'redirect_url' => url('/website/news/list')
        ]);
    }

    public function getList(SearchRequest $request)
    {
        $searcher = new NewsSearch($request->validated());
        return response()->json([
            'data' => $searcher->search(),
            'recordsTotal' => $searcher->totalCount(),
            'recordsFiltered' => $searcher->filteredCount(),
        ]);
    }
}
