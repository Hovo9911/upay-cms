<?php

namespace App\UPay\Timeline;

use App\MultiPKModel;
use App\UPay\Operator\Operator;

class TimelineAssignee extends MultiPKModel
{
    protected $table = 'timeline_assignees';
    public $timestamps = false;
    protected $primaryKey = ['timeline_id', 'operator_id'];
    public $incrementing = false;

    protected $fillable = [
        'timeline_id',
        'operator_id',
        'assigned_at',
    ];

    public function operator(){
        return $this->belongsTo(Operator::class, 'operator_id', 'id');
    }
}
