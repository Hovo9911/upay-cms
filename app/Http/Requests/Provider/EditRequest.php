<?php

namespace App\Http\Requests\Provider;

use App\Http\Requests\Request;
use App\Model;
use App\UPay\Provider\Provider;

class EditRequest extends Request
{
    protected $module = 'provider';

    public function rules()
    {
        $rules = [
            'parent_id' => 'integer|nullable',
            'logo' => 'required|max:255',
            'type' => 'in:'.Provider::TYPE_PAYMENT.','.Provider::TYPE_CASHBOX,
            'sort_order' => 'required|integer',
            'show_status' => 'in:'.Model::STATUS_ACTIVE.','.Model::STATUS_INACTIVE,
            'ml' => 'required|array',
        ];
        $rules = $this->checkStatusForRequired($rules, request()->input('show_status'));

        $rules = $this->addMlRules($rules, [
            'name' => 'required|max:255',
            'show_status' => 'in:'.Model::STATUS_ACTIVE.','.Model::STATUS_INACTIVE
        ]);
        return $rules;
    }

    protected function defaultValues()
    {
        return [
            'parent_id' => null,
            'logo' => '',
            'type' => Provider::TYPE_PAYMENT,
            'sort_order' => 0,
            'show_status' => Model::STATUS_INACTIVE,
            'ml' => [
                'name' => '',
                'show_status' => Model::STATUS_INACTIVE,
            ]
        ];
    }

}
