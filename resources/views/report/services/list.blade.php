@extends('layout')
@section('page_title') @lang('upay.report_services.list_title') @stop
@section('content')
<?php
$activeMenu = 'report_services';
?>
<script>
    var $permissions = { };
</script>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-title">
                    <h1>@lang('upay.report_services.list_title')</h1>
                </div>
                <div class="ibox-content">
                    <div class="ibox">
                        <div class="ibox-title">
                            <h5>@lang('upay.general.list.filters.title')</h5>
                            <div class="ibox-tools"><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></div>
                        </div>
                        <div class="ibox-content">
                            <form id="search-form" method="GET" action="{{url('/report-services/get-list')}}" export-action="{{url('/report-services/export')}}" role="form">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group row">
                                            <label class="col-lg-3 col-form-label text-right">@lang('upay.report_services.date')</label>
                                            <div class="col-lg-9">
                                                <div class="input-group date form-datepicker">
                                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input name="filters[date_from]" type="text" class="form-control" value="{{date('Y-m-d', strtotime('yesterday'))}}">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group row">
                                            <span class="col-lg-2 col-form-label text-right fa fa-long-arrow-right"></span>
                                            <label class="col-lg-1 col-form-label text-right"></label>
                                            <div class="col-lg-9">
                                                <div class="input-group date form-datepicker">
                                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input name="filters[date_to]" type="text" class="form-control" value="{{date('Y-m-d')}}">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group row">
                                            <label class="col-lg-3 col-form-label text-right">@lang('upay.report_services.service')</label>
                                            <div class="col-lg-9">
                                                <select class="form-control m-b select-picker" name="filters[service]">
                                                    <option value="">@lang('upay.general.dropdown.select')</option>
                                                    @foreach($services as $service)
                                                        <option value="{{$service->id}}">{{$service->name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-12">
                                        <button disabled class="btn btn-info float-right m-t-n-xs submit-btn" type="submit"><i class="fa fa-filter"></i> @lang('upay.general.list.filters.btn')</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table id="search-table" style="width: 100%" class="table table-striped table-bordered table-hover" >
                            <thead>
                                <tr>
                                    <th data-col="service" data-orderable="false">@lang('upay.report_services.service')</th>
                                    <th data-col="amount">@lang('upay.report_services.amount')</th>
                                    <th data-col="commission">@lang('upay.report_services.commission')</th>
                                    <th data-col="count">@lang('upay.report_services.count')</th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="{{script_url('upay/js/report/services.js')}}"></script>
@stop
